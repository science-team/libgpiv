AC_INIT(libgpiv, 0.6.1, ,)
AM_CONFIG_HEADER(config.h)

# Unstable (alpha), testing (beta) or stable 
GPIV_MAJOR_VERSION=0
# Large improvements against previous Minor release
GPIV_MINOR_VERSION=6
# Any changes: Improved functionality, added features, debugs, also changed API!
GPIV_MICRO_VERSION=1

GPIV_VERSION=$GPIV_MAJOR_VERSION.$GPIV_MINOR_VERSION.$GPIV_MICRO_VERSION

AC_SUBST(GPIV_MAJOR_VERSION)
AC_SUBST(GPIV_MINOR_VERSION)
AC_SUBST(GPIV_MICRO_VERSION)
AC_SUBST(GPIV_VERSION)

# libtool versioning. See libtool-doc chapter 6.3.
LT_RELEASE=$GPIV_MAJOR_VERSION.$GPIV_MINOR_VERSION
LT_CURRENT=3
LT_REVISION=0
LT_AGE=0

#`expr $GPIV_BINARY_AGE - $GPIV_INTERFACE_AGE`
AC_SUBST(LT_RELEASE)
AC_SUBST(LT_CURRENT)
AC_SUBST(LT_REVISION)
AC_SUBST(LT_AGE)

VERSION=$GPIV_VERSION
PACKAGE=libgpiv
AC_SUBST(PACKAGE)


AM_SANITY_CHECK
AC_ISC_POSIX

AC_PROG_CC
AM_PROG_CC_STDC
AC_PROG_CXX
dnl AM_PROG_LIBTOOL
AC_PROG_LIBTOOL
dnl LT_RELEASE=0.3
dnl AC_SUBST(LIBTOOL_DEPS)
AC_HEADER_STDC

AC_PROG_LN_S
dnl AM_MAINTAINER_MODE

dnl hints from autoscan START
AC_C_CONST
AC_FUNC_VPRINTF
AC_PROG_AWK
AC_PROG_RANLIB
AC_TYPE_SIZE_T
dnl hints from autoscan END

dnl search at correct directory for system configuration file
dnl SYSRSCDIR="-DSYSTEM_RSC_DIR=\"$sysconfdir\""
dnl SYSRSCDIR="-DSYSTEM_RSC_DIR=\"$sysconfdir\""
dnl AC_SUBST(SYSRSCDIR)
dnl just hard-coded. SYSTEM_RSC_DIR is defined in gpiv.h
dnl sysconfdir=/etc

dnl
dnl Check for programs
dnl
AC_CHECK_PROG(GNUPLOT, gnuplot, true, false)
AM_CONDITIONAL(HAVE_GNUPLOT, $GNUPLOT, ,AC_MSG_ERROR("gnuplot not installed"))

AC_CHECK_PROG(PERL, perl, true, false)
AM_CONDITIONAL(HAVE_PERL, $PERL)

AC_PATH_PROG(DOXYGEN, doxygen)
if test x"$DOXYGEN" = "x"; then
	AC_MSG_WARN(Cannot find doxygen to build documentation.
       Have a look at http://www.doxygen.org/)
fi

dnl
dnl Testing on Doxygen for documentation
dnl See http://www.bioinf.uni-freiburg.de/~mmann/HowTo/automake.html#doxygenSupport
dnl
dnl DX_DOXYGEN_FEATURE(ON|OFF)
dnl DX_DOT_FEATURE(ON|OFF)
DX_HTML_FEATURE(ON)
DX_CHM_FEATURE(ON|OFF)
DX_CHI_FEATURE(ON|OFF)
DX_MAN_FEATURE(ON|OFF)
DX_RTF_FEATURE(OFF)
DX_XML_FEATURE(OFF)
DX_PDF_FEATURE(OFF)
DX_PS_FEATURE(OFF)
#
DX_INIT_DOXYGEN($PACKAGE, Doxyfile)


AM_INIT_AUTOMAKE($PACKAGE, $VERSION, no-define)

dnl
dnl Check for libraries
dnl
AC_CHECK_LIB(fftw3, fftw_plan_dft_r2c_2d, , 
			AC_MSG_ERROR("libfftw3 not installed"))

dnl create
dnl hdf5-1.6.4
AC_CHECK_LIB(hdf5, H5Fopen, , 
 	AC_MSG_ERROR("libhdf5 not installed"))

PKG_CHECK_MODULES(GLIB, glib-2.0 >= 2.0.0)
AC_SUBST(GLIB_CFLAGS)
AC_SUBST(GLIB_LIBS)

AC_CHECK_LIB(m, main, , 
 	AC_MSG_ERROR("libm not installed"))

AC_CHECK_LIB(gslcblas, main, , 
	AC_MSG_ERROR("libgslcblas not installed"))

AC_CHECK_LIB(gsl, main, , 
 	AC_MSG_ERROR("libgsl not installed"))

AC_CHECK_LIB(glib-2.0, g_malloc0, , 
	AC_MSG_ERROR("libglib-2.0 not installed"))

AC_CHECK_LIB(png, main, , 
 	AC_MSG_ERROR("libpng not installed"))

AC_CHECK_PROG(PNMTOPNG, pnmtopng, true, false)
dnl AM_CONDITIONAL(HAVE_PNMTOPNG, $PNMTOPNG, ,
dnl AC_MSG_ERROR("pnmtopng from netpbm not installed");)
if test x$PNMTOPNG = xfalse ; then
  	AC_MSG_ERROR("pnmtopng from netpbm not installed");
fi


dnl
dnl Enabling IEEE-1394 / Firewire camera
dnl
AC_ARG_ENABLE(cam, 
         [  --enable-cam            enable (IEEE-1394) camera],
         [ENABLE_CAM="-DENABLE_CAM"]
         )
AC_SUBST(ENABLE_CAM)


dnl
dnl Enabling triggering system
dnl
AC_ARG_ENABLE(trig, 
         [  --enable-trig           enable (realtime) triggering],
         [ENABLE_TRIG="-DENABLE_TRIG"]
         )
AC_SUBST(ENABLE_TRIG)


if test  "${ENABLE_CAM}" == '-DENABLE_CAM'; then
	AC_CHECK_LIB(raw1394, raw1394_destroy_handle, ,
		AC_MSG_ERROR("libraw1394 not installed"))

	AC_CHECK_LIB(dc1394_control, dc1394_create_handle, ,
		AC_MSG_ERROR("libdc1394_control not installed"))

	AC_CHECK_HEADER(libdc1394/dc1394_control.h, ,
		AC_MSG_ERROR("missing dc1394_control.h"))

	AC_CHECK_HEADER(libraw1394/raw1394.h, ,
	AC_MSG_ERROR("missing raw1394.h"))
fi


dnl
dnl Enabling RTAI for triggering system
dnl
AC_ARG_ENABLE(rta, 
        [  --enable-rta=RTA_DIR     place where the RTAI code resides 
			   (default /usr/lib/realtime)],
        [RTA_TOPDIR="$enableval"],
        [RTA_TOPDIR="/usr/lib/realtime"]
        )
AC_SUBST(RTA_TOPDIR)



dnl
dnl Enabling Message Passing Interface (MPI) for parallel computing
dnl
AC_ARG_ENABLE(mpi, 
         [  --enable-mpi            enable message protocol interface],
         [ENABLE_MPI="-DENABLE_MPI"]
         )
AC_SUBST(ENABLE_MPI)


if test  "${ENABLE_MPI}" == '-DENABLE_MPI'; then
	ACX_MPI(AC_MSG_WARN("libmpi HAS BEEN installed"), 
		AC_MSG_ERROR("libmpi IS NOT installed"))

	CC="$MPICC"
	LIBS="$MPILIBS $LIBS"

	AC_CHECK_LIB(mpi, main, , 
 		AC_MSG_ERROR("libmpi not installed"))
	AC_CHECK_HEADER(mpi.h, ,
		AC_MSG_ERROR("missing mpi.h"))
fi

AM_CONDITIONAL(BUILD_MPI, test "${ENABLE_MPI}" == '-DENABLE_MPI')




dnl
dnl Enabling Open Multi-Processing (OMP) for parallel computing
dnl
AC_ARG_ENABLE(omp, 
         [  --enable-omp            enable Open Multi-Processing (OMP)],
         [ENABLE_OMP="ENABLE_OMP"]
	)

if test  "${ENABLE_OMP}" == 'ENABLE_OMP'; then
	CC="$CC -fopenmp"

	AC_CHECK_LIB(gomp, main, , 
		AC_MSG_ERROR("libgomp not installed"))
	AC_CHECK_HEADER(omp.h, ,
		AC_MSG_ERROR("missing omp.h"))

	AC_CHECK_LIB(fftw3_threads, main, , 
		AC_MSG_ERROR("libfftw3_threads not installed"))
fi
dnl No need to use -DENABLE_OMP: gcc will enable _OPENMP



AC_CHECK_HEADERS([stdlib.h string.h time.h unistd.h fftw3.h hdf5.h \
png.h])

AC_CHECK_FUNCS([memset sqrt strstr])

dnl AC_ARG_ENABLE(tmp-dir, 
dnl          [  --enable-tmp-dir=TMP_DIR   temporary directory (default /tmp)],
dnl          [OVERRIDE_TMP_DIR="-DTMP_DIR=$enableval"] 
dnl          )
dnl AC_SUBST(OVERRIDE_TMP_DIR)

AC_OUTPUT([
	Makefile
	lib/Makefile
	include/Makefile
	include/gpiv/Makefile
])


echo "===================================================================="
echo "Configuration succesfully finished"

if test $ENABLE_CAM; then
	echo "with (IEEE-1394) camera enabled"
fi

if test $ENABLE_TRIG; then
	echo "with (realtime) triggering enabled."
fi

if test $ENABLE_MPI; then
	echo "building parallel library using Message Passing Interface (MPI)"
	echo "for processing on distributed memory systems"
fi

if test $ENABLE_OMP; then
	echo "building parallel library using Open Multi-Processing (OMP)"
	echo "for processing on shared memory systems"
fi

echo "Type 'make' and 'make install' to build and install libgpiv"

if test $DOXYGEN; then
	echo "Type 'make doxygen-doc/-(un)install' for the API documentation"
else
	echo "Cannot find doxygen: API documentation cannot be build"
fi

echo "===================================================================="
