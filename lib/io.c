/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008
   Gerber van der Graaf

   This file is part of libgpiv.
   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  



---------------------------------------------------------------
FILENAME:                io.c
LIBRARY:                 libgpiv:
EXTERNAL FUNCTIONS:      gpiv_io_make_fname

			 gpiv_read_image
			 gpiv_fread_image

                         gpiv_find_pivdata_origin
                         gpiv_read_pivdata 
                         gpiv_read_pivdata_fastx 
                         gpiv_write_pivdata

                         gpiv_read_scdata 
                         gpiv_write_scdata

			 gpiv_fwrite_griddata
			 gpiv_print_histo
			 gpiv_fprint_histo


LOCAL FUNCTIONS:

LAST MODIFICATION DATE:  $Id: io.c,v 1.19 2008-09-25 13:19:53 gerber Exp $
 --------------------------------------------------------------- */

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <hdf5.h>

#include <gpiv.h>


static GpivImage *
open_img (const gchar *fname);

static gboolean
count_asciidata (FILE *fp,
                 guint *nx,
                 guint *ny
                 );

static gboolean
find_data_scaled (const gchar line[GPIV_MAX_CHARS]
                  );

static gboolean
obtain_nxny_fromline (gchar line[], 
                      gboolean *scale,
                      guint *nx, 
                      guint *ny, 

                      gboolean *increment,
                      gfloat *dat_x,
                      gfloat *dat_y,
                      guint *line_nr
                      );

static void
obtain_pivdata_fromline (gchar line[], 
                         GpivPivData *piv_data,
                         guint *i,
                         guint *j
                         );
static void
obtain_pivdata_fastx_fromline (gchar line[], 
                               GpivPivData *piv_data,
                               guint *i,
                               guint *j
                               );

static void
obtain_scdata_fromline (gchar line[], 
                        GpivScalarData *piv_data,
                        guint *i,
                        guint *j
                        );

static GpivPivData *
read_pivdata_from_file (FILE *fp,
                        gboolean fastx
                        );

static GpivPivData *
read_pivdata_from_stdin (gboolean fastx
                         );

static GpivScalarData *
read_scdata_from_file (FILE *fp
                       );

static GpivScalarData *
read_scdata_from_stdin (void
                        );

/*
 * Public functions
 */

void 
gpiv_io_make_fname (const gchar *fname_base, 
		    const gchar *EXT, 
                    gchar *fname_out
		   ) 
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *  Constructs (output) filename from base name and exetsion 
 *
 * INPUTS:
 *      fname_base:     file base name
 *      EXT:            file extension name
 * OUTPUTS:
 *      fname_out:      completed filename
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    g_snprintf(fname_out, GPIV_MAX_CHARS, "%s%s", fname_base, EXT);
}



GpivImage *
gpiv_read_image (FILE *fp
                 )
/*---------------------------------------------------------------------------*/
/**
 *     Reads image from fp, png formatted.
 */
{
    GpivImage *image = NULL;


    if (fp == NULL) fp = stdin;

    if ((image = gpiv_read_png_image (fp)) == NULL) {
        g_message ("gpiv_read_image: gpiv_read_png_image failed");
        return NULL;
    }


    return image;
}


GpivImage *
gpiv_fread_image (const gchar *fname
                  )
/* ----------------------------------------------------------------------------
 * checks filename extension on valid image / data name,
 * loads image header and data
 */
{
    GpivImage *image = NULL;

    char *err_msg = NULL;
    gchar *ext, *ext_ORG = NULL,
        *dirname, 
        *fname_base, 
        *fname_ext,
        *fname_org = g_strdup(fname);
    gchar command[2 * GPIV_MAX_CHARS];

    FILE *fp = NULL;
    gchar tmp_textfile[GPIV_MAX_CHARS];
/* BUGFIX: for the moment use the variable: cleanup_tmp_image */
    gboolean cleanup_tmp_image = TRUE;


    if (fname == NULL) {
        gpiv_warning ("gpiv_fread_image: failing \"fname == NULL\"");
        return NULL;
    }

    if (g_file_test (fname, G_FILE_TEST_EXISTS) == FALSE) {
        gpiv_warning ("gpiv_fread_image: file does not exist");
        return NULL;
    }
    
/*
 * Stripping file name to examine suffix
 *
 */
    ext = g_strdup(strrchr(fname, '.'));
    dirname = g_strdup(g_path_get_dirname(fname));
    fname_base = g_strdup(g_path_get_basename(fname));
    strtok(fname_base, ".");
    fname_ext = g_strdup (g_strconcat (dirname, G_DIR_SEPARATOR_S,
                                      fname_base, NULL));
#ifdef DEBUG
    g_message("gpiv_fread_image:: dirname = %s\n fname_base = %s\n fname_ext = %s", 
              dirname, fname_base, fname_ext);    
#endif /* DEBUG */

/*
 * Converting image from pgm, gif, tif, bmp format to png
 * Creating tempory text file to include as required comment in PNG image
 * for use in gpiv software
 */
    if (g_str_has_suffix (fname, GPIV_EXT_PGM_IMAGE)
        || g_str_has_suffix (fname, GPIV_EXT_PGM_IMAGE_UPCASE)) {
/*    if (strcmp(ext, GPIV_EXT_PGM_IMAGE) == 0) { */
        g_snprintf (command, 2 * GPIV_MAX_CHARS, 
                    "pnmtopng < %s > %s%s", 
                    fname, fname_ext, GPIV_EXT_PNG_IMAGE);

        if (system (command) != 0) {
            gpiv_warning ("gpiv_fread_image: could not exec shell command \"%s\"",
                       command);
        }

        ext_ORG = g_strdup(ext);
        g_free(ext);
        ext = g_strdup(GPIV_EXT_PNG_IMAGE);
        fname = g_strdup_printf("%s%s", fname_ext, ext);
        

    } else if (g_str_has_suffix (fname, ".gif")
               || g_str_has_suffix (fname, ".GIF")) {
        g_snprintf (command, 2 * GPIV_MAX_CHARS, 
                    "giftopnm < %s  | pnmtopng > %s%s", 
                    fname, fname_ext, GPIV_EXT_PNG_IMAGE);
        if (system (command) != 0) {
            gpiv_warning ("gpiv_fread_image: could not exec shell command \"%s\"",
                       command);
        }
        ext_ORG = g_strdup(ext);
        g_free(ext);
        ext = g_strdup(GPIV_EXT_PNG_IMAGE);
        fname = g_strdup_printf("%s%s", fname_ext, ext);
        

    } else if (g_str_has_suffix (fname, ".tif")
               || g_str_has_suffix (fname, ".TIF")) {
        g_snprintf (command, 2 * GPIV_MAX_CHARS, 
                    "tifftopnm < %s  | pnmtopng > %s%s", 
                    fname, fname_ext, GPIV_EXT_PNG_IMAGE);

        if (system (command) != 0) {
            gpiv_warning ("gpiv_fread_image: could not exec shell command \"%s\"",
                       command);
        }

        ext_ORG = g_strdup(ext);
        g_free(ext);
        ext = g_strdup(GPIV_EXT_PNG_IMAGE);
        fname = g_strdup_printf("%s%s", fname_ext, ext);
        

    } else if (g_str_has_suffix (fname, ".bmp")
               || g_str_has_suffix (fname, ".BMP")) {
        g_snprintf (command, 2 * GPIV_MAX_CHARS, 
                    "bmptoppm < %s  | pnmtopng > %s%s", 
                   fname, fname_ext, GPIV_EXT_PNG_IMAGE);

        if (system (command) != 0) {
            gpiv_warning ("gpiv_fread_image: could not exec shell command \"%s\"",
                       command);
        }

        ext_ORG = g_strdup(ext);
        g_free(ext);
        ext = g_strdup(GPIV_EXT_PNG_IMAGE);
        fname = g_strdup_printf("%s%s", fname_ext, ext);
    }

/*
 * reading image data from raw, PNG, hdf or Davis(tm) format
 */
    if (strcmp (ext, GPIV_EXT_RAW_IMAGE) == 0 
        || strcmp (ext, GPIV_EXT_PNG_IMAGE) == 0
        || strcmp (ext, GPIV_EXT_PNG_IMAGE_UPCASE) == 0
        || strcmp (ext, GPIV_EXT_GPIV) == 0
        || strcmp (ext, GPIV_EXT_GPIV_UPCASE) == 0
        || strcmp (ext, GPIV_EXT_DAVIS) == 0
        || strcmp (ext, GPIV_EXT_DAVIS_UPCASE) == 0) {
        
#ifdef DEBUG
        g_message ("gpiv_fread_image::  open_img");
#endif
        if ((image = open_img (fname)) == NULL) {
            gpiv_warning ("gpiv_fread_image: failure open_img");
            return NULL;
        }

    } else {
        gpiv_warning ("gpiv_fread_image: image format not recognised");
        return NULL;
    }

/*
 * Remove original image if it has temporarly been converted to png
 */
    if (ext_ORG != NULL
        && (strcmp(ext_ORG, GPIV_EXT_PGM_IMAGE) == 0
            || strcmp(ext_ORG, ".pgm") == 0
            || strcmp(ext_ORG, ".PGM") == 0
            || strcmp(ext_ORG, ".gif") == 0
            || strcmp(ext_ORG, ".GIF") == 0
            || strcmp(ext_ORG, ".tif") == 0
            || strcmp(ext_ORG, ".TIF") == 0
            || strcmp(ext_ORG, ".bmp") == 0
            || strcmp(ext_ORG, ".BMP") == 0
            )) {

        if (cleanup_tmp_image) {
            g_snprintf (command, 2 * GPIV_MAX_CHARS, "rm %s%s", 
                        fname_ext, GPIV_EXT_PNG_IMAGE);
        } else {
            g_snprintf (command, 2 * GPIV_MAX_CHARS, "rm %s ", fname_org);
        }
        if (system (command) != 0) {
            gpiv_warning ("gpiv_fread_image: could not exec shell command \"%s\"",
                       command);
        }
    }


    g_free (ext_ORG);
    g_free (ext);
    g_free (dirname); 
    g_free (fname_base); 
    g_free (fname_ext);
    g_free (fname_org);

    return image;
}



enum GpivDataFormat
gpiv_find_pivdata_origin (const gchar line[GPIV_MAX_CHARS]
			  )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Determines the name of the program that generated the data
 *
 * PROTOTYPE LOCATATION:
 *      io.h
 *
 * INPUTS:
 *      line:           character line containing program that generated data
 *
 * OUTPUTS:
 *
 * RETURNS:
 *      pdf:            enumerator of data origin
 *---------------------------------------------------------------------------*/
{
    enum GpivDataFormat pdf;


    if (strstr(line, "rr") != '\0') {
        pdf = GPIV_RR;
    } else if (strstr(line, "DaVis") != '\0') { 
        pdf = GPIV_DAV;
    } else {
/* fprintf (stderr,"\n%s %s warning: No format defined: taking 'rr' format", */
/* 	     LIBNAME,function_name); */
        pdf = GPIV_RR;
    }


    return pdf;
}



GpivPivData *
gpiv_fread_pivdata       (const gchar    *fname
                          )
/*-----------------------------------------------------------------------------
 *      Reads PIV data from file fname
 */
{
    FILE *fp = NULL;
    GpivPivData *piv_data = NULL;


    if ((fp = fopen (fname, "rb")) == NULL) {
        gpiv_warning ("gpiv_fread_pivdata: failing fopen %s", fname);
        return NULL;
    }

    if ((piv_data = gpiv_read_pivdata (fp)) == NULL) {
        gpiv_warning ("gpiv_fread_pivdata: failing gpiv_fread_pivdata");
        return NULL;
    }


    fclose (fp);
    return piv_data;
}



gchar *
gpiv_fwrite_pivdata       (const gchar    *fname,
                           GpivPivData    *piv_data,
                           const gboolean free
                           )
/*-----------------------------------------------------------------------------
 *      Writes PIV data to file fname
 */
{
    FILE *fp = NULL;
    gchar *err_msg = NULL;


    if ((fp = fopen (fname, "wb")) == NULL) {
        gpiv_warning ("gpiv_fwrite_pivdata: failing fopen %s", fname);
        return "gpiv_fwrite_pivdata: failing fopen";
    }

    if ((err_msg = gpiv_write_pivdata (fp, piv_data, free)) != NULL) {
/*         gpiv_warning ("gpiv_fwrite_pivdata: failing gpiv_write_pivdata"); */
        return err_msg;
    }


    fclose (fp);
    return err_msg;
}



GpivPivData *
gpiv_read_pivdata (FILE *fp
                   )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Reads data from fp
 *
 *---------------------------------------------------------------------------*/
{
    GpivPivData *piv_data = NULL;

/*
 * reads the data from stdin or file pointer
 */
    if (fp == stdin || fp == NULL) {
        piv_data = read_pivdata_from_stdin (FALSE);
    } else {
        piv_data = read_pivdata_from_file (fp, FALSE);
    }


    return piv_data;
}



GpivPivData *
gpiv_read_pivdata_fastx (FILE *fp
                         )
/*  ---------------------------------------------------------------------------
 * DESCRIPTION:
 *     Reads data from ascii data file with fast running x-position variables 
 *     (1st column in data stream)
 *
 *-------------------------------------------------------------------------- */
{
    GpivPivData *piv_data = NULL;


/*
 * reads the data from stdin or file pointer
 */
    if (fp == stdin || fp == NULL) {
        piv_data = read_pivdata_from_stdin (TRUE);
    } else {
        piv_data = read_pivdata_from_file (fp, TRUE);
    }


    return piv_data;
}



gchar *
gpiv_write_pivdata (FILE *fp,
                    GpivPivData *piv_data,
                    const gboolean free
		    )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Writes PIV data to fp
 *
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    int i, j;

/*     g_message ("gpiv_write_pivdata:: 0"); */
    if ((err_msg = gpiv_check_alloc_pivdata (piv_data)) != NULL) {
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }
    

    if (fp == stdout || fp == NULL) {
/*
 * Use stdout
 * Writing comment
 */
        if (piv_data->comment != NULL) {
            printf ("%s", piv_data->comment);
        }

/*
 * Writing data
 */
        if (piv_data->scale == TRUE) {
            printf ("\n# x(m)        y(m)         Vx(m/s)      Vy(m/s)      snr        peak#\n");
            for (i = 0; i < piv_data->ny; i++) { 
                for (j = 0; j < piv_data->nx; j++) {
                    printf (GPIV_PIV_S_FMT, 
                            piv_data->point_x[i][j], 
                            piv_data->point_y[i][j], 
                            piv_data->dx[i][j], 
                            piv_data->dy[i][j], 
                            piv_data->snr[i][j], 
                            piv_data->peak_no[i][j]);
                }
            }
            
        } else {
            printf ("\n# x(px) y(px)    dx(px)       dy(px)       snr  peak#\n");
            for (i = 0; i < piv_data->ny; i++) {
                for (j = 0; j < piv_data->nx; j++) {
                    printf (GPIV_PIV_FMT, 
                             piv_data->point_x[i][j], 
                             piv_data->point_y[i][j], 
                             piv_data->dx[i][j], 
                             piv_data->dy[i][j], 
                             piv_data->snr[i][j], 
                             piv_data->peak_no[i][j]
                             );      
                }
            }
        }


    } else {
/*
 * Use fp
 * Writing comment
 */
    if (piv_data->comment != NULL) {
        fprintf (fp, "%s", piv_data->comment);
    }

/*
 * Writing data
 */
        if (piv_data->scale == TRUE) {
            fprintf (fp, "\n# x(m)        y(m)         Vx(m/s)      Vy(m/s)      snr        peak#\n");
            for (i = 0; i < piv_data->ny; i++) { 
                for (j = 0; j < piv_data->nx; j++) {
                    fprintf (fp, GPIV_PIV_S_FMT, 
                             piv_data->point_x[i][j], 
                             piv_data->point_y[i][j], 
                             piv_data->dx[i][j], 
                             piv_data->dy[i][j], 
                             piv_data->snr[i][j], 
                             piv_data->peak_no[i][j]);
                }
            }
            
        } else {
            fprintf (fp, "\n# x(px) y(px)    dx(px)       dy(px)       snr  peak#\n");
            for (i = 0; i < piv_data->ny; i++) {
                for (j = 0; j < piv_data->nx; j++) {
                    fprintf (fp, GPIV_PIV_FMT, 
                             piv_data->point_x[i][j], 
                             piv_data->point_y[i][j], 
                             piv_data->dx[i][j], 
                             piv_data->dy[i][j], 
                             piv_data->snr[i][j], 
                             piv_data->peak_no[i][j]
                             );      
                }
            }
        }
        
        fflush (fp);
    }


    if (free) gpiv_free_pivdata (piv_data);
    return err_msg;
}




GpivScalarData *
gpiv_fread_scdata       (const gchar    *fname
                         )
/*-----------------------------------------------------------------------------
 *      Reads scalar data from file fname
 */
{
    FILE *fp = NULL;
    GpivScalarData *scalar_data = NULL;


    if ((fp = fopen (fname, "rb")) == NULL) {
        gpiv_warning ("gpiv_fread_scdata: failing fopen %s", fname);
        return NULL;
    }

    if ((scalar_data = gpiv_read_scdata (fp)) == NULL) {
        gpiv_warning ("gpiv_fread_scdata: failing gpiv_read_scdata");
        return NULL;
    }


    fclose (fp);
    return scalar_data;
}



gchar *
gpiv_fwrite_scdata      (const gchar    *fname,
                         GpivScalarData *scalar_data,
                         const gboolean free
                         )
/*-----------------------------------------------------------------------------
 *      Writes scalar data to file fname
 */
{
    FILE *fp = NULL;
    gchar *err_msg = NULL;


    if ((fp = fopen (fname, "wb")) == NULL) {
        gpiv_warning ("gpiv_fwrite_scdata: failing fopen %s", fname);
        return "gpiv_fwrite_scdata: failing fopen";
    }

    if ((err_msg = gpiv_write_scdata (fp, scalar_data, free)) != NULL) {
        return err_msg;
    }


    fclose (fp);
    return err_msg;
}



GpivScalarData *
gpiv_read_scdata (FILE *fp
                  )
/*-----------------------------------------------------------------------------
 * Reads scalar data from ascii data file
 */
{
    GpivScalarData *sc_data = NULL;


/*
 * reads the data from stdin or file pointer
 */
    if (fp == stdin || fp == NULL) {
        sc_data =  read_scdata_from_stdin ();
    } else {
        sc_data =  read_scdata_from_file (fp);
    }


    return sc_data;
}



gchar *
gpiv_write_scdata (FILE *fp,
                   GpivScalarData *scalar_data,
                   const gboolean free
		   )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Writes scalar_data data to fp
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    gint i, j;


    g_return_val_if_fail (scalar_data->point_x != NULL,
                          "gpiv_write_scdata: point_x != NULL");
    g_return_val_if_fail (scalar_data->point_y != NULL, 
                          "gpiv_write_scdata: point_y != NULL");
    g_return_val_if_fail (scalar_data->scalar != NULL, 
                          "gpiv_write_scdata: scalar != NULL");
    g_return_val_if_fail (scalar_data->flag != NULL, 
                          "gpiv_write_scdata: flag != NULL");


    if (fp == stdout || fp == NULL) {
/*
 * Use stdout
 * Writing comment
 */
        if (scalar_data->comment != NULL) {
            printf ("%s", scalar_data->comment);
        }

/*
 * Writing data
 */
        if (scalar_data->scale == TRUE) {
            printf ("\n#       x(m)         y(m)        scalar      flag\n");
            for (i=0; i< scalar_data->ny; i++) {
                for (j=0; j< scalar_data->nx; j++) { 
                    printf (GPIV_SCALAR_S_FMT, 
                            scalar_data->point_x[i][j], 
                            scalar_data->point_y[i][j], 
                            scalar_data->scalar[i][j], 
                            scalar_data->flag[i][j]);      
                }
            }
        
        } else {
            printf ("\n# x(px) y(px) scalar      flag\n");
            for (i=0; i< scalar_data->ny; i++) {
                for (j=0; j< scalar_data->nx; j++) { 
                    printf (GPIV_SCALAR_FMT, 
                            scalar_data->point_x[i][j], 
                            scalar_data->point_y[i][j], 
                            scalar_data->scalar[i][j], 
                            scalar_data->flag[i][j]);      
                }
            }
        }


    } else {
/*
 * Use fp
 * Writing comment
 */
    if (scalar_data->comment != NULL) {
        fprintf (fp, "%s", scalar_data->comment);
    }

/*
 * Writing data
 */
        if (scalar_data->scale == TRUE) {
            fprintf (fp, "\n#       x(m)         y(m)        scalar      flag\n");
            for (i=0; i< scalar_data->ny; i++) {
                for (j=0; j< scalar_data->nx; j++) { 
                    fprintf (fp, GPIV_SCALAR_S_FMT, 
                             scalar_data->point_x[i][j], 
                             scalar_data->point_y[i][j], 
                             scalar_data->scalar[i][j], 
                             scalar_data->flag[i][j]);      
                }
            }
        
        } else {
            fprintf (fp, "\n# x(px) y(px) scalar      flag\n");
            for (i=0; i< scalar_data->ny; i++) {
                for (j=0; j< scalar_data->nx; j++) { 
                    fprintf (fp, GPIV_SCALAR_FMT, 
                             scalar_data->point_x[i][j], 
                             scalar_data->point_y[i][j], 
                             scalar_data->scalar[i][j], 
                             scalar_data->flag[i][j]);      
                }
            }
        }
        fflush (fp);
    }


    if (free) gpiv_free_scdata (scalar_data);
    return err_msg;  
}



gchar *
gpiv_write_sc_griddata (FILE *fp,
                        GpivScalarData *scalar_data,
                        const gboolean free
                        )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Writes scalar data to file in grid format for gnuplot
 *
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    gint i, j;
    

    g_return_val_if_fail (scalar_data->point_x != NULL, 
                          "gpiv_fwrite_sc_griddata: point_x != NULL");
    g_return_val_if_fail (scalar_data->point_y != NULL, 
                          "gpiv_fwrite_sc_griddata: point_y != NULL");
    g_return_val_if_fail (scalar_data->scalar != NULL, 
                          "gpiv_fwrite_sc_griddata: scalar != NULL");
    g_return_val_if_fail (scalar_data->flag != NULL, 
                          "gpiv_fwrite_sc_griddata: flag != NULL");
    

/*
 *  Writing comment
 */
    if (scalar_data->comment != NULL) {
        fprintf (fp, "%s", scalar_data->comment);
    }

/*
 * Writing data
 */
    if (scalar_data->scale == TRUE) {
        fprintf (fp,"\n#       x(m)         y(m)        scalar      flag\n");
        for (i = 0; i < scalar_data->ny; i++) {
            fprintf (fp, "\n");
            for (j = 0; j< scalar_data->nx; j++) { 
                fprintf (fp, GPIV_SCALAR_S_FMT, scalar_data->point_x[i][j], 
                         scalar_data->point_y[i][j], 
                         scalar_data->scalar[i][j], scalar_data->flag[i][j]);      
            }
        }

    } else {
        fprintf (fp, "\n# x(px) y(px) scalar      flag\n");
        for (i = 0; i< scalar_data->ny; i++) {
            fprintf (fp, "\n");
            for (j = 0; j < scalar_data->nx; j++) { 
                fprintf (fp, GPIV_SCALAR_FMT, scalar_data->point_x[i][j], 
                         scalar_data->point_y[i][j], 
                         scalar_data->scalar[i][j],scalar_data->flag[i][j]);      
            }
        }
    }
    

    fflush (fp);
    if (free) gpiv_free_scdata (scalar_data);
    return err_msg;
}



gchar *
gpiv_write_sc_mtvgriddata (FILE *fp, 
                           GpivScalarData *scalar_data, 
                           const gboolean free
                           )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Writes scalar data to file in grid format for plotmtv
 *
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    gint i, j;


    g_return_val_if_fail (scalar_data->point_x != NULL, 
                          "gpiv_write_sc_mtvgriddata: point_x != NULL");
    g_return_val_if_fail (scalar_data->point_y != NULL, 
                          "gpiv_write_sc_mtvgriddata: point_y != NULL");
    g_return_val_if_fail (scalar_data->scalar != NULL, 
                          "gpiv_write_sc_mtvgriddata: scalar != NULL");
/*   g_return_val_if_fail (scalar_data->flag != NULL); */


/*
 * Opens output file
 * OBSOLETE
 */
/*   if((fp=fopen(fname,"w")) == NULL) {  */
/*       err_msg = "GPIV_FWRITE_SC_MTVGRIDDATA: Failure opening for output"; */
/*       return err_msg; */
/*   }  */


    if (fp == stdout || fp == NULL) {
/*
 *  Writing comment
 */
        if (scalar_data->comment != NULL) {
            printf ("%s", scalar_data->comment);
        }
/*
 *  Writing header for mtv format
 */
        printf ("$ DATA=CONTOUR\n");
        printf ("%% contfill=T\n");
        printf ("%% nx=%d xmin=%f xmax=%f\n", scalar_data->nx, 
                scalar_data->point_x[0][0], scalar_data->point_x[0][scalar_data->nx - 1]);
        printf ("%% ny=%d ymin=%f ymax=%f\n", scalar_data->ny, 
                scalar_data->point_y[0][0], scalar_data->point_y[scalar_data->ny - 1][0]);

/*
 * Writing data
 */
        for (i=0; i < scalar_data->ny; i++) {
            printf ("\n");
            for (j=0; j < scalar_data->nx; j++) { 
                printf ("%f ", scalar_data->scalar[i][j]);      
            }
        }


    } else {
/*
 *  Writing comment
 */
        if (scalar_data->comment != NULL) {
            fprintf (fp, "%s", scalar_data->comment);
        }

/*
 *  Writing header for mtv format
 */
        fprintf (fp, "$ DATA=CONTOUR\n");
        fprintf (fp, "%% contfill=T\n");
        fprintf (fp, "%% nx=%d xmin=%f xmax=%f\n", scalar_data->nx, 
                 scalar_data->point_x[0][0], scalar_data->point_x[0][scalar_data->nx - 1]);
        fprintf(fp, "%% ny=%d ymin=%f ymax=%f\n", scalar_data->ny, 
                scalar_data->point_y[0][0], scalar_data->point_y[scalar_data->ny - 1][0]);

/*
 * Writing data
 */
        for (i=0; i < scalar_data->ny; i++) {
            fprintf (fp, "\n");
            for (j=0; j < scalar_data->nx; j++) { 
                fprintf (fp, "%f ", scalar_data->scalar[i][j]);      
            }
        }
        fflush (fp);
    }


    if (free) gpiv_free_scdata (scalar_data);
    return err_msg;
}



/* void */
/* gpiv_print_histo (GpivBinData *klass,  */
/*                   char comment_line[GPIV_MAX_LINES_C][GPIV_MAX_CHARS],  */
/*                   int ncomment_lines,  */
/*                   char *RCSID */
/*                   ) */

gchar *
gpiv_print_histo (FILE *fp, 
                  GpivBinData *klass, 
                  const gboolean free
                  )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Writing histogram data to fp
 *
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    gint i, *count = klass->count, nbins = klass->nbins;
    gfloat *centre = klass->centre;


    g_return_val_if_fail (klass->count != NULL, 
                          "gpiv_print_histo: count != NULL");
    g_return_val_if_fail (klass->bound != NULL, 
                          "gpiv_print_histo: bound != NULL");
    g_return_val_if_fail (klass->centre != NULL, 
                          "gpiv_print_histo: klass != NULL");
  

    if (fp == stdout || fp == NULL) {
        if (klass->comment != NULL) {
            printf ("%s", klass->comment);
        }

        printf ("\n# bin  frequency\n");
        for (i = 0; i < nbins; i++) {
            printf("\n   %f          %d", centre[i], count[i]); 
        }

    } else {
        if (klass->comment != NULL) {
            fprintf (fp, "%s", klass->comment);
        }

        fprintf (fp, "\n# bin  frequency\n");
        for (i = 0; i < nbins; i++) {
            fprintf(fp, "\n   %f          %d", centre[i], count[i]); 
        }
        fflush (fp);
    }

    if (free) gpiv_free_bindata (klass);
    return err_msg;
}



gchar *
gpiv_print_cumhisto_eqdatbin (FILE *fp,
                              GpivBinData *klass, 
                              const gboolean free
                              )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Writing cumulative histogram data with an equal number of date per bin 
 *     or klass to ouput file. Special output for validation; work around to
 *     print float data as y-values.
 *
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    gint i, nbins = klass->nbins;
    gfloat *bound = klass->bound, *centre = klass->centre;


    g_return_val_if_fail (klass->bound != NULL, 
                          "gpiv_print_cumhisto_eqdatbin: bound != NULL");
    g_return_val_if_fail (klass->centre != NULL, 
                          "gpiv_print_cumhisto_eqdatbin: bound != NULL");
  

    if (klass->comment != NULL) {
        fprintf (fp, "%s", klass->comment);
    }

    fprintf (fp,"\n# bound      value\n");
    for (i = 0; i < nbins; i++) {
        fprintf (fp, "\n   %f          %f", bound[i], centre[i]); 
    }


    fflush (fp);
    if (free) gpiv_free_bindata (klass);
    return err_msg;
}


/*
 * Local functions
 */

static GpivImage *
open_img (const gchar *fname
          )
/*-----------------------------------------------------------------------------
 * Opens an image from png, hdf or raw-data file
 */
{
    GpivImage *image = NULL;
    gchar *err_msg = NULL;
    gchar *ext = g_strdup(strrchr(fname, '.'));
    FILE *fp;

#ifdef DEBUG
    g_message ("open_img:: 0 fname=%s ext=%s", fname, ext);
#endif

    if (fname == NULL) {
        gpiv_warning ("LIBGPIV internal error: open_img: failing \"fname == NULL\"");
        return NULL;
    }

/*
 * Reads hdf format
 */
    if (strcmp (ext, GPIV_EXT_GPIV) == 0) {
#ifdef DEBUG
        g_message ("OPEN_IMG:: gpiv_fread_hdf5_image");
#endif
        if ((image = gpiv_fread_hdf5_image (fname)) == NULL) {
            g_message ("LIBGPIV internal error: open_img: gpiv_fread_hdf5_image failed");
            return NULL;
        }

/*
 * Reads Portable Network Graphics image format
 */
    } else if (strcmp (ext, GPIV_EXT_PNG_IMAGE) == 0) {
#ifdef DEBUG
        g_message ("OPEN_IMG:: calling gpiv_fread_png_image");
#endif
        if ((fp = fopen (fname, "rb")) == NULL) {
            gpiv_warning ("LIBGPIV internal error: open_img: failure opening %s", fname);
            return NULL;
        }

        if ((image = gpiv_read_png_image (fp)) == NULL) {
            g_message ("LIBGPIV internal error: open_img: gpiv_read_png_image failed");
            return NULL;
        }

        fclose (fp);

/*
 * Reads raw data format
 */
    } else if (strcmp (ext, GPIV_EXT_RAW_IMAGE) == 0) {
#ifdef DEBUG
        g_message ("OPEN_IMG:: gpiv_fread_raw_image");
#endif
       if ((image = gpiv_fread_raw_image (fname)) == NULL) {
            g_message ("LIBGPIV internal error: open_img: gpiv_read_raw_image failed");
            return NULL;
        }
        
/*
 * Reads LaVision's (tm) DaVis format (.img)
 */
    } else if (strcmp (ext, GPIV_EXT_DAVIS) == 0) {
#ifdef DEBUG
        g_message ("OPEN_IMG:: gpiv_fread_davis_image");
#endif
        if ((fp = fopen (fname, "rb")) == NULL) {
            gpiv_warning ("LIBGPIV internal error: open_img: failure opening %s", fname);
            return NULL;
        }

       if ((image = gpiv_read_davis_image (fp)) == NULL) {
            g_message ("LIBGPIV internal error: open_img: gpiv_read_davis_image failed");
            return NULL;
        }

        fclose (fp);


    } else {
        err_msg = "LIBGPIV internal error: open_img: should not arrive here";
            gpiv_warning ("%s", err_msg);
            return NULL;
    }


    g_free(ext);
    return image;
}


static gboolean
find_data_scaled (const gchar line[GPIV_MAX_CHARS]
                  )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Determines if the data have been scaled in order to read x-and y 
 *     positions as floating point varibles
 *
 * PROTOTYPE LOCATATION:
 *     io.h
 *
 * INPUTS:
 *      line:           character line containing program that generated data
 *
 * OUTPUTS:
 *
 * RETURNS:
 *      scale:          TRUE if scaled, else FALSE
 *---------------------------------------------------------------------------*/
{
    gboolean scale = FALSE;
    

    if (strstr(line,"scale") != '\0') {
        scale = TRUE;
    }


    return scale;
}



static gboolean
count_asciidata (FILE *fp,
                 guint *nx,
                 guint *ny
                 )
/*---------------------------------------------------------------------------*/
/**
 *     Reads ASCII data from fp to find array length of x-and y data, 
 *     provided that both x and y-data are in 
 *     incremental or decremental order. Determines if scaled data have been used 
 *     in order to determine which format/header has to be used for writing the data
 *
 *     @param[out] nx          number of columns in piv data array
 *     @param[out] ny          number of columns in piv data array
 *     @param[in] fp           input file. If NULL, stdin is used.
 *     @return scale               TRUE or FALSE for scaled data
 */
/*---------------------------------------------------------------------------*/
{
    gboolean scale = FALSE;
    gchar line[GPIV_MAX_CHARS];
  
    gboolean increment = FALSE;
    gfloat x = 0, y = 0;
    gfloat dat_x = -10.0e5, dat_y = -10.0e5;
    gint line_nr = 0; 


    *nx = 0;
    *ny = 0;

    if (fp == stdin || fp == NULL) {
        while (gets (line) != NULL) {
            g_message ("count_asciidata:: line = %s", line);
            obtain_nxny_fromline (line, &scale, nx, ny, 
                                  &increment, &dat_x, &dat_y, &line_nr);
        }

    } else {
        while (fgets (line, GPIV_MAX_CHARS, fp) != NULL) {
            obtain_nxny_fromline (line, &scale, nx, ny, 
                                  &increment, &dat_x, &dat_y, &line_nr);


        }
    }

/*
 * Array size nx, ny
 */
    *nx = *nx + 1;
    *ny = *ny + 1;
#ifdef DEBUG
    g_message ("count_asciidata:: nx = %d ny = %d", *nx, *ny);
#endif


   return scale;
}



static gboolean
obtain_nxny_fromline (gchar line[], 
                      gboolean *scale,
                      guint *nx, 
                      guint *ny, 
                      gboolean *increment,
                      gfloat *dat_x,
                      gfloat *dat_y,
                      guint *line_nr
                      )
/*-----------------------------------------------------------------------------
 * Scans a line to get nx and ny and scale
 */
{
    gfloat x = 0, y = 0;


    if (line[0] == '#' 
        &&  *scale == FALSE) {
        *scale = find_data_scaled (line);
    }

/*
 * Skip comment, blank lines and header line
 */
    if (line[0] != '#' 
        && line[0] != '\n' 
        && line[0] != '\t') {
        sscanf (line, "%f %f", &x, &y);
/*
 * The first line containig data has been found. 
 * When the second has been found it is 
 * determined whether the data have been arranged column-wise or row-wise
 */
        if (*line_nr == 0) {
/* get file position for first data line */
            *dat_x = x;
            *dat_y = y;
        }

        if (*line_nr == 1) {
            if (x > *dat_x || y > *dat_y) {
                *increment = TRUE;
            } else {
                *increment = FALSE;
            }
        }


        *line_nr = *line_nr + 1;
        if (*increment == TRUE) {
            if (x > *dat_x){
                *dat_x = x;
                *nx = *nx + 1;
            }
            if (y > *dat_y) {
                *dat_y = y;
                *ny = *ny + 1;
            }
        } else {
            if (x < *dat_x){
                *dat_x = x;
                *nx = *nx + 1;
            }
            if (y < *dat_y) {
                *dat_y = y;
                *ny = *ny + 1;
            }
        }

    }

}


static void
obtain_pivdata_fromline (gchar line[], 
                         GpivPivData *piv_data,
                         guint *i,
                         guint *j
                         )
/*-----------------------------------------------------------------------------
 * Scans a line to get GpivPivData
 */
{

/*
 * Fill in comment, skip header
 */
    if (line[0] == '#'
        && g_strstr_len (line, GPIV_MAX_CHARS, "x(m)") == NULL
        && g_strstr_len (line, GPIV_MAX_CHARS, "x(px)") == NULL) {
        if (piv_data->comment == NULL) {
            piv_data->comment = g_strdup (line);
        } else {
            piv_data->comment = g_strconcat (piv_data->comment, line, NULL);
        }
        /*             printf ("\n Commentaar: %s\n\n", piv_data->comment); */
            

    } else if (line[0] != '\n'
               && line[0] != '\t'
               && g_strstr_len (line, GPIV_MAX_CHARS, "x(m)") == NULL
               && g_strstr_len (line, GPIV_MAX_CHARS, "x(px)") == NULL) {
/*
 * Fill in PIV data, skip empty lines and header
 */
        sscanf (line, "%f %f %f %f %f %d", 
                &piv_data->point_x[*i][*j], 
                &piv_data->point_y[*i][*j],
                &piv_data->dx[*i][*j], 
                &piv_data->dy[*i][*j], 
                &piv_data->snr[*i][*j], 
                &piv_data->peak_no[*i][*j]);
        if (*j < piv_data->nx - 1) {
            *j = *j + 1; 
        } else if (*i < piv_data->ny - 1) { 
            *j = 0; 
            *i = *i + 1; 
            /*             } else { */
            /*                 break;  */
        }
    }


    return;
}



static void
obtain_pivdata_fastx_fromline (gchar line[], 
                               GpivPivData *piv_data,
                               guint *i,
                               guint *j
                               )
/*-----------------------------------------------------------------------------
 * Scans a line to get GpivPivData, but with column (x-position) as inner loop 
 * variable
 */
{

/*
 * Fill in comment, skip header
 */
    if (line[0] == '#'
        && g_strstr_len (line, GPIV_MAX_CHARS, "x(m)") == NULL
        && g_strstr_len (line, GPIV_MAX_CHARS, "x(px)") == NULL) {
        if (piv_data->comment == NULL) {
            piv_data->comment = g_strdup (line);
        } else {
            piv_data->comment = g_strconcat (piv_data->comment, line, NULL);
        }
            
    } else if (line[0] != '\n'
               && line[0] != '\t'
               && g_strstr_len (line, GPIV_MAX_CHARS, "x(m)") == NULL
               && g_strstr_len (line, GPIV_MAX_CHARS, "x(px)") == NULL) {
        /*
         * Fill in data, skip empty lines and header
         */
        sscanf (line,"%f %f %f %f %f %d", 
                &piv_data->point_x[*i][*j], 
                &piv_data->point_y[*i][*j],
                &piv_data->dx[*i][*j], 
                &piv_data->dy[*i][*j], 
                &piv_data->snr[*i][*j], 
                &piv_data->peak_no[*i][*j]);
        /*
         * Increase first index (i) here!
         */
        if (*i < piv_data->nx - 1) {
            *i = *i + 1; 
        } else if (*j < piv_data->ny - 1) { 
            *i = 0; 
            *j = *j + 1; 
        }
    }


    return;
}



static void
obtain_scdata_fromline (gchar line[], 
                        GpivScalarData *sc_data,
                        guint *i,
                        guint *j
                        )
/*-----------------------------------------------------------------------------
 * Scans a line to get GpivScalarData
 */
{
/*     *i = 0; */
/*     *j = 0; */

/*
 * Fill in comment, skip header
 */
    if (line[0] == '#' 
        && g_strstr_len(line, GPIV_MAX_CHARS, "x(m)") == NULL
        && g_strstr_len (line, GPIV_MAX_CHARS, "x(px)") == NULL) {

        if (sc_data->comment == NULL) {
            sc_data->comment = g_strdup (line);
        } else {
            sc_data->comment = g_strconcat (sc_data->comment, line, NULL);
        }


    } else if (line[0] != '\n' 
               && line[0] != '\t'
               && g_strstr_len (line, GPIV_MAX_CHARS, "x(m)") == NULL 
               && g_strstr_len (line, GPIV_MAX_CHARS, "x(px)") == NULL) {
/*
 * Fill in SCALAR data, skip empty lines and header
 */
        sscanf (line, "%f %f %f %d", 
                &sc_data->point_x[*i][*j], 
                &sc_data->point_y[*i][*j], 
                &sc_data->scalar[*i][*j], 
                &sc_data->flag[*i][*j]);
        if (*j < sc_data->nx - 1) {
            *j = *j + 1; 
        } else if (*i < sc_data->ny - 1) { 
            *j = 0; 
            *i = *i + 1; 
        }
    }


    return;
}



static GpivPivData *
read_pivdata_from_file (FILE *fp,
                        gboolean fastx
                        )
/*-----------------------------------------------------------------------------
 * Reads PIV data from file pointer
 */
{
    GpivPivData *piv_data = NULL;
    gint i = 0, j = 0;
    gchar line[GPIV_MAX_CHARS];

    guint nx = 0, ny = 0;
    gboolean scale;


    scale = count_asciidata (fp, &nx, &ny);
    piv_data = gpiv_alloc_pivdata (nx, ny);
    piv_data->scale = scale;

    fseek (fp, 0, SEEK_SET);
    while ((i < piv_data->ny - 1) || (j < piv_data->nx - 1)) {
        fgets (line, GPIV_MAX_CHARS, fp);
        if (fastx) {
            obtain_pivdata_fastx_fromline (line, piv_data, &i, &j);
        } else {
            obtain_pivdata_fromline (line, piv_data, &i, &j);
        }
    }
    fgets (line, GPIV_MAX_CHARS, fp);
    if (fastx) {
        obtain_pivdata_fastx_fromline (line, piv_data, &i, &j);
    } else {
        obtain_pivdata_fromline (line, piv_data, &i, &j);
    }    


    return piv_data;
}


static GpivPivData *
read_pivdata_from_stdin (gboolean fastx
                         )
/*-----------------------------------------------------------------------------
 * Reads PIV data from file pointer
 */
{
    GpivPivData *piv_data = NULL;
    gint i = 0, j = 0, k = 0;
    gchar line[GPIV_MAX_LINES][GPIV_MAX_CHARS];

    guint nx = 0, ny = 0;
    gboolean scale = FALSE;
    gulong bufsize = 2500;

    gboolean increment = FALSE;
    gfloat x = 0, y = 0;
    gfloat dat_x = -10.0e5, dat_y = -10.0e5;
    gint line_nr = 0; 

    gboolean bool = FALSE;


    /* 
     * Count nx, ny, similar to count_asciidata
     */


    k = 0;
    while (gets (line[k]) != NULL) {
        obtain_nxny_fromline (line[k], &scale, &nx, &ny, 
                              &increment, &dat_x, &dat_y, &line_nr);
        k++;
    }
    nx = nx + 1;
    ny = ny + 1;
    piv_data = gpiv_alloc_pivdata (nx, ny);
    piv_data->scale = scale;


    k = 0;
    i = 0;
    j = 0;
    while ((i < piv_data->ny - 1) || (j < piv_data->nx - 1)) {
        if (fastx) {
            obtain_pivdata_fastx_fromline (line[k], piv_data, &i, &j);
        } else {
            obtain_pivdata_fromline (line[k], piv_data, &i, &j);
        }
        k++;
    }
    if (fastx) {
        obtain_pivdata_fastx_fromline (line[k], piv_data, &i, &j);
    } else {
        obtain_pivdata_fromline (line[k], piv_data, &i, &j);
    }


    return piv_data;
}



static GpivScalarData *
read_scdata_from_file (FILE *fp
                       )
/*-----------------------------------------------------------------------------
 * Reads scalar data from file pointer
 */
{
    GpivScalarData *sc_data = NULL;
    gint i = 0, j = 0;
    gchar line[GPIV_MAX_CHARS];

    guint nx = 0, ny = 0;
    gboolean scale;


    scale = count_asciidata (fp, &nx, &ny);
    sc_data = gpiv_alloc_scdata (nx, ny);
    sc_data->scale = scale;

    fseek (fp, 0, SEEK_SET);
    while ((i < sc_data->ny - 1) || (j < sc_data->nx - 1)) {
        fgets (line, GPIV_MAX_CHARS, fp);
        obtain_scdata_fromline (line, sc_data, &i, &j);
    }
    fgets (line, GPIV_MAX_CHARS, fp);
    obtain_scdata_fromline (line, sc_data, &i, &j);
    


    return sc_data;
}


static GpivScalarData *
read_scdata_from_stdin (void
                        )
/*-----------------------------------------------------------------------------
 * Reads scalar data from stdin
 */
{
    GpivScalarData *sc_data = NULL;
    gint i = 0, j = 0, k = 0;
    gchar line[GPIV_MAX_LINES][GPIV_MAX_CHARS];

    guint nx = 0, ny = 0;
    gboolean scale = FALSE;

    gboolean increment = FALSE;
    gfloat x = 0, y = 0;
    gfloat dat_x = -10.0e5, dat_y = -10.0e5;
    gint line_nr = 0; 


/* 
 * Count nx, ny, similar to count_asciidata
 */

    k = 0;
    while (gets (line[k]) != NULL) {
        obtain_nxny_fromline (line[k], &scale, &nx, &ny, 
                              &increment, &dat_x, &dat_y, &line_nr);
        k++;
    }
    nx = nx + 1;
    ny = ny + 1;
    sc_data = gpiv_alloc_scdata (nx, ny);
    sc_data->scale = scale;


    k = 0;
    i = 0;
    j = 0;
    while ((i < sc_data->ny - 1) || (j < sc_data->nx - 1)) {
       obtain_scdata_fromline (line[k], sc_data, &i, &j);
        k++;
    }
    obtain_scdata_fromline (line[k], sc_data, &i, &j);


    return sc_data;
}
