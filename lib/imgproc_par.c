/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008
   Gerber van der Graaf

   This file is part of libgpiv.
   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  



-------------------------------------------------------------------------------
FILENAME:                imgproc_par.c
LIBRARY:                 libgpiv:
EXTERNAL FUNCTIONS:
                        gpiv_imgproc_parameters_set
                        gpiv_imgproc_default_parameters
                        gpiv_imgproc_read_parameters
                        gpiv_imgproc_check_parameters_read
                        gpiv_imgproc_test_parameters
                        gpiv_imgproc_print_parameters
                        gpiv_imgproc_fprint_parameters


LAST MODIFICATION DATE:  $Id: imgproc_par.c,v 1.9 2008-05-05 14:38:41 gerber Exp $
 --------------------------------------------------------------------------- */

#include <gpiv.h>

enum {
    IMGPROCPAR_DEFAULT__BIT          = 0,         /**< Default parameter for bit of __GpivImageProcPar */
    IMGPROCPAR_DEFAULT__FILTER       = 0,         /**< Default parameter for filter of __GpivImageProcPar */
    IMGPROCPAR_DEFAULT__SMOOTH_OPERATOR = 0,      /**< Default parameter for smooth_operator of __GpivImageProcPar */
    IMGPROCPAR_DEFAULT__THRESHOLD    = 30,        /**< Default parameter for threshold of __GpivImageProcPar */
    IMGPROCPAR_DEFAULT__WINDOW       = 10         /**< Default parameter for window of __GpivImageProcPar */
};

const gchar *IMGPROCPAR_KEY__BIT        = "Bit";           /**< Key for bit of __GpivImageProcPar */
const gchar *IMGPROCPAR_KEY__FILTER     = "Filter";        /**< Key for filter of __GpivImageProcPar */
const gchar *IMGPROCPAR_KEY__SMOOTH_OPERATOR = "Smooth_operator"; /**< Key for smooth_operator of __GpivImageProcPar */
const gchar *IMGPROCPAR_KEY__THRESHOLD = "Threshold";      /**< Key for threshold of __GpivImageProcPar */
const gchar *IMGPROCPAR_KEY__WINDOW    = "Window";         /**< Key for window of __GpivImageProcPar */



static void
print_parameters (FILE *fp,
                  const GpivImageProcPar *image_proc_par
                  );

void
gpiv_imgproc_parameters_set (GpivImageProcPar *image_proc_par,
                             const gboolean flag
                             )
/*-----------------------------------------------------------------------------
 */
{
    image_proc_par->bit__set = flag;
    image_proc_par->filter__set = flag; 
    image_proc_par->smooth_operator__set = flag; 
    image_proc_par->threshold__set = flag;
    image_proc_par->window__set = flag;

}


void
gpiv_imgproc_default_parameters (GpivImageProcPar *image_proc_par_default,
                                 const gboolean force
                                 )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Sets default parameter values
 *
 * INPUTS:
 *
 * OUTPUTS:
 *     piv_imgproc_par_default:   structure of image processing parameters
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    if (!image_proc_par_default->bit__set || force)
        image_proc_par_default->bit = IMGPROCPAR_DEFAULT__BIT;

    if (!image_proc_par_default->filter__set || force)
        image_proc_par_default->filter = IMGPROCPAR_DEFAULT__FILTER;

    if (!image_proc_par_default->smooth_operator__set || force)
        image_proc_par_default->smooth_operator = IMGPROCPAR_DEFAULT__SMOOTH_OPERATOR;

    if (!image_proc_par_default->window__set || force)
        image_proc_par_default->window = IMGPROCPAR_DEFAULT__WINDOW;

    if (!image_proc_par_default->threshold__set || force)
        image_proc_par_default->threshold = IMGPROCPAR_DEFAULT__THRESHOLD;


}


void
gpiv_imgproc_read_parameters (FILE *fp_par, 
                              GpivImageProcPar *image_proc_par, 
                              const gboolean print_par
                              )
/*----------------------------------------------------------------------------
  DESCRIPTION:
  Reads each line of the file and looks for image parameters parameters

  PROTOTYPE LOCATATION:
  gpiv_io.h

  INPUTS:
  fp_par:           file pointer to input file
  print_par         flag to print parameter to stdout

  OUTPUTS:
  ---------------------------------------------------------------------------- */
{
    char line[GPIV_MAX_CHARS], par_name[GPIV_MAX_CHARS];


    while (fgets(line, GPIV_MAX_CHARS, fp_par) != NULL) {
	if (line[0] != '#' && line[0] != '\n' && line[0] != ' '
	    && line[0] != '\t') {
	    sscanf(line, "%s", par_name);

	    if (image_proc_par->smooth_operator__set == FALSE) {
		image_proc_par->smooth_operator__set =
		    gpiv_scan_iph (GPIV_IMGPROCPAR_KEY, 
                                   IMGPROCPAR_KEY__SMOOTH_OPERATOR, TRUE,
                                   line, par_name, (int *) &image_proc_par->smooth_operator, 
                                   print_par, NULL);
	    }

	    if (image_proc_par->window__set == FALSE) {
		image_proc_par->window__set =
		    gpiv_scan_iph (GPIV_IMGPROCPAR_KEY, 
                                   IMGPROCPAR_KEY__WINDOW, TRUE,
                                   line, par_name, &image_proc_par->window, 
                                   print_par, NULL);
	    }

	    if (image_proc_par->threshold__set == FALSE) {
		image_proc_par->threshold__set =
		    gpiv_scan_iph (GPIV_IMGPROCPAR_KEY, 
                                   IMGPROCPAR_KEY__THRESHOLD, TRUE,
                                   line, par_name, &image_proc_par->threshold, 
                                   print_par, NULL);
	    }

	    if (image_proc_par->bit__set == FALSE) {
		image_proc_par->bit__set =
		    gpiv_scan_iph (GPIV_IMGPROCPAR_KEY, 
                                   IMGPROCPAR_KEY__BIT, TRUE,
                                   line, par_name, &image_proc_par->bit, 
                                   print_par, NULL);
	    }



	}
    }


}



gchar *
gpiv_imgproc_check_parameters_read (GpivImageProcPar *image_proc_par,
                                    const GpivImageProcPar *image_proc_par_default
                                    )
/*----------------------------------------------------------------------------
  DESCRIPTION:
      Check out if all image processing parameters have been read

  INPUTS:
      image_proc_par:   structure to image processing parameters

  RETURNS:
      NULL on success or *err_msg on failure
---------------------------------------------------------------------------- */
{
    gchar *err_msg = NULL;


    if (image_proc_par->smooth_operator__set == FALSE) {
        image_proc_par->smooth_operator__set = TRUE;
        if (image_proc_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            image_proc_par->smooth_operator = IMGPROCPAR_DEFAULT__SMOOTH_OPERATOR;
        } else {
            err_msg = "Using default: ";
            image_proc_par->smooth_operator = image_proc_par_default->smooth_operator;
        }
        gpiv_warning("%s\n%s.%s %d", err_msg,
                     GPIV_IMGPROCPAR_KEY,
                     IMGPROCPAR_KEY__SMOOTH_OPERATOR,
                     image_proc_par->smooth_operator);
    }

    if (image_proc_par->window__set == FALSE) {
        image_proc_par->window__set = TRUE;
        if (image_proc_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            image_proc_par->window = IMGPROCPAR_DEFAULT__WINDOW;
        } else {
            err_msg = "Using default: ";
            image_proc_par->window = image_proc_par_default->window;
        }
        gpiv_warning("%s\n%s.%s %d", err_msg,
                     GPIV_IMGPROCPAR_KEY,
                     IMGPROCPAR_KEY__WINDOW,
                     image_proc_par->window);
    }

    if (image_proc_par->threshold__set == FALSE) {
        image_proc_par->threshold__set = TRUE;
        if (image_proc_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            image_proc_par->threshold = IMGPROCPAR_DEFAULT__THRESHOLD;
        } else {
            err_msg = "Using default: ";
            image_proc_par->threshold = image_proc_par_default->threshold;
        }
        gpiv_warning("%s\n%s.%s %d", err_msg,
                     GPIV_IMGPROCPAR_KEY,
                     IMGPROCPAR_KEY__THRESHOLD,
                     image_proc_par->threshold);
    }

    if (image_proc_par->bit__set == FALSE) {
        image_proc_par->bit__set = TRUE;
        if (image_proc_par_default == NULL) {
            err_msg = "Using default from libgpiv: ";
            image_proc_par->bit = IMGPROCPAR_DEFAULT__BIT;
        } else {
            err_msg = "Using default: ";
            image_proc_par->bit = image_proc_par_default->bit;
        }
        gpiv_warning("%s\n%s.%s %d", err_msg,
                     GPIV_IMGPROCPAR_KEY,
                     IMGPROCPAR_KEY__BIT,
                      image_proc_par->bit);
    }


    return err_msg;
}



gchar *
gpiv_imgproc_test_parameters(const GpivImageProcPar *image_proc_par
			     )
/* ----------------------------------------------------------------------------
   Test parameters information on validity */
{
    char *err_msg = NULL;

    if (image_proc_par->window > GPIV_MAX_IMG_SIZE) {
        err_msg = "gpiv_imgproc_test_parameters: window larger than GPIV_MAX_IMG_SIZE";
         gpiv_warning("%s", err_msg);
         return err_msg;
     }

    if (image_proc_par->threshold < 0 
        || image_proc_par->threshold > pow((double)2, GPIV_MAX_IMG_DEPTH)) {
        err_msg = "gpiv_imgproc_test_parameters: threshold larger than GPIV_MAX_IMG_DEPTH";
         gpiv_warning("%s", err_msg);
         return err_msg;
     }

    if (image_proc_par->bit > GPIV_MAX_IMG_DEPTH) {
        err_msg = "gpiv_imgproc_test_parameters: bit larger than GPIV_MAX_IMG_DEPTH";
         gpiv_warning("%s", err_msg);
         return err_msg;
     }

    if (image_proc_par->window > GPIV_MAX_IMG_SIZE) {
        err_msg = "gpiv_imgproc_test_parameters: window larger than GPIV_MAX_IMG_SIZE";
         gpiv_warning("%s", err_msg);
         return err_msg;
     }
}



GpivImageProcPar *
gpiv_imgproc_cp_parameters (const GpivImageProcPar *imgproc_par
                            )
/*-----------------------------------------------------------------------------
 *     Copies image processing parameters.
 */
{
    GpivImageProcPar *par = g_new0 (GpivImageProcPar, 1);


    par->bit = imgproc_par->bit;
    par->bit__set = TRUE;

    par->filter = imgproc_par->filter;
    par->filter__set = TRUE;

    par->smooth_operator = imgproc_par->smooth_operator;
    par->smooth_operator__set = TRUE;

    par->window = imgproc_par->window;
    par->window__set = TRUE;

    par->threshold = imgproc_par->threshold;
    par->threshold__set = TRUE;


    return par;
}

void
gpiv_imgproc_print_parameters (FILE *fp, 
                               const GpivImageProcPar *image_proc_par
                               )
/*  ---------------------------------------------------------------------------
 * prints image processing parameters parameters to fp_par_out
 */
{
    print_parameters (fp, image_proc_par);
}


/*
 * Local functions
 */
static void
print_parameters (FILE *fp,
                  const GpivImageProcPar *image_proc_par
                  )
/*  ---------------------------------------------------------------------------
 * prints image processing parameters parameters to fp_par_out
 */
{
    if (fp == stdout || fp == NULL) {
        if (image_proc_par->filter__set)
            printf ("%s.%s %d\n", 
                    GPIV_IMGPROCPAR_KEY, 
                    IMGPROCPAR_KEY__FILTER, 
                    image_proc_par->filter);

        if (image_proc_par->smooth_operator__set)
            printf ("%s.%s %d\n", 
                    GPIV_IMGPROCPAR_KEY, 
                    IMGPROCPAR_KEY__SMOOTH_OPERATOR, 
                    image_proc_par->smooth_operator);

        if (image_proc_par->window__set)
            printf ("%s.%s %d\n", 
                    GPIV_IMGPROCPAR_KEY, 
                    IMGPROCPAR_KEY__WINDOW,
                    image_proc_par->window);

        if (image_proc_par->threshold__set)
            printf ("%s.%s %d\n", 
                    GPIV_IMGPROCPAR_KEY, 
                    IMGPROCPAR_KEY__THRESHOLD,
                    image_proc_par->threshold);

        if (image_proc_par->bit__set)
            printf ("%s.%s %d\n", 
                    GPIV_IMGPROCPAR_KEY, 
                    IMGPROCPAR_KEY__BIT,
                    image_proc_par->bit);

    } else {
        if (image_proc_par->filter__set)
            fprintf (fp, "%s.%s %d\n", 
                     GPIV_IMGPROCPAR_KEY, 
                     IMGPROCPAR_KEY__FILTER, 
                     image_proc_par->filter);

        if (image_proc_par->smooth_operator__set)
            fprintf (fp, "%s.%s %d\n", 
                     GPIV_IMGPROCPAR_KEY, 
                     IMGPROCPAR_KEY__SMOOTH_OPERATOR, 
                     image_proc_par->smooth_operator);

        if (image_proc_par->window__set)
            fprintf (fp, "%s.%s %d\n", 
                     GPIV_IMGPROCPAR_KEY, 
                     IMGPROCPAR_KEY__WINDOW,
                     image_proc_par->window);

        if (image_proc_par->threshold__set)
            fprintf (fp, "%s.%s %d\n", 
                     GPIV_IMGPROCPAR_KEY, 
                     IMGPROCPAR_KEY__THRESHOLD,
                     image_proc_par->threshold);

        if (image_proc_par->bit__set)
            fprintf (fp, "%s.%s %d\n", 
                     GPIV_IMGPROCPAR_KEY, 
                     IMGPROCPAR_KEY__BIT,
                     image_proc_par->bit);
    }
}
