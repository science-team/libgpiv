/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008
   Gerber van der Graaf

   This file is part of libgpiv.
   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  



-------------------------------------------------------------------------------
FILENAME:                imgproc.c
LIBRARY:                 libgpiv:
EXTERNAL FUNCTIONS:
                        gpiv_imgproc_mktestimg
                        gpiv_imgproc_smooth
                        gpiv_imgproc_highlow
                        gpiv_imgproc_clip
                        gpiv_imgproc_getbit
                        gpiv_imgproc_fft
                        gpiv_imgproc_lowpass
                        gpiv_imgproc_highpass
                        gpiv_imgproc_correlate
                        gpiv_imgproc_convolve

LAST MODIFICATION DATE:  $Id: imgproc.c,v 1.7 2008-04-09 06:10:28 gerber Exp $
 --------------------------------------------------------------------------- */

#include <gpiv.h>



static void
smooth_frame (guint16 **frame_src,
              guint16 **frame_dest,
              const GpivImagePar *image_par,
              const GpivImageProcPar *image_proc_par
              );

static void
cp_img_data_frame (guint16 **img_src, 
                   guint16 **img_dest,
                   const GpivImagePar *image_par
                   );

static gchar *
imgproc_clip_frame (const GpivImagePar *image_par,
                    const GpivImageProcPar *image_proc_par,
                    guint16 **img
                    );

static gchar *
imgproc_getbit_frame (const GpivImagePar *image_par,
                      const GpivImageProcPar *image_proc_par,
                      guint16 **img
                      );

static gchar *
imgproc_highlow_frame (const GpivImagePar *image_par,
                       const GpivImageProcPar *image_proc_par,
                       guint16 **img
                       );

static gchar *
imgproc_fft_frame (const GpivImagePar *image_par, 
                   const GpivImageProcPar *image_proc_par,
                   guint16 **img 
                   );

/*
 * Public functions
 */

GpivImage *
gpiv_imgproc_mktestimg (const GpivImagePar *image_par,
                        const GpivImageProcPar *image_proc_par
                        )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Genarates test image for PIV processing
 *---------------------------------------------------------------------------*/
{
    GpivImage *image = NULL;
    gchar *err_msg = NULL;
    int i, j, k, l;
    int M = image_par->nrows;
    int N = image_par->ncolumns;
    int window = image_proc_par->window;
    int threshold = image_proc_par->threshold;
    int bit = image_proc_par->bit;
/*     guint16 **img = NULL; */


    image = gpiv_alloc_img (image_par);

    for (i = 0; i < M; i++) {
        for (j = 0; j < N; j++) {
            image->frame1[i][j] = 100;
        }
    }

    for (k = 0; k < M; k = k + window + k * 0.25) {
        for (l = 0; l < N; l = l + threshold + l * 0.25) {

            for (i = 0; i < (window + k * 0.25)/2; i++) {
                for (j = 0; j < (threshold + l * 0.25) / 2; j++) {

#ifdef DEBUG
                    gpiv_warning("gpiv_imgproc_mktestimg: i=%d j=%d j_end=%d k=%d l=%d i+k=%d j+l=%d", 
                                 i, j, (threshold + l *bit)/2, k, l, i+k, j+l);
#endif
                    if (M/2 + (i+k) < M && N/2 + (j+l) < N 
                        && M/2 - (i+k) >= 0 && N/2 - (j+l) >=0
                        ) {
#ifdef DEBUG
                        gpiv_warning("gpiv_imgproc_mktestimg: passed");
#endif
                            image->frame1[M/2 + (i+k)][N/2 + (j+l)] = 200 /* ~0 */;
                            image->frame1[M/2 - (i+k)][N/2 + (j+l)] = 200 /* ~0 */;
                            image->frame1[M/2 + (i+k)][N/2 - (j+l)] = 200 /* ~0 */;
                            image->frame1[M/2 - (i+k)][N/2 - (j+l)] = 200 /* ~0 */;
                    }

                }
            }

        }
    }
    

    return image;
}



gchar *
gpiv_imgproc_subtractimg (const GpivImage *image_in,
                          GpivImage *image_out
                          )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Subtracts image intensities of image_in from image_out. To avoid swapping of 
 *     intensities, output values are set to zero if img_in is larger than img_out.
 *     Images will have to be of identic dimensions.
 *
 * INPUTS:
 *     img_in:                 image containing the intensities to be subtracted
 *
 * OUTPUTS:
 *     img_out:                modified image. will have to exist and allocated
 *
 * RETURNS:
 *     NULL on success or *err_msg on failure
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    gint i, j;

    if ((err_msg = gpiv_check_alloc_img (image_in)) != NULL) {
        return err_msg;
    }

    if ((err_msg = gpiv_check_alloc_img (image_out)) != NULL) {
        return err_msg;
    }


    if (image_in->header->ncolumns != image_out->header->ncolumns
        || image_in->header->nrows != image_out->header->nrows
        || image_in->header->depth != image_out->header->depth
        || image_in->header->x_corr != image_out->header->x_corr
        ) {
        err_msg = "gpiv_imgproc_subtractimg: in-and output images are of different dimensions";
        return (err_msg);
    }


    for (i = 0; i < image_in->header->nrows; i++) {
        for (j = 0; j < image_in->header->ncolumns; j++) {
                 if (image_out->frame1[i][j] >= image_in->frame1[i][j]) {
                     image_out->frame1[i][j] = image_out->frame1[i][j] 
                         - image_in->frame1[i][j];
                 } else {
                     image_out->frame1[i][j] = 0;
                 }
        }
    }


    if (image_in->header->x_corr) {
        for (i = 0; i < image_in->header->nrows; i++) {
            for (j = 0; j < image_in->header->ncolumns; j++) {
                if (image_out->frame2[i][j] >= image_in->frame2[i][j]) {
                    image_out->frame2[i][j] = image_out->frame2[i][j] 
                        - image_in->frame2[i][j];
                } else {
                    image_out->frame2[i][j] = 0;
                }
            }
        }
    }


    return (err_msg);
}



gchar *
gpiv_imgproc_smooth (GpivImage *image, 
                     const GpivImageProcPar *image_proc_par 
                     )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Smoothing filter by taking mean value of surrounding 
 *     window x window pixels
 *
 * INPUTS:
 *     image_par:             structure of image parameters
 *     image_proc_par:        structure of image processing parameters
 *
 * OUTPUTS:
 *     img:                   modified input image
 *
 * RETURNS:
 *     NULL on success or *err_msg on failure
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    GpivImage *image_l = NULL;


    if ((err_msg = gpiv_check_alloc_img (image)) != NULL) {
        return err_msg;
    }


    image_l = gpiv_alloc_img (image->header);


    smooth_frame (image->frame1, image_l->frame1, image->header, 
                  image_proc_par);
    if (image->header->x_corr) {
        smooth_frame (image->frame2, image_l->frame2, image->header, 
                      image_proc_par);
    }

    gpiv_cp_img_data (image_l, image);
    gpiv_free_img (image_l); 
    return (err_msg);   
}



gchar *
gpiv_imgproc_highpass (GpivImage *image, 
                       const GpivImageProcPar *image_proc_par
                       )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Highpass filter on an image
 *
 * INPUTS:
 *     image_par:             structure of image parameters
 *     image_proc_par:        structure of image processing parameters
 *     img:                   input image. Needs to be allocated
 *
 * OUTPUTS:
 *     img:                   modified input image
 *
 * RETURNS:
 *     NULL on success or *err_msg on failure
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    gint i, j;
    GpivImage *l_image = NULL;
    GpivImageProcPar *l_par = NULL;

    if ((err_msg = gpiv_check_alloc_img (image)) != NULL) {
        return err_msg;
    }


    l_image = gpiv_cp_img (image);
    l_par = gpiv_imgproc_cp_parameters (image_proc_par);
    l_par->window = l_par->threshold;
    l_par->smooth_operator == GPIV_IMGOP_EQUAL;

    if ((err_msg = gpiv_imgproc_smooth (l_image, l_par)) != NULL) {
        return err_msg;
    }

    if ((err_msg = gpiv_imgproc_subtractimg (image, l_image)) != NULL) {
        return err_msg;
    }


    gpiv_free_img (l_image);
    return err_msg;
}


gchar *
gpiv_imgproc_highlow (GpivImage *image,
                      const GpivImageProcPar *image_proc_par
                      )
/*---------------------------------------------------------------------------*/
/**
 *     High-low filter to maximize contrast by stretching pixel values
 *     to local max and min within window x window area
 *
 *     @param[in] image                  image that will be filtered
 *     @param[in] image_proc_par         structure of image processing parameters
 *     @return                           NULL on success or *err_msg on failure
 */
/*---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;


    if ((err_msg = gpiv_check_alloc_img (image)) != NULL) {
        return err_msg;
    }


    if ((err_msg = imgproc_highlow_frame (image->header, image_proc_par, 
                                          image->frame1)) != NULL) {
        return err_msg;
    }

    if (image->header->x_corr) {
        if ((err_msg = imgproc_highlow_frame (image->header, image_proc_par, 
                                              image->frame2)) != NULL) {
            return err_msg;
        }
    }


    return err_msg;
}



gchar *
gpiv_imgproc_clip (GpivImage *image,
                   const GpivImageProcPar *image_proc_par
                   )
/*---------------------------------------------------------------------------*/
/**
 *     Sets all pixel values lower than threshold to zero
 *
 *     @param[in] image                  image to be clipped.
 *     @param[in] image_proc_par         structure of image processing parameters
 *     @return                           NULL on success or *err_msg on failure
 */
/*---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;


    if ((err_msg = gpiv_check_alloc_img (image)) != NULL) {
        return err_msg;
    }


    if ((err_msg = imgproc_clip_frame (image->header, image_proc_par, 
                                         image->frame1)) != NULL) {
        return err_msg;
    }

    if (image->header->x_corr) {
        if ((err_msg = imgproc_clip_frame (image->header, image_proc_par, 
                                             image->frame2)) != NULL) {
            return err_msg;
        }
    }


    return err_msg;
}



gchar *
gpiv_imgproc_getbit (GpivImage *image,
                     const GpivImageProcPar *image_proc_par
                     )
/*---------------------------------------------------------------------------*/
/**
 *     Sets all pixel values lower than threshold to zero
 *
 *     @param[in] image                  image to be clipped.
 *     @param[in] image_proc_par         structure of image processing parameters
 *     @return                           NULL on success or *err_msg on failure
 */
/*---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;


    if ((err_msg = gpiv_check_alloc_img (image)) != NULL) {
        return err_msg;
    }


    if ((err_msg = imgproc_getbit_frame (image->header, image_proc_par, 
                                        image->frame1)) != NULL) {
        return err_msg;
    }

    if (image->header->x_corr) {
        if ((err_msg = imgproc_getbit_frame (image->header, image_proc_par, 
                                            image->frame2)) != NULL) {
            return err_msg;
        }
    }


    return err_msg;
}



gchar *
gpiv_imgproc_fft (GpivImage *image,
                  const GpivImageProcPar *image_proc_par
                  )
/*---------------------------------------------------------------------------*/
/**
 *     Sets all pixel values lower than threshold to zero
 *
 *     @param[in] image                  image to be clipped.
 *     @param[in] image_proc_par         structure of image processing parameters
 *     @return                           NULL on success or *err_msg on failure
 */
/*---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;


    if ((err_msg = gpiv_check_alloc_img (image)) != NULL) {
        return err_msg;
    }


    if ((err_msg = imgproc_fft_frame (image->header, image_proc_par, 
                                        image->frame1)) != NULL) {
        return err_msg;
    }

    if (image->header->x_corr) {
        if ((err_msg = imgproc_fft_frame (image->header, image_proc_par, 
                                            image->frame2)) != NULL) {
            return err_msg;
        }
    }


    return err_msg;
}



#ifndef USE_FFTW3
gchar *
gpiv_imgproc_lowpass (GpivImagePar image_par, 
                      GpivImageProcPar image_proc_par,
                      guint16 **img
                      )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Lowpass filter on an image
 *
 * INPUTS:
 *     image_par:             structure of image parameters
 *     image_proc_par:        structure of image processing parameters
 *     img:                   input image. Needs to be allocated
 *
 * OUTPUTS:
 *     img:                   modified input image
 *
 * RETURNS:
 *     NULL on success or *err_msg on failure
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    int i, j, ij;
    int M = 2 * image_par.nrows, N = 2 * image_par.ncolumns;
    int window = image_proc_par->window;
    fftw_complex **a, **b;
    fftw_complex **A, **B;
    fftwnd_plan p = NULL, pinv = NULL;
    fftw_real scale = 1.0 / (M * N);
    double mean_img;
    guint16 **img_l = NULL;


    if (img == NULL) {
        err_msg = "gpiv_imgproc_lowpass: img == NULL";
        return (err_msg);
    }

/*     img_l = gpiv_alloc_img (image_par); */
    img_l = gpiv_matrix_guint16 (image_par.nrows, image_par.ncolumns);

    gpiv_fread_fftw_wisdom(1);
    gpiv_fread_fftw_wisdom(-1);

    a = gpiv_fftw_complex_matrix(M, N);
/* gpiv_gdouble_matrix(M, 2 * (N / 2 + 1)); */
    A = gpiv_fftw_complex_matrix(M, N);
/* (fftw_complex *) &a[0][0]; */

/*     b = gpiv_gdouble_matrix(M, N); */
    b = gpiv_fftw_complex_matrix(M, N);
    B = gpiv_fftw_complex_matrix(M, N);
/* gpiv_fftw_complex_matrix(M, N /2 + 1); */

    for (i = 0; i < image_par.nrows; i++) {
	for (j = 0; j < image_par.ncolumns; j++) {
	    a[i][j].re = (double) img[i][j];
	    a[i][j].im = 0.0;
	}
    }
    

/*
 * import_wisdom from string and make plans
 * FFT transform to get A and B from a and b;
 */
    
 /* FFTW_REAL_TO_COMPLEX */
 /*| FFTW_USE_WISDOM  | FFTW_IN_PLACE */
    p = fftw2d_create_plan(M, N, FFTW_FORWARD,
                           FFTW_MEASURE | FFTW_USE_WISDOM
                           );

/*     rfftwnd_one_real_to_complex(p, &a[0][0], &A[0][0]); */
    fftwnd_one(p, &a[0][0], &A[0][0]);
    gpiv_fwrite_fftw_wisdom(1);
    fftwnd_destroy_plan(p);

/*     mean_img = A[0][0].re; */
    



/* 
 *lowpass filter: passing data data from 0,..,window/2
 */

    for (i = 0; i < M; i++) {
        for (j = 0; j < N; j++) {
            B[i][j].re = 0.0;
            B[i][j].im = 0.0;
        }
    }
    
    B[0][0].re = A[0][0].re * scale;
    B[0][0].im = A[0][0].im * scale;
    
    
    
    for (j = 0, i = 1; i <= window; i++) {

#ifdef DEBUG
        gpiv_warning("gpiv_imgproc_lowpass:: A.re[%d][%d] = %f A.im[%d][%d] = %f "
                     "=> A.re[%d][%d] = %f A.im[%d][%d] = %f", 
                     i, j, A[i][j].re, i, j, A[i][j].im,
                     M - i, j, A[M - i][j].re, 
                     M - i, j, A[M - i][j].im);
#endif        
        B[i][j].re = A[i][j].re * scale;
        B[i][j].im = A[i][j].im * scale;
        B[M - i][j].re = (A[M - i][j].re) * scale;
        B[M - i][j].im = (A[M - i][j].im) * scale;
    }



    for (i = 0, j = 1; j <= window; j++) {
#ifdef DEBUG
        gpiv_warning("gpiv_imgproc_lowpass:: A.re[%d][%d] = %f A.im[%d][%d] = %f "
                     "=> A.re[%d][%d] = %f A.im[%d][%d] = %f", 
                     i, j, A[i][j].re, i, j, A[i][j].im,
                     i, N - j, A[i][N - j].re, 
                     i, N - j, A[i][N - j].im);
#endif
        B[i][j].re = A[i][j].re * scale;
        B[i][j].im = A[i][j].im * scale;
        B[i][N - j].re = (A[i][N - j].re) * scale;
        B[i][N - j].im = (A[i][N - j].im) * scale;
    }
/*         } */


    if (window >= 1) {
        for (i = 1; i <= window; i++) {
            for (j = 1; j <= window; j++) {
#ifdef DEBUG                
            gpiv_warning("gpiv_imgproc_lowpass:: A.re[%d][%d] = %f A.im[%d][%d] = %f "
                         "=> A.re[%d][%d] = %f A.im[%d][%d] = %f", 
                         i, j, A[i][j].re, i, j, A[i][j].im,
                         M - i, N - j, A[M - i][N - j].re, 
                         M - i, N - j, A[M - i][N - j].im);
#endif            
                B[i][j].re = A[i][j].re * scale;
                B[i][j].im = A[i][j].im * scale;
                
                B[M - i][j].re = (A[M - i][j].re) * scale;
                B[M - i][j].im = (A[M - i][j].im) * scale;
                
                B[i][N - j].re = (A[i][N - j].re) * scale;
                B[i][N - j].im = (A[i][N - j].im) * scale;
                
                B[M - i][N - j].re = (A[M - i][N - j].re) * scale;
                B[M - i][N - j].im = (A[M - i][N - j].im) * scale;
            }
        }
    }



#ifdef DEBUG                
    for (i = 0; i < M; i++) {
        for (j = 0; j < N; j++) {
            if (B[i][j].re == 0.0 && B[i][j].im == 0.0)
                gpiv_warning("gpiv_imgproc_lowpass:: B[%d][%d].re = 0.0 B[%d][%d].im = 0.0", 
                             i, j, i, j); 
        }
    }
#endif




/*
 * import_wisdom from string and make plans
 * inverse transform to get c, the convolution of a and b;
 * this has the side effect of overwriting C
 */
    pinv = fftw2d_create_plan(M, N, FFTW_BACKWARD/* FFTW_COMPLEX_TO_REAL */,
                              FFTW_MEASURE | FFTW_USE_WISDOM
                              );

/*     fftwnd_one_complex_to_real(pinv, &B[0][0], &b[0][0]); */
    fftwnd_one(pinv, &B[0][0], &b[0][0]);
    
    
    gpiv_fwrite_fftw_wisdom(-1);
    rfftwnd_destroy_plan(pinv);
    
    
    
    for (i = 0; i < image_par.nrows; i++) {
        for (j = 0; j < image_par.ncolumns; j++) {
            img_l[i][j] = (guint16) b[i][j].re;
        }
    }






    gpiv_cp_img_data (img_l, img);
    gpiv_free_img (img_l); 
/*     gpiv_free_fftw_real_matrix(a); */
/*     gpiv_free_fftw_real_matrix(b); */
    gpiv_free_fftw_complex_matrix(a);
    gpiv_free_fftw_complex_matrix(b);
    gpiv_free_fftw_complex_matrix(B);
}



gchar *
gpiv_imgproc_highpass_fft (GpivImagePar *image_par, 
                           GpivImageProcPar *image_proc_par,
                           guint16 **img
                           )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Highpass filter on an image
 *     passing data from M - window,..,M/2, N - window,..,N/2
 *
 * INPUTS:
 *     image_par:             structure of image parameters
 *     image_proc_par:        structure of image processing parameters
 *     img:                   input image. Needs to be allocated
 *
 * OUTPUTS:
 *     img:                   modified input image
 *
 * RETURNS:
 *     NULL on success or *err_msg on failure
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
/*     int img_size_0 = ZEROPAD_FACT  */
/*         * gpiv_max(image_par->nrows, image_par->ncolumns); */
    int i, j, ij;
    int M = 2 * image_par->nrows, N = 2 * image_par->ncolumns;
    int window = image_proc_par->window;
/*      fftw_real **a, **b; */
    fftw_complex **a, **b;
    fftw_complex **A, **B;
    rfftwnd_plan p = NULL, pinv = NULL;
    fftw_real scale = 1.0 / (M * N);
    double mean_img;
    guint16 **img_l = NULL;


    if (img == NULL) {
        err_msg = "gpiv_imgproc_highpass: img == NULL";
        return (err_msg);
    }

/*         img_l = gpiv_alloc_img (image_par); */
    img_l = gpiv_matrix_guint16 (image_par->nrows, image_par->ncolumns);

/*     gpiv_warning("highpass:: 0, window = %d", window); */
    gpiv_fread_fftw_wisdom(1);
    gpiv_fread_fftw_wisdom(-1);

    a = gpiv_fftw_complex_matrix(M, N);
/* gpiv_fftw_real_matrix(M, 2 * (N / 2 + 1)); */
    A = gpiv_fftw_complex_matrix(M, N);
/* (fftw_complex *) &a[0][0]; */

/*     b = gpiv_fftw_real_matrix(M, N); */
    b = gpiv_fftw_complex_matrix(M, N);
    B = gpiv_fftw_complex_matrix(M, N);
/* gpiv_fftw_complex_matrix(M, N /2 + 1); */

    for (i = 0; i < image_par->nrows; i++) {
	for (j = 0; j < image_par->ncolumns; j++) {
	    a[i][j].re = (double) img[i][j];
	    a[i][j].im = 0.0;
	}
    }
    

/*
 * import_wisdom from string and make plans
 * FFT transform to get A and B from a and b;
 */

 /* FFTW_REAL_TO_COMPLEX */
 /*| FFTW_USE_WISDOM  | FFTW_IN_PLACE */
    p = fftw2d_create_plan(M, N, FFTW_FORWARD,
                            FFTW_MEASURE | FFTW_USE_WISDOM
                            );

/*     rfftwnd_one_real_to_complex(p, &a[0][0], &A[0][0]); */
    fftwnd_one(p, &a[0][0], &A[0][0]);
    gpiv_fwrite_fftw_wisdom(1);
    rfftwnd_destroy_plan(p);

/*     mean_img = A[0][0].re; */
    


    for (i = 0; i < M; i++) {
        for (j = 0; j < N; j++) {
            B[i][j].re = 0.0;
            B[i][j].im = 0.0;
        }
    }
    
    
    
    if (window == M/2 || window == N/2) {
        gpiv_warning("gpiv_imgproc_highpass: window >= M/2 && window >= N/2: A.re[0][0] = %f A.im[0][0] = %f ",
                     A[0][0].re, A[0][0].im);
        B[0][0].re = A[0][0].re * scale;
        B[0][0].im = A[0][0].im * scale;
    }

    if (window == N/2) {
    for (j = 0, i = M/2 - window + 1; i <= M/2; i++) {
            
/*         gpiv_warning("highpass: window >= N/2: A.re[%d][%d] = %f A.im[%d][%d] = %f " */
/*                      "=> A.re[%d][%d] = %f A.im[%d][%d] = %f",  */
/*                      i, j, A[i][j].re, i, j, A[i][j].im, */
/*                      M - i, j, A[M - i][j].re,  */
/*                      M - i, j, A[M - i][j].im); */
        
        B[i][j].re = A[i][j].re * scale;
        B[i][j].im = A[i][j].im * scale;
        B[M - i][j].re = A[M - i][j].re * scale;
        B[M - i][j].im = A[M - i][j].im * scale;
    }
    }


    if (window == M/2) {
    for (i = 0, j = N/2 - window + 1; j <= N/2; j++) {

/*         gpiv_warning("highpass: window >= M/2: A.re[%d][%d] = %f A.im[%d][%d] = %f " */
/*                      "=> A.re[%d][%d] = %f A.im[%d][%d] = %f",  */
/*                      i, j, A[i][j].re, i, j, A[i][j].im, */
/*                      i, N - j, A[i][N - j].re,  */
/*                      i, N - j, A[i][N - j].im); */
        
        B[i][j].re = A[i][j].re * scale;
        B[i][j].im = A[i][j].im * scale;
        B[i][N - j].re = A[i][N - j].re * scale;
        B[i][N - j].im = A[i][N - j].im * scale;
    }
    }


        for (i = M/2 - window; i <= M/2; i++) {
            for (j = N/2 - window; j <= N/2; j++) {
                
                if (i >= 1 && j >= 1) {
/*                     gpiv_warning("highpass: passing data: A.re[%d][%d] = %f A.im[%d][%d] = %f " */
/*                                  "=> A.re[%d][%d] = %f A.im[%d][%d] = %f",  */
/*                                  i, j, A[i][j].re, i, j, A[i][j].im, */
/*                                  M - i, N - j, A[M - i][N - j].re,  */
/*                                  M - i, N - j, A[M - i][N - j].im); */
                    
                    B[i][j].re = A[i][j].re * scale;
                    B[i][j].im = A[i][j].im * scale;
                    
                    B[M - i][j].re = A[M - i][j].re * scale;
                    B[M - i][j].im = A[M - i][j].im * scale;
                    
                    B[i][N - j].re = A[i][N - j].re * scale;
                    B[i][N - j].im = A[i][N - j].im * scale;
                    
                    B[M - i][N - j].re = A[M - i][N - j].re * scale;
                    B[M - i][N - j].im = A[M - i][N - j].im * scale;
                }
            }
        }




/*     for (i = 0; i < M; i++) { */
/*         for (j = 0; j < N; j++) { */
/*             if (B[i][j].re == 99.9 && B[i][j].im == 99.9) */
/*                 gpiv_warning("B[%d][%d].re = 99.9 ",  */
/*                              i, j);  */
/*         } */
/*     } */




/*
 * import_wisdom from string and make plans
 * inverse transform to get c, the convolution of a and b;
 * this has the side effect of overwriting C
 */
    pinv = fftw2d_create_plan(M, N, FFTW_BACKWARD/* FFTW_COMPLEX_TO_REAL */,
                              FFTW_MEASURE | FFTW_USE_WISDOM
                              );

/*     fftwnd_one_complex_to_real(pinv, &B[0][0], &b[0][0]); */
    fftwnd_one(pinv, &B[0][0], &b[0][0]);


    gpiv_fwrite_fftw_wisdom(-1);
    rfftwnd_destroy_plan(pinv);



    for (i = 0; i < image_par->nrows; i++) {
        for (j = 0; j < image_par->ncolumns; j++) {
/*             if (b[i][j].re == 0.0 || b[i][j].im == 0.0) { */
/*                 gpiv_warning("inv FT image:: b[%d][%d].re=%f b[%d][%d].im=%f", */
/*                              i, j, b[i][j].re, i, j, b[i][j].im); */
/*             } */
            img_l[i][j] = (guint16) b[i][j].re;
        }
    }






    gpiv_cp_img_data (img_l, img);
    gpiv_free_img (img_l); 
/*     gpiv_free_fftw_real_matrix(a); */
/*     gpiv_free_fftw_real_matrix(b); */
    gpiv_free_fftw_complex_matrix(a);
    gpiv_free_fftw_complex_matrix(b);
    gpiv_free_fftw_complex_matrix(B);
}



gchar *
gpiv_imgproc_correlate (GpivImagePar image_par, 
                        GpivImageProcPar image_proc_par,
                        guint16 **img1_in, 
                        guint16 **img2_in, 
                        guint16* **img_out
                        )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Correlates two images
 *
 * INPUTS:
 *     image_par:             structure of image parameters
 *     image_proc_par:        structure of image processing parameters
 *     img_in1:               first input image. Needs to be allocated
 *     img_in2:               second input image. Needs to be allocated
 *
 * OUTPUTS:
 *     img_out:               pointer to output image. Not necessarily allocated
 *
 * RETURNS:
 *     NULL on success or *err_msg on failure
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    int i, j, ij;
    int M = image_par.nrows, N = image_par.ncolumns;
    guint16 **img = NULL;

    fftw_real **a, **b, **c;
    fftw_complex *A, *B, **C;
    rfftwnd_plan p, pinv;
    fftw_real scale = 1.0 / (M * N);

    if (img1_in == NULL) {
        err_msg = "gpiv_imgproc_correlate: img1_in == NULL";
        return (err_msg);
    }

    if (img2_in == NULL) {
        err_msg = "gpiv_imgproc_correlate: img2_in == NULL";
        return (err_msg);
    }

    if (img_out[0] == NULL) {
/*         img = gpiv_alloc_img (image_par); */
        img = gpiv_matrix_guint16 (image_par.nrows, image_par.ncolumns);
        *img_out = img;
    }

    gpiv_fread_fftw_wisdom(1);
    gpiv_fread_fftw_wisdom(-1);

    a = gpiv_fftw_real_matrix(M, 2 * (N / 2 + 1));
    A = (fftw_complex *) &a[0][0];
    b = gpiv_fftw_real_matrix(M, 2 * (N / 2 + 1));
    B = (fftw_complex *) &b[0][0];
    c = gpiv_fftw_real_matrix(M, N);
    C = gpiv_fftw_complex_matrix(M, N /2 + 1);

    for (i = 0; i < M; i++) {
	for (j = 0; j < N; j++) {
	    a[i][j] = (double) img1_in[i][j];
	    b[i][j] = (double) img2_in[i][j];
	}
    }
    

/*
 * import_wisdom from string and make plans
 * FFT transform to get A and B from a and b;
 */
    p = rfftw2d_create_plan(M, N, FFTW_REAL_TO_COMPLEX,
                            FFTW_MEASURE | FFTW_USE_WISDOM | FFTW_IN_PLACE
                            );

    rfftwnd_one_real_to_complex(p, &a[0][0], NULL);
    rfftwnd_one_real_to_complex(p, &b[0][0], NULL);


    gpiv_fwrite_fftw_wisdom(1);
    rfftwnd_destroy_plan(p);


/*
 * Correlates FFT(b) = B with FFT(a) = A to C following
 * B * conjugate(A) result in correct sign of displacements!
 */
    for (i = 0; i < M; i++) {
	for (j = 0; j < N / 2 + 1; j++) {
	    ij = i * (N / 2 + 1) + j;
	    C[i][j].re = (B[ij].re * A[ij].re + B[ij].im * A[ij].im) * scale;
	    C[i][j].im = (B[ij].im * A[ij].re - B[ij].re * A[ij].im) * scale;
	}
    }


/*
 * import_wisdom from string and make plans
 * inverse transform to get c, the convolution of a and b;
 * this has the side effect of overwriting C
 */
    pinv = rfftw2d_create_plan(M, N, FFTW_COMPLEX_TO_REAL,
                               FFTW_MEASURE| FFTW_USE_WISDOM );

    rfftwnd_one_complex_to_real(pinv, &C[0][0], &c[0][0]);

    gpiv_fwrite_fftw_wisdom(-1);
    rfftwnd_destroy_plan(pinv);

    for (i = 0; i < M; i++) {
        for (j = 0; j < N; j++) {
            *img_out[i][j] = /* (char) */ (guint16) c[i][j];
        }
    }


    gpiv_free_fftw_real_matrix(a);
    gpiv_free_fftw_real_matrix(b);
    gpiv_free_fftw_real_matrix(c);
    gpiv_free_fftw_complex_matrix(C);
    return (err_msg);
}


gchar *
gpiv_imgproc_convolve (GpivImagePar image_par, 
                       GpivImageProcPar image_proc_par,
                       guint16 **img1_in, 
                       guint16 **img2_in, 
                       guint16* **img_out
                       )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Convolves two images
 *
 * INPUTS:
 *     image_par:             structure of image parameters
 *     image_proc_par:        structure of image processing parameters
 *     img_in1:               first input image. Needs to be allocated
 *     img_in2:               second input image. Needs to be allocated
 *
 * OUTPUTS:
 *     img_out:               pointer to output image. Not necessarily allocated
 *
 * RETURNS:
 *     NULL on success or *err_msg on failure
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    int i, j, ij;
    int M = image_par.nrows, N = image_par.ncolumns;
    guint16 **img = NULL;

    fftw_real **a, **b, **c;
    fftw_complex *A, *B, **C;
    rfftwnd_plan p, pinv;
    fftw_real scale = 1.0 / (M * N);

    if (img1_in == NULL) {
        err_msg = "gpiv_imgproc_convolve: img1_in == NULL";
        return (err_msg);
    }
    if (img2_in == NULL) {
        err_msg = "gpiv_imgproc_convolve: img2_in == NULL";
        return (err_msg);
    }
    if (img_out[0] == NULL) {
/*         img = gpiv_alloc_img (image_par); */
        img = gpiv_matrix_guint16 (image_par.nrows, image_par.ncolumns);
        *img_out = img;
    }

    gpiv_fread_fftw_wisdom(1);
    gpiv_fread_fftw_wisdom(-1);

    a = gpiv_fftw_real_matrix(M, 2 * (N / 2 + 1));
    A = (fftw_complex *) &a[0][0];
    b = gpiv_fftw_real_matrix(M, 2 * (N / 2 + 1));
    B = (fftw_complex *) &b[0][0];
    c = gpiv_fftw_real_matrix(M, N);
    C = gpiv_fftw_complex_matrix(M, N /2 + 1);

    for (i = 0; i < M; i++) {
	for (j = 0; j < N; j++) {
	    a[i][j] = (double) img1_in[i][j];
	    b[i][j] = (double) img2_in[i][j];
	}
    }
    

/*
 * import_wisdom from string and make plans
 * FFT transform to get A and B from a and b;
 */
    p = rfftw2d_create_plan(M, N, FFTW_REAL_TO_COMPLEX,
                            FFTW_MEASURE | FFTW_USE_WISDOM | FFTW_IN_PLACE
                            );

    rfftwnd_one_real_to_complex(p, &a[0][0], NULL);
    rfftwnd_one_real_to_complex(p, &b[0][0], NULL);


    gpiv_fwrite_fftw_wisdom(1);
    rfftwnd_destroy_plan(p);


/*
 * Convolves  FFT(a) = A with FFT(b) = B to C following
 * FFT(a) * FFT(b) = C
 */
    for (i = 0; i < M; i++) {
        for (j = 0; j < N/2+1; j++) {
            ij = i*(N/2+1) + j;
            C[i][j].re = (A[ij].re * B[ij].re
                          - A[ij].im * B[ij].im) * scale;
            C[i][j].im = (A[ij].re * B[ij].im
                          + A[ij].im * B[ij].re) * scale;
        }
    }


/*
 * import_wisdom from string and make plans
 * inverse transform to get c, the convolution of a and b;
 * this has the side effect of overwriting C
 */
    pinv = rfftw2d_create_plan(M, N, FFTW_COMPLEX_TO_REAL,
                               FFTW_MEASURE| FFTW_USE_WISDOM );

    rfftwnd_one_complex_to_real(pinv, &C[0][0], &c[0][0]);

    gpiv_fwrite_fftw_wisdom(-1);
    rfftwnd_destroy_plan(pinv);

    for (i = 0; i < M; i++) {
        for (j = 0; j < N; j++) {
            *img_out[i][j] = /* (char) */ (guint16) c[i][j];
        }
    }


    gpiv_free_fftw_real_matrix(a);
    gpiv_free_fftw_real_matrix(b);
    gpiv_free_fftw_real_matrix(c);
    gpiv_free_fftw_complex_matrix(C);
    return (err_msg);
}

#endif /* USE_FFTW3 */


/*
 * Local functions
 */
static void
smooth_frame (guint16 **frame_src,
              guint16 **frame_dest,
              const GpivImagePar *image_par,
              const GpivImageProcPar *image_proc_par
              )
/*-----------------------------------------------------------------------------
 * Smooths individual image frame
 */
{
    long int img_sum = 0;
    float img_mean = 0.0;
    int i, j, k, l, count = 0;
    int window = image_proc_par->window;


    for (i = 0; i < image_par->nrows; i++) {
        for (j = 0; j < image_par->ncolumns; j++) {
            img_mean = 0.0;
            img_sum = 0;
            count = 0;

            for (k = -(window - 1) / 2; k <= (window - 1) / 2; k++) {
                if ((i+k >= 0) && (i+k < image_par->nrows)) {
                    for (l = -(window - 1) / 2; l <= (window - 1) / 2; l++) {
                        if ((j+l >= 0) && (j+l < image_par->ncolumns)) {
                            img_sum += (long) frame_src[i+k][j+l];
                            count++;
                        }
                    }
                }
            }

            img_mean = (float)img_sum / (float)count;

            if (image_proc_par->smooth_operator == GPIV_IMGOP_EQUAL) {
                frame_dest[i][j] = img_mean;
            } else if (image_proc_par->smooth_operator == GPIV_IMGOP_SUBTRACT) {
                if (frame_src[i][j] >= img_mean) {
                    frame_src[i][j] = frame_src[i][j] - img_mean;
                } else {
                    frame_src[i][j] = 0;
                }
            } else if (image_proc_par->smooth_operator == GPIV_IMGOP_ADD) {
                frame_dest[i][j] = frame_src[i][j] + img_mean;
            } else if (image_proc_par->smooth_operator == GPIV_IMGOP_MULTIPLY) {
                frame_dest[i][j] = frame_src[i][j] * img_mean;
            } else if (image_proc_par->smooth_operator == GPIV_IMGOP_DIVIDE) {
                frame_dest[i][j] = frame_src[i][j] / img_mean;
            } else {
                gpiv_error("LIBGPIV internal error: smooth_frame: unvalid smooth_operator %d", 
                           image_proc_par->smooth_operator);
            }
        }
    }
}


static void
cp_img_data_frame (guint16 **img_src, 
                   guint16 **img_dest,
                   const GpivImagePar *image_par
                   )
/*-----------------------------------------------------------------------------
 */
{
    guint i, j;


    if (img_src == NULL) {
        gpiv_warning ("LIBGPIV internal warning: cp_img_data_frame: img_src not allocated");
       return;
    }

    if (img_dest[0] == NULL) {
        gpiv_warning ("LIBGPIV internal warning: cp_img_data_frame: img_dest not allocated");
       return;
    }


    for (i = 0; i < image_par->nrows; i++) {
        for (j = 0; j < image_par->ncolumns; j++) {
            img_dest[i][j] = img_src[i][j];
        }
    }
}


static gchar *
imgproc_clip_frame (const GpivImagePar *image_par,
                    const GpivImageProcPar *image_proc_par,
                    guint16 **img
                   )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Sets all pixel values lower than threshold to zero
 *
 * INPUTS:
 *     image_par:             structure of image parameters
 *     image_proc_par:        structure of image processing parameters
 *     img:                   input image. Needs to be allocated
 *
 * OUTPUTS:
 *     img:                   modified input image
 *
 * RETURNS:
 *     NULL on success or *err_msg on failure
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    guint i, j;
    guint nrows = image_par->nrows;
    guint ncolumns = image_par->ncolumns;
    guint threshold = image_proc_par->threshold;
    guint16 **img_l = NULL;
    
    if (img == NULL) {
        err_msg = "LIBGPIV internal warning: imgproc_clip_frame: img == NULL";
        return (err_msg);
    }

    img_l = gpiv_matrix_guint16 (image_par->nrows, image_par->ncolumns);
    for (i = 0; i < nrows; i++) {
        for (j = 0; j < ncolumns; j++) {
            if (img[i][j] <= threshold) {
                img_l[i][j] = 0;
            } else {
                img_l[i][j] = img[i][j];
            }
#ifdef DEBUG
            g_message ("imgproc_clip_frame:: A img[%d][%d] = %d, threshold = %d img_l[%d][%d] = %d",
                       i, j, img[i][j], threshold, i, j, img_l[i][j]);

#endif
        }
    }


    cp_img_data_frame (img_l, img, image_par);
    gpiv_free_matrix_guint16 (img_l); 
    return (err_msg);
}



static gchar *
imgproc_getbit_frame (const GpivImagePar *image_par,
                      const GpivImageProcPar *image_proc_par,
                      guint16 **img
                      )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Pointer operation to get the N least significant bits and moves them to 
 *     most the significant bits
 *
 * INPUTS:
 *     image_par:             structure of image parameters
 *     image_proc_par:        structure of image processing parameters
 *     img:                   input image. Needs to be allocated
 *
 * OUTPUTS:
 *     img:                   modified input image
 *
 * RETURNS:
 *     NULL on success or *err_msg on failure
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    guint i, j;
    guint nrows = image_par->nrows;
    guint ncolumns = image_par->ncolumns;
    guint depth = image_par->depth/*  = GPIV_MAX_IMG_DEPTH */;
    guint bit = image_proc_par->bit;
    unsigned char mask = ~0;
    guint16 **img_l = NULL;

    if (img == NULL) {
        err_msg = "LIBGPIV internal warning: imgproc_getbit_frame: img == NULL";
        return (err_msg);
    }

    img_l = gpiv_matrix_guint16 (image_par->nrows, image_par->ncolumns);
    mask = (mask & ~0) >> (depth - bit);
/*     gpiv_warning("getbit:: bit = %d depth = %d ==> mask = %X",  */
/*                  bit, depth, mask); */
    for (i = 0; i < nrows; i++) {
        for (j = 0; j < ncolumns; j++) {
            img_l[i][j] = (img[i][j] & mask);
            img_l[i][j] = img[i][j] << (depth - bit);
        }
    }

    cp_img_data_frame (img_l, img, image_par);
    gpiv_free_matrix_guint16 (img_l); 
    return (err_msg);
}


/* char * */
/* gpiv_imgproc_invfft(GpivImagePar image_par, */
/*                GpivImageProcPar image_proc_par, */
/*                guint16 **img_in,  */
/*                guint16 **img_out) */
/* 
 * Inverse Fast Fourier Transformation
 */
/* { */
/*     char *err_msg = NULL; */
/*     gpiv_fread_fftw_wisdom(-1); */


/*     pinv = fftw_plan_dft_r2c_2d(M, N, FFTW_COMPLEX_TO_REAL, */
/* 			       FFTW_MEASURE | FFTW_USE_WISDOM); */



/*     gpiv_fwrite_fftw_wisdom(-1); */
/* } */


static gchar *
imgproc_highlow_frame (const GpivImagePar *image_par,
                       const GpivImageProcPar *image_proc_par,
                       guint16 **img
                      )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     High-low filter to maximize contrast by stretching pixel values
 *     to local max and min within window x window area
 *
 * INPUTS:
 *     image_par:             structure of image parameters
 *     image_proc_par:        structure of image processing parameters
 *     img:                   input image. Needs to be allocated
 *
 * OUTPUTS:
 *     img:                   modified input image
 *
 * RETURNS:
 *     NULL on success or *err_msg on failure
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    int i, j, k, l, img_top = (1 << image_par->depth) - 1 /* (img_top << image_par->depth) - 1 */ ;
    guint16 img_max = 0, img_min = 0 /* (1 << image_par->depth) */;
    guint16 **max, **min, **max_smooth, **min_smooth;
    float denomerator, factor;
    guint16 **img_l = NULL;


#ifdef DEBUG
    g_message ("imgproc_highlow_frame:: img_min = %d img_top = %d", 
               img_min, img_top);
#endif

    if (img == NULL) {
        err_msg = "LIBGPIV internal warning: imgproc_highlow_frame: img == NULL";
        return (err_msg);
    }

    img_l = gpiv_matrix_guint16 (image_par->nrows, image_par->ncolumns);


    max = gpiv_matrix_guint16 (image_par->nrows, image_par->ncolumns);
    min = gpiv_matrix_guint16 (image_par->nrows, image_par->ncolumns);
/*     max_smooth = gpiv_matrix_guint16(image_par->nrows, image_par->ncolumns); */
/*     min_smooth = gpiv_matrix_guint16(image_par->nrows, image_par->ncolumns); */



/*
 * Calculate local min and max for each pixel within window
 */

    for (i = 0; i < image_par->nrows; i++) {
        for (j = 0; j < image_par->ncolumns; j++) {
            img_max = 0; 
            img_min = img_top;
            for (k = -(image_proc_par->window-1)/2; k <= (image_proc_par->window-1)/2; k++) {
                if ((i+k >= 0) && (i+k < image_par->nrows)) {
                    for (l = -(image_proc_par->window-1)/2; l <= (image_proc_par->window-1)/2; l++) {
                        if ((j+l >= 0) && (j+l < image_par->ncolumns)) {
                            
                            if (img[i+k][j+l] > img_max) {
                                img_max = img[i+k][j+l];
                            }
                            
                            if (img[i+k][j+l] < img_min) {
                                img_min = img[i+k][j+l];
                            }
                        }
                    }
                }
            }
            max[i][j] = img_max;
            min[i][j] = img_min;
            img_max = 0; 
            img_min = img_top;
        }
    }



/*
 * Spatial interpolation/smoothing of min and max
 */

/*     image_proc_par->smooth_operator = GPIV_IMGOP_EQUAL; */
/*     gpiv_imgproc_smooth(image_par, image_proc_par, max); */
/*     gpiv_imgproc_smooth(image_par, image_proc_par, min); */
    max_smooth = max;
    min_smooth = min;

/*
 * Subtracting min_smooth from each pixel value (background), stretching to 
 * max_smooth to top image value
 */

    for (i = 0; i < image_par->nrows; i++) {
        for (j = 0; j < image_par->ncolumns; j++) {
/*             denomerator = img_top - ((int) max_smooth[i][j]  */
/*                                      - (int) min_smooth[i][j]); */
            denomerator = (int) max_smooth[i][j] - (int) min_smooth[i][j];
            if (denomerator == 0) {
                /* 	l_img[i][j] = max_smooth[i][j]; */
                img_l[i][j] = img[i][j];
            } else {
                factor = (float) img_top/(float) denomerator;
                img_l[i][j] = (img[i][j] - min_smooth[i][j]) * factor;
            }
        }
    }


    cp_img_data_frame (img_l, img, image_par);
    gpiv_free_matrix_guint16 (img_l); 
    gpiv_free_matrix_guint16 (max);
    gpiv_free_matrix_guint16 (min); 
    return (err_msg);
}


static gchar *
imgproc_fft_frame (const GpivImagePar *image_par, 
                   const GpivImageProcPar *image_proc_par,
                   guint16 **img 
                  )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Fast Fourier Transformation
 *
 * INPUTS:
 *     image_par:             structure of image parameters
 *     image_proc_par:        structure of image processing parameters
 *     img:                   input image. Needs to be allocated
 *
 * OUTPUTS:
 *     img:                   modified input image
 *
 * RETURNS:
 *     NULL on success or *err_msg on failure
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
/*     int img_size_0 = ZEROPAD_FACT  */
/*         * gpiv_max(image_par->nrows, image_par->ncolumns); */
    int i, j, ij;
    int M = image_par->nrows, N = image_par->ncolumns;
    guint16 **img_l = NULL;
    
    double **a, **b;
    fftw_complex *A, **B;
    fftw_plan plan, plan_inv;
    double scale = 1.0 / (M * N);

    g_return_val_if_fail (img[0] != NULL, "LIBGPIV internal warning: imgproc_fft_frame: img not allocated");

    img_l = gpiv_matrix_guint16 (image_par->nrows, image_par->ncolumns);

    a = gpiv_double_matrix(M, 2 * (N / 2 + 1));
    A = (fftw_complex *) &a[0][0];

    b = gpiv_double_matrix(M, N);
    B = gpiv_fftw_complex_matrix(M, N /2 + 1);

    for (i = 0; i < M; i++) {
	for (j = 0; j < N; j++) {
	    a[i][j] = (double) img[i][j];
	}
    }
    

/*
 * import_wisdom from string and make plans
 * FFT transform to get A and B from a and b;
 */
    gpiv_fread_fftw_wisdom(1);
    gpiv_fread_fftw_wisdom(-1);
    plan = fftw_plan_dft_r2c_2d(M, N, (double *) &a[0][0], 
                                (fftw_complex *) &a[0][0], 
                                FFTW_MEASURE);
    fftw_execute(plan);
    gpiv_fwrite_fftw_wisdom(1);
    fftw_destroy_plan(plan);


    for (i = 0; i < M; i++) {
        for (j = 0; j < N/2+1; j++) {
            ij = i*(N/2+1) + j;
            B[i][j][0] = (A[ij][0]) * scale;
            B[i][j][1] = (A[ij][1]) * scale;
        }
    }
/*
 * import_wisdom from string and make plans
 * inverse transform to get c, the convolution of a and b;
 * this has the side effect of overwriting C
 */
    plan_inv = fftw_plan_dft_c2r_2d(M, N, (fftw_complex *) &B[0][0], 
                                (double *) &b[0][0],
                                FFTW_MEASURE);
    fftw_execute(plan_inv);
    gpiv_fwrite_fftw_wisdom(-1);
    fftw_destroy_plan(plan_inv);

    for (i = 0; i < M; i++) {
        for (j = 0; j < N; j++) {
            img_l[i][j] = (guint16) b[i][j];
        }
    }


    cp_img_data_frame (img_l, img, image_par);
    gpiv_free_matrix_guint16 (img_l); 
    gpiv_free_double_matrix(a);
    gpiv_free_double_matrix(b);
    gpiv_free_fftw_complex_matrix(B);
    A = NULL;
}


