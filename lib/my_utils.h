/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2008 Gerber van der Graaf

   This file is part of libgpiv.

   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  




-------------------------------------------------------------------------------
FILENAME:               my_utils.h
LIBRARY:                libgpiv
LAST MODIFICATION DATE: $Id: my_utils.h,v 1.1 2008-09-25 13:19:53 gerber Exp $

*/

FILE *
my_utils_fopen_tmp (const gchar *tmpfile,
		    const gchar *mode
		    );

gchar *
my_utils_write_tmp_image (const GpivImage *image, 
			  const gchar *basename,
			  const gchar *message
			  );

