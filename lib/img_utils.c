/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008
   Gerber van der Graaf

   This file is part of libgpiv.
   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  



-------------------------------------------------------------------------------
FILENAME:                img_utils.c
LIBRARY:                 libgpiv:
EXTERNAL FUNCTIONS:
                         gpiv_alloc_img
                         gpiv_check_alloc_image
                         gpiv_free_img
                         gpiv_cp_img
                         gpiv_cp_img_data

                         piv_mpi_bcast_image

LAST MODIFICATION DATE:  $Id: img_utils.c,v 1.3 2008-09-25 13:19:53 gerber Exp $
 --------------------------------------------------------------------------- */

#include <string.h>
#include <gpiv.h>
#include "my_utils.h"

/*
 * Public functions
 */

void
gpiv_alloc_imgframe(GpivImage *image)
/*-----------------------------------------------------------------------------
 * Allocates GpivImage frame data
 */
{    image->frame1 = gpiv_matrix_guint16 (image->header->nrows, 
                                          image->header->ncolumns);
    if (image->header->x_corr) {
        image->frame2 = gpiv_matrix_guint16 (image->header->nrows, 
                                             image->header->ncolumns);
    } else {
        image->frame2 = image->frame1;
    }
}


GpivImage *
gpiv_alloc_img (const GpivImagePar *image_par
                )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Allocates memory for GpivImage frames
 *
 * INPUTS:
 *      image_par:      image parameters
 *
 * OUTPUTS:
 *
 * RETURNS:
 *      img:            2-dim image data arry
 *---------------------------------------------------------------------------*/
{
    GpivImage *image = g_new0 (GpivImage, 1);;
    GpivImagePar *image_par_dest = NULL;


    image_par_dest = gpiv_img_cp_parameters (image_par);
    image->header = image_par_dest;

    gpiv_alloc_imgframe (image);


    return image;
}



gchar *
gpiv_check_alloc_img (const GpivImage *image
                      )
/*-----------------------------------------------------------------------------
 * Check if gpiv_image frames have been allocated
 */
{
    gchar *err_msg = NULL;
    guint x;

    g_return_val_if_fail (image->frame1 != NULL, 
                          "gpiv_check_alloc_image: frame1 != NULL");
    g_return_val_if_fail (image->frame2 != NULL, 
                          "gpiv_check_alloc_image: frame2 != NULL");

/*     g_message ("gpiv_check_alloc_image: sizeof frame = %d",  */
/*                sizeof (image->frame1)); */
/*     g_message ("gpiv_check_alloc_image: sizeof guint16 x nrows x ncolumns = %d x %d x %d = %d",  */
/*                sizeof (guint16),  */
/*                image->header->nrows,  */
/*                image->header->ncolumns, */
/*                (sizeof (guint16)) * image->header->nrows * image->header->ncolumns */
/*                ); */

    return err_msg;
}


GpivImage *
gpiv_cp_img (const GpivImage *image
             )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Duplicates img. The returned image will have to be freed with 
 *      gpiv_free_img when no longer needed.
 *
 * INPUTS:
 *      image_par:      image parameters
 *      img_src:            2-dim image data arry
 *
 * OUTPUTS:
 *      img_dest:            2-dim image data arry
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    gint i, j;
    GpivImage *image_dest = NULL;


    if (image->frame1[0] == NULL) {
        gpiv_warning ("gpiv_cp_img: image->frame1 not allocated");
        return NULL;
    }

    if (image->header->x_corr__set == FALSE) {
        gpiv_warning ("gpiv_cp_img: x_corr not set");
        return NULL;
    }

    if (image->header->x_corr) {
        if (image->frame2[0] == NULL) {
            gpiv_warning ("gpiv_cp_img: x_corr && image->frame2 not allocated");
            return NULL;
        }
    }


    image_dest = gpiv_alloc_img (image->header);


    for (i = 0; i < image_dest->header->nrows; i++) {
        for (j = 0; j < image_dest->header->ncolumns; j++) {
            image_dest->frame1[i][j] = image->frame1[i][j];
        }
    }

    if (image->header->x_corr) {
        for (i = 0; i < image_dest->header->nrows; i++) {
            for (j = 0; j < image_dest->header->ncolumns; j++) {
                image_dest->frame2[i][j] = image->frame2[i][j];
            }
        }
    } else {
        image_dest->frame2[0] = image->frame1[0];
    }


    return image_dest;
}



gchar *
gpiv_cp_img_data (const GpivImage *image_src, 
                  GpivImage *image_dest
                  )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Copies contents of image_src to image_dest. image_src and image_dest
 *      will have to be allocated with gpiv_alloc_img before and will have to
 *      be freed with gpiv_free_img when no longer needed.
 *
 * INPUTS:
 *      image_par:      image parameters
 *      image_src:            2-dim image data arry
 *
 * OUTPUTS:
 *      image_dest:            2-dim image data arry
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    gint i, j;


    if (image_src->frame1[0] == NULL) {
       err_msg = "gpiv_cp_img_data: image_src not allocated";
       gpiv_warning ("%s", err_msg);
       return err_msg;
    }

    if (image_dest->frame1[0] == NULL) {
        err_msg = "gpiv_cp_img_data: image_dest not allocated";
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }

    if (image_src->header->x_corr__set == FALSE) {
        err_msg = "gpiv_cp_img_data: image_src: x_corr not set";
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }

    if (image_dest->header->x_corr__set == FALSE) {
        err_msg = "gpiv_cp_img_data: image_dest: x_corr not set";
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }

    if ((image_src->header->x_corr == FALSE
         && image_dest->header->x_corr == TRUE)
        || (image_src->header->x_corr == TRUE
            && image_dest->header->x_corr == FALSE)) {
        err_msg = "gpiv_cp_img_data: image_src and image_dest: x_corr unequal";
        gpiv_warning ("%s", err_msg);
        return err_msg;
   }

    if (image_src->header->nrows != image_dest->header->nrows) {
        gpiv_warning ("gpiv_cp_img_data: unequal dimensions: image_src->header->nrows = %d image_dest->header->nrows = %d",
                      image_src->header->nrows,
                      image_dest->header->nrows);
        err_msg = "gpiv_cp_img_data: unequal dimensions of image_src and image_dest";
        return err_msg;
    }

    if (image_src->header->ncolumns != image_dest->header->ncolumns) {
        gpiv_warning ("gpiv_cp_img_data: unequal dimensions: image_src->header->ncolumns = %d image_dest->header->ncolumns = %d",
                      image_src->header->ncolumns,
                      image_dest->header->ncolumns);
        err_msg = "gpiv_cp_img_data: unequal dimensions of image_src and image_dest";
        return err_msg;
    }


    if (image_src->header->x_corr) {
        if (image_src->frame2[0] == NULL) {
            err_msg = "gpiv_cp_img_data: x_corr && image_src->frame2 not allocated";
            gpiv_warning ("%s", err_msg);
            return err_msg;
        }

        if (image_dest->frame2[0] == NULL) {
            err_msg = "gpiv_cp_img_data: x_corr && image_dest->frame2 not allocated";
            gpiv_warning ("%s", err_msg);
            return err_msg;
        }
    }

    

    for (i = 0; i < image_src->header->nrows; i++) {
        for (j = 0; j < image_src->header->ncolumns; j++) {
#ifdef DEBUG2
	  g_message ("gpiv_cp_img_data:: frame1 i = %d j = %d", i, j);
#endif
            image_dest->frame1[i][j] = image_src->frame1[i][j];
        }
    }

    if (image_src->header->x_corr) {
        for (i = 0; i < image_src->header->nrows; i++) {
            for (j = 0; j < image_src->header->ncolumns; j++) {
#ifdef DEBUG2
	  g_message ("gpiv_cp_img_data:: frame2 i = %d j = %d", i, j);
#endif
                image_dest->frame2[i][j] = image_src->frame2[i][j];
            }
        }
    }


    return NULL;
}



void 
gpiv_free_img (GpivImage *image
               )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Frees memory for image
 *
 * INPUTS:
 *      image_par:      image parameters
 *      img:            2-dim image data arry
 *
 * OUTPUTS:
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    g_return_if_fail (image->frame1[0] != NULL); /* gpiv_error ("img not allocated"); */

    gpiv_free_matrix_guint16 (image->frame1);
    image->frame1 = NULL;
 
   if (image->header->x_corr == TRUE) {
        g_return_if_fail (image->frame2[0] != NULL); /* gpiv_error ("img not allocated"); */
        gpiv_free_matrix_guint16 (image->frame2);
        image->frame2 = NULL;
    }
}


#ifdef ENABLE_MPI
void
gpiv_img_mpi_bcast_image (GpivImage *image,
                          const gboolean alloc_frame
                      )
/*-----------------------------------------------------------------------------
 * Broadcasts image 
 */
{
    int rank = 0;


    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    gpiv_img_mpi_bcast_imgpar (image->header);
    if (alloc_frame && rank != 0) gpiv_alloc_imgframe (image);
    gpiv_img_mpi_bcast_imgframe (image);

#ifdef DEBUG
    my_utils_write_tmp_image (image, 
                              g_strdup_printf ("%s%d",  "bcast_rank", rank),
                              "Writing mpi-bcasted image to: ")
#endif
}


void
gpiv_img_mpi_bcast_imgframe (GpivImage *image
                             )
/*-----------------------------------------------------------------------------
 * Broadcasts image frame data
 */
{
    guint size  = image->header->ncolumns * image->header->nrows;

    
    if (MPI_Bcast(*image->frame1, size, MPI_SHORT, 0, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        ) {
        gpiv_error("@&%#$!, mpi_bcast_img_frame: An error ocurred in MPI_Bcast");
    }

    if (image->header->x_corr) {
        if (MPI_Bcast(*image->frame2, size, MPI_SHORT, 0, MPI_COMM_WORLD) 
            != MPI_SUCCESS) {
            gpiv_error("@&%#$!, mpi_bcast_img_frame: An error ocurred in MPI_Bcast");
        }
    }
}


#endif /* ENABLE_MPI */
