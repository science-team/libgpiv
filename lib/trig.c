
/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2004, 2005, 2006, 2007, 2008
   Gerber van der Graaf <gerber_graaf@users.sourceforge.net>
   Julio Soria <julio.soria@eng.monash.edu.au>

   This file is part of libgpiv.

   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  



------------------------------------------------------------------------------
FILENAME:               trig.c
LIBRARY:                libgpiv
EXTERNAL FUNCTIONS:      
                        gpiv_trig_openrtfs
                        

LAST MODIFICATION DATE:  $Id: trig.c,v 1.2 2007-11-23 16:16:15 gerber Exp $
--------------------------------------------------------------------------- */


#ifdef ENABLE_TRIG
#include "gpiv.h"



gint 
gpiv_trig_openrtfs (gint *init, 
                    gint *trig, 
                    gint *stop, 
                    gint *error
                    )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Opens communication channels to camlasco-rtl kernel module
 *
 * INPUTS:
 *     init:            initialization or uploading of trigger parameters
 *     trig:            ?
 *     stop:            stop signal
 *     error:           error signal
 *
 * OUTPUTS:
 *     piv_trig_par_default:     structure of data aquisition parameters
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    gint ok = 0;

    if((*init = open("/dev/rtf/1", O_WRONLY )) < 0)  {
        printf("fail in \"init\" FIFO open\n");
        return ok;
    }

    if((*trig = open("/dev/rtf/2", O_WRONLY )) < 0)  {
        printf("fail in \"trig\" FIFO open\n");
        return ok;
    }

    if((*stop = open("/dev/rtf/3", O_WRONLY )) < 0)  {
        printf("fail in \"stop\" FIFO open\n");
        return ok;
    }

    if((*error = open("/dev/rtf/4", O_RDONLY )) < 0) {
        printf("fail in \"error\" FIFO open\n");
        return ok;
    }

    ok = 1;
    return ok;
}



#endif /* ENABLE_TRIG */
