/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008
   Gerber van der Graaf

   This file is part of libgpiv.
   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  



-----------------------------------------------------------------------
FILENAME:               post.c
LIBRARY:                libgpiv


EXTERNAL FUNCTIONS:
                        gpiv_post_subtract_dxdy
			gpiv_post_manipiv 
                        gpiv_post_savg
                        gpiv_post_uvhisto
                        gpiv_post_scale
                        gpiv_post_inverse_scale
                        gpiv_post_vorstra


LAST MODIFICATION DATE: $Id: post.c,v 1.20 2007-12-19 08:46:46 gerber Exp $
 ------------------------------------------------------------------- */

#include <stdio.h> 
#include <string.h>
#include <math.h>

#include <gpiv.h>


/* 
 * Local functions
 */

static gchar * 
flip (GpivPivData *piv_data,
      const GpivPostPar *piv_post_par
      )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *    Flips piv data in x-and/or y direction in-place
 *
 * PROTOTYPE LOCATATION:
 *     post.h
 *
 * INPUTS:
 *
 * OUTPUTS:
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    GpivPivData *lo_data = NULL;
    guint i, j;
    

    if ((err_msg = gpiv_check_alloc_pivdata (piv_data)) != NULL) {
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }

    lo_data = gpiv_alloc_pivdata (piv_data->nx, piv_data->ny);


    switch (piv_post_par->operator_manipiv) {
    case GPIV_FLIP_X:
        for (i=0; i < piv_data->ny; i++) {
            for (j=0; j < piv_data->nx; j++) {
                lo_data->point_x[i][j] = piv_data->point_x[i][j];
                lo_data->point_y[i][j] = piv_data->point_y[i][j];
                lo_data->dx[lo_data->ny-i-1][j] = piv_data->dx[i][j];
                /*
                 * Change sign
                 */
                lo_data->dy[lo_data->ny-i-1][j] = -piv_data->dy[i][j];
                lo_data->snr[lo_data->ny-i-1][j] = piv_data->snr[i][j];
                lo_data->peak_no[lo_data->ny-i-1][j] = piv_data->peak_no[i][j];
            }
        }
        break;
        
    case GPIV_FLIP_Y:
        for (i = 0; i < piv_data->ny; i++) {
            for (j = 0; j < piv_data->nx; j++) {
                lo_data->point_x[i][j] = piv_data->point_x[i][j];
                lo_data->point_y[i][j] = piv_data->point_y[i][j];
                /*
                 * Change sign
                 */
                lo_data->dx[i][lo_data->nx-j-1] = -piv_data->dx[i][j];
                lo_data->dy[i][lo_data->nx-j-1] = piv_data->dy[i][j]; 
                lo_data->snr[i][lo_data->nx-j-1] = piv_data->snr[i][j];
                lo_data->peak_no[i][lo_data->nx-j-1] = piv_data->peak_no[i][j];
            }
        }
        break;

        /*
         * Identic to rotating over 180 degr
         */
    case GPIV_ROT180:
/*         lo_data->nx = piv_data->ny; */
/*         lo_data->ny = piv_data->nx; */
        for (i = 0; i < lo_data->ny; i++) {
            for (j = 0; j < lo_data->nx; j++) {
                lo_data->point_x[i][j] = piv_data->point_x[j][i];
                lo_data->point_y[i][j] = piv_data->point_y[j][i];
                /*
                 * Change sign
                 */
/*                 lo_data->dx[lo_data->ny-i-1][piv_data->nx-j-1] =  */
/*                     -piv_data->dx[i][j];   */
                lo_data->dx[lo_data->ny-i-1][lo_data->nx-j-1] = 
                    -piv_data->dx[i][j];  
                 /*
                 * Change sign
                 */
                lo_data->dy[lo_data->ny-i-1][lo_data->nx-j-1] = 
                    -piv_data->dy[i][j];  
                lo_data->snr[lo_data->ny-i-1][lo_data->nx-j-1] = 
                    piv_data->snr[i][j];
                lo_data->peak_no[lo_data->ny-i-1][lo_data->nx-j-1] = 
                    piv_data->peak_no[i][j];
            }
        }
        break;

    case GPIV_ROT90:
/*         lo_data->nx = piv_data->nx; */
/*         lo_data->ny = piv_data->ny; */
        for (i = 0; i < piv_data->ny; i++) {
            for (j = 0; j < piv_data->nx; j++) {
                lo_data->point_x[i][j] = piv_data->point_y[i][j];
                lo_data->point_y[i][j] = piv_data->point_x[i][j];
                /*
                 * Change sign
                 */
                lo_data->dx[i][lo_data->nx-j-1] = -piv_data->dy[i][j];
                lo_data->dy[i][lo_data->nx-j-1] = piv_data->dx[i][j];
                lo_data->snr[i][lo_data->nx-j-1] = piv_data->snr[i][j];
                lo_data->peak_no[i][lo_data->nx-j-1] = piv_data->peak_no[i][j];
                
            }
        }

    default:
        err_msg = "LIBGPIV internal error: flip: no valid operation_manipiv";
        gpiv_warning ("%s", err_msg);
        return err_msg;
        break;
    }


    if ((err_msg = gpiv_ovwrt_pivdata (lo_data, piv_data)) != NULL) {
        return err_msg;
    }
    gpiv_free_pivdata (lo_data);

    return err_msg;
}



static gchar *
revert_data_col (GpivPivData *piv_data
                 )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Interchanges column indexes to reverse the order of data in-place
 *
 * PROTOTYPE LOCATATION:
 *     post.h
 *
 * INPUTS:
 *
 * OUTPUTS:
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    GpivPivData *lo_data = NULL;
    guint i, j;
    

    if ((err_msg = gpiv_check_alloc_pivdata (piv_data)) != NULL) {
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }

    lo_data = gpiv_alloc_pivdata (piv_data->nx, piv_data->ny);

    for (i = 0; i < piv_data->ny; i++) {
        for (j = 0; j < piv_data->nx; j++) {
            lo_data->point_x[i][piv_data->nx-j-1] = 
                piv_data->point_x[i][j];
            lo_data->point_y[i][piv_data->nx-j-1] = 
                piv_data->point_y[i][j];
            lo_data->dx[i][piv_data->nx-j-1] = piv_data->dx[i][j];
            lo_data->dy[i][piv_data->nx-j-1] = piv_data->dy[i][j];
            lo_data->snr[i][piv_data->nx-j-1] = piv_data->snr[i][j];
            lo_data->peak_no[i][piv_data->nx-j-1] = 
                piv_data->peak_no[i][j];
        }
    }
    

    if ((err_msg = gpiv_ovwrt_pivdata (lo_data, piv_data)) != NULL) {
        return err_msg;
    }
    gpiv_free_pivdata (lo_data);

    return err_msg;
}



static gchar *
revert_data_row (GpivPivData *piv_data
                 )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Interchanges row indexes to reverse the order of data in-place
 *
 * PROTOTYPE LOCATATION:
 *     post.h
 *
 * INPUTS:
 *
 * OUTPUTS:
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    GpivPivData *lo_data = NULL;
    guint i, j;
    

    if ((err_msg = gpiv_check_alloc_pivdata (piv_data)) != NULL) {
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }

    lo_data = gpiv_alloc_pivdata (piv_data->nx, piv_data->ny);
    
    for (i = 0; i < piv_data->ny; i++) {
        for (j = 0; j < piv_data->nx; j++) {
            lo_data->point_x[piv_data->ny-i-1][j] = 
                piv_data->point_x[i][j];
            lo_data->point_y[piv_data->ny-i-1][j] = 
                piv_data->point_y[i][j];
            lo_data->dx[piv_data->ny-i-1][j] = piv_data->dx[i][j];
            lo_data->dy[piv_data->ny-i-1][j] = piv_data->dy[i][j];
            lo_data->snr[piv_data->ny-i-1][j] = piv_data->snr[i][j];
            lo_data->peak_no[piv_data->ny-i-1][j] = 
                piv_data->peak_no[i][j];
        }
    }
    

    if ((err_msg = gpiv_ovwrt_pivdata (lo_data, piv_data)) != NULL) {
        return err_msg;
    }
    gpiv_free_pivdata (lo_data);

    return err_msg;
}



static gchar *
revert_data (GpivPivData *piv_data
             )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Interchanges array indexes to reverse the order of data in-place
 *
 * PROTOTYPE LOCATATION:
 *     post.h
 *
 * INPUTS:
 *
 * OUTPUTS:
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    

    if ((err_msg = revert_data_col (piv_data)) != NULL) {
        g_message ("revert_data: %s", err_msg);
        return err_msg;
    }

    if ((err_msg = revert_data_row (piv_data)) != NULL) {
        g_message ("revert_data: %s", err_msg);
        return err_msg;
    }


    return err_msg;
}



static gchar *
fasty (GpivPivData *piv_data
       )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     BUGFIX: fasty: repair or remove! Doesn't anything
 *     Copies input to output data; writes with normal gpiv write function
 *
 * PROTOTYPE LOCATATION:
 *     post.h
 *
 * INPUTS:
 *
 * OUTPUTS:
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    GpivPivData *lo_data;
    guint i, j;
    

    if ((err_msg = gpiv_check_alloc_pivdata (piv_data)) != NULL) {
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }

    lo_data = gpiv_alloc_pivdata (piv_data->nx, piv_data->ny);
    
    for (i = 0; i < piv_data->ny; i++) {
        for (j = 0; j < piv_data->nx; j++) {
            lo_data->point_x[i][j] = piv_data->point_x[i][j];
            lo_data->point_y[i][j] = piv_data->point_y[i][j];
            lo_data->dx[i][j] = piv_data->dx[i][j];
            lo_data->dy[i][j] = piv_data->dy[i][j];
            lo_data->snr[i][j] = piv_data->snr[i][j];
            lo_data->peak_no[i][j] = piv_data->peak_no[i][j];
        }
    }


    if ((err_msg = gpiv_ovwrt_pivdata (lo_data, piv_data)) != NULL) {
        return err_msg;
    }
    gpiv_free_pivdata (lo_data);

    return err_msg;
}



static gchar *
disable_data_block (GpivPivData *piv_data,
                    const GpivPostPar *piv_post_par
                    )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Disables a block of data from the data stream in-place
 *
 * PROTOTYPE LOCATATION:
 *     post.h
 *
 * INPUTS:
 *
 * OUTPUTS:
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    guint i, j;
    

    if ((err_msg = gpiv_check_alloc_pivdata (piv_data)) != NULL) {
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }
    
    switch (piv_post_par->operator_manipiv) {

        /*
         * Filtering out data defined by block
         */
    case GPIV_FILTER_BLOCK:
        for (i = 0; i < piv_data->ny; i++) {
            for (j = 0; j < piv_data->nx; j++) {

                if (piv_data->peak_no[i][j] != -1
                    && piv_data->point_x[i][j] >= piv_post_par->block->x_1
                    && piv_data->point_x[i][j] <= piv_post_par->block->x_2
                    && piv_data->point_y[i][j] >= piv_post_par->block->y_1
                    && piv_data->point_y[i][j] <= piv_post_par->block->y_2) {
                    piv_data->dx[i][j] = 0.00;
                    piv_data->dy[i][j] = 0.00;
                    piv_data->snr[i][j] = 10;
                    piv_data->peak_no[i][j] = -1;

                } else {
                    piv_data->dx[i][j] = piv_data->dx[i][j];
                    piv_data->dy[i][j] = piv_data->dy[i][j];
                    piv_data->snr[i][j] = piv_data->snr[i][j];
                    piv_data->peak_no[i][j] = piv_data->peak_no[i][j];
                }

            }
        }
        break;

        /*
         * Passing through data defined by block
         */
    case GPIV_PASS_BLOCK:
        for (i = 0; i < piv_data->ny; i++) {
            for (j = 0; j < piv_data->nx; j++) {

                if (piv_data->peak_no[i][j] != -1
                    && piv_data->point_x[i][j] >= piv_post_par->block->x_1 
                    && piv_data->point_x[i][j] <= piv_post_par->block->x_2
                    && piv_data->point_y[i][j] >= piv_post_par->block->y_1 
                    && piv_data->point_y[i][j] <= piv_post_par->block->y_2) {
                    piv_data->dx[i][j] = piv_data->dx[i][j];
                    piv_data->dy[i][j] = piv_data->dy[i][j];
                    piv_data->snr[i][j] = piv_data->snr[i][j];
                    piv_data->peak_no[i][j] = piv_data->peak_no[i][j];

                } else {
                    piv_data->dx[i][j] = 0.00;
                    piv_data->dy[i][j] = 0.00;
                    piv_data->snr[i][j] = 10;
                    piv_data->peak_no[i][j] = -1;
                }

            }
        }

    default:
        err_msg = "LIBGPIV internal error: disable_data_block: no valid operation_manipiv";
        return err_msg;
        break;
    }
  

    return err_msg;
}


static gchar *
set_value_block (GpivPivData *piv_data, 
                 const GpivPostPar *piv_post_par
                 )
/* ----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Set displacement values for a block of disabled data in-place
 *
 * PROTOTYPE LOCATATION:
 *     post.h
 *
 * INPUTS:
 *     piv_data:       GpivPivData containing all variables in a datafile. 
 *     piv_post_par:   parameters for post processing
 *
 * RETURNS:
 *     piv_data:       GpivPivData with resulting set values. 
 *-------------------------------------------------------------------------- */
{
    gchar *err_msg = NULL;
    guint i, j;


    if ((err_msg = gpiv_check_alloc_pivdata (piv_data)) != NULL) {
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }

    for (i = 0; i < piv_data->ny; i++) {
        for (j = 0; j < piv_data->nx; j++) {
            if (piv_data->peak_no[i][j] == -1
                && piv_data->point_x[i][j] >= piv_post_par->block->x_1 
                && piv_data->point_x[i][j] <= piv_post_par->block->x_2
                && piv_data->point_y[i][j] >= piv_post_par->block->y_1 
                && piv_data->point_y[i][j] <= piv_post_par->block->y_2) {
                piv_data->dx[i][j] = piv_post_par->set_dx;
                piv_data->dy[i][j] = piv_post_par->set_dx;
            }
        }
    }


    return err_msg;
}



static gchar *
s_stats (GpivPivData *piv_data
         )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Calculates spatial global mean and sdev of piv data
 *
 * PROTOTYPE LOCATATION:
 *     post.h
 *
 * INPUTS:
 *
 * OUTPUTS:
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    guint i, j, count = 0;
    gfloat dx_sum = 0, dy_sum = 0, dx_sum_q = 0, dy_sum_q = 0;


    if ((err_msg = gpiv_check_alloc_pivdata (piv_data)) != NULL) {
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }

    for (i = 0; i < piv_data->ny; i++) {
        for (j = 0; j < piv_data->nx; j++) {
            if (piv_data->peak_no[i][j] != -1) {
                if (piv_data->dx[i][j] > piv_data->max_dx) 
                    piv_data->max_dx = piv_data->dx[i][j];

                if (piv_data->dx[i][j] < piv_data->min_dx) 
                    piv_data->min_dx = piv_data->dx[i][j];

                if (piv_data->dy[i][j] > piv_data->max_dy) 
                    piv_data->max_dy = piv_data->dy[i][j];

                if (piv_data->dy[i][j] < piv_data->min_dx) 
                    piv_data->min_dy = piv_data->dy[i][j];

                dx_sum += piv_data->dx[i][j];
                dy_sum += piv_data->dy[i][j];
                dx_sum_q += piv_data->dx[i][j] * piv_data->dx[i][j];
                dy_sum_q += piv_data->dy[i][j] * piv_data->dy[i][j];
                count++;
            }
        }
    }


    piv_data->mean_dx = dx_sum / count;
    piv_data->sdev_dx = sqrt (dx_sum_q/(count-1) - dx_sum*dx_sum/(count*(count-1)));
    piv_data->mean_dy = dy_sum / count;
    piv_data->sdev_dy = sqrt (dy_sum_q/(count-1) - dy_sum*dy_sum/(count*(count-1)));
    piv_data->count = count;


    return err_msg;
}



/*
 * Public functions
 */

gchar *
gpiv_post_subtract_dxdy (GpivPivData *piv_data, 
                         const gfloat z_off_dx, 
                         const gfloat z_off_dy
                         )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Subtracts displacements or velocity from piv data
 *
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    guint i, j;


    if ((err_msg = gpiv_check_alloc_pivdata (piv_data)) != NULL) {
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }

    for (i = 0; i < piv_data->ny; i++) {
        for (j = 0; j < piv_data->nx; j++) {
            if (piv_data->peak_no[i][j] != -1) {
                piv_data->dx[i][j] = piv_data->dx[i][j] - z_off_dx;
                piv_data->dy[i][j] = piv_data->dy[i][j] - z_off_dy;
                piv_data->peak_no[i][j] =piv_data->peak_no[i][j];
            }
        }
    }


    return err_msg;
}



gchar *
gpiv_post_manipiv (GpivPivData *piv_data, 
                   const GpivPostPar *piv_post_par
                   )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Piv post processing function to manipulate data; flipping rotating, etc
 *
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;



    if ((err_msg = gpiv_check_alloc_pivdata (piv_data)) != NULL) {
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }
    

    /*
     * Manipulating of piv data
     */
    if (piv_post_par->operator_manipiv == GPIV_FLIP_X || 
        piv_post_par->operator_manipiv == GPIV_FLIP_Y || 
        piv_post_par->operator_manipiv == GPIV_ROT90) { 
        if ((err_msg = flip (piv_data, piv_post_par)) != NULL) {
            gpiv_warning ("%s", err_msg);
            return err_msg;
        }
        
    } else if (piv_post_par->operator_manipiv == GPIV_REVERT) {
        if ((err_msg = revert_data (piv_data)) != NULL) {
            gpiv_warning ("gpiv_post_manipiv: %s", err_msg);
            return err_msg;
        }
        
    } else if (piv_post_par->operator_manipiv == GPIV_FAST_Y) {
        if ((err_msg = fasty (piv_data)) != NULL) {
            gpiv_warning ("%s", err_msg);
            return err_msg;
        }
        
    } else if (piv_post_par->operator_manipiv == GPIV_FILTER_BLOCK ||
               piv_post_par->operator_manipiv == GPIV_PASS_BLOCK) {
        if ((err_msg = disable_data_block (piv_data, piv_post_par)) != NULL) {
            gpiv_warning ("gpiv_post_manipiv: %s", err_msg);
            return err_msg;
        }

        gpiv_warning ("gpiv_post_manipiv:: 1");
        if (piv_post_par->set == TRUE) {
            gpiv_warning ("gpiv_post_manipiv:: 2");
            set_value_block (piv_data, piv_post_par);
            gpiv_warning ("gpiv_post_manipiv:: 2b");
        }
        gpiv_warning ("gpiv_post_manipiv:: 3");

    }


    return err_msg;
}



gchar *
gpiv_post_savg (GpivPivData *piv_data, 
                const GpivPostPar *piv_post_par
                )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Piv post processing function to calculate spatial mean, variances
 *
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;


    if ((err_msg = gpiv_check_alloc_pivdata (piv_data)) != NULL) {
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }

    s_stats (piv_data);

    if (piv_post_par->subtract == 1) {
        gpiv_post_subtract_dxdy (piv_data, piv_data->mean_dx, 
                                 piv_data->mean_dy);
    } else if (piv_post_par->subtract == 2) {
        gpiv_post_subtract_dxdy (piv_data, piv_post_par->z_off_dx, 
                                 piv_post_par->z_off_dy);
    }


    return err_msg;
}



GpivBinData *
gpiv_post_uvhisto (const GpivPivData *piv_data, 
                   const guint nbins,
                   const enum GpivVelComponent velcomp
                   )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Calculating histogram of U (horizontal) or V (vertical) particle 
 *     displacements
 *
 * PROTOTYPE LOCATATION:
 *     valid.h
 *
 * INPUTS:
 *      piv_data:        input piv data
 *      velcomp:        velocity component from which histogram is calculated
 *
 * OUTPUTS:
 *      klass:          histogram
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    GpivBinData *klass = NULL;
    gchar *err_msg = NULL;

    guint i, j, k;
    gfloat delta, fract;
    gfloat *bound, *centre;
    gint *count;
    gfloat lower_bound = 10.0e9, upper_bound = -10.0e9;


    if ((err_msg = gpiv_check_alloc_pivdata (piv_data)) != NULL) {
        gpiv_warning ("%s", err_msg);
        return NULL;
    }

    if ((klass = gpiv_alloc_bindata (nbins)) == NULL) {
        gpiv_warning ("gpiv_post_uvhisto: failing gpiv_alloc_bindata");
        return NULL;
    }

    bound = klass->bound;
    centre = klass->centre;
    count = klass->count;

    /*
     * Determine minimum and maximum displacements
     */
    for (i = 0; i<piv_data->ny; i++) {
        for (j = 0; j<piv_data->nx; j++) {

            if (piv_data->peak_no[i][j] != -1) {
                if (velcomp == GPIV_U) {
                    if (piv_data->dx[i][j] > upper_bound) 
                        upper_bound = piv_data->dx[i][j];
                    if (piv_data->dx[i][j] < lower_bound) 
                        lower_bound = piv_data->dx[i][j];
                } else {
                    if (piv_data->dy[i][j] > upper_bound) 
                        upper_bound = piv_data->dy[i][j];
                    if (piv_data->dy[i][j] < lower_bound) 
                        lower_bound = piv_data->dy[i][j];
                }
            }

        }
    }

    delta = (gfloat) (upper_bound - lower_bound) / (gfloat) nbins;

    for (i = 0; i < nbins; i++) {
	centre[i] = lower_bound + delta / 2.0 + (float) i * delta;
	bound[i] = lower_bound + (float) i * delta;
	count[i] = 0;
    }

    /*
     * Subdividing particle displacements in bins
     */
    for (i = 0; i < piv_data->ny; i++) {
	for (j = 0; j < piv_data->nx; j++) {

            if (piv_data->peak_no[i][j] != -1) {
                for (k = 0; k < nbins; k++) {
                    if (velcomp == GPIV_U) {
                        if (( piv_data->dx[i][j] >= bound[k]) 
                            && (piv_data->dx[i][j] < bound[k + 1])) {
                            count[k] = count[k] + 1;
                        }
                    } else {
                        if (( piv_data->dy[i][j] >= bound[k]) 
                            && (piv_data->dy[i][j] < bound[k + 1])) {
                            count[k] = count[k] + 1;
                        }
                    }
                }
            }

        }
    }


    return klass;
}



gchar *
gpiv_post_scale (GpivPivData *piv_data, 
                 const GpivImagePar *image_par
                 )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Scales piv data in x-and/or y direction
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    guint i, j;


    if ((err_msg = gpiv_check_alloc_pivdata (piv_data)) != NULL) {
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }
    

    for (i = 0; i < piv_data->ny; i++) {
        for (j = 0; j < piv_data->nx; j++) {
            piv_data->point_x[i][j] = piv_data->point_x[i][j] * 
                image_par->s_scale * 1e-3;
            piv_data->point_y[i][j] = piv_data->point_y[i][j] * 
                image_par->s_scale * 1e-3;
            piv_data->dx[i][j] = piv_data->dx[i][j] * image_par->s_scale / 
                image_par->t_scale;
            piv_data->dy[i][j] = piv_data->dy[i][j] * image_par->s_scale / 
                image_par->t_scale;
        }
    }

    /*
     * Zero offset of positions
     */
    if (image_par->z_off_x__set && image_par->z_off_y__set) {
        for (i = 0; i < piv_data->ny; i++) {
            for (j = 0; j < piv_data->nx; j++) {
                piv_data->point_x[i][j] = piv_data->point_x[i][j] + 
                    image_par->z_off_x;
                piv_data->point_y[i][j] = piv_data->point_y[i][j] + 
                    image_par->z_off_y;
            }
        }
    }
    
    piv_data->scale = TRUE;
    piv_data->scale__set = TRUE;
    return err_msg;
}


gchar *
gpiv_post_inverse_scale (GpivPivData *piv_data, 
                         const GpivImagePar *image_par
                         )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Inverse scaling of piv data in x-and/or y direction 
 *
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    guint i, j;


    if ((err_msg = gpiv_check_alloc_pivdata (piv_data)) != NULL) {
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }

    /*
     * Reverse Zero offset of positions
     */
    if (image_par->z_off_x__set && image_par->z_off_y__set) {
        for (i = 0; i < piv_data->ny; i++) {
            for (j = 0; j < piv_data->nx; j++) {
                piv_data->point_x[i][j] = piv_data->point_x[i][j] - 
                    image_par->z_off_x;
                piv_data->point_y[i][j] = piv_data->point_y[i][j] - 
                    image_par->z_off_y;
            }
        }
    }
    
    for (i = 0; i < piv_data->ny; i++) {
        for (j = 0; j < piv_data->nx; j++) {
            piv_data->point_x[i][j] = piv_data->point_x[i][j] / 
                image_par->s_scale * 1e+3;
            piv_data->point_y[i][j] = piv_data->point_y[i][j] / 
                image_par->s_scale * 1e+3;
            piv_data->dx[i][j] = piv_data->dx[i][j] / image_par->s_scale * 
                image_par->t_scale;
            piv_data->dy[i][j] = piv_data->dy[i][j] / image_par->s_scale * 
                image_par->t_scale;
       }
    }
    
    return err_msg;
}



/*
 * Post processing function to calculate vorticity and strain
 */


GpivScalarData *
gpiv_post_vorstra (const GpivPivData *piv_data, 
                   const GpivPostPar *piv_post_par
                   )
/* ----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Calculates vorticity or shear/normal strain quantities from piv data
 * 
 *-------------------------------------------------------------------------- */
{
    GpivScalarData *sc_data = NULL;
    gchar *err_msg = NULL;

    guint i, j, k, l;
    gboolean valid = FALSE;
    guint diff_order = 0;
    gfloat delta;
    

    if ((err_msg = gpiv_check_alloc_pivdata (piv_data)) != NULL) {
        gpiv_warning ("%s", err_msg);
        return NULL;
    }
   
    sc_data = gpiv_alloc_scdata (piv_data->nx, piv_data->ny);    
    delta = piv_data->point_x[0][1] - piv_data->point_x[0][0];


    for (i = 0; i < sc_data->ny; i++) {
        for (j = 0; j < sc_data->nx; j++) {
	    sc_data->point_x[i][j] = piv_data->point_x[i][j];
	    sc_data->point_y[i][j] = piv_data->point_y[i][j];
	    sc_data->flag[i][j] = piv_data->peak_no[i][j];
	    sc_data->scalar[i][j] = 0.0;
	}
    }

    /*
     * determine order of differentiation
     */
    if (piv_post_par->diff_type == GPIV_CENTRAL || 
        piv_post_par->diff_type == GPIV_CIRCULATION) {
        diff_order = 1;
    } else if (piv_post_par->diff_type == GPIV_LEAST_SQUARES || 
               piv_post_par->diff_type == GPIV_RICHARDSON) {
        diff_order = 2;
    } else {
        gpiv_warning ("gpiv_post_vorstra: non-existing diff_typ");
        return NULL;
    }


/*
 * exclude neighbours of excluded data or invalid points
 */
/*      for (i = 0; i < piv_data->ny; i++) { */
/* 	  for (j = 0; j < piv_data->nx; j++) { */
/* 	       if (piv_data->peak_no[i][j] == -1) { */
/* 		    for (k=i-diff_order; k <= i+diff_order; k++) { */
/* 			 if ((k >= 0) && (k < piv_data->ny)) { */
/* 			      for (l=j-diff_order; l <= j+diff_order; l++) { */
/* 				   if ((l >= 0) && (j < piv_data->nx)) { */
/* 					sc_data->scalar[k][l] = 0; */
/* 					sc_data.flag[k][l] = -1; */
/* 				   } */
/* 			      } */
/* 			 } */
/* 		    } */
/* 	       } */
/* 	  } */
/*      } */


    /*
     * check data if valid; peak !=-1
     */
    for (i = diff_order; i < piv_data->ny - diff_order; i++) {
        for (j = diff_order; j < piv_data->nx - diff_order; j++) {
            valid = TRUE;
            for (k=i-diff_order; k <= i + diff_order; k++) {
                if ((k >= 0) && (k < piv_data->ny)) {
                    for (l = j-diff_order; l <= j + diff_order; l++) {
                        if ((l >= 0) && (j < piv_data->nx)) {
                            /*
                             * Only disables actual datapoint
                             */

/* 				      if (piv_data->peak_no[k][l] != -1)  */
/* 					   valid = piv_data->peak_no[k][l];  */

                            /*
                             * Unvalid if one or more of the points used in the derivative is unvalid
                             */
                            if (piv_data->peak_no[k][l] == -1) 
                                valid = FALSE; 
                        }
                    }
                }
            }


            /*  	for (i=0; i < piv_data->ny; i++) { */
            /* 	     for (j=0; j < piv_data->nx; j++) { */

            /*
             * Check data if valid (peak != -1), i.e. are accepted
             */
            if (valid == TRUE) {
                
                switch (piv_post_par->diff_type) {

                case GPIV_CENTRAL:
                    switch (piv_post_par->operator_vorstra) {
                    case GPIV_VORTICITY:
                        sc_data->scalar[i][j] = 
                            ((piv_data->dy[i][j+1] - 
                              piv_data->dy[i][j-1]) -
                             (piv_data->dx[i+1][j] - 
                              piv_data->dx[i-1][j])) / (2 * delta);
                        break;

                    case GPIV_S_STRAIN:
                        sc_data->scalar[i][j] = 
                            ((piv_data->dx[i+1][j] - 
                              piv_data->dx[i-1][j]) + 
                             (piv_data->dy[i][j+1] - 
                              piv_data->dy[i][j-1])) / (2 * delta);
                        break;

                    case GPIV_N_STRAIN:
                        sc_data->scalar[i][j] = 
                            ((piv_data->dx[i][j+1] - 
                              piv_data->dx[i][j]-1) +
                             (piv_data->dy[i+1][j] - 
                              piv_data->dy[i-1][j])) / (2 * delta);
                        break;

                    default:
                        err_msg = "gpiv_post_vorstra: no valid GPIV_CENTRAL operation";
                        gpiv_warning("%s", err_msg);
                        return NULL;
                        break;
                    }
                    break;
                    


                case GPIV_CIRCULATION:
                    switch (piv_post_par->operator_vorstra) {

                    case GPIV_VORTICITY:
                        sc_data->scalar[i][j] = 
                            ((piv_data->dx[i-1][j-1] + 
                              2 * piv_data->dx[i-1][j] + 
                              piv_data->dx[i-1][j+1]) +
                             (piv_data->dy[i-1][j+1] + 
                              2 * piv_data->dy[i][j+1] + 
                              piv_data->dy[i+1][j+1]) -
                             (piv_data->dx[i+1][j+1] + 
                              2 * piv_data->dx[i+1][j] + 
                              piv_data->dx[i+1][j-1]) -
                             (piv_data->dy[i+1][j-1] + 
                              2 * piv_data->dy[i][j-1] + 
                              piv_data->dy[i-1][j-1])) / (8 * delta);
                        break;

                    default:
                        err_msg = "gpiv_post_vorstra: no valid GPIV_CIRCULATION operation";
                        gpiv_warning("%s", err_msg);
                        return NULL;
                        break;
                    }	  
                    break;



                case GPIV_LEAST_SQUARES:
                    switch (piv_post_par->operator_vorstra) {
                        
                    case GPIV_VORTICITY:
                        sc_data->scalar[i][j] = 
                            (2 * piv_data->dy[i][j+2] + 
                             piv_data->dy[i][j+1] - 
                             piv_data->dy[i][j-1] - 
                             2 * piv_data->dy[i][j-2]) / 
                            (10*delta) - (2 * piv_data->dx[i+2][j] + 
                                          piv_data->dx[i+1][j] - 
                                          piv_data->dx[i-1][j] - 
                                          2 * piv_data->dx[i-2][j]) / (10*delta);
                        break;

                    case GPIV_S_STRAIN:
                        sc_data->scalar[i][j] =
                            (2 * piv_data->dx[i+2][j] + 
                             piv_data->dx[i+1][j] - 
                             piv_data->dx[i-1][j] - 
                             2 * piv_data->dx[i-2][j]) / (10*delta) +
                            (2 * piv_data->dy[i][j+2] + 
                             piv_data->dy[i][j+1] - 
                             piv_data->dy[i][j-1] - 
                             2 * piv_data->dy[i][j-2]) / (10*delta);
                        break;

                    case GPIV_N_STRAIN:
                        sc_data->scalar[i][j] =
                            (2 * piv_data->dx[i][j+2] + 
                             piv_data->dx[i][j+1] - 
                             piv_data->dx[i][j-1] - 
                             2 * piv_data->dx[i][j-2]) / (10*delta) +
                            (2 * piv_data->dy[i+2][j] + 
                             piv_data->dy[i+1][j] - 
                             piv_data->dy[i-1][j] - 
                             2 * piv_data->dy[i-2][j]) / (10*delta);
                        break;

                    default:
                        err_msg = "gpiv_post_vorstra: no valid GPIV_LEAST_SQUARES operation";
                        gpiv_warning("%s", err_msg);
                        return NULL;
                        break;
                    }	  
                    break;
                    


                case GPIV_RICHARDSON:
                    switch (piv_post_par->operator_vorstra) {
 
                    case GPIV_VORTICITY:
                        sc_data->scalar[i][j] = 
                            (piv_data->dy[i][j-2] - 
                             8 * piv_data->dy[i][j-1] + 
                             8 * piv_data->dy[i][j+1] - 
                             piv_data->dy[i][j+2]) / (12*delta) -
                            (piv_data->dx[i-2][j] - 
                             8 * piv_data->dx[i-1][j] + 
                             8 * piv_data->dx[i+1][j] - 
                             piv_data->dx[i+2][j]) / (12*delta);
                        break;

                    case GPIV_S_STRAIN:
                        sc_data->scalar[i][j] =
                            (piv_data->dx[i-2][j] - 
                             8 * piv_data->dx[i-1][j] + 
                             8 * piv_data->dx[i+1][j] - 
                             piv_data->dx[i+2][j]) / (12*delta) + 
                            (piv_data->dy[i][j-2] - 
                             8 * piv_data->dy[i][j-1] + 
                             8 * piv_data->dy[i][j+1] - 
                             piv_data->dy[i][j+2]) / (12*delta);
                        break;

                    case GPIV_N_STRAIN:
                        sc_data->scalar[i][j] = 
                            (piv_data->dx[i][j-2] - 
                             8 * piv_data->dx[i][j-1] + 
                             8 * piv_data->dx[i][j+1] - 
                             piv_data->dx[i][j+2]) / (12*delta) +
                            (piv_data->dy[i-2][j] - 
                             8 * piv_data->dy[i-1][j] + 
                             8 * piv_data->dy[i+1][j] - 
                             piv_data->dy[i+2][j]) / (12*delta);
                        break;

                    default:
                        err_msg = "gpiv_post_vorstra: no valid GPIV_RICHARDSON operation";
                        gpiv_warning("%s", err_msg);
                        return NULL;
                        break;
                    }	  
                    break;
                    
                    

                default:
                    err_msg = "gpiv_post_vorstra: no valid differential operator type";
                    gpiv_warning ("%s", err_msg);
                    return NULL;
                    break;
                }
            } else {
                sc_data->flag[i][j] = -1;
            }
            
        }
    }
    
    

    /*
     * exclude all data near the boundaries of the dataset
     */
    for (i=0; i < diff_order; i++) {
        for (j=0; j < piv_data->nx; j++) {
            sc_data->scalar[i][j] = 0;
            sc_data->flag[i][j] = -1;
        }
    }
    
    for (i=0; i < piv_data->ny; i++) {
        for (j=0; j < diff_order; j++) {
            sc_data->scalar[i][j] = 0;
            sc_data->flag[i][j] = -1;
        }
    }
    
    for (i=piv_data->ny - diff_order; i < piv_data->ny; i++) {
        for (j=0; j < piv_data->nx; j++) {
            sc_data->scalar[i][j] = 0;
            sc_data->flag[i][j] = -1;
        }
    }
    
    for (i=0; i < piv_data->ny; i++) {
        for (j=piv_data->nx - diff_order; j < piv_data->nx; j++) {
            sc_data->scalar[i][j] = 0;
            sc_data->flag[i][j] = -1;
        }
    }
    
    
    return sc_data;
}
