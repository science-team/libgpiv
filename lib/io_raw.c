/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2006, 2007, 2008 Gerber van der Graaf

   This file is part of libgpiv.
   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  



-------------------------------------------------------------------------------
FILENAME:                io_raw.c
LIBRARY:                 libgpiv:
EXTERNAL FUNCTIONS:
                         gpiv_fread_raw_image
                         gpiv_fwrite_raw_image
                         gpiv_read_raw_image
                         gpiv_write_raw_image

LOCAL FUNCTIONS:

LAST MODIFICATION DATE:  $Id: io_raw.c,v 1.4 2007-12-19 08:46:46 gerber Exp $
 --------------------------------------------------------------------------- */

#include <gpiv.h>

static void 
read_raw_image_frames (FILE *fp, 
                       GpivImage *image                        
                       );

static void
write_raw_image_frames (FILE *fp, 
                        GpivImage *image
                        );

/*
 * Public functions
 */

GpivImage * 
gpiv_read_raw_image (FILE *fp 
                     )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Read raw image data from fp. 
 *      Frames memory will be allocated here.
 *
 * INPUTS:
 *      fname:          pointer to complete filename to be written
 *
 * RETURNS:
 *     GpivImage on success or NULL on failure
 *---------------------------------------------------------------------------*/
{
    GpivImage *image = NULL;
    GpivImagePar *image_par = g_new0 (GpivImagePar, 1);
    gint i, j;
    gchar *err_msg = NULL;

/*
 * Read header info. If required parameters are absent, scan from resource 
 * files, check again if present, else bail out.
 */
    gpiv_img_parameters_set (image_par, FALSE);
    gpiv_img_read_header (fp, image_par, FALSE, FALSE, NULL);

#ifdef GPIV_IMG_PARAM_RESOURCES
    if ((err_msg = gpiv_img_check_header_required_read (image_par))
        != NULL) {
#ifdef DEBUG
        gpiv_scan_resourcefiles (GPIV_IMGPAR_KEY, image_par, TRUE);
#else
        gpiv_scan_resourcefiles (GPIV_IMGPAR_KEY, image_par, FALSE);
#endif /* DEBUG */
    }
#endif /* GPIV_IMG_PARAM_RESOURCES */


    if ((err_msg = gpiv_img_check_header_required_read (image_par)) 
        != NULL) {
        gpiv_warning ("%s", err_msg);
        return NULL;
    }

/*
 * From header info memory allocation of image arrays can be done
 */
    image = gpiv_alloc_img (image_par);
    read_raw_image_frames (fp, image);


    return image;
}


GpivImage *
gpiv_fread_raw_image (const gchar *fname
                      ) 
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Reads an image from raw binary file.
 *     Expects the arrray(s) have not been allocated yet
 *     Reads image header data from file.h
 *
 * INPUTS:
 *     fname:          file name
 *
 * OUTPUTS:
 *     image1_p:       pointer to first 2-dimensional image array
 *     image1_p:       pointer to second 2-dimensional image array
 *     image_par:      pointer to image parameter structure
 *
 * RETURNS:
 *     GpivImage on success or NULL on failure
 *--------------------------------------------------------------------------- */
{
    GpivImage *image = NULL;
    GpivImagePar *image_par = g_new0 (GpivImagePar, 1);
    gchar *err_msg = NULL;
    gchar fname_header[GPIV_MAX_CHARS];
    gchar *dirname, *fname_base, *fname_nosuffix;
    FILE *fp;


    if (fname == NULL) {
        gpiv_warning ("gpiv_fread_raw_image: failing \"fname == NULL\"");
        return NULL;
    }

/*     image->header = image_par; */
    gpiv_img_parameters_set (image_par, FALSE);

/*
 * Stripping fname and create header filename
 */
    dirname = g_strdup (g_path_get_dirname (fname));
    fname_base = g_strdup (g_path_get_basename (fname));
    strtok (fname_base, ".");
    fname_nosuffix = g_strdup (g_strconcat (dirname, G_DIR_SEPARATOR_S,
                                            fname_base, NULL));
#ifdef DEBUG
    g_message ("gpiv_fread_raw_image: dirname = %s\n fname_base = %s\n fname_nosuffix = %s", 
              dirname, fname_base, fname_nosuffix);    
#endif /* DEBUG */

    g_snprintf(fname_header, GPIV_MAX_CHARS, "%s%s", 
               fname_nosuffix, GPIV_EXT_HEADER);

#ifdef DEBUG
    g_message ("gpiv_fread_raw_image: image header is: %s", fname_header);
#endif /* DEBUG */

/*
 * Read header info. If required parameters are absent, scan from resource files,
 * check again if present, else bail out.
 */
    if ((fp = fopen (fname_header, "rb")) == NULL) {
        gpiv_warning ("gpiv_fread_raw_image: failure opening %s for input", fname_header);
        return NULL;
    }
    gpiv_img_read_header (fp, image_par, FALSE, FALSE, NULL);
    fclose (fp);


#ifdef GPIV_IMG_PARAM_RESOURCES
    if ((err_msg = gpiv_img_check_header_required_read (image_par))
        != NULL) {
#ifdef DEBUG
        gpiv_scan_resourcefiles (GPIV_IMGPAR_KEY, image_par, TRUE);
#else
        gpiv_scan_resourcefiles (GPIV_IMGPAR_KEY, image_par, FALSE);
#endif /* DEBUG */
    }
#endif /* GPIV_IMG_PARAM_RESOURCES */


    if ((err_msg = gpiv_img_check_header_required_read (image_par)) 
        != NULL) {
        gpiv_warning ("%s", err_msg);
        return NULL;
    }


/*
 * reads image data from file in binary format
 */
    if ((fp = fopen (fname, "rb")) == NULL) {
	err_msg = "gpiv_fread_raw_image: failure opening for input";
        gpiv_warning ("%s", err_msg);
        return NULL;
    }

    image = gpiv_alloc_img (image_par);
    read_raw_image_frames (fp, image);
    fclose (fp);


    g_free(dirname);
    g_free(fname_base);
    g_free(fname_nosuffix);
    return image;
}


gchar *
gpiv_write_raw_image (FILE *fp, 
                      GpivImage *image
                      )
/*---------------------------------------------------------------------------
 *     Writes raw binary image to fp
 *
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    

    gpiv_img_print_header (fp, image->header);
    write_raw_image_frames (fp, image);
    
    
    return err_msg;
}


gchar *
gpiv_fwrite_raw_image (const gchar *fname,
                       GpivImage *image
                      )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Writess binary image to file fname.r and header to fname.h
 *
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    gchar fname_header[GPIV_MAX_CHARS];
    gchar *dirname, *fname_base, *fname_nosuffix;
    FILE *fp;


    if (fname == NULL) {
        gpiv_warning ("gpiv_fwrite_raw_image: failing \"fname == NULL\"");
        return NULL;
    }

    g_return_if_fail (image->frame1[0] != NULL);
    g_return_if_fail (image->frame2[0] != NULL);



/*
 * Stripping file name, create header filename and print header info
 */
    dirname = g_strdup (g_path_get_dirname (fname));
    fname_base = g_strdup (g_path_get_basename (fname));
    strtok (fname_base, ".");
    fname_nosuffix = g_strdup (g_strconcat (dirname, G_DIR_SEPARATOR_S,
                                            fname_base, NULL));
    g_snprintf(fname_header, GPIV_MAX_CHARS, "%s%s", 
               fname_nosuffix, GPIV_EXT_HEADER);

    if ((fp = fopen (fname_header, "wb")) == NULL) {
        gpiv_warning ("gpiv_fwrite_raw_image: failure opening %s for output", 
                      fname_header);
        return "PIV_FWRITE_RAW_IMAGE: failure opening header for output";
    }

    gpiv_img_print_header (fp, image->header);
    fclose (fp);

/*
 * Write image frames
 */
    if ((fp = fopen (fname, "wb")) == NULL) {
        gpiv_warning ("gpiv_fwrite_raw_image: failure opening %s for output", 
                      fname);
        return "gpiv_fwrite_raw_image: failure opening image for output";
    }

    write_raw_image_frames (fp, image);
    fclose (fp);


    return err_msg;
}



/*
 * Local functions
 */

static void
read_raw_image_frames (FILE *fp, 
                       GpivImage *image
                       )
/*-----------------------------------------------------------------------------
 *      Read raw image data frames from fp. 
 */
{
    guint8 **lo_img1, **lo_img2;
    gint i, j;


    if (fp == stdin || fp == NULL) {
        if (image->header->depth <= 8) {
            lo_img1 = gpiv_matrix_guint8 (image->header->nrows, 
                                          image->header->ncolumns);
            read (0, lo_img1[0], image->header->ncolumns 
                   * image->header->nrows * (sizeof(guint8)));
                  
            if (image->header->x_corr) {
                lo_img2 = gpiv_matrix_guint8(image->header->nrows, 
                                             image->header->ncolumns);
                read (0, lo_img2[0], image->header->ncolumns * 
                       image->header->nrows * (sizeof(guint8)));
            } else {
                lo_img2 = lo_img1;
            }
            
            
            for (i = 0; i < image->header->nrows; i++) {
                for (j = 0; j < image->header->ncolumns; j++) {
                    image->frame1[i][j] = lo_img1[i][j];
                    image->frame2[i][j] = lo_img2[i][j];
                }
            }
            
            gpiv_free_matrix_guint8 (lo_img1);
            if (image->header->x_corr) {
                gpiv_free_matrix_guint8 (lo_img2);
            }
            
        } else {
            read (0, image->frame1[0], image->header->ncolumns 
                   * image->header->nrows 
                   * (sizeof(guint16)));
            
            if (image->header->x_corr) {
                read (0, image->frame2[0], image->header->ncolumns 
                       * image->header->nrows 
                       * (sizeof(guint16)));
            } else {
                image->frame2 = image->frame1;
            }
        
        }


    } else {
        if (image->header->depth <= 8) {
            lo_img1 = gpiv_matrix_guint8 (image->header->nrows, 
                                          image->header->ncolumns);
            fread (lo_img1[0], image->header->ncolumns 
                   * image->header->nrows * (sizeof(guint8)), 1, fp);
            
            if (image->header->x_corr) {
                lo_img2 = gpiv_matrix_guint8(image->header->nrows, 
                                             image->header->ncolumns);
                fread (lo_img2[0], image->header->ncolumns * 
                       image->header->nrows * (sizeof(guint8)), 1, fp);
            } else {
                lo_img2 = lo_img1;
            }
            
            
            for (i = 0; i < image->header->nrows; i++) {
                for (j = 0; j < image->header->ncolumns; j++) {
                    image->frame1[i][j] = lo_img1[i][j];
                    image->frame2[i][j] = lo_img2[i][j];
                }
            }
            
            gpiv_free_matrix_guint8 (lo_img1);
            if (image->header->x_corr) {
                gpiv_free_matrix_guint8 (lo_img2);
            }
            
            
        } else {
            fread (image->frame1[0], image->header->ncolumns 
                   * image->header->nrows 
                   * (sizeof(guint16)), 1, fp);
            
            if (image->header->x_corr) {
                fread (image->frame2[0], image->header->ncolumns 
                       * image->header->nrows 
                       * (sizeof(guint16)), 1, fp);
            } else {
                image->frame2 = image->frame1;
            }
        }
    }
}


static void
write_raw_image_frames (FILE *fp, 
                        GpivImage *image
                        )
/*---------------------------------------------------------------------------
 *     Writes raw binary image frames to fp
 */
{
    guint ncolumns = image->header->ncolumns;
    guint nrows = image->header->nrows;


    if (fp == stdout || fp == NULL) {
        if (image->header->depth <= 8) {
            write (1, image->frame1[0], ncolumns * nrows * (sizeof (guint8)));
 
            if (image->header->x_corr) {
                write (1, image->frame2[0], ncolumns * nrows * (sizeof (guint8)));
            }


        } else {
            write (1, image->frame1[0], ncolumns * nrows * (sizeof (guint16)));

            if (image->header->x_corr) {
                write (1, image->frame2[0], ncolumns * nrows * (sizeof (guint16)));
            }
        }

    } else {
        if (image->header->depth <= 8) {
            fwrite (image->frame1[0], ncolumns * nrows * (sizeof (guint8)), 1, fp);
 
            if (image->header->x_corr) {
                fwrite (image->frame2[0], ncolumns * nrows * (sizeof (guint8)), 1, fp);
            }


        } else {
            fwrite (image->frame1[0], ncolumns * nrows * (sizeof (guint16)), 1,
                    fp);

            if (image->header->x_corr) {
                fwrite (image->frame2[0], ncolumns * nrows * (sizeof (guint16)), 1,
                        fp);
            }
        }
    }
}
