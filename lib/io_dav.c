/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2007, 2008 Gerber van der Graaf

   This file is part of libgpiv.
   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  



---------------------------------------------------------------
FILENAME:                io_raw.c
LIBRARY:                 libgpiv:
EXTERNAL FUNCTIONS:
			 gpiv_read_davis_image

LOCAL FUNCTIONS:

LAST MODIFICATION DATE:  $Id: io_dav.c,v 1.1 2008-04-22 13:00:49 gerber Exp $
 --------------------------------------------------------------- */

#include <gpiv.h>


GpivImage *
gpiv_read_davis_image (FILE *fp
                       ) 
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Reads Davis formatted image, with ext .IMG from file
 *
 * PROTOTYPE LOCATATION:
 *     io.h
 *
 * INPUTS:
 *     fp:             pointer to input file
 * RETURNS:
 *     NULL on success or *err_msg on failure
 *---------------------------------------------------------------------------*/
{
    GpivImage *gpiv_image = NULL;
    GpivImagePar *gpiv_image_par = g_new0 (GpivImagePar, 1);
    gchar *err_msg = NULL;
    

/*
 * Obtaining image dimensions
 */
    fseek (fp, 10, SEEK_SET);
    fread (&gpiv_image_par->nrows, sizeof (guint16), 1, fp);
    fread (&gpiv_image_par->ncolumns, sizeof (guint16), 1, fp);

/*
 * Obtaining other requitered parameters, like depth and x_corr.
 * These will definitely no be present in the image header.
 * So searching unconditionally.
 */
#ifdef DEBUG
    gpiv_scan_resourcefiles (GPIV_IMGPAR_KEY, &gpiv_image_par, TRUE);
#else
    gpiv_scan_resourcefiles (GPIV_IMGPAR_KEY, &gpiv_image_par, FALSE);
#endif /* DEBUG */

    if ((err_msg = gpiv_img_check_header_required_read (gpiv_image_par)) 
        != NULL) {
        gpiv_warning ("gpiv_read_davis_image: %s", err_msg);
        return NULL;
    }

/*
 * Reading image data
 */
    gpiv_image = gpiv_alloc_img (gpiv_image_par);
    fseek (fp, 256, SEEK_SET);
    fread (gpiv_image->frame1[0], sizeof (guint16) 
           * gpiv_image->header->ncolumns 
           * gpiv_image->header->nrows, 1, fp);
    fread (gpiv_image->frame2[0], sizeof (guint16)
           * gpiv_image->header->ncolumns 
           * gpiv_image->header->nrows, 1, fp);

    
    return gpiv_image;
}


