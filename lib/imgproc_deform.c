/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008
   Gerber van der Graaf

   This file is part of libgpiv.
   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  



-------------------------------------------------------------------------------
FILENAME:                imgproc_deform.c
LIBRARY:                 libgpiv:
EXTERNAL FUNCTIONS:      gpiv_imgproc_deform

LOCAL FUNCTIONS:         compare_float

LAST MODIFICATION DATE:  $Id: imgproc_deform.c,v 1.7 2008-09-25 13:19:53 gerber Exp $

-------------------------------------------------------------------------------


  This software is provided by P. Th�venaz and based on the following paper:

  P. Th�venaz, T. Blu, M. Unser, "Interpolation Revisited," IEEE  
  Transactions on Medical Imaging, vol. 19, no. 7, pp. 739-758, July 2000.

  EPFL/STI/IOA/BIG
  Philippe Thevenaz
  Bldg. BM-Ecublens 4.137
  CH-1015 Lausanne
  phone (CET):	+41(21)693.51.61
  fax:			+41(21)693.37.01
  RFC-822:		philippe.thevenaz@epfl.ch
  X-400:		/C=ch/A=400net/P=switch/O=epfl/S=thevenaz/G=philippe/
  URL:			http://bigwww.epfl.ch/
------------------------------------------------------------------------------*/

#include	<float.h>
/* #include	<math.h> */
/* #include	<stddef.h> */
/* #include	<stdio.h> */
/* #include	<stdlib.h> */

#include <stddef.h>
#include <gpiv.h>

/*
 * Prototyping of static functions
 */
static void
ConvertToInterpolationCoefficients
				(double	c[],	        /* input samples --> output coefficients */
				long	DataLength,	/* number of samples or coefficients */
				double	z[],		/* poles */
				long	NbPoles,        /* number of poles */
				double	Tolerance	/* admissible relative error */
				);

static void 
GetColumn			(float	*Image,		/* input image array */
				long	Width,		/* width of the image */
				long	x,		/* x coordinate of the selected line */
				double	Line[],		/* output linear array */
				long	Height		/* length of the line and height of the image */
				);

static void
GetRow				(float	*Image,		/* input image array */
				long	y,		/* y coordinate of the selected line */
				double	Line[],		/* output linear array */
				long	Width		/* length of the line and width of the image */
				);

static double
InitialCausalCoefficient	(double	c[],		/* coefficients */
				long	DataLength,	/* number of coefficients */
				double	z,	       	/* actual pole */
				double	Tolerance	/* admissible relative error */
				);

static double
InitialAntiCausalCoefficient	(double	c[],		/* coefficients */
				long	DataLength,	/* number of samples or coefficients */
				double	z	       	/* actual pole */
				);

static void
PutColumn			(float	*Image,		/* output image array */
				long	Width,		/* width of the image */
				long	x,		/* x coordinate of the selected line */
				double	Line[],		/* input linear array */
				long	Height		/* length of the line and height of the image */
				);

static void
PutRow				(float	*Image,		/* output image array */
				long	y,		/* y coordinate of the selected line */
				double	Line[],		/* input linear array */
				long	Width		/* length of the line and width of the image */
				);



static int
SamplesToCoefficients		(float	*Image,		/* in-place processing */
				long	Width,		/* width of the image */
				long	Height,		/* height of the image */
				long	SplineDegree    /* degree of the spline model */
				);

static gchar *
imgproc_deform_frame_from_pivdata(guint16 **img,
                                  const GpivImagePar *image_par,
                                  const GpivPivData *piv_data,
                                  const gdouble magnitude
                                  );

static int
imgproc_deform_frame		(guint16	        **img,
                                 const GpivImagePar	*image_par,
                                 const GpivPivData	*piv_data
				);

static double	
InterpolatedValue		(float	*Bcoeff,	/* input B-spline array of coefficients */
				const long	Width,		/* width of the image */
				const long	Height,		/* height of the image */
				const double	x,	      	/* x coordinate where to interpolate */
				const double	y,	       	/* y coordinate where to interpolate */
				const long	SplineDegree	/* degree of the spline model */
				);

/*
 * Public functions
 */
gchar *
gpiv_imgproc_deform (GpivImage *image,
                     const GpivPivData *piv_data
                     )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Image shifting and deformation routine for a single exposed, double 
 *     frame PIV image pair with magnitude of PIV estimations at each pixel
 *     Deforms first frame halfway forward and second frame halfway backward.
 *
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;


    if (image->header->x_corr__set == FALSE
        || image->header->x_corr == FALSE) {
        err_msg = "gpiv_imgproc_deform: only to be used for cross correlation";
        return (err_msg);
    }

    if (image->frame1[0] == NULL) {
        err_msg = "gpiv_imgproc_deform: image->frame1 == NULL";
        return (err_msg);
    }

    if (image->frame2[0] == NULL) {
        err_msg = "gpiv_imgproc_deform: image->frame2 == NULL";
        return (err_msg);
    }

    /*
     * Deform first image frame with half shift forwards 
     * and second frame with half shift backwards.
     */
/* OK: results approved */
#pragma omp parallel sections num_threads(2)
    {
#pragma omp section 
        {
            if ((err_msg = 
                 imgproc_deform_frame_from_pivdata(image->frame1, image->header, 
                                                   piv_data, 0.5))
                != NULL) {
#ifdef _OPENMP
                exit;
#else
                return (err_msg);
#endif
            }
        }
#pragma omp section 
        {
            if ((err_msg = 
                 imgproc_deform_frame_from_pivdata(image->frame2, image->header, 
                                                   piv_data, -0.5))
                != NULL) {
#ifdef _OPENMP
                exit;
#else
                return (err_msg);
#endif
            }
        }
    }

    return (err_msg);
}



/*
 * Definition of static functions
 */

static void
ConvertToInterpolationCoefficients (
                                    double	c[],		/* input samples --> output coefficients */
				    long	DataLength,	/* number of samples or coefficients */
				    double	z[],		/* poles */
				    long	NbPoles,	/* number of poles */
				    double	Tolerance	/* admissible relative error */
				    )
/*--------------------------------------------------------------------------*/
{
	double	Lambda = 1.0;
	long	n, k;
	/* special case required by mirror boundaries */
	if (DataLength == 1L) {
		return;
	}
	/* compute the overall gain */
	for (k = 0L; k < NbPoles; k++) {
		Lambda = Lambda * (1.0 - z[k]) * (1.0 - 1.0 / z[k]);
	}
	/* apply the gain */
	for (n = 0L; n < DataLength; n++) {
		c[n] *= Lambda;
	}
	/* loop over all poles */
	for (k = 0L; k < NbPoles; k++) {
		/* causal initialization */
		c[0] = InitialCausalCoefficient(c, DataLength, z[k], Tolerance);
		/* causal recursion */
		for (n = 1L; n < DataLength; n++) {
			c[n] += z[k] * c[n - 1L];
		}
		/* anticausal initialization */
		c[DataLength - 1L] = InitialAntiCausalCoefficient(c, DataLength, z[k]);
		/* anticausal recursion */
		for (n = DataLength - 2L; 0 <= n; n--) {
			c[n] = z[k] * (c[n + 1L] - c[n]);
		}
	}
}



static double
InitialCausalCoefficient (
			  double	c[],	/* coefficients */
			  long	        DataLength,	/* number of coefficients */
			  double	z,	/* actual pole */
			  double	Tolerance	/* admissible relative error */
			  )
/*--------------------------------------------------------------------------*/
{
	double	Sum, zn, z2n, iz;
	long	n, Horizon;
	/* this initialization corresponds to mirror boundaries */
	Horizon = DataLength;
	if (Tolerance > 0.0) {
		Horizon = (long)ceil(log(Tolerance) / log(fabs(z)));
	}
	if (Horizon < DataLength) {
		/* accelerated loop */
		zn = z;
		Sum = c[0];
		for (n = 1L; n < Horizon; n++) {
			Sum += zn * c[n];
			zn *= z;
		}
		return(Sum);
	}
	else {
		/* full loop */
		zn = z;
		iz = 1.0 / z;
		z2n = pow(z, (double)(DataLength - 1L));
		Sum = c[0] + z2n * c[DataLength - 1L];
		z2n *= z2n * iz;
		for (n = 1L; n <= DataLength - 2L; n++) {
			Sum += (zn + z2n) * c[n];
			zn *= z;
			z2n *= iz;
		}
		return(Sum / (1.0 - zn * zn));
	}
}



static void
GetColumn (
	   float	*Image,		/* input image array */
	   long	        Width,		/* width of the image */
	   long	        x,		/* x coordinate of the selected line */
	   double	Line[],		/* output linear array */
	   long	        Height		/* length of the line */
	   )
/*--------------------------------------------------------------------------*/
{ /* begin GetColumn */
	long	y;
	Image = Image + (ptrdiff_t)x;
	for (y = 0L; y < Height; y++) {
		Line[y] = (double)*Image;
		Image += (ptrdiff_t)Width;
	}
} /* end GetColumn */



static void
GetRow (
	float	*Image,		/* input image array */
	long	y,		/* y coordinate of the selected line */
	double	Line[],		/* output linear array */
	long	Width		/* length of the line */
	)
/*--------------------------------------------------------------------------*/
{
	long	x;
	Image = Image + (ptrdiff_t)(y * Width);
	for (x = 0L; x < Width; x++) {
		Line[x] = (double)*Image++;
	}
}



static double
InitialAntiCausalCoefficient (
			      double	c[],		/* coefficients */
			      long	DataLength,	/* number of samples or coefficients */
			      double	z		/* actual pole */
			      )
/*--------------------------------------------------------------------------*/
{
	/* this initialization corresponds to mirror boundaries */
	return((z / (z * z - 1.0)) * (z * c[DataLength - 2L] + c[DataLength - 1L]));
}



static void
PutColumn (
	   float	*Image,		/* output image array */
	   long	Width,		        /* width of the image */
	   long	x,			/* x coordinate of the selected line */
	   double	Line[],		/* input linear array */
	   long	Height		        /* length of the line and height of the image */
	   )
/*--------------------------------------------------------------------------*/
{
	long	y;
	Image = Image + (ptrdiff_t)x;
	for (y = 0L; y < Height; y++) {
		*Image = (float)Line[y];
		Image += (ptrdiff_t)Width;
	}
}



static void
PutRow (
	float	*Image,		/* output image array */
	long	y,		/* y coordinate of the selected line */
	double	Line[],		/* input linear array */
	long	Width		/* length of the line and width of the image */
	)
/*--------------------------------------------------------------------------*/
{
	long	x;
	Image = Image + (ptrdiff_t)(y * Width);
	for (x = 0L; x < Width; x++) {
		*Image++ = (float)Line[x];
	}
}



static int
SamplesToCoefficients (
		       float	*Image,		/* in-place processing */
		       long	Width,		/* width of the image */
		       long	Height,		/* height of the image */
		       long	SplineDegree    /* degree of the spline model */
		       )
/*--------------------------------------------------------------------------*/
{
	double	*Line;
	double	Pole[2];
	long	NbPoles;
	long	x, y;
	/* recover the poles from a lookup table */

	switch (SplineDegree) {
		case 2L:
			NbPoles = 1L;
			Pole[0] = sqrt(8.0) - 3.0;
			break;
		case 3L:
			NbPoles = 1L;
			Pole[0] = sqrt(3.0) - 2.0;
			break;
		case 4L:
			NbPoles = 2L;
			Pole[0] = sqrt(664.0 - sqrt(438976.0)) + sqrt(304.0) - 19.0;
			Pole[1] = sqrt(664.0 + sqrt(438976.0)) - sqrt(304.0) - 19.0;
			break;
		case 5L:
			NbPoles = 2L;
			Pole[0] = sqrt(135.0 / 2.0 - sqrt(17745.0 / 4.0)) + sqrt(105.0 / 4.0)
				- 13.0 / 2.0;
			Pole[1] = sqrt(135.0 / 2.0 + sqrt(17745.0 / 4.0)) - sqrt(105.0 / 4.0)
				- 13.0 / 2.0;
			break;
		default:
			printf("Invalid spline degree\n");
			return(1);
	}

/*
 * convert the image samples into interpolation coefficients
 * in-place separable process, along x
 */
	Line = (double *)malloc((size_t)(Width * (long)sizeof(double)));
	if (Line == (double *)NULL) {
		printf("Row allocation failed\n");
		return(1);
	}
 

/* BUGFIX SamplesToCoefficients: OMP causes error */
/* #pragma omp parallel for */
	for (y = 0L; y < Height; y++) {
		GetRow(Image, y, Line, Width);
		ConvertToInterpolationCoefficients(Line, Width, Pole, NbPoles, 
                                                   DBL_EPSILON);
		PutRow(Image, y, Line, Width);
	}


	free(Line);
	/* in-place separable process, along y */
	Line = (double *)malloc((size_t)(Height * (long)sizeof(double)));
	if (Line == (double *)NULL) {
		printf("Column allocation failed\n");
		return(1);
	}


/* BUGFIX SamplesToCoefficients: OMP causes error */
/* #pragma omp parallel for */
	for (x = 0L; x < Width; x++) {
		GetColumn(Image, Width, x, Line, Height);
		ConvertToInterpolationCoefficients(Line, Height, Pole, NbPoles, DBL_EPSILON);
		PutColumn(Image, Width, x, Line, Height);
	}


	free(Line);
	return(0);
}



static double	
InterpolatedValue(
                  float	*Bcoeff,	/* input B-spline array of coefficients */
		  const long   Width,		/* width of the image */
		  const long   Height,	/* height of the image */
		  const double x,	      	/* x coordinate where to interpolate */
		  const double y,	       	/* y coordinate where to interpolate */
		  const long SplineDegree     /* degree of the spline model */
		  )
/*--------------------------------------------------------------------------*/
{
    float	*p;
    double	xWeight[6], yWeight[6];
    double	interpolated;
    double	w, w2, w4, t, t0, t1;
    long	xIndex[6], yIndex[6];
    long	Width2 = 2L * Width - 2L, Height2 = 2L * Height - 2L;
    long	i, j, k;

/*
 * compute the interpolation indexes
 */
    if (SplineDegree & 1L) {
        i = (long)floor(x) - SplineDegree / 2L;
        j = (long)floor(y) - SplineDegree / 2L;
        for (k = 0L; k <= SplineDegree; k++) {
            xIndex[k] = i++;
            yIndex[k] = j++;
        }
    } else {
        i = (long)floor(x + 0.5) - SplineDegree / 2L;
        j = (long)floor(y + 0.5) - SplineDegree / 2L;
        for (k = 0L; k <= SplineDegree; k++) {
            xIndex[k] = i++;
            yIndex[k] = j++;
        }
    }

/*
 * compute the interpolation weights
 */
    switch (SplineDegree) {
    case 2L:
/*
 * x
 */
        w = x - (double)xIndex[1];
        xWeight[1] = 3.0 / 4.0 - w * w;
        xWeight[2] = (1.0 / 2.0) * (w - xWeight[1] + 1.0);
        xWeight[0] = 1.0 - xWeight[1] - xWeight[2];
/*
 * y
 */
        w = y - (double)yIndex[1];
        yWeight[1] = 3.0 / 4.0 - w * w;
        yWeight[2] = (1.0 / 2.0) * (w - yWeight[1] + 1.0);
        yWeight[0] = 1.0 - yWeight[1] - yWeight[2];
        break;

    case 3L:
/*
 * x
 */
        w = x - (double)xIndex[1];
        xWeight[3] = (1.0 / 6.0) * w * w * w;
        xWeight[0] = (1.0 / 6.0) + (1.0 / 2.0) * w * (w - 1.0) - xWeight[3];
        xWeight[2] = w + xWeight[0] - 2.0 * xWeight[3];
        xWeight[1] = 1.0 - xWeight[0] - xWeight[2] 
            - xWeight[3];
/*
 * y
 */
        w = y - (double)yIndex[1];
        yWeight[3] = (1.0 / 6.0) * w * w * w;
        yWeight[0] = (1.0 / 6.0) + (1.0 / 2.0) * w * (w - 1.0) - yWeight[3];
        yWeight[2] = w + yWeight[0] - 2.0 * yWeight[3];
        yWeight[1] = 1.0 - yWeight[0] - yWeight[2] 
            - yWeight[3];
        break;

    case 4L:
/*
 * x
 */
        w = x - (double)xIndex[2];
        w2 = w * w;
        t = (1.0 / 6.0) * w2;
        xWeight[0] = 1.0 / 2.0 - w;
        xWeight[0] *= xWeight[0];
        xWeight[0] *= (1.0 / 24.0) * xWeight[0];
        t0 = w * (t - 11.0 / 24.0);
        t1 = 19.0 / 96.0 + w2 * (1.0 / 4.0 - t);
        xWeight[1] = t1 + t0;
        xWeight[3] = t1 - t0;
        xWeight[4] = xWeight[0] + t0 + (1.0 / 2.0) * w;
        xWeight[2] = 1.0 - xWeight[0] - xWeight[1] - xWeight[3] - xWeight[4];
/*
 * y
 */
        w = y - (double)yIndex[2];
        w2 = w * w;
        t = (1.0 / 6.0) * w2;
        yWeight[0] = 1.0 / 2.0 - w;
        yWeight[0] *= yWeight[0];
        yWeight[0] *= (1.0 / 24.0) * yWeight[0];
        t0 = w * (t - 11.0 / 24.0);
        t1 = 19.0 / 96.0 + w2 * (1.0 / 4.0 - t);
        yWeight[1] = t1 + t0;
        yWeight[3] = t1 - t0;
        yWeight[4] = yWeight[0] + t0 + (1.0 / 2.0) * w;
        yWeight[2] = 1.0 - yWeight[0] - yWeight[1] - yWeight[3] - yWeight[4];
        break;

    case 5L:
/*
 * x
 */
        w = x - (double)xIndex[2];
        w2 = w * w;
        xWeight[5] = (1.0 / 120.0) * w * w2 * w2;
        w2 -= w;
        w4 = w2 * w2;
        w -= 1.0 / 2.0;
        t = w2 * (w2 - 3.0);
        xWeight[0] = (1.0 / 24.0) * (1.0 / 5.0 + w2 + w4) - xWeight[5];
        t0 = (1.0 / 24.0) * (w2 * (w2 - 5.0) + 46.0 / 5.0);
        t1 = (-1.0 / 12.0) * w * (t + 4.0);
        xWeight[2] = t0 + t1;
        xWeight[3] = t0 - t1;
        t0 = (1.0 / 16.0) * (9.0 / 5.0 - t);
        t1 = (1.0 / 24.0) * w * (w4 - w2 - 5.0);
        xWeight[1] = t0 + t1;
        xWeight[4] = t0 - t1;
/*
 * y
 */
        w = y - (double)yIndex[2];
        w2 = w * w;
        yWeight[5] = (1.0 / 120.0) * w * w2 * w2;
        w2 -= w;
        w4 = w2 * w2;
        w -= 1.0 / 2.0;
        t = w2 * (w2 - 3.0);
        yWeight[0] = (1.0 / 24.0) * (1.0 / 5.0 + w2 + w4) - yWeight[5];
        t0 = (1.0 / 24.0) * (w2 * (w2 - 5.0) + 46.0 / 5.0);
        t1 = (-1.0 / 12.0) * w * (t + 4.0);
        yWeight[2] = t0 + t1;
        yWeight[3] = t0 - t1;
        t0 = (1.0 / 16.0) * (9.0 / 5.0 - t);
        t1 = (1.0 / 24.0) * w * (w4 - w2 - 5.0);
        yWeight[1] = t0 + t1;
        yWeight[4] = t0 - t1;
        break;

    default:
        printf("Invalid spline degree\n");
        return(0.0);
    }

/*
 * apply the mirror boundary conditions
 */
    for (k = 0L; k <= SplineDegree; k++) {
        xIndex[k] = (Width == 1L) ? (0L) : ((xIndex[k] < 0L) ?
                                            (-xIndex[k] - Width2 
                                             * ((-xIndex[k]) / Width2))
                                            : (xIndex[k] - Width2 
                                               * (xIndex[k] / Width2)));
        if (Width <= xIndex[k]) {
            xIndex[k] = Width2 - xIndex[k];
        }
        yIndex[k] = (Height == 1L) ? (0L) : ((yIndex[k] < 0L) ?
                                             (-yIndex[k] - Height2 
                                              * ((-yIndex[k]) / Height2))
                                             : (yIndex[k] - Height2 
                                                * (yIndex[k] / Height2)));
        if (Height <= yIndex[k]) {
            yIndex[k] = Height2 - yIndex[k];
        }
    }
    
/*
 * perform interpolation
 */
    interpolated = 0.0;
    for (j = 0L; j <= SplineDegree; j++) {
        p = Bcoeff + (ptrdiff_t)(yIndex[j] * Width);
        w = 0.0;
        for (i = 0L; i <= SplineDegree; i++) {
            w += xWeight[i] * p[xIndex[i]];
        }
        interpolated += yWeight[j] * w;
    }
    return(interpolated);
}



static gchar *
imgproc_deform_frame_from_pivdata(guint16 **img,
                                  const GpivImagePar *image_par,
                                  const GpivPivData *piv_data,
                                  const gdouble magnitude)
/*-----------------------------------------------------------------------------
 */
{
    gchar *err_msg = NULL;
    gint i, j;
    GpivPivData *pd = NULL;
    gint k, l, count = 0, window = 4;
    gfloat dy_sum, dx_sum;


    if (img[0] == NULL) {
        err_msg = "imgproc_deform_frame_from_pivdata: image->frame1 == NULL";
        return (err_msg);
    }

    /*
     * Interpolate piv data at each pixel point of the images.
     */

    pd = gpiv_alloc_pivdata (image_par->ncolumns, 
                             image_par->nrows);
/* OK: results approved */
#pragma omp parallel for
    for (i = 0; i < pd->ny; i++) {
        for (j = 0; j < pd->nx; j++) {
            pd->point_x[i][j] = (float) j;
            pd->point_y[i][j] = (float) i;
        }
    }
    gpiv_piv_dxdy_at_new_grid (piv_data, pd);

#undef SMOOTH
#ifdef SMOOTH
    /*
     * Smoothing
     */
    for (i = 0; i < pd->ny; i++) {
        for (j = 0; j < pd->nx; j++) {
            for (k = -window / 2; k < window / 2; k++) {
                if (i + k > 0 && i + k < pd->ny) {
                    for (l = -window / 2; l < window / 2; l++) {
                        if (j + l > 0 && j + l < pd->nx) {
                            dy_sum += pd->dy[i+k][j+l];
                            dx_sum += pd->dx[i+k][j+l];
                            count++;
                        }
                    }
                }       
            }
            count = 0;
        }
    }
#endif /* SMOOTH */

/* OK: results approved */
#pragma omp parallel for
    for (i = 0; i < pd->ny; i++) {
        for (j = 0; j < pd->nx; j++) {
            pd->dx[i][j] = magnitude * pd->dx[i][j];
            pd->dy[i][j] = magnitude * pd->dy[i][j];
        }
    }

    imgproc_deform_frame (img, image_par, pd);


    gpiv_free_pivdata (pd);
#ifdef SMOOTH
#undefine SMOOTH
#endif
    return (err_msg);
}


static int
imgproc_deform_frame (guint16 **img,
                      const GpivImagePar *image_par,
                      const GpivPivData *piv_data
                      )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Image deformation/interpolation routine for a single image frame
 *      Origins from demo.c
 *
 * INPUTS:
 *      piv_data:       PIV data structure which defines shift/deformation at 
 *                      each image pixel
 *      img:            input image frame
 *
 * OUTPUTS:
 *      img:            deformed image frame
 *
 * RETURNS:
 *
 *---------------------------------------------------------------------------*/
{
    float	*ImageRasterArray = NULL, *OutputImage = NULL;
    float	*p;
    double	x1, y1;
    long	Width = image_par->ncolumns, Height = image_par->nrows;
    long	SplineDegree = 5;
    long	x, y;
    int		Masking = 1;
    int		Error;

    guint img_top = (1 << image_par->depth) - 1;
    unsigned char *Line;
    gint i, j;

/*     double	a11, a12, a21, a22; */
/*     double	x0, y0, x1, y1; */
/*     double	xOrigin = 0.0, yOrigin = 0.0; */
/*     double	Angle = 1.0, xShift = 0.0, yShift = 0.0; */

    long	rounded;


    ImageRasterArray = gpiv_vector (image_par->nrows * image_par->ncolumns);
    p = ImageRasterArray;

/* OK: results approved */
#pragma omp parallel for
    for (i = 0; i < image_par->nrows; i++) {
        for (j = 0; j < image_par->ncolumns; j++) {
            p[i*image_par->ncolumns + j] = (float) img[i][j];
/*             *p++ = (float) img[i][j]; */
        }
    }


/* 
 * convert between a representation based on image samples
 * and a representation based on image B-spline coefficients
 */
    if (Error = SamplesToCoefficients (ImageRasterArray, Width, Height, 
                                       SplineDegree)) {
        gpiv_free_vector (ImageRasterArray);
        g_warning("LIBGPIV internal error: imgproc_deform_frame: Change of basis failed");
        return(1);
    }

/*
 * visit all pixels of the output image and assign their value
 */
    OutputImage = gpiv_vector (image_par->nrows * image_par->ncolumns);
    p = OutputImage;

/* BUGFIX imgproc_deform_frame: OMP causes differences in results */
/* BUGFIX imgproc_deform_frame: When disabled OMP in imgproc_deform */
/* #pragma omp parallel for private(x1, y1) */
    for (i = 0; i < image_par->nrows; i++) {
        for (j = 0; j < image_par->ncolumns; j++) {
            x1 = (double) j - (double) piv_data->dx[i][j];
            y1 = (double) i - (double) piv_data->dy[i][j];
            if (Masking) {
                if ((x1 <= -0.5) 
                    || (((double)Width - 0.5) <= x1)
                    || (y1 <= -0.5) 
                    || (((double)Height - 0.5) <= y1)) {
/*                     *p++ = 0.0F; */
                    p[i*image_par->ncolumns + j] = 0.0F;
                } else {
/*                     *p++ = */
                    p[i*image_par->ncolumns + j] = 
                        (float)InterpolatedValue (ImageRasterArray, 
                                                  Width, Height,
                                                  x1, y1, SplineDegree);
                }
            } else {
/*                 *p++ = */
                p[i*image_par->ncolumns + j] = 
                    (float)InterpolatedValue (ImageRasterArray, 
                                              Width, Height,
                                              x1, y1, SplineDegree);
            }

        }
    }

/*
 * Store back into img
 */

    p = OutputImage;
/* OK: results approved */
#pragma omp parallel for private(rounded)
    for (i = 0; i < image_par->nrows; i++) {
        for (j = 0; j < image_par->ncolumns; j++) {
            rounded = (long)floor ((double) p[i*image_par->ncolumns + j] + 0.5);
            if (rounded < 0.0) rounded = 0.0;
            if (rounded > (gfloat) img_top /* 255.0 */) rounded = (gfloat) img_top /* 255.0 */;
            img[i][j] =  (guint16) rounded;
        }
    }



    gpiv_free_vector (ImageRasterArray);
    gpiv_free_vector (OutputImage);
    return(0);
}



