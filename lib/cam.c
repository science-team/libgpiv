/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2004, 2005, 2006, 2007, 2008
   Gerber van der Graaf <gerber_graaf@users.sourceforge.net>
   Julio Soria <julio.soria@eng.monash.edu.au>

   This file is part of libgpiv.

   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  



------------------------------------------------------------------------------
FILENAME:               cam.c
LIBRARY:                libgpiv
EXTERNAL FUNCTIONS:      
                        gpiv_cam_get_camvar
                        gpiv_cam_free_camvar
                        

LAST MODIFICATION DATE:  $Id: cam.c,v 1.3 2007-11-23 16:16:14 gerber Exp $
--------------------------------------------------------------------------- */


#ifdef ENABLE_CAM
#include <stdio.h>
#include <gpiv.h>



GpivCamVar *
gpiv_cam_get_camvar (const gboolean verbose
                     )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Get variables of connected cameras using firewire
 *---------------------------------------------------------------------------*/
{
    GpivCamVar *cam_var = g_new0 (GpivCamVar, 1);
    gchar *err_msg = NULL;
    gint err, i;
/*
 * Coriander stuff
 */
/*     Format7Info format7_info; */
/*     UIInfo *uiinfos; */


    cam_var->maxspeed = SPEED_400;
/*
 * port 0 is the first IEEE1394 card. We ONLY probe this one.
 */
    cam_var->port = 0;

/*
 * Open ohci and asign handle to it
 */
    cam_var->handle = dc1394_create_handle(cam_var->port);
    if (cam_var->handle == NULL) {
        g_message ("Unable to aquire a raw1394 handle\n\
did you insmod the drivers?");
        return NULL;
    }

    cam_var->numNodes = raw1394_get_nodecount(cam_var->handle);
    cam_var->camera_nodes = dc1394_get_camera_nodes(cam_var->handle, 
                                                    &cam_var->numCameras, 
                                                    verbose);
        fflush(stdout);

    if (cam_var->numCameras < 1) {
        raw1394_destroy_handle(cam_var->handle);
        g_message ("gpiv_cam_get_camvar: no cameras found");
        return NULL;
    }

/*
 * to prevent the iso-transfer bug from raw1394 system, check if
 * camera is highest node. For details see 
 * http://linux1394.sourceforge.net/faq.html#DCbusmgmt
 * and
 * http://sourceforge.net/tracker/index.php?func=detail&aid=435107&group_id=8157&atid=108157
 */
    if (cam_var->camera_nodes[0] == cam_var->numNodes-1) {
        err_msg = "\n\
Sorry, your camera is the highest numbered node\n\
of the bus, and has therefore become the root node.\n\
The root node is responsible for maintaining \n\
the timing of isochronous transactions on the IEEE \n\
1394 bus.  However, if the root node is not cycle master \n\
capable (it doesn't have to be), then isochronous \n\
transactions will not work.  The host controller card is \n\
cycle master capable, however, most cameras are not.\n\
\n\
The quick solution is to add the parameter \n\
attempt_root=1 when loading the OHCI driver as a \n\
module.  So please do (as root):\n\
\n\
rmmod ohci1394\n\
insmod ohci1394 attempt_root=1\n\
\n\
for more information see the FAQ at \n\
http://linux1394.sourceforge.net/faq.html#DCbusmgmt\n\
\n";
        g_message ("%s", err_msg);
        return NULL
    }
  
/*
 * from coriander:
 * allocate memory space for all camera infos & download all infos
 * to make an  array of cam_var variables, create camera struct
 * with:
    gint numNodes;
    gint numCameras;
    gint maxspeed;
    gint port;
    raw1394handle_t handle;
    nodeid_t *camera_nodes;
 */
/*     cam_var = (GpivCamVar*) calloc(camera->numCameras, */
/*                                    sizeof(dc1394_cameracapture) */
/*                                    + sizeof(dc1394_camerainfo) */
/*                                    + sizeof(dc1394_feature_info) */
/*                                    + sizeof(dc1394_feature_set) */
/*                                    + sizeof(dc1394_miscinfo)); */



    cam_var->capture = (dc1394_cameracapture*) calloc(cam_var->numCameras,
                                                  sizeof(dc1394_cameracapture));

    cam_var->camera = (dc1394_camerainfo*) calloc(cam_var->numCameras, 
                                                    sizeof(dc1394_camerainfo));

    cam_var->feature_info = (dc1394_feature_info*) calloc(cam_var->numCameras,
                                                          sizeof(dc1394_feature_info));
 
    cam_var->feature_set = (dc1394_feature_set*) calloc(cam_var->numCameras,
                                                        sizeof(dc1394_feature_set));

    cam_var->misc_info = (dc1394_miscinfo*) calloc(cam_var->numCameras,
                                          sizeof(dc1394_miscinfo));


    err = 1;
    for (i = 0; i < cam_var->numCameras; i++) {
/*         err *= dc1394_get_camera_misc_info(camera->handle,  */
/*                                            cam_var[i]->camera_nodes,  */
/*                                            &cam_var[i]->misc_info); */

        err *= dc1394_get_camera_misc_info(cam_var->handle, 
                                           cam_var->camera_nodes[i], 
                                           &cam_var->misc_info[i]);

        err *= dc1394_get_camera_feature(cam_var->handle, 
                                         cam_var->camera_nodes[i], 
                                         &cam_var->feature_info[i]);

        err *= dc1394_get_camera_info(cam_var->handle, 
                                      cam_var->camera_nodes[i], 
                                      &cam_var->camera[i]);

        err *= dc1394_get_camera_feature_set(cam_var->handle, 
                                             cam_var->camera[i].id, 
                                             &cam_var->feature_set[i]);
        if (!err) {
            g_message ("Could not get camera basic informations!");
            return NULL;
        }
/*         GetFormat7Capabilities(handle, camera[i].id, &format7_info[i]); */
        /*         image_pipes[i] = NULL; */
/*         uiinfos[i].test_pattern = 0; */
/*         uiinfos[i].want_to_display = 0; */
    }
    


/*     return (err_msg); */
    return cam_var;
}



gchar *
gpiv_cam_free_camvar (GpivCamVar *cam_var
                      )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Free memory variables of connected cameras using firewire
 *
 * INPUTS:
 *
 * OUTPUTS:
 *     cam_var:         structure of camera variables
 *
 * RETURNS:
 *     NULL on success or *err_msg on failure
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
/*         g_free(cam_var.capture); */
        g_free(cam_var->feature_set);
        g_free(cam_var->camera);
        g_free(cam_var->feature_info);
        g_free(cam_var->misc_info);

    return (err_msg);
}



#endif /* ENABLE_CAM */
