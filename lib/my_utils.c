/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2008 Gerber van der Graaf

   This file is part of libgpiv.
   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  



-------------------------------------------------------------------------------
FILENAME:                my_utils.c
LIBRARY:                 libgpiv
LAST MODIFICATION DATE:  $Id: my_utils.c,v 1.2 2008-10-09 14:02:03 gerber Exp $
 --------------------------------------------------------------------------- */

#include <string.h>
#include <gpiv.h>

FILE *
my_utils_fopen_tmp (const gchar *tmpfile,
		    const gchar *mode)
/*-----------------------------------------------------------------------------
 * Attempts to open a filepointer for TMPDIR/USER/tmpfile, else in 
 * TMPDIR/tmpfile
 *
 * RETURNS:
 * filepointer or NULL
 */
{
    FILE *fp = NULL;
    gchar *tmp_dir = g_strdup (g_get_tmp_dir ());
    gchar *user_name = g_strdup (g_get_user_name ());
    gchar *tmp_user_dir = g_strdup_printf ("%s/%s", tmp_dir, user_name);
    gchar *filename =  g_strdup_printf ("%s/%s/%s", tmp_dir, user_name, 
                                        tmpfile);
    gint return_val;


    if ((fp = fopen(filename, mode)) != NULL) {
        if ((return_val = g_mkdir(tmp_user_dir, 700)) != 0) {
            g_free (filename);
            filename = g_strdup_printf ("%s/%s", tmp_dir, tmpfile);
        }
        if ((fp = fopen(filename, mode)) == NULL) {
            gpiv_warning("LIBGPIV internal error: my_utils_fopen_tmp: Failure opening %s for output", 
                         filename);
            return NULL;
        }

    }


    g_free (tmp_dir);
    g_free (user_name);
    g_free (tmp_user_dir);
    g_free (filename);
    return fp;
}


gchar *
my_utils_write_tmp_image (GpivImage *image, 
			  const gchar *basename,
			  const gchar *message
			  )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Stores deformed image to file system with pre defined name to TMPDIR
 *     and prints message to stdout
 *
 * INPUTS:
 *     img1:                   first image of PIV image pair
 *     img2:                   second image of PIV image pair
 *     image_par:              image parameters to be stored in header
 *     verbose:                prints the storing to stdout
 *
 * OUTPUTS:
 *
 * RETURNS:
 *     char * to NULL on success or *err_msg on failure
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    gchar *tmp_dir = g_strdup (g_get_tmp_dir ());
    gchar *user_name = g_strdup (g_get_user_name ());
    gchar *filename = g_strdup_printf ("%s%s",  basename, GPIV_EXT_PNG_IMAGE);
    FILE *fp;


    if ((fp = my_utils_fopen_tmp (filename, "wb")) == NULL) {
        err_msg = "gpiv_piv_write_tmp_image: Failure opening for output";
        return err_msg;
    }

    g_message ("my_utils_write_tmp_image: %s %s",
               message, g_strdup_printf ("%s/%s/%s", tmp_dir, user_name, filename));

    if ((err_msg = 
         gpiv_write_png_image (fp, image, FALSE)) != NULL) {
        fclose (fp);
        return err_msg;
    }

    fclose(fp);
    g_free (tmp_dir);
    g_free (filename);
    return err_msg;
}

