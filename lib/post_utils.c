/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2007, 2008 Gerber van der Graaf

   This file is part of libgpiv.
   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  



-------------------------------------------------------------------------------
FILENAME:                utils.c
LIBRARY:                 libgpiv:
EXTERNAL FUNCTIONS:
                         gpiv_null_scdata
			 gpiv_alloc_scdata
			 gpiv_free_scdata

                         gpiv_null_bindata
			 gpiv_alloc_bindata
			 gpiv_free_bindata

LAST MODIFICATION DATE:  $Id: post_utils.c,v 1.2 2007-12-19 08:46:46 gerber Exp $
 --------------------------------------------------------------------------- */

#include <gpiv.h>


void
gpiv_null_scdata (GpivScalarData *scal_data
                  )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Sets all elements of scal_data structure to NULL
 *---------------------------------------------------------------------------*/
{
    scal_data->point_x = NULL;
    scal_data->point_y = NULL;
    scal_data->scalar = NULL;
    scal_data->flag = NULL;
}



GpivScalarData *
gpiv_alloc_scdata (const gint nx,
                   const gint ny
                   )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Allocates memory for GpivScalarData
 *
 * INPUTS:
 *      scal_data:      .nx and .ny members of GpivScalarData structure
 *
 * OUTPUTS:
 *      scal_data:       point_x, point_y, scalar, flag of GpivScalarData
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    GpivScalarData *scal_data = g_new0 (GpivScalarData, 1);
    gint i, j;


    if (ny <= 0 || nx <= 0) {
        gpiv_warning ("gpiv_alloc_scdata: nx = %d ny = %d",
                      nx, ny);
        return NULL;
    }
    scal_data->nx = nx;
    scal_data->ny = ny;

    scal_data->point_x = gpiv_matrix (scal_data->ny, scal_data->nx);
    scal_data->point_y = gpiv_matrix (scal_data->ny, scal_data->nx);
    scal_data->scalar = gpiv_matrix (scal_data->ny, scal_data->nx);
    scal_data->flag = gpiv_imatrix (scal_data->ny, scal_data->nx);
        
    for (i = 0; i < scal_data->ny; i++) {
        for (j = 0; j < scal_data->nx; j++) {
            scal_data->point_x[i][j] = 0.0;
            scal_data->point_y[i][j] = 0.0;
            scal_data->scalar[i][j] = 0.0;
            scal_data->flag[i][j] = 0;
        }
    }

    scal_data->scale = FALSE;
    scal_data->scale__set = FALSE;
    scal_data->comment = NULL;


    return scal_data;
}



void 
gpiv_free_scdata (GpivScalarData *scal_data
                  )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Frees memory for GpivScalarData
 *
 * INPUTS:
 *      scal_data:      scalar data structure
 *
 * OUTPUTS:
 *      scal_data:       NULL pointer to point_x, point_y, scalar, flag
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{

    g_return_if_fail (scal_data->point_x != NULL); /* gpiv_error ("scal_data->point_x not allocated"); */
    g_return_if_fail (scal_data->point_y != NULL); /* gpiv_error ("scal_data->point_y not allocated"); */
    g_return_if_fail (scal_data->scalar != NULL); /* gpiv_error ("scal_data->scalar not allocated"); */
    g_return_if_fail (scal_data->flag != NULL); /* gpiv_error ("scal_data->flag not allocated"); */
    
    gpiv_free_matrix (scal_data->point_x);
    gpiv_free_matrix (scal_data->point_y);
    gpiv_free_matrix (scal_data->scalar);
    gpiv_free_imatrix (scal_data->flag);
    
    g_free (scal_data->comment);
    gpiv_null_scdata (scal_data);
}


gchar *
gpiv_scalar_gnuplot (const gchar * fname_out, 
                     const gchar *title, 
                     const gchar *GNUPLOT_DISPLAY_COLOR, 
                     const gint GNUPLOT_DISPLAY_SIZE
                     )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Graphical output with gnuplot for scalar data
 *
 * INPUTS:
 *      fname_out:     file name containing plot
 *      title:         title of plot
 *      GNUPLOT_DISPLAY_COLOR:  display color of window containing graph
 *      GNUPLOT_DISPLAY_SIZE:   display size of window containing graph
 *
 * OUTPUTS:
 *
 * RETURNS:
 *     NULL on success or *err_msg on failure
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    FILE *fp_cmd;
    const gchar *tmp_dir = g_get_tmp_dir ();
    gchar *fname_loc = "gpiv_gnuplot.cmd";
    gchar fname_cmd[GPIV_MAX_CHARS];
    gchar command[GPIV_MAX_CHARS];


    snprintf (fname_cmd, GPIV_MAX_CHARS, "%s/%s", tmp_dir, fname_loc);
    if ((fp_cmd = fopen (fname_cmd, "w")) == NULL) {
        err_msg = "gpiv_scalar_gnuplot: Failure opening %s for output";
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }

    fprintf (fp_cmd, "set xlabel \"x (pixels)\"");
    fprintf (fp_cmd, "\nset ylabel \"y (pixels)\"");
    fprintf (fp_cmd, "\nset contour; set nosurface");
    fprintf (fp_cmd, "\nset view 0, 0");
    fprintf (fp_cmd,
	     "\nsplot \"%s\" title \"%s\" with lines",
	     fname_out, title);
    fprintf (fp_cmd, "\npause -1 \"Hit return to exit\"");
    fprintf (fp_cmd, "\nquit");
    fclose (fp_cmd);

    snprintf (command, GPIV_MAX_CHARS, "gnuplot -bg %s -geometry %dx%d %s",
	      GNUPLOT_DISPLAY_COLOR, GNUPLOT_DISPLAY_SIZE, 
	      GNUPLOT_DISPLAY_SIZE, fname_cmd);

    if (system (command) != 0) {
        err_msg = "gpiv_scalar_gnuplot: could not exec shell command";
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }


    return err_msg;
}


void
gpiv_null_bindata (GpivBinData *bin_data
                   )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Sets all elements of bin_data structure to NULL
 *
 * INPUTS:
 *      bin_data:      Input Bin data structure
 *
 * OUTPUTS:
 *      bin_data:      Output Bin data structure
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    bin_data->count = NULL;
    bin_data->bound = NULL;
    bin_data->centre = NULL;
}



GpivBinData *
gpiv_alloc_bindata (const guint nbins
                    )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Allocates memory for GpivScalarData
 *
 * INPUTS:
 *      bin_data:       nbins of GpivScalarData
 *
 * OUTPUTS:
 *      bin_data:       count, bound, centre of GpivScalarData
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{ 
    GpivBinData *bin_data = g_new0 (GpivBinData, 1);
    gint i;

    
    if (nbins == 0) {
        gpiv_warning ("GPIV_ALLOC_BINDATA: nbins = %d", nbins);
        return NULL;
    }
    bin_data->nbins = nbins;

    /* BUGFIX */
    /*     g_warning("gpiv_alloc_bindata:: 4/4 nbins = %d", bin_data->nbins); */
    bin_data->count = gpiv_ivector (bin_data->nbins);
    bin_data->bound = gpiv_vector (bin_data->nbins);
    bin_data->centre = gpiv_vector (bin_data->nbins);
        
    for (i = 0; i < bin_data->nbins; i++) {
        bin_data->count[i] = 0;
        bin_data->bound[i] = 0.0;
        bin_data->centre[i] = 0.0;
    }

    bin_data->min = 914.6e9;
    bin_data->max = -914.6e9;
    bin_data->comment = NULL;


    return bin_data;
}      



void 
gpiv_free_bindata (GpivBinData *bin_data
                   )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Frees memory for GpivScalarData
 *
 * INPUTS:
 *      bin_data:       data of bins
 *
 * OUTPUTS:
 *      bin_data:       NULL pointer to count, bound, centre
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{

    g_return_if_fail (bin_data->count != NULL); /* gpiv_error ("bin_data->count not allocated"); */
    g_return_if_fail (bin_data->bound != NULL); /* gpiv_error ("bin_data->bound not allocated"); */
    g_return_if_fail (bin_data->centre != NULL); /* gpiv_error ("bin_data->centre not allocated"); */
    
    /*     g_warning ("gpiv_free_bindata:: 0 nbins = %d", bin_data->nbins); */
    gpiv_free_ivector (bin_data->count);

    gpiv_free_vector (bin_data->centre);
    gpiv_free_vector (bin_data->bound);
    
    bin_data->bound = NULL;
    bin_data->centre = NULL;
    bin_data->count = NULL;
}


void 
gpiv_histo (const GpivPivData *data, 
            GpivBinData *klass
            )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Calculates histogram from GpivPivData (NOT from ScalarData!!)
 *
 * INPUTS:
 *      data:           Input data
 *
 * OUTPUTS:
 *      klass:          Output data
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    gint i, j, k;
    gint nx = data->nx, ny = data->ny, **peak_no = data->peak_no;
    float **snr = data->snr;
    float delta;
    float *bound = klass->bound, *centre = klass->centre;
    gint *count = klass->count, nbins = klass->nbins;
    

    g_return_if_fail (data->point_x != NULL); /* gpiv_error ("data->point_x not allocated"); */
    g_return_if_fail (data->point_y != NULL); /* gpiv_error ("data->point_y not allocated"); */
    g_return_if_fail (data->dx != NULL); /* gpiv_error ("data->dx not allocated"); */
    g_return_if_fail (data->dy != NULL); /* gpiv_error ("data->dy not allocated"); */
    g_return_if_fail (data->snr != NULL); /* gpiv_error ("data->snr not allocated"); */
    g_return_if_fail (data->peak_no != NULL); /* gpiv_error ("ata->peak_no not allocated"); */
    
    g_return_if_fail (klass->count != NULL); /* gpiv_error ("klass->count not allocated"); */
    g_return_if_fail (klass->bound != NULL); /* gpiv_error ("klass->bound not allocated"); */
    g_return_if_fail (klass->centre != NULL); /* gpiv_error ("klass->centre not allocated"); */
    
    klass->min = 10.0e+9, klass->max = -10.0e+9;
    /*
     * find min and max value
     */
    for (i = 0; i < ny; i++) {
	for (j = 0; j < nx; j++) {
	    if (peak_no[i][j] != -1) {

		if (snr[i][j] < klass->min)
		    klass->min = snr[i][j];
		if (snr[i][j] >= klass->max)
		    klass->max = snr[i][j];

	    }
	}
    }



    /*
     * Calculating boundaries of bins
     */
    delta = (klass->max - klass->min) / nbins;
    for (i = 0; i < nbins; i++) {
	centre[i] = klass->min + delta / 2.0 + (float) i *delta;
	count[i] = 0;
    }

    for (i = 0; i < nbins; i++) {
	bound[i] = klass->min + (float) i * delta;
    }



    /*
     * Sorting of snr data in bins
     */
    for (i = 0; i < ny; i++) {
	for (j = 0; j < nx; j++) {
	    if (peak_no[i][j] != -1) {

		for (k = 0; k < nbins; k++) {
		    if ((snr[i][j] > bound[k])
			&& (snr[i][j] <= bound[k] + delta)) {
			count[k] = count[k] + 1;
		    }

		}
	    }
	}
    }
}



void 
gpiv_cumhisto (const GpivPivData *data, 
               GpivBinData *klass
               )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Calculates cumulative histogram from GpivPivData (NOT from ScalarData!!)
 *
 * INPUTS:
 *      data:           Input data
 *
 * OUTPUTS:
 *      klass:          Output data
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    gint i, j, k;
    gint nx = data->nx, ny = data->ny, **peak_no = data->peak_no;
    float **snr = data->snr;
    float delta;
    float *bound = klass->bound, *centre = klass->centre;
    gint *count = klass->count, nbins = klass->nbins;


    g_return_if_fail (data->point_x != NULL); /* gpiv_error ("data->point_x not allocated"); */
    g_return_if_fail (data->point_y != NULL); /* gpiv_error ("data->point_y not allocated"); */
    g_return_if_fail (data->dx != NULL); /* gpiv_error ("data->dx not allocated"); */
    g_return_if_fail (data->dy != NULL); /* gpiv_error ("data->dy not allocated"); */
    g_return_if_fail (data->snr != NULL); /* gpiv_error ("data->snr not allocated"); */
    g_return_if_fail (data->peak_no != NULL); /* gpiv_error ("ata->peak_no not allocated"); */
    
    g_return_if_fail (klass->count != NULL); /* gpiv_error ("klass->count not allocated"); */
    g_return_if_fail (klass->bound != NULL); /* gpiv_error ("klass->bound not allocated"); */
    g_return_if_fail (klass->centre != NULL); /* gpiv_error ("klass->centre not allocated"); */
    
    klass->min = 10e+9, klass->max = -10e+9;
    /*
     * find min and max value
     */
    for (i = 0; i < ny; i++) {
	for (j = 0; j < nx; j++) {
	    if (peak_no[i][j] != -1) {

		if (snr[i][j] < klass->min)
		    klass->min = snr[i][j];
		if (snr[i][j] >= klass->max)
		    klass->max = snr[i][j];

	    }
	}
    }


    /*
     * Calculating boundaries of bins
     */
    delta = (klass->max - klass->min) / nbins;
    for (i = 0; i < nbins; i++) {
	centre[i] = klass->min + delta / 2.0 + (float) i *delta;
	count[i] = 0;
    }

    for (i = 0; i < nbins; i++) {
	bound[i] = klass->min + (float) i * delta;
    }


    /*
     * Sorting of snr data in bins
     */
    for (i = 0; i < ny; i++) {
	for (j = 0; j < nx; j++) {
	    if (peak_no[i][j] != -1) {

		for (k = 0; k < nbins; k++) {
		    if (snr[i][j] <= bound[k] + delta) {
			count[k] = count[k] + 1;
		    }

		}
	    }
	}
    }
}



gchar *
gpiv_histo_gnuplot (const gchar *fname_out, 
                    const gchar *title, 
                    const gchar *GNUPLOT_DISPLAY_COLOR,
                    const gint GNUPLOT_DISPLAY_SIZE
                    )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Plots histogram on screen with gnuplot
 *
 * INPUTS:
 *      fname_out:	output filename
 *      title:	        plot title
 *      GNUPLOT_DISPLAY_COLOR:  display color of window containing graph
 *      GNUPLOT_DISPLAY_SIZE:   display size of window containing graph
 *
 * OUTPUTS:
 *
 * RETURNS:
 *     NULL on success or *err_msg on failure
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    FILE *fp_cmd;
    const gchar *tmp_dir = g_get_tmp_dir ();
    gchar *fname_loc = "gpiv_gnuplot.cmd";
    gchar *function_name = "gpiv_histo_gnuplot";
    gchar fname_cmd[GPIV_MAX_CHARS];
    gchar command[GPIV_MAX_CHARS];

    
    snprintf (fname_cmd, GPIV_MAX_CHARS, "%s/%s", tmp_dir, fname_loc);
    if ((fp_cmd = fopen (fname_cmd,"w")) == NULL) { 
        err_msg = "GPIV_HISTO_GNUPLOT: Failure opening for output";
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }

    fprintf (fp_cmd, "set xlabel \"residu (pixels)\"");
    fprintf (fp_cmd, "\nset ylabel \"frequency\"");
    fprintf (fp_cmd, "\nplot \"%s\" title \"%s\" with boxes", 
             fname_out, title);
    
    fprintf (fp_cmd, "\npause -1 \"Hit return to exit\"");
    fprintf (fp_cmd, "\nquit");
    
    fclose (fp_cmd);
  
    
    snprintf (command, GPIV_MAX_CHARS, "gnuplot -bg %s -geometry %dx%d %s",
              GNUPLOT_DISPLAY_COLOR, GNUPLOT_DISPLAY_SIZE, 
              GNUPLOT_DISPLAY_SIZE, fname_cmd);
    
    if (system (command) != 0) {
        g_warning ("%s:%s could not exec shell command", 
                   LIBNAME, function_name);

        exit (1);
    }

    
    return err_msg;
}



