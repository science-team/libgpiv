/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008
   Gerber van der Graaf

   This file is part of libgpiv.
   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  



-------------------------------------------------------------------------------
FILENAME:                piv_utils.c
LIBRARY:                 libgpiv:
EXTERNAL FUNCTIONS:
                         gpiv_null_pivdata
			 gpiv_alloc_pivdata
			 gpiv_check_alloc_pivdata
			 gpiv_free_pivdata
                         gpiv_cp_pivdata
                         gpiv_0_pivdata
                         gpiv_add_dxdy_pivdata
                         gpiv_sum_dxdy_pivdata

                         gpiv_piv_mpi_scatter_pivdata
                         gpiv_piv_mpi_gather_pivdata
                         gpiv_piv_mpi_compute_counts
                         gpiv_piv_mpi_compute_displs
                         gpiv_piv_mpi_scatterv_pivdata
                         gpiv_piv_mpi_gatherv_pivdata
                         gpiv_piv_mpi_bcast_pivdata

LAST MODIFICATION DATE:  $Id: piv_utils.c,v 1.3 2008-09-25 13:19:53 gerber Exp $
 --------------------------------------------------------------------------- */

#include <gpiv.h>

static void
bounds_cov (GpivCov *cov,
            const guint int_size_0,
            const gboolean x_corr
            );


void
gpiv_null_pivdata (GpivPivData *piv_data
                   )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Sets all elements of piv_data structure to NULL
 *
 * INPUTS:
 *      piv_data:      Input PIV data structure
 *
 * OUTPUTS:
 *      piv_data:      Output PIV data structure
 *
 * RETURNS:
 *     void
 *---------------------------------------------------------------------------*/
{
    piv_data->point_x = NULL;
    piv_data->point_y = NULL;
    piv_data->dx = NULL;
    piv_data->dy = NULL;
    piv_data->snr = NULL;
    piv_data->peak_no = NULL;

    piv_data->comment = NULL;
    piv_data = NULL;
}



GpivPivData *
gpiv_alloc_pivdata (const guint nx,
                    const guint ny
                    )
/*-----------------------------------------------------------------------------
 */
{
    GpivPivData *piv_data = g_new0 (GpivPivData, 1);
    gint i, j;


    if (ny <= 0 || nx <= 0) {
        gpiv_warning ("gpiv_alloc_pivdata: nx = %d ny = %d",
                      nx, ny);
        return NULL;
    }
    piv_data->nx = nx;
    piv_data->ny = ny;

    piv_data->point_x = gpiv_matrix (piv_data->ny, piv_data->nx);
    piv_data->point_y = gpiv_matrix (piv_data->ny, piv_data->nx);
    piv_data->dx = gpiv_matrix (piv_data->ny, piv_data->nx);
    piv_data->dy = gpiv_matrix (piv_data->ny, piv_data->nx);
    piv_data->snr = gpiv_matrix (piv_data->ny, piv_data->nx);
    piv_data->peak_no = gpiv_imatrix (piv_data->ny, piv_data->nx);
        
/*
 * Initializing values for structure members
 */
#pragma omp parallel for
    for (i = 0; i < piv_data->ny; i++) {
        for (j = 0; j < piv_data->nx; j++) {
            piv_data->point_x[i][j] = 0.0;
            piv_data->point_y[i][j] = 0.0;
            piv_data->dx[i][j] = 0.0;
            piv_data->dy[i][j] = 0.0;
            piv_data->snr[i][j] = 0.0;
            piv_data->peak_no[i][j] = 0;
        }
    }


/*
 *Take some strange number as initial values for minima and maximuma
 */
    piv_data->mean_dx = 0.0;
    piv_data->sdev_dx = 0.0;
    piv_data->min_dx = 73.0e9;
    piv_data->max_dx = -73.0e9;

    piv_data->mean_dy = 0.0;
    piv_data->sdev_dy = 0.0;
    piv_data->min_dy = 73.0e9;
    piv_data->max_dy = -73.0e9;

    piv_data->mean_dz = 0.0;
    piv_data->sdev_dz = 0.0;
    piv_data->min_dz = 73.0e9;
    piv_data->max_dz = -73.0e9;

    piv_data->scale = FALSE;
    piv_data->scale__set = FALSE;
    piv_data->comment = NULL;


    return piv_data;
}



gchar *
gpiv_check_alloc_pivdata (const GpivPivData *piv_data
                          )
/*-----------------------------------------------------------------------------
 * Check if piv_data have been allocated
 */
{
    gchar *err_msg = NULL;


    g_return_val_if_fail (piv_data->point_x != NULL, 
                          "gpiv_check_alloc_pivdata: point_x != NULL");
    g_return_val_if_fail (piv_data->point_y != NULL, 
                          "gpiv_check_alloc_pivdata: point_y != NULL");
    g_return_val_if_fail (piv_data->dx != NULL, 
                          "gpiv_check_alloc_pivdata: dx != NULL");
    g_return_val_if_fail (piv_data->dy != NULL, 
                          "gpiv_check_alloc_pivdata: dy != NULL");
    g_return_val_if_fail (piv_data->snr != NULL, 
                          "gpiv_check_alloc_pivdata: snr != NULL");
    g_return_val_if_fail (piv_data->peak_no != NULL, 
                          "gpiv_check_alloc_pivdata: peak_no != NULL");


    return err_msg;
}



void 
gpiv_free_pivdata (GpivPivData *piv_data
                   )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Frees memory for GpivPivData
 *
 * INPUTS:
 *      piv_data:       PIV data structure
 *
 * OUTPUTS:
 *      piv_data:      NULL pointer to point_x, point_y, dx, dy, snr, peak_no
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    g_return_if_fail (piv_data != NULL);
    g_return_if_fail (piv_data->point_x != NULL); /* gpiv_error ("piv_data->point_x not allocated"); */
    g_return_if_fail (piv_data->point_y != NULL); /* gpiv_error ("piv_data->point_y not allocated");   */
    g_return_if_fail (piv_data->dx != NULL); /* gpiv_error ("piv_data->dx not allocated"); */
    g_return_if_fail (piv_data->dy != NULL); /* gpiv_error ("piv_data->dy not allocated"); */
    g_return_if_fail (piv_data->snr != NULL); /* gpiv_error ("piv_data->snr not allocated"); */
    g_return_if_fail (piv_data->peak_no != NULL); /* gpiv_error ("piv_data->peak_no not allocated"); */

    gpiv_free_matrix (piv_data->point_x);
    gpiv_free_matrix (piv_data->point_y);
    gpiv_free_matrix (piv_data->dx);
    gpiv_free_matrix (piv_data->dy);
    gpiv_free_matrix (piv_data->snr);
    gpiv_free_imatrix (piv_data->peak_no);

    g_free (piv_data->comment);
    gpiv_null_pivdata (piv_data);
}



GpivPivData *
gpiv_cp_pivdata (const GpivPivData *piv_data
                 )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Copies data from piv_data_in to piv_data_out.
 *
 * INPUTS:
 *     piv_data_in:   Input PIV data structure
 *
 *
 * RETURNS:
 *     piv_data_out on success or NULL on failure
 *---------------------------------------------------------------------------*/
{
    GpivPivData *piv_data_out = NULL;
    gchar *err_msg = NULL;
    gint i, j;


    if ((err_msg = gpiv_check_alloc_pivdata (piv_data)) != NULL) {
        gpiv_warning ("%s", err_msg);
        return NULL;
    }

    if ((piv_data_out = gpiv_alloc_pivdata (piv_data->nx, piv_data->ny))
        == NULL) {
        gpiv_warning ("gpiv_cp_piv_data: failing gpiv_alloc_piv_data");
        return NULL;
    }

#pragma omp parallel for
    for (i = 0; i < piv_data_out->ny; i++) {
        for (j = 0; j < piv_data_out->nx; j++) {
            piv_data_out->point_x[i][j] = piv_data->point_x[i][j];
            piv_data_out->point_y[i][j] = piv_data->point_y[i][j];
            piv_data_out->dx[i][j] = piv_data->dx[i][j];
            piv_data_out->dy[i][j] = piv_data->dy[i][j];
            piv_data_out->snr[i][j] = piv_data->snr[i][j];
            piv_data_out->peak_no[i][j] = piv_data->peak_no[i][j];
        }
    }
    

#ifdef DEBUG2
    gpiv_write_pivdata (NULL, piv_data_out, FALSE);
#endif
    return piv_data_out;
}



gchar *
gpiv_ovwrt_pivdata (const GpivPivData *piv_data_in,
                    GpivPivData *piv_data_out
                    )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Copies data from piv_data_in to piv_data_out.
 *
 * INPUTS:
 *     piv_data_in:   Input PIV data structure
 *
 *
 * RETURNS:
 *     piv_data_out on success or NULL on failure
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    gint i, j;


    if (piv_data_in->nx != piv_data_out->nx
        || piv_data_in->ny != piv_data_out->ny) {
        err_msg = "gpiv_ovwrt_pivdata: input and output data are of differnet sizes";
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }

/*
 * Checking input data if allocated
 */
    if ((err_msg = gpiv_check_alloc_pivdata (piv_data_in)) != NULL) {
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }


/*
 * Checking output data if allocated
 */
    if ((err_msg = gpiv_check_alloc_pivdata (piv_data_out)) != NULL) {
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }

/*
 * Overwriting
 */
#pragma omp parallel for
    for (i = 0; i < piv_data_in->ny; i++) {
        for (j = 0; j < piv_data_in->nx; j++) {
            piv_data_out->point_x[i][j] = piv_data_in->point_x[i][j];
            piv_data_out->point_y[i][j] = piv_data_in->point_y[i][j];
            piv_data_out->dx[i][j] = piv_data_in->dx[i][j];
            piv_data_out->dy[i][j] = piv_data_in->dy[i][j];
            piv_data_out->snr[i][j] = piv_data_in->snr[i][j];
            piv_data_out->peak_no[i][j] = piv_data_in->peak_no[i][j];
        }
    }
    

#ifdef DEBUG2
    gpiv_write_pivdata (NULL, piv_data_out, FALSE);
#endif
    return err_msg;
}



gchar *
gpiv_0_pivdata (GpivPivData *piv_data
                )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Sets estimators, snr and peak_nr of piv_data to 0 or 0.0. 
 *      The structure will have to be allocated before (with 
 *      gpiv_alloc_pivdata).
 *
 * INPUTS:
 *      piv_data:   PIV data structure
 *
 * OUTPUTS:
 *
 * RETURNS:
 *     gchar * to NULL on success or *err_msg on failure
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    gint i, j;


    /*     check_if_null(piv_data, "GPIV_0_PIVDATA: "); */
    /*     gchar *check_if_null(GpivPivData * piv_data, gchar * string) */
    if (piv_data->point_x == NULL) {
        err_msg = "gpiv_0_pivdata: piv_data->point_x == NULL";
        gpiv_warning ("%s", err_msg);
    }

    if (piv_data->point_y == NULL) {
        err_msg = "gpiv_0_pivdata: piv_data->point_y == NULL";
        gpiv_warning ("%s", err_msg);
    }

    if (piv_data->dx == NULL) {
        err_msg = "gpiv_0_pivdata: piv_data->dx == NULL";
        gpiv_warning ("%s", err_msg);
    }

    if (piv_data->dy == NULL) {
        err_msg = "gpiv_0_pivdata: piv_data->dy == NULL";
        gpiv_warning ("%s", err_msg);
    }

    if (piv_data->snr == NULL) {
        err_msg = "gpiv_0_pivdata: piv_data->snr == NULL";
        gpiv_warning ("%s", err_msg);
    }

    if (piv_data->peak_no == NULL) {
        err_msg = "gpiv_0_pivdata: piv_data->peak_no == NULL";
        gpiv_warning ("%s", err_msg);
    }


    if (err_msg != NULL) {
        return err_msg;
    }


#pragma omp parallel for
    for (i = 0; i < piv_data->ny; i++) {
        for (j = 0; j < piv_data->nx; j++) {
            /*             piv_data->point_x[i][j] = 0.0; */
            /*             piv_data->point_y[i][j] = 0.0; */
            piv_data->dx[i][j] = 0.0;
            piv_data->dy[i][j] = 0.0;
            piv_data->snr[i][j] = 0.0;
            piv_data->peak_no[i][j] = 0;
        }
    }
    

    return NULL;
}



gchar *
gpiv_add_dxdy_pivdata (const GpivPivData *piv_data_in,
                       GpivPivData *piv_data_out
                       )
/*-----------------------------------------------------------------------------
 *      Adds displacements (dx, dy), snr and peak_nr from piv_data_in to 
 *      piv_data_out. Both structures will have to be allocated before 
 *      (with gpiv_alloc_pivdata) and of same dimansions.
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    gint i, j;


    if (piv_data_in->nx != piv_data_out->nx
        || piv_data_in->ny != piv_data_out->ny) {
        err_msg = "gpiv_add_dxdy_pivdata: piv_data_in and piv_data_out are of different dimensions";
        return err_msg;
    }

    if ((err_msg = gpiv_check_alloc_pivdata (piv_data_in)) != NULL) {
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }

    if ((err_msg = gpiv_check_alloc_pivdata (piv_data_out)) != NULL) {
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }

#pragma omp parallel for
    for (i = 0; i < piv_data_out->ny; i++) {
        for (j = 0; j < piv_data_out->nx; j++) {
            piv_data_out->point_x[i][j] = piv_data_in->point_x[i][j];
            piv_data_out->point_y[i][j] = piv_data_in->point_y[i][j];
            piv_data_out->dx[i][j] += piv_data_in->dx[i][j];
            piv_data_out->dy[i][j] += piv_data_in->dy[i][j];
            piv_data_out->snr[i][j] = piv_data_in->snr[i][j];
            piv_data_out->peak_no[i][j] = piv_data_in->peak_no[i][j];
        }
    }
    

    return NULL;
}



gchar * 
gpiv_sum_dxdy_pivdata (const GpivPivData *gpd,
                       gfloat *sum
                       )
/*-----------------------------------------------------------------------------
 *      Adds all displacements in order to calculate residuals
 *      The structure will have to be allocated before (with 
 *      gpiv_alloc_pivdata).
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    gint i, j;


    if ((err_msg = gpiv_check_alloc_pivdata (gpd)) != NULL) {
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }


#pragma omp parallel for
    for (i = 0; i < gpd->ny; i++) {
        for (j = 0; j < gpd->nx; j++) {
            *sum += gpd->dx[i][j];
            *sum += gpd->dy[i][j];
        }
    }
    

    return NULL;
}



gchar *
gpiv_piv_gnuplot (const gchar *title, 
                  const gfloat gnuplot_scale,
                  const gchar *GNUPLOT_DISPLAY_COLOR, 
                  const guint GNUPLOT_DISPLAY_SIZE,
                  const GpivImagePar *image_par, 
                  const GpivPivPar *piv_par,
                  const GpivPivData *piv_data
                  )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Plots piv data as vectors on screen with gnuplot
 *
 * INPUTS:
 *      fname:         file name containing plot
 *      title:         title of plot
 *      gnuplot_scale: vector scale
 *      GNUPLOT_DISPLAY_COLOR:  display color of window containing graph
 *      GNUPLOT_DISPLAY_SIZE:   display size of window containing graph
 *      image_par:      image parameters
 *      piv_par:   piv evaluation parameters
 *      piv_data:       piv data
 *      RCSID:          program name and version that interrogated the image
 *
 * OUTPUTS:
 *
 * RETURNS:
 *     NULL on success or *err_msg on failure
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    FILE *fp_cmd;
    const gchar *tmp_dir = g_get_tmp_dir ();
    gchar *fname_loc = "gpiv_gnuplot.cmd";
    gchar command[GPIV_MAX_CHARS];
    gchar fname_cmd[GPIV_MAX_CHARS];
    gint i, j;


    snprintf (fname_cmd, GPIV_MAX_CHARS, "%s/%s", tmp_dir, fname_loc);
    
    if ((fp_cmd = fopen (fname_cmd, "w")) == NULL)
        gpiv_error ("gpiv_piv_gnuplot: error: Failure opening %s for output",
                    fname_cmd);
    
    fprintf (fp_cmd, "set xlabel \"x (pixels)\"");
    fprintf (fp_cmd, "\nset ylabel \"y (pixels)\"");
    fprintf (fp_cmd, "\nset title \"Piv of %s\" ", title);
    
    for (i = 0; i < piv_data->ny; i++) {
        for (j = 0; j < piv_data->nx; j++) {
            fprintf (fp_cmd, "\nset arrow from %f, %f to %f, %f",
                     piv_data->point_x[i][j], 
                     piv_data->point_y[i][j],
                     piv_data->point_x[i][j] + piv_data->dx[i][j] 
                     * gnuplot_scale,
                     piv_data->point_y[i][j] + piv_data->dy[i][j] 
                     * gnuplot_scale);
        }
    }
    
    fprintf (fp_cmd, "\nset nokey");
    if (piv_par->int_geo == GPIV_AOI) {
        fprintf (fp_cmd, "\nplot [%d:%d] [%d:%d] %d", 
                 piv_par->col_start, piv_par->col_end,
                 piv_par->row_start, piv_par->row_end,
                 piv_par->row_end);
    } else {
        fprintf (fp_cmd, "\nplot [%d:%d] [%d:%d] %d", 
                 0, image_par->ncolumns,
                 0, image_par->nrows,
                 piv_par->row_end);
        
    }
    fprintf (fp_cmd, "\npause -1 \"Hit return to exit\"");
    fprintf (fp_cmd, "\nquit");
    fclose (fp_cmd);
    
    snprintf (command, GPIV_MAX_CHARS, 
              "gnuplot -bg %s -geometry %dx%d %s",
              GNUPLOT_DISPLAY_COLOR, GNUPLOT_DISPLAY_SIZE, GNUPLOT_DISPLAY_SIZE, 
              fname_cmd);
    
    if (system (command) != 0) {
        err_msg = "gpiv_piv_gnuplot: could not exec shell command";
        gpiv_warning ("%s", err_msg);
        return err_msg;
    }


    return err_msg;
}



GpivCov *
gpiv_alloc_cov          (const guint int_size0,
                         const gboolean x_corr
                         )
/*---------------------------------------------------------------------------*/
/**
 *      Allocates memory for GpivCov
 *
 *      @param[in] int_size0     size of zere-padded interrogation area
 *      @param[in] x_corr        two frame image / cross correlation
 *      @return                  covariance on success or NULL on failure
 */
/*---------------------------------------------------------------------------*/
{
    GpivCov *cov = g_new0 (GpivCov, 1);

    bounds_cov (cov, int_size0, x_corr);
    cov->z = gpiv_matrix_index (cov->z_rl, cov->z_rh, cov->z_cl, cov->z_ch);

    return cov;
}



void 
gpiv_free_cov       (GpivCov *cov
                     )
/*-----------------------------------------------------------------------------
 */
{
    gpiv_free_matrix_index (cov->z, cov->z_rl, cov->z_rh, cov->z_cl, cov->z_ch);
    cov = NULL;
}

/*
 * Local functions
 */
static void
bounds_cov (GpivCov *cov,
            const guint int_size_0,
            const gboolean x_corr
            )
/* ----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Defines boundaries of array to use for pos. and neg. displacements
 *     in covariance[][] and puts the data in z[][].
 *     Uses area of -2 until int_size/2 for auto-corr
 *
 * INPUTS:
 *     int_size_0:     size of zere-padded interrogation area
 *     image_par:      image parameters
 *
 * OUTPUTS:
 *     cov:            structure containing covariance boundaries
 *
 * SOME MNENOSYNTACTICS ON ARRAY BOUNDARIES:
 *    z:               name of array
 *    r:               row
 *    c:               column
 *    p:               positive displacements; from 0 to int_size_0/4 of array
 *                     covariance
 *    n:               negative displacements; from 3*int_size_0/4 to
 *                     int_size_0 of array covariance
 *    l:               lowest index
 *    h:               highest index without n or p indicates general array
 *                     boundaries (both auto and cross correlation)
 *---------------------------------------------------------------------------*/
{
    g_return_if_fail (cov != NULL);

    if (x_corr == TRUE) {
	cov->z_rnl = 3.0 * (int_size_0) / 4 + 1;
	cov->z_rnh = int_size_0;
	cov->z_rpl = 0;
	cov->z_rph = int_size_0 / 4;
	cov->z_cnl = 3.0 * (int_size_0) / 4.0 + 1;
	cov->z_cnh = int_size_0;
	cov->z_cpl = 0;
	cov->z_cph = int_size_0 / 4;
    } else {
	cov->z_rnl = int_size_0 - 2;
	cov->z_rnh = int_size_0;
	cov->z_rpl = 0;
	cov->z_rph = int_size_0 / 4;
	cov->z_cnl = int_size_0 - 2;
	cov->z_cnh = int_size_0;
	cov->z_cpl = 0;
	cov->z_cph = int_size_0 / 4;
    }

    cov->z_rl = cov->z_rnl - int_size_0;
    cov->z_rh = cov->z_rph;
    cov->z_cl = cov->z_cnl - int_size_0;
    cov->z_ch = cov->z_cph;
}


#ifdef ENABLE_MPI
void
gpiv_piv_mpi_scatter_pivdata (GpivPivData *pd, 
                     GpivPivData *pd_scat, 
                     guint nprocs)
/*-----------------------------------------------------------------------------
  Scatters PivData structure for parallel processing with mpi
  (NOT: except of pd->nx and pd->ny, which should broadcasted in forward
  in order to allocate memory.)

  INPUT:
  pd 
  nprocs: number of processes

  OUTPUT:
  pd_scat: scattered piv data
*/
{
    gint root = 0;


    if (MPI_Scatter(*pd->point_x, (pd->nx * pd->ny)/nprocs, MPI_FLOAT, 
                    *pd_scat->point_x, (pd->nx * pd->ny)/nprocs, MPI_FLOAT, root, 
                    MPI_COMM_WORLD) 
        != MPI_SUCCESS) {
        gpiv_error("mpi_scatter_pivdata: An error ocurred in MPI_Scatter for point_x");
    }

    if (MPI_Scatter(*pd->point_y, (pd->nx * pd->ny)/nprocs, MPI_FLOAT, 
                    *pd_scat->point_y, (pd->nx * pd->ny)/nprocs, MPI_FLOAT, root, 
                    MPI_COMM_WORLD) 
        != MPI_SUCCESS) {
        gpiv_error("mpi_scatter_pivdata: An error ocurred in MPI_Scatter for point_y");
    }

    if (MPI_Scatter(*pd->dx, (pd->nx * pd->ny) / nprocs, MPI_FLOAT, 
                    *pd_scat->dx, (pd->nx * pd->ny)/nprocs, MPI_FLOAT, root, 
                    MPI_COMM_WORLD) 
        != MPI_SUCCESS) {
        gpiv_error("mpi_scatter_pivdata: An error ocurred in MPI_Scatter for dx");
    }

    if (MPI_Scatter(*pd->dy, (pd->nx * pd->ny) / nprocs, MPI_FLOAT, 
                    *pd_scat->dy, (pd->nx * pd->ny)/nprocs, MPI_FLOAT, root, 
                    MPI_COMM_WORLD) 
        != MPI_SUCCESS) {
        gpiv_error("mpi_scatter_pivdata: An error ocurred in MPI_Scatter for dy");
    }

    if (MPI_Scatter(*pd->snr, (pd->nx * pd->ny) / nprocs, MPI_FLOAT, 
                    *pd_scat->snr, (pd->nx * pd->ny)/nprocs, MPI_FLOAT, root, 
                    MPI_COMM_WORLD) 
        != MPI_SUCCESS) {
        gpiv_error("mpi_scatter_pivdata: An error ocurred in MPI_Scatter for snr");
    }

    if (MPI_Scatter(*pd->peak_no, (pd->nx * pd->ny) / nprocs, MPI_INT, 
                    *pd_scat->peak_no, (pd->nx * pd->ny)/nprocs, MPI_INT, root, 
                    MPI_COMM_WORLD) 
        != MPI_SUCCESS) {
        gpiv_error ("mpi_scatter_pivdata: An error ocurred in MPI_Scatter for peak_no");
    }
}


void 
gpiv_piv_mpi_gather_pivdata (GpivPivData *pd_scat, 
                    GpivPivData *pd, 
                    guint nprocs)
/*-----------------------------------------------------------------------------
 Gathers PivData structure for parallel processing with mpi.

 INPUT:
 pd_scat: scattered piv data

 OUTPUT:
 pd: 
*/
{
    gint root = 0;


    if (MPI_Gather(*pd_scat->point_x, (pd->nx * pd->ny)/nprocs, MPI_FLOAT, 
                   *pd->point_x, (pd->nx * pd->ny)/nprocs, MPI_FLOAT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS) {
        gpiv_error ("Libgpiv: mpi_gather_pivdata: An error ocurred in MPI_Gather");
    }

    if (MPI_Gather(*pd_scat->point_y, (pd->nx * pd->ny)/nprocs, MPI_FLOAT, 
                   *pd->point_y, (pd->nx * pd->ny)/nprocs, MPI_FLOAT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS) {
        gpiv_error ("Libgpiv: mpi_gather_pivdata: An error ocurred in MPI_Gather");
    }

    if (MPI_Gather(*pd_scat->dx, (pd->nx * pd->ny) / nprocs, MPI_FLOAT, 
                   *pd->dx, (pd->nx * pd->ny)/nprocs, MPI_FLOAT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS) {
        gpiv_error ("Libgpiv: mpi_gather_pivdata: An error ocurred in MPI_Gather");
    }

    if (MPI_Gather(*pd_scat->dy, (pd->nx * pd->ny) / nprocs, MPI_FLOAT, 
                   *pd->dy, (pd->nx * pd->ny)/nprocs, MPI_FLOAT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        ) {
        gpiv_error ("Libgpiv: mpi_gather_pivdata: An error ocurred in MPI_Gather");
    }

    if (MPI_Gather(*pd_scat->snr, (pd->nx * pd->ny) / nprocs, MPI_FLOAT, 
                   *pd->snr, (pd->nx * pd->ny)/nprocs, MPI_FLOAT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS) {
        gpiv_error ("Libgpiv: mpi_gather_pivdata: An error ocurred in MPI_Gather");
    }

    if (MPI_Gather(*pd_scat->peak_no, (pd->nx * pd->ny) / nprocs, MPI_INT, 
                   *pd->peak_no, (pd->nx * pd->ny)/nprocs, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS) {
        gpiv_error ("Libgpiv: mpi_gather_pivdata: An error ocurred in MPI_Gather");
    }
}



gint * 
gpiv_piv_mpi_compute_counts(const guint nx, 
                            const guint ny)
/*-----------------------------------------------------------------------------
 * Calculates counts and displc for scatterv rows of data
 */
{
    gint *counts = NULL;
    gint i, nprocs, rank;


    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
    MPI_Comm_size(MPI_COMM_WORLD, &rank);

    counts = gpiv_ivector (nprocs);
    counts[0] = (ny  / nprocs + (0 < (ny)%nprocs ? 1 : 0)) * nx;
#ifdef DEBUG
    if (rank ==0) g_message ("mpi_compute_counts:: counts[0] = %d", 
                             counts[0]);
#endif

    for (i = 1; i < nprocs; i++) {
        counts[i] = (ny  / nprocs + (i < (ny)%nprocs ? 1 : 0)) * nx;
#ifdef DEBUG
        if (rank ==0) g_message ("mpi_compute_counts:: counts[%d] = %d", 
                                 i, counts[i]);
#endif
    }


    return counts;
}


gint *
gpiv_piv_mpi_compute_displs(gint *counts, 
                            const guint nx, 
                            const guint ny)
/*-----------------------------------------------------------------------------
 * Calculates displc for scatterv rows of data
 * As rows are scattered, nx is needed for correctly calculation of 
 * displacements
 */
{
    gint *displs = NULL;
    gint i, nprocs, rank;


    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
    MPI_Comm_size(MPI_COMM_WORLD, &rank);

    displs = gpiv_ivector (nprocs);
    displs[0] = 0;
#ifdef DEBUG
    if (rank ==0) g_message ("mpi_compute_displs:: counts[0] = %d", 
                             displs[0]);
#endif

    for (i = 1; i < nprocs; i++) {
        displs[i] = displs[i-1] + counts[i-1];
#ifdef DEBUG
        if (rank ==0) g_message ("mpi_compute_displs:: displs[%d] = %d", 
                                 i, displs[i]);
#endif
    }


    return displs;
}


void
gpiv_piv_mpi_scatterv_pivdata (GpivPivData *pd, 
                               GpivPivData *pd_scat, 
                               gint *counts,
                               gint *displs)
/*-----------------------------------------------------------------------------
  Scatters PivData structure for parallel processing with mpi
  except of pd->nx and pd->ny, which should already be broadcasted for
  memory allocation.)

  INPUT:
  pd 
  nprocs: number of processes

  OUTPUT:
  pd_scat: scattered piv data
*/
{
    gint root = 0, rank;



    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
#ifdef DEBUG
    g_message ("mpi_scatterv_pivdata:: rank=%d counts[%d]=%d nx=%d ny=%d", 
               rank, rank, counts[rank], pd_scat->nx, pd_scat->ny);
#endif

    if (MPI_Scatterv(*pd->point_x, counts, displs, MPI_FLOAT, 
                    *pd_scat->point_x, counts[rank], MPI_FLOAT, root, 
                    MPI_COMM_WORLD) 
        != MPI_SUCCESS) {
        gpiv_error("mpi_scatterv_pivdata: An error ocurred in MPI_Scatter for point_x");
    }

    if (MPI_Scatterv(*pd->point_y, counts, displs, MPI_FLOAT, 
                    *pd_scat->point_y, counts[rank], MPI_FLOAT, root, 
                    MPI_COMM_WORLD) 
        != MPI_SUCCESS) {
        gpiv_error("mpi_scatterv_pivdata: An error ocurred in MPI_Scatter for point_y");
    }

    if (MPI_Scatterv(*pd->dx, counts, displs, MPI_FLOAT, 
                    *pd_scat->dx, counts[rank], MPI_FLOAT, root, 
                    MPI_COMM_WORLD) 
        != MPI_SUCCESS) {
        gpiv_error("mpi_scatterv_pivdata: An error ocurred in MPI_Scatter for dx");
    }

    if (MPI_Scatterv(*pd->dy, counts, displs, MPI_FLOAT, 
                    *pd_scat->dy, counts[rank], MPI_FLOAT, root, 
                    MPI_COMM_WORLD) 
        != MPI_SUCCESS) {
        gpiv_error("mpi_scatterv_pivdata: An error ocurred in MPI_Scatter for dy");
    }

    if (MPI_Scatterv(*pd->snr, counts, displs, MPI_FLOAT, 
                    *pd_scat->snr, counts[rank], MPI_FLOAT, root, 
                    MPI_COMM_WORLD) 
        != MPI_SUCCESS) {
        gpiv_error("mpi_scatterv_pivdata: An error ocurred in MPI_Scatter for snr");
    }

    if (MPI_Scatterv(*pd->peak_no, counts, displs, MPI_INT, 
                    *pd_scat->peak_no, counts[rank], MPI_INT, root, 
                    MPI_COMM_WORLD) 
        != MPI_SUCCESS) {
        gpiv_error ("mpi_scatterv_pivdata: An error ocurred in MPI_Scatter for peak_no");
    }

}



void 
gpiv_piv_mpi_gatherv_pivdata (GpivPivData *pd_scat, 
                              GpivPivData *pd, 
                              gint *counts,
                              gint *displs)
/*-----------------------------------------------------------------------------
 Gathers PivData structure for parallel processing with mpi

 INPUT:
 pd_scat: scattered piv data
 comm: mpi common

 OUTPUT:
 pd: 
*/
{
    gint root = 0, rank;



    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
#ifdef DEBUG
    g_message ("mpi_gatherv_pivdata:: 0 rank=%d counts[%d]=%d", 
               rank, rank, counts[rank]);
#endif

    if (MPI_Gatherv(*pd_scat->point_x, counts[rank], MPI_FLOAT, 
                   *pd->point_x, counts, displs, MPI_FLOAT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS) {
        gpiv_error ("Libgpiv: mpi_gatherv_pivdata: An error ocurred in MPI_Gather");
    }

    if (MPI_Gatherv(*pd_scat->point_y, counts[rank], MPI_FLOAT, 
                   *pd->point_y, counts, displs, MPI_FLOAT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS) {
        gpiv_error ("Libgpiv: mpi_gatherv_pivdata: An error ocurred in MPI_Gather");
    }

    if (MPI_Gatherv(*pd_scat->dx, counts[rank], MPI_FLOAT, 
                   *pd->dx, counts, displs, MPI_FLOAT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS) {
        gpiv_error ("Libgpiv: mpi_gatherv_pivdata: An error ocurred in MPI_Gather");
    }

    if (MPI_Gatherv(*pd_scat->dy, counts[rank], MPI_FLOAT, 
                   *pd->dy, counts, displs, MPI_FLOAT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        ) {
        gpiv_error ("Libgpiv: mpi_gatherv_pivdata: An error ocurred in MPI_Gather");
    }

    if (MPI_Gatherv(*pd_scat->snr, counts[rank], MPI_FLOAT, 
                   *pd->snr, counts, displs, MPI_FLOAT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS) {
        gpiv_error ("Libgpiv: mpi_gatherv_pivdata: An error ocurred in MPI_Gather");
    }

    if (MPI_Gatherv(*pd_scat->peak_no, counts[rank], MPI_INT, 
                   *pd->peak_no, counts, displs, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS) {
        gpiv_error ("Libgpiv: mpi_gatherv_pivdata: An error ocurred in MPI_Gather");
    }
}



void 
gpiv_piv_mpi_bcast_pivdata (GpivPivData *pd
                   )
/*-----------------------------------------------------------------------------
 Gathers PivData structure for parallel processing with mpi
 */
{
    gint root = 0;


    if (
        MPI_Bcast(*pd->point_x, pd->nx * pd->ny, MPI_FLOAT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        ) {
        gpiv_error("Libgpiv: mpi_bcast_pivdata: An error ocurred in MPI_Bcast");
    }

    if (
        MPI_Bcast(*pd->point_y, pd->nx * pd->ny, MPI_FLOAT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        ) {
        gpiv_error("Libgpiv: mpi_bcast_pivdata: An error ocurred in MPI_Bcast");
    }

    if (
        MPI_Bcast(*pd->dx, pd->nx * pd->ny, MPI_FLOAT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        ) {
        gpiv_error("Libgpiv: mpi_bcast_pivdata: An error ocurred in MPI_Bcast");
    }

    if (
        MPI_Bcast(*pd->dy, pd->nx * pd->ny, MPI_FLOAT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        ) {
        gpiv_error("Libgpiv: mpi_bcast_pivdata: An error ocurred in MPI_Bcast");
    }

    if (
        MPI_Bcast(*pd->snr, pd->nx * pd->ny, MPI_FLOAT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        ) {
        gpiv_error("Libgpiv: mpi_bcast_pivdata: An error ocurred in MPI_Bcast");
    }

    if (
        MPI_Bcast(*pd->peak_no, pd->nx * pd->ny, MPI_INT, root, MPI_COMM_WORLD) 
        != MPI_SUCCESS
        ) {
        gpiv_error("Libgpiv: mpi_bcast_pivdata: An error ocurred in MPI_Bcast");
    }
}
#endif /* ENABLE_MPI */
