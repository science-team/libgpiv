/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2006, 2007, 2008 Gerber van der Graaf

   This file is part of libgpiv.
   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.


-------------------------------------------------------------------------------
FILENAME:              io_png_image.c
LIBRARY:               libgpiv:
EXTERNAL FUNCTIONS:
                       gpiv_read_png_image
                       gpiv_write_png_image

LOCAL FUNCTIONS:
                        get_png_val
			matrix_png_byte
			free_matrix_png_byte
			get_imgpar_from_textchunks
			set_textchunks_from_imgpar

LAST MODIFICATION DATE:  $Id: io_png.c,v 1.6 2008-09-25 13:19:53 gerber Exp $
 --------------------------------------------------------------------------- */
#include <gpiv.h>
/* #include "libgpiv_private.h" */


#define PNM_GET1(x) PPM_GETB(x)
#define PPM_GETB(x) ((x) & 0x3ff)
#define PNG_BYTES_TO_CHECK 4
#define PNG_KEY__PIV_XCORR "Piv: Xcorr (TRUE/FALSE)"
#define PNG_KEY__PIV_DT    "Piv: Delta_t (milliseconds)"


static png_uint_16 
get_png_val (png_byte **pp, 
	     int bit_depth
	     );

static png_byte **
matrix_png_byte (long nr, 
                 long nc
                 );

static void 
free_matrix_png_byte (png_byte **m
                      );

static void
get_imgpar_from_textchunks (GpivImagePar *image_par, 
                            png_structp png_ptr,
                            png_infop info_ptr
                            );
static void
set_textchunks_from_imgpar (GpivImagePar *image_par, 
                            png_structp png_ptr, 
                            png_infop info_ptr
                            );


void
print_img_parameters_set (GpivImagePar *gpiv_image_par)
{
    printf ("ncolumns__set = %d \n", gpiv_image_par->ncolumns__set);
    printf ("nrows__set = %d \n", gpiv_image_par->nrows__set);
    printf ("x_corr__set = %d \n", gpiv_image_par->x_corr__set);
    printf ("depth__set = %d \n", gpiv_image_par->depth__set);
    printf ("s_scale__set = %d \n", gpiv_image_par->s_scale__set);
    printf ("t_scale__set = %d \n", gpiv_image_par->t_scale__set);
    printf ("z_off_x__set = %d \n", gpiv_image_par->z_off_x__set);
    printf ("z_off_y__set = %d \n", gpiv_image_par->z_off_y__set);
    printf ("title__set = %d \n", gpiv_image_par->title__set);
    printf ("creation_date__set = %d \n", gpiv_image_par->creation_date__set);
    printf ("location__set = %d \n", gpiv_image_par->location__set);
    printf ("author__set = %d \n", gpiv_image_par->author__set);
    printf ("software__set = %d \n", gpiv_image_par->software__set);
    printf ("source__set = %d \n", gpiv_image_par->source__set);
    printf ("usertext__set = %d \n", gpiv_image_par->usertext__set);
    printf ("warning__set = %d \n", gpiv_image_par->warning__set);
    printf ("disclaimer__set = %d \n", gpiv_image_par->disclaimer__set);
    printf ("comment__set = %d \n", gpiv_image_par->comment__set);
    printf ("copyright__set = %d \n", gpiv_image_par->copyright__set);
    printf ("email__set = %d \n", gpiv_image_par->email__set);
    printf ("url__set = %d \n", gpiv_image_par->url__set);
}

/*
 * Public functions
 */

GpivImage *
gpiv_read_png_image (FILE *fp
                     )
/*---------------------------------------------------------------------------*/
/**
 *     Reads png formatted image.
 *
 *     @param[in] fp           input file
 *     @return                 GpivImage
 */
/*---------------------------------------------------------------------------*/
{
    GpivImage *gpiv_image = NULL;
    GpivImagePar *gpiv_image_par = g_new0 (GpivImagePar, 1);

    char buf[PNG_BYTES_TO_CHECK];
    
    png_structp png_ptr;
    png_infop info_ptr, end_info_ptr;
    unsigned int sig_read = 0;
    png_uint_32 width, height;
    gint bit_depth, color_type, interlace_type, compression_type, 
        unit_type_offs, filter_method;

    gint row, column;
    png_int_32  x_offset = 0, y_offset = 0;
    double res_x = 0;
    gint linesize;
    png_byte **png_image = NULL;
    png_byte *png_rowpointer;

    
/*     @param[in] allocate     boolean whether to allocate memory for image arrays
 *     @param[in] x_corr__set  boolean if x_corr has been defined
 *     @param[in] x_corr       boolean whether image contains two frames for cross-correlation.
 *                             Will only be used if absent in header of input image and if 
 *                              x_corr__set = TRUE
 */

#ifdef PNG_tIME_SUPPORTED
    png_timep mod_time;
#endif /* PNG_tIME_SUPPORTED */

    gpiv_img_parameters_set (gpiv_image_par, FALSE);
#ifdef DEBUG
    g_message ("gpiv_read_png_image:: 00");
#endif

    /* Read in some of the signature bytes */
    if (fread (buf, 1, PNG_BYTES_TO_CHECK, fp) != PNG_BYTES_TO_CHECK) {
        g_warning ("gpiv_read_png_image: failing fread (buf, 1, PNG_BYTES_TO_CHECK, fp)");
        return NULL;
    }

    /* Testing the signature bytes */
    if (png_sig_cmp ((png_bytep)buf, (png_size_t)0, PNG_BYTES_TO_CHECK) != 0) {
        g_warning ("gpiv_read_png_image: failing signature bytes test");
        return NULL;
    } else {
        sig_read = 1;
    }
    
    png_ptr = png_create_read_struct (PNG_LIBPNG_VER_STRING,
                                      png_voidp_NULL, png_error_ptr_NULL, 
                                      png_error_ptr_NULL);
    if (png_ptr == NULL) {
        g_warning ("gpiv_read_png_image: failing png_create_read_struct()");
        return NULL;
    }
   
    info_ptr = png_create_info_struct (png_ptr);
    if (info_ptr == NULL) {
        fclose (fp);
        png_destroy_read_struct (&png_ptr, png_infopp_NULL, png_infopp_NULL);
        g_warning ("gpiv_read_png_image: failing png_create_info_struct (png_ptr)");
        return NULL;
    }
    
    end_info_ptr = png_create_info_struct (png_ptr);
    
    if (setjmp (png_jmpbuf (png_ptr))) {
        /* Free all of the memory associated with the png_ptr and info_ptr */
        /* If we get here, we had a problem reading the file */
        png_destroy_read_struct (&png_ptr, &info_ptr, 
                                 /* NULL */ &end_info_ptr );
        g_warning ("gpiv_read_png_image: failing setjmp (png_jmpbuf (png_ptr)");
        return NULL;
    }

    png_init_io (png_ptr, fp);

    /* If we have already read some of the signature */
    if (sig_read == 1) {
        png_set_sig_bytes (png_ptr, PNG_BYTES_TO_CHECK);
    }
    
#ifdef HILEVEL
    png_read_png (png_ptr, info_ptr, PNG_TRANSFORM_IDENTITY, png_voidp_NULL);
#else
    png_read_info (png_ptr, info_ptr);
#endif /* HILEVEL */


    /* Quering image info: */
    png_get_IHDR (png_ptr, info_ptr, &width, &height, &bit_depth, &color_type,
                  &interlace_type, &compression_type, &filter_method);

    /* Other metadata chunks: */
#ifdef PNG_oFFs_SUPPORTED
    png_get_oFFs (png_ptr, info_ptr, &x_offset, &y_offset, &unit_type_offs);
#endif /* PNG_oFFs_SUPPORTED */

#ifdef PNG_pCAL_SUPPORTED
    res_x = png_get_x_pixels_per_meter (png_ptr, info_ptr);
#endif /* PNG_pCAL_SUPPORTED */

#ifdef PNG_tIME_SUPPORTED
    png_get_tIME (png_ptr, info_ptr, &mod_time);
#endif /* PNG_tIME_SUPPORTED */


#ifdef PNG_TEXT_SUPPORTED
    get_imgpar_from_textchunks (gpiv_image_par, png_ptr, info_ptr);
#endif /* PNG_TEXT_SUPPORTED */

/*     if (gpiv_image_par.x_corr__set == FALSE) { */
/*         g_warning ("GPIV_READ_PNG_IMAGE: couldn't read or x_corr"); */
/*         return NULL; */
/*     } */

#ifdef DEBUG
    printf ("\n");
    printf ("Image info:\nwidth = %d height = %d depth = %d \n",
            (int)width,
            (int)height,
            (int)bit_depth
            );

    if (color_type == PNG_COLOR_TYPE_GRAY) {
        printf ("color_type = PNG_COLOR_TYPE_GRAY\n");
    } else if (color_type == PNG_COLOR_TYPE_GRAY_ALPHA) {
        printf ("color_type = PNG_COLOR_TYPE_GRAY_ALPHA\n");
    } else if (color_type == PNG_COLOR_TYPE_PALETTE) {
        printf ("color_type = PNG_COLOR_TYPE_PALETTE\n");
    } else if (color_type == PNG_COLOR_TYPE_RGB) {
        printf ("color_type = PNG_COLOR_TYPE_RGB\n");
    } else if (color_type == PNG_COLOR_TYPE_RGB_ALPHA) {
        printf ("color_type = PNG_COLOR_TYPE_RGB_ALPHA\n");
    } else if (color_type == PNG_COLOR_MASK_PALETTE) {
        printf ("color_type = PNG_COLOR_MASK_PALETTE\n");
    } else if (color_type == PNG_COLOR_MASK_COLOR) {
        printf ("color_type = PNG_COLOR_MASK_COLOR\n");
    } else if (color_type == PNG_COLOR_MASK_ALPHA) {
        printf ("color_type = PNG_COLOR_MASK_ALPHA\n");
    } else {
        g_warning ("gpiv_read_png_image: no existing color type");
        return NULL;
    }

    if (compression_type == PNG_COMPRESSION_TYPE_DEFAULT) {
        printf ("compression_type = PNG_COMPRESSION_TYPE_DEFAULT\n");
    } else {
        g_warning ("gpiv_read_png_image: no existing compression type");
        return NULL;
   }
        
    if (interlace_type == PNG_INTERLACE_NONE) {
        printf ("interlace_type = PNG_INTERLACE_NONE\n");
    } else if (interlace_type == PNG_INTERLACE_ADAM7) {
        printf ("interlace_type = PNG_INTERLACE_ADAM7\n");
    } else {
        g_warning ("gpiv_read_png_image: no existing interlace type");
        return NULL;
    }
    

#ifdef PNG_oFFs_SUPPORTED
    printf ("x_offset = %d y_offset = %d\n", 
            (int)x_offset, (int)y_offset);
#endif /* PNG_oFFs_SUPPORTED */

#ifdef PNG_pCAL_SUPPORTED
    printf ("resolution (pixels/meter) res_x = %d \n", 
            (int)res_x);
#endif

#ifdef PNG_tIME_SUPPORTED
    printf ("Modification GMT time: (Y/M/D: h/m/s) %d/%d/%d: %d/%d/%d \n", 
            mod_time->year, mod_time->month, mod_time->day,
            mod_time->hour, mod_time->minute, mod_time->second
            );
#endif /* PNG_tIME_SUPPORTED */

    printf ("\n");
#endif /* DEBUG */


/*
 * If the image is of correct format GRAY and has no INTERLACE, apply header 
 * info to gpiv_image structure
 */
    gpiv_image_par->depth = bit_depth;
    gpiv_image_par->depth__set = TRUE;

    if (color_type != PNG_COLOR_TYPE_GRAY) {
        g_warning ("gpiv_read_png_image: image is not PNG_COLOR_TYPE_GRAY");
        return NULL;
    }

    if (interlace_type != PNG_INTERLACE_NONE) {
        g_warning ("gpiv_read_png_image: image is not PNG_INTERLACE_NONE");
        return NULL;
    }

    if (info_ptr->bit_depth == 16) {
        linesize = 2 * width;
    } else {
        linesize = width;
    }
    gpiv_image_par->ncolumns = width;
    gpiv_image_par->ncolumns__set = TRUE;


#ifdef GPIV_IMG_PARAM_RESOURCES
#ifdef DEBUG
    gpiv_scan_resourcefiles (GPIV_IMGPAR_KEY, gpiv_image_par, TRUE);
#else
    gpiv_scan_resourcefiles (GPIV_IMGPAR_KEY, gpiv_image_par, FALSE);
#endif /* DEBUG */
#endif /* GPIV_IMG_PARAM_RESOURCES */

    if (gpiv_image_par->x_corr__set == FALSE) {
        g_warning ("gpiv_read_png_image: couldn't read or x_corr");
        return NULL;
    }
    
    if (gpiv_image_par->x_corr) {
        gpiv_image_par->nrows = height / 2;
    } else {
        gpiv_image_par->nrows = height;
    }
    gpiv_image_par->nrows__set = TRUE;
    
    
    if (res_x != 0.0) {
        gpiv_image_par->s_scale = res_x;
        gpiv_image_par->s_scale__set = TRUE;
    } else {
        gpiv_image_par->s_scale__set = FALSE;
    }


    /*
     * Allocating (png) image
     */
    gpiv_image = gpiv_alloc_img (gpiv_image_par);
    
    if ((png_image = matrix_png_byte (height, linesize)) == NULL) {
        png_destroy_read_struct (&png_ptr, &info_ptr, &end_info_ptr);
        g_warning ("gpiv_read_png_image: couldn't allocate space for image");
        return NULL;
    }


    /*
     * Obtaining image frame data
     */
#ifdef HILEVEL
    png_image = png_get_rows (png_ptr, info_ptr);
#else
    png_read_image (png_ptr, png_image);
    png_read_end (png_ptr, info_ptr);
#endif /* HILEVEL */


    /*
     * Copy png image data to img1, img2
     */
    for (row = 0; row < gpiv_image->header->nrows; row++) {
#ifdef DEBUG2
        printf ("\ngpiv_read_png_image: IMG1  row = %d img_png: \n", row);
#endif /* DEBUG2 */
        png_rowpointer = png_image[row];
        for (column = 0; column < gpiv_image->header->ncolumns; column++) {
            gpiv_image->frame1[row][column] = 
                get_png_val (&png_rowpointer, bit_depth);
#ifdef DEBUG2
            printf ("%d ", (int) gpiv_image->frame1[row][column]);
#endif /* DEBUG2 */
        }
    }

    if (gpiv_image->header->x_corr) {
        for (row = 0; row < gpiv_image->header->nrows; row++) {
#ifdef DEBUG2
            printf ("\ngpiv_read_png_image: IMG2  row = %d img_png: \n", 
                    row);
#endif /* DEBUG2 */
            png_rowpointer = png_image[row + gpiv_image->header->nrows];
            for (column = 0; column < gpiv_image->header->ncolumns; 
                 column++) {
                gpiv_image->frame2[row][column] = 
                    get_png_val (&png_rowpointer, bit_depth);
#ifdef DEBUG2
                printf ("%d ", (int) gpiv_image->frame2[row][column]);
#endif /* DEBUG2 */
            }
        }
    }

    /*
     * clean up after the read, and free any memory allocated
     */
    free_matrix_png_byte (png_image);


    png_destroy_read_struct (&png_ptr, &info_ptr, &end_info_ptr);
    return gpiv_image;
}



gchar *
gpiv_write_png_image (FILE *fp,
                      GpivImage *gpiv_image,
                      const gboolean free
                      )
/*------------------------------------------------------------------------------
 * DESCRIPTION:
 *     Writes image data and header to png formatted image. 
 *     Frees allocated image data.
 *
 *--------------------------------------------------------------------------- */
{
    gchar *err_msg = NULL;
/*     guint16 **img1, **img2; */

    png_structp png_ptr;
    png_infop info_ptr;
    png_uint_32 width = gpiv_image->header->ncolumns, height;
    gint bit_depth = gpiv_image->header->depth;
    gint  color_type = PNG_COLOR_TYPE_GRAY;
    gint interlace_type = PNG_INTERLACE_NONE;
    gint compression_type  = PNG_COMPRESSION_TYPE_BASE;
    gint filter_method = PNG_FILTER_TYPE_BASE;
    gint linesize = 2 * width;

#ifdef PNG_tIME_SUPPORTED
    time_t ltime_t;
    png_time mod_time;
#endif /* PNG_tIME_SUPPORTED */
    gint i = 0, j = 0;


    if (gpiv_image->frame1 == NULL) {
        err_msg = "gpiv_write_png_image: gpiv_image->frame1 == NULL";
        return (err_msg);
    }
    if (gpiv_image->header->x_corr
        && gpiv_image->frame2 == NULL) {
        err_msg = "gpiv_write_png_image: gpiv_image->frame2 == NULL";
        return (err_msg);
    }

    /*
     * Allocate png_ structures
     */
    png_ptr = png_create_write_struct (PNG_LIBPNG_VER_STRING,
                                       png_voidp_NULL, png_error_ptr_NULL, 
                                       png_error_ptr_NULL);
    if (png_ptr == NULL) {
        err_msg = "gpiv_write_png_image: failing png_create_write_struct()";
        return err_msg;
    }
   
    info_ptr = png_create_info_struct (png_ptr);
    if (info_ptr == NULL) {
        png_destroy_write_struct (&png_ptr, &info_ptr);
        err_msg = "gpiv_write_png_image: failing png_create_info_struct (png_ptr)";
        return err_msg;
    }

    if (setjmp (png_jmpbuf (png_ptr))) {
        png_destroy_write_struct (&png_ptr, &info_ptr);
        err_msg = "gpiv_write_png_image: failing at setjmp(png_jmpbuf (png_ptr)";
        return err_msg;
    }


    png_init_io (png_ptr, fp);

    /*
     * Apply gpiv_image->header to png header
     */
    if (gpiv_image->header->x_corr) {
        height = 2 * gpiv_image->header->nrows;
    } else {
        height = gpiv_image->header->nrows;
    }

    png_set_compression_level (png_ptr, Z_BEST_COMPRESSION);
    png_set_IHDR (png_ptr, info_ptr, width, height, bit_depth, color_type,
                  interlace_type, compression_type, filter_method);


#ifdef PNG_pCAL_SUPPORTED
    if (gpiv_image->header->s_scale__set) {
        png_set_sCAL (png_ptr, info_ptr, PNG_SCALE_METER,
                      (png_int_32) gpiv_image->header->s_scale * width, 
                      (png_int_32) gpiv_image->header->s_scale * height);
    }
#endif /* PNG_pCAL_SUPPORTED */

#ifdef PNG_oFFs_SUPPORTED
    if (gpiv_image->header->z_off_x__set 
        && gpiv_image->header->z_off_y__set) {
        /* / gpiv_image->header.sscale * 1.0e3 */ /* x_offset*/ 
        /* / gpiv_image->header.sscale * 1.0e3 */ /* y_offset*/ 
        png_set_oFFs (png_ptr, info_ptr, 
                      (png_uint_32) gpiv_image->header->z_off_x,
                      (png_uint_32) gpiv_image->header->z_off_y, 
                      PNG_OFFSET_PIXEL);
    }
#endif /* PNG_oFFs_SUPPORTED */


#ifdef PNG_tIME_SUPPORTED
    ltime_t = time (&ltime_t);
    png_convert_from_time_t (&mod_time, ltime_t);
    png_set_tIME (png_ptr, info_ptr, &mod_time);
#endif /* PNG_tIME_SUPPORTED */


#ifdef PNG_TEXT_SUPPORTED
    set_textchunks_from_imgpar (gpiv_image->header, png_ptr, info_ptr);
#endif /* PNG_TEXT_SUPPORTED */

    png_write_info (png_ptr, info_ptr);

    /* 
     * Write image to png struct
     */
    if (info_ptr->color_type == PNG_COLOR_TYPE_GRAY) {
#ifdef WRITE_IMAGEDATA_LINE
        png_byte *line;
#else
        png_byte **png_image;
#endif /* WRITE_IMAGEDATA_LINE */
        unsigned long p_png; /* (pi)xel */
        png_byte *pp;

#ifdef WRITE_IMAGEDATA_LINE
        if ((line = (png_byte *) g_malloc (linesize)) == NULL) {
            png_destroy_write_struct (&png_ptr, &info_ptr);
            err_msg = "gpiv_write_png_image: out of memory allocating PNG row buffer";
            return (err_msg);
        }
#else
        if ((png_image = matrix_png_byte (gpiv_image->header->nrows, linesize))
            == NULL) {
            png_destroy_write_struct (&png_ptr, &info_ptr);
            err_msg = "gpiv_write_png_image: out of memory allocating PNG row buffer";
            return (err_msg);
        }
#endif /* WRITE_IMAGEDATA_LINE */

 
        for (i = 0; i < gpiv_image->header->nrows; i++) {
#ifdef WRITE_IMAGEDATA_LINE
            pp = line;
#else
            pp = png_image[i];
#endif /* WRITE_IMAGEDATA_LINE */
            for (j = 0; j < width; j++) {
                p_png = gpiv_image->frame1[i][j];
                if (bit_depth == 16) *pp++ = PNM_GET1 (p_png) >> 8;
                *pp++ = PNM_GET1 (p_png) & 0xff;
            }
#ifdef WRITE_IMAGEDATA_LINE
            png_write_row (png_ptr, line);
#endif /* WRITE_IMAGEDATA_LINE */
        }
#ifndef WRITE_IMAGEDATA_LINE
        png_write_rows (png_ptr, png_image, gpiv_image->header->nrows);
#endif /* WRITE_IMAGEDATA_LINE */


        if (gpiv_image->header->x_corr) {
            for (i = 0; i < gpiv_image->header->nrows; i++) {
#ifdef WRITE_IMAGEDATA_LINE
                pp = line;
#else
                pp = png_image[i];
#endif /* WRITE_IMAGEDATA_LINE */
                for (j = 0; j < width; j++) {
                    p_png = gpiv_image->frame2[i][j];
                    if (bit_depth == 16) *pp++ = PNM_GET1 (p_png) >> 8;
                    *pp++ = PNM_GET1 (p_png) & 0xff;
                }
#ifdef WRITE_IMAGEDATA_LINE
                png_write_row (png_ptr, line);
#endif /* WRITE_IMAGEDATA_LINE */
            }
#ifndef WRITE_IMAGEDATA_LINE
            png_write_rows (png_ptr, png_image, gpiv_image->header->nrows);
#endif /* WRITE_IMAGEDATA_LINE */
        }


#ifdef WRITE_IMAGEDATA_LINE
        g_free (line);
#else
        free_matrix_png_byte (png_image);
#endif /* WRITE_IMAGEDATA_LINE */

    } else {
        png_destroy_write_struct (&png_ptr, &info_ptr);
        err_msg = "gpiv_write_png_image: color_type is not PNG_COLOR_TYPE_GRAY";
        return (err_msg);
    }

    png_write_end (png_ptr, info_ptr);
    fflush (fp);

    /*
     * Freeing image memory
     */
    if (free == TRUE) {
        gpiv_free_img (gpiv_image);
    }


    png_destroy_write_struct (&png_ptr, &info_ptr);
    return err_msg;
}

/*
 * Local functions
 */
static png_uint_16 
get_png_val (png_byte **pp, 
	     int bit_depth
	     )
/*-----------------------------------------------------------------------------
 */
{
    png_uint_16 c = 0;


    if (bit_depth == 16) {
        c = (*((*pp)++)) << 8;
    }
    c |= (*((*pp)++));

    /*   if (maxval > maxmaxval) */
    /*     c /= ((maxval + 1) / (maxmaxval + 1)); */


    return c;
}



static png_byte **
matrix_png_byte (long nr, 
                 long nc
                 )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      allocate a png_byte matrix with subscript range m[0..nr][0..nc]
 *
 * INPUTS:
 *      nr:             number of rows
 *      nc:             number of columns
 *
 * OUTPUTS:
 *
 * RETURNS:
 *      matrix
 *---------------------------------------------------------------------------*/
{
    long i;
    png_byte **m;


    m = (png_byte **) g_malloc0 (nr * sizeof (png_byte*));
    m[0] = (png_byte *) g_malloc0 (nr * nc * sizeof (png_byte));
    for (i = 1; i < nr; i++) {
        m[i] = m[0] + i * nc;
    }


    return m;
}



static void 
free_matrix_png_byte (png_byte **m
                      )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      free a png_byte matrix allocated by matrix_png_byte
 *
 * INPUTS:
 *      m:	        matrix 
 *
 * OUTPUTS:
 *      m:              NULL pointer
 *
 * RETURNS:
 *---------------------------------------------------------------------------*/
{
    g_return_if_fail (m[0] != NULL);
    g_free (m[0]);
    g_free (m);
    m = NULL;
}



static void
get_imgpar_from_textchunks (GpivImagePar *image_par, 
                            png_structp png_ptr,
                            png_infop info_ptr
                            )
/*------------------------------------------------------------------------------
 * DESCRIPTION:
 *      Obtains image paramameters from text chunks in png image
 *
 * INPUTS:
 *      png_ptr:	        png structure
 *     info_ptr:                png info structure
 *
 * OUTPUTS:
 *    image_par:               image parameter structure
 *
 * RETURNS:
 *--------------------------------------------------------------------------- */
{
    png_textp text;
    gint num_text = 0, i = 0;
    png_uint_32 num_comments = 0;


    num_comments = png_get_text (png_ptr, info_ptr, &text, &num_text);

    for (i = 0; i < num_comments; i++) {
        if (strcmp (text[i].key, PNG_KEY__PIV_XCORR) == 0) {
            sscanf (text[i].text, "%d", &image_par->x_corr);
            image_par->x_corr__set = TRUE;
        }
        
        if (strcmp (text[i].key, PNG_KEY__PIV_DT) == 0) {
            sscanf (text[i].text, "%f", &image_par->t_scale);
            image_par->t_scale__set = TRUE;
        }


        if (strcmp (text[i].key, GPIV_IMGPAR_KEY__TITLE) == 0) {
            g_snprintf (image_par->title, GPIV_MAX_CHARS, "%s", text[i].text);
            image_par->title__set = TRUE;
        }
        
        if (strcmp (text[i].key, GPIV_IMGPAR_KEY__AUTHOR) == 0) {
            g_snprintf (image_par->author, GPIV_MAX_CHARS, "%s", text[i].text);
            image_par->author__set = TRUE;
        }
        
        if (strcmp (text[i].key, GPIV_IMGPAR_KEY__USERTEXT) == 0) {
            g_snprintf (image_par->usertext, GPIV_MAX_CHARS, "%s", text[i].text);
            image_par->usertext__set = TRUE;
        }
        
        if (strcmp (text[i].key, GPIV_IMGPAR_KEY__SOFTWARE) == 0) {
            g_snprintf (image_par->software, GPIV_MAX_CHARS, "%s", text[i].text);
            image_par->software__set = TRUE;
        }
        
        if (strcmp (text[i].key,GPIV_IMGPAR_KEY__DISCLAIMER ) == 0) {
            g_snprintf (image_par->disclaimer, GPIV_MAX_CHARS, "%s", text[i].text);
            image_par->disclaimer__set = TRUE;
        }
        
        if (strcmp (text[i].key, GPIV_IMGPAR_KEY__WARNING) == 0) {
            g_snprintf (image_par->warning, GPIV_MAX_CHARS, "%s", text[i].text);
            image_par->warning__set = TRUE;
        }
        
        if (strcmp (text[i].key, GPIV_IMGPAR_KEY__SOURCE) == 0) {
            g_snprintf (image_par->source, GPIV_MAX_CHARS, "%s", text[i].text);
            image_par->source__set = TRUE;
        }
        
        if (strcmp (text[i].key, GPIV_IMGPAR_KEY__COMMENT) == 0) {
            g_snprintf (image_par->comment, GPIV_MAX_CHARS, "%s", text[i].text);
            image_par->comment__set = TRUE;
        }
        
        if (strcmp (text[i].key, GPIV_IMGPAR_KEY__CREATION_DATE) == 0) {
            g_snprintf (image_par->creation_date, GPIV_MAX_CHARS, "%s", text[i].text);
            image_par->creation_date__set = TRUE;
        }
        
        if (strcmp (text[i].key,  GPIV_IMGPAR_KEY__COPYRIGHT) == 0) {
            g_snprintf (image_par->copyright, GPIV_MAX_CHARS, "%s", text[i].text);
            image_par->copyright__set = TRUE;
        }
        
        if (strcmp (text[i].key, GPIV_IMGPAR_KEY__EMAIL) == 0) {
            g_snprintf (image_par->email, GPIV_MAX_CHARS, "%s", text[i].text);
            image_par->email__set = TRUE;
        }
        
        if (strcmp (text[i].key, GPIV_IMGPAR_KEY__URL) == 0) {
            g_snprintf (image_par->url, GPIV_MAX_CHARS, "%s", text[i].text);
            image_par->url__set = TRUE;
        }

    }
    
#ifdef DEBUG
    printf ("num_comments = %d\n", (int) num_comments);
    for (i = 0; i < num_comments; i++) {
        printf ("key[%d] = %s: text[%d] = %s\n", 
                i, text[i].key, i, text[i].text);
    }
#endif /* DEBUG */

}




static void
set_textchunks_from_imgpar (GpivImagePar *image_par, 
                            png_structp png_ptr, 
                            png_infop info_ptr
                            )
/*------------------------------------------------------------------------------
 * DESCRIPTION:
 *      Set text chunks for png image from image paramameters
 *
 * INPUTS:
 *    image_par:               image parameter structure
 *
 * OUTPUTS:
 *      png_ptr:	        png structure
 *     info_ptr:                png info structure
 *
 * RETURNS:
 *--------------------------------------------------------------------------- */
{
    gint num_text = 0;
    png_text text[14];


    /*
     * Additional header text, needed for (G)PIV
     */
    if (image_par->x_corr__set == TRUE) {
        text[num_text].key = PNG_KEY__PIV_XCORR;
        text[num_text].text = g_strdup_printf ("%d", image_par->x_corr);
        text[num_text].compression = PNG_TEXT_COMPRESSION_NONE;
        ++num_text;
    }
#ifdef PNG_iTXt_SUPPORTED
    text[0].lang = NULL;
#endif

   if (image_par->t_scale__set == TRUE) {
        text[num_text].key = PNG_KEY__PIV_DT;
        text[num_text].text = g_strdup_printf ("%f", image_par->t_scale);
        text[num_text].compression = PNG_TEXT_COMPRESSION_NONE;
        ++num_text;
    }
#ifdef PNG_iTXt_SUPPORTED
    text[1].lang = NULL;
#endif

    /*
     * non required header info
     * PNG recognized header text
     */
    if (image_par->title__set == TRUE) {
        text[num_text].key = GPIV_IMGPAR_KEY__TITLE;
        text[num_text].text = image_par->title;
        text[num_text].compression = PNG_TEXT_COMPRESSION_NONE;
        ++num_text;
    }
#ifdef PNG_iTXt_SUPPORTED
    text[2].lang = NULL;
#endif

    if (image_par->author__set == TRUE) {
        text[num_text].key =  GPIV_IMGPAR_KEY__AUTHOR;
        text[num_text].text = image_par->author; 
        text[num_text].compression = PNG_TEXT_COMPRESSION_NONE;
        ++num_text;
    }
#ifdef PNG_iTXt_SUPPORTED
    text[3].lang = NULL;
#endif

    if (image_par->usertext__set == TRUE) {
        text[num_text].key = GPIV_IMGPAR_KEY__USERTEXT;
        text[num_text].text = image_par->usertext;
        text[num_text].compression = PNG_TEXT_COMPRESSION_NONE;
        ++num_text;
    }
#ifdef PNG_iTXt_SUPPORTED
    text[4].lang = NULL;
#endif

    if (image_par->software__set == TRUE) {
        text[num_text].key = GPIV_IMGPAR_KEY__SOFTWARE;
        text[num_text].text = image_par->software;
        text[num_text].compression = PNG_TEXT_COMPRESSION_NONE;
        ++num_text;
    }
#ifdef PNG_iTXt_SUPPORTED
    text[5].lang = NULL;
#endif

    if (image_par->disclaimer__set == TRUE) {
        text[num_text].key = GPIV_IMGPAR_KEY__DISCLAIMER;
        text[num_text].text = image_par->disclaimer;
        text[num_text].compression = PNG_TEXT_COMPRESSION_NONE;
        ++num_text;
    }
#ifdef PNG_iTXt_SUPPORTED
    text[6].lang = NULL;
#endif

    if (image_par->warning__set == TRUE) {
        text[num_text].key = GPIV_IMGPAR_KEY__WARNING;
        text[num_text].text = image_par->warning;
        text[num_text].compression = PNG_TEXT_COMPRESSION_NONE;
        ++num_text;
#ifdef PNG_iTXt_SUPPORTED
    text[7].lang = NULL;
#endif
    }

    if (image_par->source__set == TRUE) {
        text[num_text].key = GPIV_IMGPAR_KEY__SOURCE;
        text[num_text].text = image_par->source;
        text[num_text].compression = PNG_TEXT_COMPRESSION_NONE;
        ++num_text;
    }
#ifdef PNG_iTXt_SUPPORTED
    text[8].lang = NULL;
#endif

    if (image_par->comment__set == TRUE) {
        text[num_text].key = GPIV_IMGPAR_KEY__COMMENT;
        text[num_text].text = image_par->comment;
        text[num_text].compression =  PNG_TEXT_COMPRESSION_NONE;
        ++num_text;
    }
#ifdef PNG_iTXt_SUPPORTED
    text[9].lang = NULL;
#endif

    if (image_par->creation_date__set == TRUE) {
        text[num_text].key = GPIV_IMGPAR_KEY__CREATION_DATE;
        text[num_text].text = image_par->creation_date;
        text[num_text].compression = PNG_TEXT_COMPRESSION_NONE;
        ++num_text;
    }
#ifdef PNG_iTXt_SUPPORTED
    text[10].lang = NULL;
#endif


    if (image_par->copyright__set == TRUE) {
        text[num_text].key = GPIV_IMGPAR_KEY__COPYRIGHT;
        text[num_text].text = image_par->copyright;
        text[num_text].compression = PNG_TEXT_COMPRESSION_NONE;
        ++num_text;
    }
#ifdef PNG_iTXt_SUPPORTED
    text[11].lang = NULL;
#endif

    if (image_par->email__set == TRUE) {
        text[num_text].key = GPIV_IMGPAR_KEY__EMAIL;
        text[num_text].text = image_par->email;
        text[num_text].compression = PNG_TEXT_COMPRESSION_NONE;
        ++num_text;
    }
#ifdef PNG_iTXt_SUPPORTED
    text[12].lang = NULL;
#endif
    
    if (image_par->url__set == TRUE) {
        text[num_text].key = GPIV_IMGPAR_KEY__URL;
        text[num_text].text = image_par->url;
        text[num_text].compression = PNG_TEXT_COMPRESSION_NONE;
        ++num_text;
    }
#ifdef PNG_iTXt_SUPPORTED
    text[13].lang = NULL;
#endif


    png_set_text (png_ptr, info_ptr,  text, num_text);
}

#ifdef DEBUG
#undef DEBUG
#endif

#ifdef DEBUG2
#undef DEBUG2
#endif
