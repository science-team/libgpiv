/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2006, 2007, 2008 Gerber van der Graaf

   This file is part of libgpiv.
   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  



---------------------------------------------------------------
FILENAME:                io_hdf.c
LIBRARY:                 libgpiv:
EXTERNAL FUNCTIONS:
                         gpiv_fcreate_hdf5
                         gpiv_fread_hdf5_image,
                         gpiv_fwrite_hdf5_image,

                         gpiv_open_hdf5_image,
                         gpiv_fread_hdf5_parameters,
                         gpiv_fwrite_hdf5_parameters,

			 gpiv_fread_hdf5_pivdata, 
			 gpiv_fwrite_hdf5_pivdata, 

                         gpiv_fread_hdf5_scdata, 
			 gpiv_fwrite_hdf5_scdata, 

                         gpiv_fread_hdf5_histo, 
			 gpiv_fwrite_hdf5_histo

LOCAL FUNCTIONS:

LAST MODIFICATION DATE:  $Id: io_hdf.c,v 1.4 2007-12-19 08:46:46 gerber Exp $
 --------------------------------------------------------------- */
#include <gpiv.h>


/*
 * Prtototyping
 */
static gchar *
fcount_hdf5_data (const gchar *fname,
                  gint *nx,
                  gint *ny
                  );

static gchar *
fread_hdf5_piv_position (const gchar *fname, 
                         GpivPivData *piv_data
                         );

static gchar *
fwrite_hdf5_piv_position (const gchar *fname, 
                          GpivPivData *piv_data
                          );

static gchar *
fread_hdf5_pivdata (const gchar *fname, 
                    GpivPivData *piv_data,
                    const gchar *DATA_KEY
                    );

static gchar *
fwrite_hdf5_pivdata (const gchar *fname, 
                     GpivPivData *piv_data, 
                     const gchar *DATA_KEY
                     );

static gchar *
fread_hdf5_sc_position (const gchar *fname, 
                        GpivScalarData *scalar_data
                        );

static gchar *
fwrite_hdf5_sc_position (const gchar *fname, 
                         GpivScalarData *scalar_data
                         );

static gchar *
fread_hdf5_scdata (const gchar *fname, 
                   GpivScalarData *scalar_data,
                   const gchar *DATA_KEY
                   );

static gchar *
fwrite_hdf5_scdata (const char *fname, 
                    GpivScalarData *scalar_data,
                    const gchar *DATA_KEY
                    );
/*
 * Public functions
 */

gchar *
gpiv_fcreate_hdf5 (const gchar *fname
                  )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Creates an hdf5 data file with DATA and PARAMETERS groups
 *
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
/*
 * HDF declarations
 */
    hid_t       file_id, group_id; 
    herr_t      status;

    if ((file_id = H5Fcreate(fname, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT))
        < 0) {
        err_msg = "gpiv_fcreate_hdf5: H5Fcreate unable to create file";
        gpiv_warning("%s", err_msg);
        return err_msg;
    }

    group_id = H5Gcreate (file_id, "IMAGE", 0);
    status = H5Gclose (group_id);
    group_id = H5Gcreate (file_id, "POSITIONS", 0);
    status = H5Gclose (group_id);
    group_id = H5Gcreate (file_id, "PIV", 0);
    status = H5Gclose (group_id);
/*     group_id = H5Gcreate (file_id, "VECTORS", 0); */
/*     status = H5Gclose (group_id); */
    group_id = H5Gcreate (file_id, "SCALARS", 0);
    status = H5Gclose (group_id);

    status = H5Fclose(file_id); 
    return err_msg;
}



/*
 * Public functions
 */

GpivImage *
gpiv_fread_hdf5_image (const gchar *fname
                       )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Opens an image from hdf file
 *     Expects the arrray(s) have not been allocated yet
 *
 *--------------------------------------------------------------------------- */
{
    GpivImage *gpiv_image = NULL;
    GpivImagePar *gpiv_image_par = g_new0 (GpivImagePar, 1);
    gchar *err_msg = NULL;
    int i, j, rank_x, rank_y, rank_vx, rank_vy;
    float *point_x_hdf = NULL, *point_y_hdf = NULL;

/*
 * HDF declarations
 */
    hid_t       file_id, group_id, attribute_id, dataset_id, 
        dataspace_id; 
    hsize_t     dims[2];
    herr_t      status;



    if (fname == NULL) {
        gpiv_warning ("gpiv_fread_hdf5_image: failing \"fname == NULL\"");
        return NULL;
    }

    if ((i = H5Fis_hdf5 (fname)) == 0)  {
        gpiv_warning ("gpiv_fread_hdf5_image: not an hdf5 file");
        return NULL;
    }


    gpiv_img_parameters_set (gpiv_image_par, FALSE);
    if ((gpiv_image_par = gpiv_img_fread_hdf5_parameters (fname))
        == NULL) {
        gpiv_warning ("gpiv_fread_hdf5_image: failing gpiv_img_fread_hdf5_parameters"); 
        return NULL;
    }

#ifdef GPIV_IMG_PARAM_RESOURCES
    gpiv_scan_resourcefiles (GPIV_IMGPAR_KEY, &gpiv_image_par, TRUE);
#endif
    if ((err_msg = 
         gpiv_img_check_header_required_read (gpiv_image_par)) 
        != NULL) {
        gpiv_warning (err_msg); 
        return NULL;
    }
    
    gpiv_image = gpiv_alloc_img (gpiv_image_par);
    
    
    file_id = H5Fopen(fname, H5F_ACC_RDONLY, H5P_DEFAULT);
    group_id = H5Gopen (file_id, "IMAGE");

/*
 * image #1
 */

    dataset_id = H5Dopen (group_id, "#1");
    dataspace_id = H5Dget_space (dataset_id);
#ifdef HD5_IMAGE_INT
    if ((status = H5Dread (dataset_id, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, 
                          H5P_DEFAULT, gpiv_image->frame1[0])) < 0) {
        gpiv_warning ("gpiv_fread_hdf5_image: failing H5Dread");
        return NULL;
    }
#else
    if ((status = H5Dread(dataset_id, H5T_NATIVE_USHORT, H5S_ALL, H5S_ALL, 
                          H5P_DEFAULT, gpiv_image->frame1[0])) < 0) {
        gpiv_warning ("gpiv_fread_hdf5_image: failing H5Dread");
        return NULL;
    }
#endif
/*     msg_error = "H5Dread: no image data";
 *     GpivIo
 *     gpiv_io
 */
    status = H5Sclose(dataspace_id);
    status = H5Dclose(dataset_id);

/*
 * image #2
 */
    if (gpiv_image->header->x_corr) {
        dataset_id = H5Dopen(group_id, "#2");
        dataspace_id = H5Dget_space(dataset_id);
#ifdef HD5_IMAGE_INT
        status = H5Dread(dataset_id, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, 
                         H5P_DEFAULT, gpiv_image->frame2[0]);
#else
        status = H5Dread(dataset_id, H5T_NATIVE_USHORT, H5S_ALL, H5S_ALL, 
                         H5P_DEFAULT, gpiv_image->frame2[0]);
#endif
        status = H5Sclose(dataspace_id);
        status = H5Dclose(dataset_id);
    } else {
	gpiv_image->frame2 = gpiv_image->frame1;
    }



    H5Gclose (group_id);
    status = H5Fclose(file_id);

    return gpiv_image;
}


gchar *
gpiv_fwrite_hdf5_image (const gchar *fname, 
                        GpivImage *image,
                        gboolean free
                        )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Writes IMAGE data to file in hdf version 5 format
 *
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    gint i, j, k, line_nr=0, image_white_is_zero = 0;
    gfloat *point_x_hdf = NULL;
/*
 * HDF file, dataset,-space identifier
 */
    hid_t       file_id, group_id, dataset_id, dataspace_id, 
        attribute_id, atype; 
    hsize_t     dims[2];
    herr_t      status;


    g_return_val_if_fail (image->frame1[0] != NULL, "frame1[0] != NULL");
    if (image->header->x_corr) g_return_val_if_fail (image->frame2[0] != NULL, "frame2[0] != NULL");



    if ((err_msg = gpiv_img_fwrite_hdf5_parameters (fname, image->header))
        != NULL) {
        return err_msg;
    }


    if ((i = H5Fis_hdf5(fname)) == 0) {
        err_msg = "gpiv_fwrite_hdf5_image: not an hdf5 file";
        gpiv_warning("%s", err_msg);
        return err_msg;
    }
    file_id = H5Fopen(fname, H5F_ACC_RDWR, H5P_DEFAULT);
    group_id = H5Gopen (file_id, "IMAGE");
/*     H5Gset_comment (group_id, ".", RCSID); */

/*
 * Hdf required image specifications
 */
    dims[0] = 1;

    dataspace_id = H5Screate(H5S_SCALAR);
    atype = H5Tcopy(H5T_C_S1);
    H5Tset_size(atype, 5);
    attribute_id = H5Acreate(group_id, "CLASS", atype, 
                             dataspace_id, H5P_DEFAULT);
    status = H5Awrite(attribute_id, atype, 
                      "IMAGE"); 
    status = H5Aclose(attribute_id); 
    status = H5Sclose(dataspace_id);


    dataspace_id = H5Screate(H5S_SCALAR);
    atype = H5Tcopy(H5T_C_S1);
    H5Tset_size(atype, 15);
    attribute_id = H5Acreate(group_id, "SUBCLASS", atype, 
                             dataspace_id, H5P_DEFAULT);
    status = H5Awrite(attribute_id, atype, 
                      "IMAGE_GRAYSCALE"); 
    status = H5Aclose(attribute_id); 
    status = H5Sclose(dataspace_id);


    dataspace_id = H5Screate_simple(1, dims, NULL);
    attribute_id = H5Acreate(group_id, "IMAGE_WHITE_IS_ZERO", 
                             H5T_NATIVE_USHORT, dataspace_id, H5P_DEFAULT);
    status = H5Awrite(attribute_id, H5T_NATIVE_USHORT, &image_white_is_zero); 
    status = H5Aclose(attribute_id); 
    status = H5Sclose(dataspace_id);


    dataspace_id = H5Screate(H5S_SCALAR);
    atype = H5Tcopy(H5T_C_S1);
    H5Tset_size(atype, 3);
    attribute_id = H5Acreate(group_id, "IMAGE_VERSION", atype, 
                             dataspace_id, H5P_DEFAULT);
    status = H5Awrite(attribute_id, atype, 
                      "1.2"); 
    status = H5Aclose(attribute_id); 
    status = H5Sclose(dataspace_id);


 /*
 * Create the data space.
 */
   dims[0] = image->header->ncolumns; 
   dims[1] = image->header->nrows;
   dataspace_id = H5Screate_simple(2, dims, NULL);


/* 
 * image #1
 */
#ifdef HD5_IMAGE_INT
   dataset_id = H5Dcreate (group_id, "#1", H5T_NATIVE_INT, 
			  dataspace_id, H5P_DEFAULT);
   status = H5Dwrite (dataset_id, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, 
		     H5P_DEFAULT, image->frame1[0][0]);
#else
/*    dataset_id = H5Dcreate (group_id, "#1", H5T_NATIVE_USHORT,  */
/* 			  dataspace_id, H5P_DEFAULT); */
/*    status = H5Dwrite (dataset_id, H5T_NATIVE_USHORT, H5S_ALL, H5S_ALL,  */
/* 		     H5P_DEFAULT, image->frame1[0][0]); */
#endif
   status = H5Dclose (dataset_id);


/* 
 * image #2
 */
   if (image->header->x_corr) {
#ifdef HD5_IMAGE_INT
       dataset_id = H5Dcreate(group_id, "#2", H5T_NATIVE_INT, 
                              dataspace_id, H5P_DEFAULT);
       status = H5Dwrite(dataset_id, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, 
                         H5P_DEFAULT, &image->frame2[0][0]);
#else
/*        dataset_id = H5Dcreate(group_id, "#2", H5T_NATIVE_USHORT,  */
/*                               dataspace_id, H5P_DEFAULT); */
/*        status = H5Dwrite(dataset_id, H5T_NATIVE_USHORT, H5S_ALL, H5S_ALL,  */
/*                          H5P_DEFAULT, &image->frame2[0][0]); */
#endif
       status = H5Dclose(dataset_id);
   }


   status = H5Sclose(dataspace_id);
   status = H5Gclose (group_id);
   status = H5Fclose(file_id);
   if (free == TRUE) gpiv_free_img (image);
   return err_msg;
}



gchar *
gpiv_fread_hdf5_parameters (const gchar *fname,
                            const gchar *par_key,
                            void *pstruct
                            )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *      Reads parameters from hdf5 data file
 *
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;    

    if (strcmp(par_key, GPIV_PIVPAR_KEY) == 0) {
        pstruct = gpiv_piv_fread_hdf5_parameters (fname);
    } else {
        err_msg = "gpiv_scan_parameters: called with unexisting key";
        gpiv_warning("%s", err_msg);
        return err_msg;
    }

  return err_msg;
}



gchar *
gpiv_fwrite_hdf5_parameters (const gchar *fname,
                             const gchar *par_key,
                             void *pstruct
                             )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Writes parameters to hdf5 data file
 *
 *---------------------------------------------------------------------------*/
{    
    gchar *err_msg = NULL;
    if (strcmp(par_key, GPIV_PIVPAR_KEY) == 0) {
        gpiv_piv_fwrite_hdf5_parameters (fname, pstruct);
    } else {
        err_msg = "gpiv_fwrite_hdf5_parameters: called with unexisting key";
        gpiv_warning("%s", err_msg);
        return err_msg;
    }

    return err_msg;
}


GpivPivData *
gpiv_fread_hdf5_pivdata (const gchar *fname,
                         const gchar *DATA_KEY
                         )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Reads piv data from hdf5 data file
 *
 *---------------------------------------------------------------------------*/
{
    GpivPivData *piv_data = NULL;
    gchar *err_msg = NULL;
    guint nx, ny;


    if ((err_msg = fcount_hdf5_data (fname, &nx, &ny)) != NULL)  {
        g_message ("gpiv_fread_hdf5_pivdata: %s", err_msg);
        return NULL;
    }

    if ((piv_data = gpiv_alloc_pivdata (nx, ny)) == NULL) {
        g_message ("gpiv_fread_hdf5_pivdata: failing gpiv_alloc_pivdata");
        return NULL;
    }

    if ((err_msg = fread_hdf5_piv_position (fname, piv_data)) != NULL)  {
        g_message ("gpiv_fread_hdf5_pivdata: %s", err_msg);
        return NULL;
    }
    g_free (piv_data->comment);

    if ((err_msg = fread_hdf5_pivdata (fname, piv_data, DATA_KEY)) != NULL)  {
        g_message ("gpiv_fread_hdf5_pivdata: %s", err_msg);
        return NULL;
    }



    return piv_data;
}



gchar *
gpiv_fwrite_hdf5_pivdata (const gchar *fname, 
                          GpivPivData *piv_data, 
                          const gchar *DATA_KEY,
                          gboolean free
                          )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Writes PIV data to file in hdf version 5 format
 *
*---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;


    if ((err_msg = fwrite_hdf5_pivdata (fname, piv_data, DATA_KEY)) != NULL) {
        gpiv_error ("gpiv_fwrite_hdf5_pivdata: %s", err_msg);
    }

    if ((err_msg = fwrite_hdf5_piv_position (fname, piv_data)) != NULL) {
        gpiv_error ("gpiv_fwrite_hdf5_piv_position: %s", err_msg);
    }

    if (free) gpiv_free_pivdata (piv_data);


    return err_msg;
}


GpivScalarData *
gpiv_fread_hdf5_scdata (const gchar *fname,
                        const gchar *DATA_KEY
                        )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Reads scalar data from hdf5 data file
 *
 *---------------------------------------------------------------------------*/
{
    GpivScalarData *scalar_data = NULL;
    gchar *err_msg = NULL;
    guint nx, ny;


    if ((err_msg = fcount_hdf5_data (fname, &nx, &ny)) != NULL)  {
        g_message ("gpiv_fread_hdf5_scdata: %s", err_msg);
        return NULL;
    }

    if ((scalar_data = gpiv_alloc_scdata (nx, ny)) == NULL) {
        g_message ("gpiv_fread_hdf5_scdata: failing gpiv_alloc_pivdata");
        return NULL;
    }

    if ((err_msg = fread_hdf5_sc_position (fname, scalar_data)) != NULL)  {
        g_message ("gpiv_fread_hdf5_scdata: %s", err_msg);
        return NULL;
    }

    if ((err_msg = fread_hdf5_scdata (fname, scalar_data, DATA_KEY)) != NULL)  {
        g_message ("gpiv_fread_hdf5_scdata: %s", err_msg);
        return NULL;
    }



    return scalar_data;
}



gchar *
gpiv_fwrite_hdf5_scdata (const gchar *fname, 
                          GpivScalarData *scalar_data, 
                          const gchar *DATA_KEY,
                          gboolean free
                          )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Writes scalar data to file in hdf version 5 format
 *
*---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;


    if ((err_msg = fwrite_hdf5_scdata (fname, scalar_data, DATA_KEY)) != NULL) {
        gpiv_error ("gpiv_fwrite_hdf5_scdata: %s", err_msg);
    }

    if ((err_msg = fwrite_hdf5_sc_position (fname, scalar_data)) != NULL) {
        gpiv_error ("gpiv_fwrite_hdf5_sc_position: %s", err_msg);
    }

    if (free) gpiv_free_scdata (scalar_data);


    return err_msg;
}


gchar *
gpiv_fread_hdf5_histo (const gchar *fname, 
                       GpivBinData *klass, 
                       const gchar *DATA_KEY
                       )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Reads histogram data to ouput file in hdf version 5 format
 *
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    gchar *RCSID = NULL;
    gint i;

/*
 * HDF declarations
 */
    hid_t       file_id, group_data, group_id, attribute_id, dataset_id, 
        dataspace_id; 
    hsize_t     dims[2];
    herr_t      status;
    
    g_return_val_if_fail (klass->count != NULL, "klass->count != NULL");
    g_return_val_if_fail (klass->bound != NULL, "klass->bound != NULL");
    g_return_val_if_fail (klass->centre != NULL, "klass->centre != NULL");
  

    if ((i = H5Fis_hdf5(fname)) == 0)  {
        err_msg = "gpiv_fread_hdf5_histo: not an hdf5 file";
        gpiv_warning("%s", err_msg);
        return err_msg;
    }
    file_id = H5Fopen(fname, H5F_ACC_RDONLY, H5P_DEFAULT);
    group_data = H5Gopen (file_id, "SCALARS");
    group_id = H5Gopen (group_data, DATA_KEY);
    H5Gget_comment (group_id, ".", GPIV_MAX_CHARS, RCSID);
    klass->comment = g_strdup (RCSID);

/*
 * centre
 */
    dataset_id = H5Dopen(group_id, "centre");
    dataspace_id = H5Dget_space(dataset_id);
    status = H5Dread(dataset_id, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, 
                     H5P_DEFAULT, klass->centre);
    status = H5Dclose(dataset_id);
    status = H5Sclose(dataspace_id);

/*
 * count
 */
    dataset_id = H5Dopen(group_id, "count");
    dataspace_id = H5Dget_space(dataset_id);
    status = H5Dread(dataset_id, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, 
                     H5P_DEFAULT, klass->count);
    status = H5Dclose(dataset_id);
    status = H5Sclose(dataspace_id);

    H5Gclose (group_id);
    H5Gclose (group_data);
    status = H5Fclose(file_id);
    return err_msg;
 }



gchar *
gpiv_fwrite_hdf5_histo (const gchar *fname, 
                        const GpivBinData *klass, 
                        const gchar *DATA_KEY
                        )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Writes histogram data to ouput file in hdf version 5 format
 *
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    gchar *RCSID = NULL;
    gint i;

/*
 * HDF declarations
 */
    hid_t       file_id, group_data, group_id, attribute_id, dataset_id, 
        dataspace_id; 
    hsize_t     dims[1];
    herr_t      status;
    
    g_return_val_if_fail (klass->count != NULL, "klass->count != NULL");
    g_return_val_if_fail (klass->bound != NULL, "klass->bound != NULL");
    g_return_val_if_fail (klass->centre != NULL, "klass->centre != NULL");
  

    if ((i = H5Fis_hdf5(fname)) == 0)  {
        err_msg = "gpiv_fwrite_hdf5_histo: not an hdf5 file";
        gpiv_warning("%s", err_msg);
        return err_msg;
    }
    file_id = H5Fopen(fname, H5F_ACC_RDWR, H5P_DEFAULT);
    group_data = H5Gopen (file_id, "SCALARS");
    group_id = H5Gopen (group_data, DATA_KEY);
    H5Gset_comment (group_id, ".", klass->comment);

/*
 * Create the data space.
 */
   dims[0] = klass->nbins; 
/*
 * centre
 */
    dataspace_id = H5Screate_simple(1, dims, NULL);
    dataset_id = H5Dcreate(group_id, "centre", H5T_NATIVE_FLOAT, 
                           dataspace_id, H5P_DEFAULT);
    status = H5Dwrite(dataset_id, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, 
                     H5P_DEFAULT, &klass->centre[0]);
    status = H5Dclose(dataset_id);
    status = H5Sclose(dataspace_id);

/*
 * count
 */
    dataspace_id = H5Screate_simple(1, dims, NULL);
    dataset_id = H5Dcreate(group_id, "count", H5T_NATIVE_INT, 
                          dataspace_id, H5P_DEFAULT);
    status = H5Dread(dataset_id, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, 
                     H5P_DEFAULT, &klass->count[0]);
    status = H5Dclose(dataset_id);
    status = H5Sclose(dataspace_id);

    H5Gclose (group_id);
    H5Gclose (group_data);
    status = H5Fclose(file_id);
    return err_msg;
}


/*
 * Local functions
 */
static gchar *
fcount_hdf5_data (const gchar *fname,
                  gint *nx,
                  gint *ny
                  )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Reads array lengths of 2-dimensional data
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    int i, j, rank_x, rank_y, rank_vx, rank_vy;
    float *point_x_hdf = NULL, *point_y_hdf = NULL;
/*
 * HDF declarations
 */
    hid_t       file_id, group_id, dataset_id, dataspace_id; 
    hsize_t     dims[2];
    herr_t      status;

    if ((i = H5Fis_hdf5(fname)) == 0) {
        err_msg = "gpiv_fcount_hdf5_pivdata: not an hdf5 file";
        gpiv_warning("%s", err_msg);
        return err_msg;
    }
    file_id = H5Fopen(fname, H5F_ACC_RDONLY, H5P_DEFAULT);
    group_id = H5Gopen (file_id, "POSITIONS");

/*
 * getting nx
 */
    dataset_id = H5Dopen(group_id, "point_x");
    dataspace_id = H5Dget_space(dataset_id);
    rank_x = H5Sget_simple_extent_ndims (dataspace_id);
    if (rank_x != 1) {
        err_msg = "gpiv_fcount_hdf5_pivdata: rank_x != 1";
        gpiv_warning("%s", err_msg);
        return err_msg;
    }
    H5Sget_simple_extent_dims(dataspace_id, dims, NULL);
    *nx = dims[0];
    status = H5Dclose(dataset_id);
    status = H5Sclose(dataspace_id);
	
/*
 * getting ny
 */
    dataset_id = H5Dopen(group_id, "point_y");
    dataspace_id = H5Dget_space(dataset_id);
    rank_y = H5Sget_simple_extent_ndims (dataspace_id);
    if (rank_y != 1) {
        err_msg = "gpiv_fcount_hdf5_pivdata: rank_y != 1";
        gpiv_warning("%s", err_msg);
        return err_msg;
    }
    H5Sget_simple_extent_dims(dataspace_id, dims, NULL);
    *ny = dims[0];
    status = H5Dclose(dataset_id);
    status = H5Sclose(dataspace_id);


    H5Gclose (group_id);
    status = H5Fclose(file_id); 
    return err_msg;
}



static gchar *
fread_hdf5_piv_position (const gchar *fname, 
                         GpivPivData *piv_data
                         )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Reads PIV position data from hdf5 data file into GpivPivData struct
 *
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    gchar *RCSID = NULL;
    int i, j, rank_x, rank_y, rank_vx, rank_vy;
    float *point_x_hdf = NULL, *point_y_hdf = NULL;
/*
 * HDF declarations
 */
    hid_t       file_id, group_id, attribute_id, dataset_id, 
        dataspace_id; 
    hsize_t     dims[2];
    herr_t      status;


    g_return_val_if_fail (piv_data->point_x != NULL, "piv_data->point_x != NULL");
    g_return_val_if_fail (piv_data->point_y != NULL, "piv_data->point_y != NULL");


    if ((i = H5Fis_hdf5(fname)) == 0)  {
        err_msg = "gpiv_fread_hdf5_piv_position: not an hdf5 file";
        gpiv_warning("%s", err_msg);
        return err_msg;
    }
    file_id = H5Fopen(fname, H5F_ACC_RDONLY, H5P_DEFAULT);
/*
 * POSITION group with RCSID comment and scale attribute
 */
    group_id = H5Gopen (file_id, "POSITIONS");
    H5Gget_comment (group_id, ".", GPIV_MAX_CHARS, RCSID);
    piv_data->comment = g_strdup (RCSID);
    attribute_id = H5Aopen_name(group_id, "scale");
    H5Aread(attribute_id, H5T_NATIVE_INT, &piv_data->scale); 
    H5Aclose(attribute_id); 

/*
 * point_x
 */
    dataset_id = H5Dopen(group_id, "point_x");
    dataspace_id = H5Dget_space(dataset_id);
    rank_x = H5Sget_simple_extent_ndims (dataspace_id);
    if (rank_x != 1) {
        err_msg = "gpiv_fread_hdf5_piv_position: rank_x != 1";
        gpiv_warning("%s", err_msg);
        return err_msg;
    }
    H5Sget_simple_extent_dims(dataspace_id, dims, NULL);
    piv_data->nx = dims[0];
    point_x_hdf = gpiv_vector(piv_data->nx) ;
    status = H5Dread(dataset_id, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, 
                     H5P_DEFAULT, point_x_hdf);
    status = H5Dclose(dataset_id);
    status = H5Sclose(dataspace_id);
	

/*
 * point_y
 */
    dataset_id = H5Dopen(group_id, "point_y");
    dataspace_id = H5Dget_space(dataset_id);
    rank_y = H5Sget_simple_extent_ndims (dataspace_id);
    if (rank_y != 1) {
        err_msg = "gpiv_fread_hdf5_piv_position: rank_y != 1";
        gpiv_warning("%s", err_msg);
        return err_msg;
    }
    H5Sget_simple_extent_dims(dataspace_id, dims, NULL);
    piv_data->ny = dims[0];
    point_y_hdf = gpiv_vector(piv_data->ny) ;
    status = H5Dread(dataset_id, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, 
                     H5P_DEFAULT, point_y_hdf);
    status = H5Dclose(dataset_id);
    status = H5Sclose(dataspace_id);

/*
 * filling GpivPivData structure with point_x an point_y
 */
    for (i = 0; i < piv_data->ny; i++) {
            for (j = 0; j < piv_data->nx; j++) {
                piv_data->point_x[i][j] = point_x_hdf[j];
                piv_data->point_y[i][j] = point_y_hdf[i];
            }
    }
    gpiv_free_vector(point_x_hdf);
    gpiv_free_vector(point_y_hdf);


    H5Gclose (group_id);
    status = H5Fclose(file_id);
    return err_msg;
}


static gchar *
fwrite_hdf5_piv_position (const gchar *fname, 
                          GpivPivData *piv_data
                          )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Writess PIV position data to hdf5 data file
 *
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    int i, j, rank_x, rank_y, rank_vx, rank_vy;
    float *point_x_hdf = NULL, *point_y_hdf = NULL;

/*
 * HDF declarations
 */
    hid_t       file_id, group_id, attribute_id, dataset_id, 
        dataspace_id, str_MC; 
    hsize_t     dims[2], dims_attrib[1];
    herr_t      status;


    g_return_val_if_fail (piv_data->point_x != NULL, "piv_data->point_x != NULL");
    g_return_val_if_fail (piv_data->point_y != NULL, "piv_data->point_y != NULL");


    if ((i = H5Fis_hdf5(fname)) == 0) {
        err_msg = "gpiv_fwrite_hdf5_piv_position: not an hdf5 file";
        gpiv_warning("%s", err_msg);
        return err_msg;
    }
    file_id = H5Fopen(fname, H5F_ACC_RDWR, H5P_DEFAULT);

/*
 * POSITION group with RCSID comment and scale attribute
 */
    group_id = H5Gopen (file_id, "POSITIONS");
    H5Gset_comment (group_id, ".", piv_data->comment);
    dims_attrib[0] = 1;
    dataspace_id = H5Screate_simple(1, dims_attrib, NULL);
    attribute_id = H5Acreate(group_id, "scale", H5T_NATIVE_INT, 
                             dataspace_id, H5P_DEFAULT);
    H5Awrite(attribute_id, H5T_NATIVE_INT, &piv_data->scale); 
    H5Aclose(attribute_id); 

/*
 * data dimension
 */
    str_MC = H5Tcopy (H5T_C_S1);
    H5Tset_size (str_MC, GPIV_MAX_CHARS);
    dataspace_id = H5Screate_simple(1, dims_attrib, NULL);
    attribute_id = H5Acreate(group_id, "dimension", str_MC, 
                             dataspace_id, H5P_DEFAULT);
    if (piv_data->scale) {
        H5Awrite(attribute_id, str_MC, "Positions in m");
    } else {
        H5Awrite(attribute_id, str_MC, "Positions in pixels");
    }

    H5Aclose(attribute_id); 

/*
 * point_x
 */
    point_x_hdf = gpiv_vector(piv_data->nx);
    for (i = 0; i < piv_data->nx; i++) point_x_hdf[i] = piv_data->point_x[0][i];
    dims[0] = piv_data->nx; 
    dataspace_id = H5Screate_simple(1, dims, NULL);
    dataset_id = H5Dcreate(group_id, "point_x", H5T_NATIVE_FLOAT, dataspace_id, 
                           H5P_DEFAULT);
    status = H5Dwrite(dataset_id, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, 
                      H5P_DEFAULT, point_x_hdf);
    status = H5Dclose(dataset_id);
    status = H5Sclose(dataspace_id);
    gpiv_free_vector(point_x_hdf);

/*
 * point_y
 */
    point_y_hdf = gpiv_vector(piv_data->ny);
    for (i = 0; i < piv_data->ny; i++) point_y_hdf[i] = piv_data->point_y[i][0];
    dims[0] = piv_data->ny; 
    dataspace_id = H5Screate_simple(1, dims, NULL);
    dataset_id = H5Dcreate(group_id, "point_y", H5T_NATIVE_FLOAT, 
                           dataspace_id, H5P_DEFAULT);
    status = H5Dwrite(dataset_id, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, 
                      H5P_DEFAULT, point_y_hdf);
    status = H5Dclose(dataset_id);
    status = H5Sclose(dataspace_id);
    gpiv_free_vector(point_y_hdf);

/* 
 *Closing group and file
 */
    status = H5Gclose (group_id);
    status = H5Fclose(file_id); 

    return err_msg;
}



static gchar *
fread_hdf5_pivdata (const gchar *fname, 
                    GpivPivData *piv_data,
                    const gchar *DATA_KEY
                    )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Reads _only_ PIV displacements (no positions) from hdf5 data file
 *     into a GpivPivData struct
 *
 *---------------------------------------------------------------------------*/
{    
    gchar *err_msg = NULL;
    gchar *RCSID = NULL;
    gint i, j, rank_x, rank_y, rank_vx, rank_vy;
    gfloat *point_x_hdf = NULL, *point_y_hdf = NULL;
/*
 * HDF declarations
 */
    hid_t       file_id, group_id, attribute_id, dataset_id, 
        dataspace_id; 
    hsize_t     dims[2];
    herr_t      status;


/*     g_return_val_if_fail (piv_data->nx == 0); */
/*     g_return_val_if_fail (piv_data->nx == 0); */
    g_return_val_if_fail (piv_data->point_x != NULL, "(piv_data->point_x != NULL");
    g_return_val_if_fail (piv_data->point_y != NULL, "(piv_data->point_y != NULL");
    g_return_val_if_fail (piv_data->dx != NULL, "piv_data->dx != NULL");
    g_return_val_if_fail (piv_data->dy != NULL, "piv_data->dy != NULL");
    g_return_val_if_fail (piv_data->snr != NULL, "piv_data->snr != NULL");
    g_return_val_if_fail (piv_data->peak_no != NULL, "piv_data->peak_no != NULL");


    if ((i = H5Fis_hdf5(fname)) == 0)  {
        err_msg = "gpiv_fread_hdf5_pivdata: not an hdf5 file";
        gpiv_warning("%s", err_msg);
        return err_msg;
    }
    file_id = H5Fopen(fname, H5F_ACC_RDONLY, H5P_DEFAULT);

/*
 * PIV group with RCSID comment and scale attribute
 */
    group_id = H5Gopen (file_id, DATA_KEY);
    H5Gget_comment (group_id, ".", GPIV_MAX_CHARS, RCSID);
    piv_data->comment = g_strdup (RCSID);

    attribute_id = H5Aopen_name(group_id, "scale");
    H5Aread(attribute_id, H5T_NATIVE_INT, &piv_data->scale); 
    H5Aclose(attribute_id); 


/*
 * dx
 */
    dataset_id = H5Dopen(group_id, "dx");
    dataspace_id = H5Dget_space(dataset_id);
    status = H5Dread(dataset_id, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, 
                     H5P_DEFAULT, piv_data->dx[0]);
    status = H5Dclose(dataset_id);
    status = H5Sclose(dataspace_id);

/*
 * dy
 */
    dataset_id = H5Dopen(group_id, "dy");
    dataspace_id = H5Dget_space(dataset_id);
    status = H5Dread(dataset_id, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, 
                     H5P_DEFAULT, piv_data->dy[0]);
    status = H5Dclose(dataset_id);
    status = H5Sclose(dataspace_id);

/*
 * snr
 */
    dataset_id = H5Dopen(group_id, "snr");
    dataspace_id = H5Dget_space(dataset_id);
    status = H5Dread(dataset_id, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, 
                     H5P_DEFAULT, piv_data->snr[0]);
    status = H5Dclose(dataset_id);
    status = H5Sclose(dataspace_id);

/*
 * peak_no
 */
    dataset_id = H5Dopen(group_id, "peak_no");
    dataspace_id = H5Dget_space(dataset_id);
    status = H5Dread(dataset_id, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, 
                     H5P_DEFAULT, piv_data->peak_no[0]);
    status = H5Dclose(dataset_id);
    status = H5Sclose(dataspace_id);



    status = H5Gclose (group_id);
    status = H5Fclose(file_id); 
    return err_msg;
}



static gchar *
fwrite_hdf5_pivdata (const gchar *fname, 
                     GpivPivData *piv_data, 
                     const gchar *DATA_KEY
                     )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Writes _only_ PIV displacements (no positions) to file in hdf version 
 *     5 format
 *
*---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    gint i, j, k, line_nr = 0;
    gfloat *point_x_hdf = NULL,  *point_y_hdf = NULL;
    const  gchar *string = "RcsId";
    gchar *dir /*=  "/point_x" */;
    gint point = 5;

/*
 * HDF file, dataset,-space identifier
 */
    hid_t       file_id, dataset_id, dataspace_id, 
        group_id, attribute_id,
        str_MC; 
    hsize_t     dims[2], dims_attrib[1];
    herr_t      status;	      


    g_return_val_if_fail (piv_data->point_x != NULL, "piv_data->point_x != NULL");
    g_return_val_if_fail (piv_data->point_y != NULL, "piv_data->point_y != NULL");
    g_return_val_if_fail (piv_data->dx != NULL, "piv_data->dx != NULL");
    g_return_val_if_fail (piv_data->dy != NULL, "piv_data->dy != NULL");
    g_return_val_if_fail (piv_data->snr != NULL, "piv_data->snr != NULL");
    g_return_val_if_fail (piv_data->peak_no != NULL, "piv_data->peak_no != NULL");

    if ((i = H5Fis_hdf5(fname)) == 0) {
        err_msg = "LIBGPIV: fwrite_hdf5_pivdata: not an hdf5 file";
        gpiv_warning("%s", err_msg);
        return err_msg;
    }
     file_id = H5Fopen(fname, H5F_ACC_RDWR, H5P_DEFAULT);

/*
 * PIV group with RCSID comment and scale attribute
 */
    group_id = H5Gopen (file_id, DATA_KEY);
    H5Gset_comment (group_id, ".", piv_data->comment);
    dims_attrib[0] = 1;
    dataspace_id = H5Screate_simple(1, dims_attrib, NULL);
    attribute_id = H5Acreate(group_id, "scale", H5T_NATIVE_INT, 
                             dataspace_id, H5P_DEFAULT);
    H5Awrite(attribute_id, H5T_NATIVE_INT, &piv_data->scale); 
    H5Aclose(attribute_id); 
    H5Sclose(dataspace_id);

/*
 * data dimension
 */
    str_MC = H5Tcopy (H5T_C_S1);
    H5Tset_size (str_MC, GPIV_MAX_CHARS);
    dataspace_id = H5Screate_simple(1, dims_attrib, NULL);
    attribute_id = H5Acreate(group_id, "dimension", str_MC, 
                             dataspace_id, H5P_DEFAULT);
    if (piv_data->scale) {
        H5Awrite(attribute_id, str_MC, "PIV estimators in m/s");
    } else {
        H5Awrite(attribute_id, str_MC, "PIV estimators in pixels");
    }
    H5Aclose(attribute_id); 
    H5Sclose(dataspace_id);


    dims[0] = piv_data->ny; 
    dims[1] = piv_data->nx;

/* 
 * dx
 */
   dataspace_id = H5Screate_simple(2, dims, NULL);
   dataset_id = H5Dcreate(group_id, "dx", H5T_NATIVE_FLOAT, 
                          dataspace_id, H5P_DEFAULT);
   status = H5Dwrite(dataset_id, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, 
		     H5P_DEFAULT, &piv_data->dx[0][0]);
   status = H5Dclose(dataset_id);
   status = H5Sclose(dataspace_id);

/*
 * dy
 */
   dataspace_id = H5Screate_simple(2, dims, NULL);
   dataset_id = H5Dcreate(group_id, "dy", H5T_NATIVE_FLOAT, 
                              dataspace_id, H5P_DEFAULT);
   status = H5Dwrite(dataset_id, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, 
		     H5P_DEFAULT,&piv_data->dy[0][0]);
   status = H5Dclose(dataset_id);
   status = H5Sclose(dataspace_id);

/*
 * snr
 */
   dataspace_id = H5Screate_simple(2, dims, NULL);
   dataset_id = H5Dcreate(group_id, "snr", H5T_NATIVE_FLOAT, 
			  dataspace_id, H5P_DEFAULT);
   status = H5Dwrite(dataset_id, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, 
		     H5P_DEFAULT,&piv_data->snr[0][0]);
   status = H5Dclose(dataset_id);
   status = H5Sclose(dataspace_id);

/*
 * peak_no
 */
   dataspace_id = H5Screate_simple(2, dims, NULL);
   dataset_id = H5Dcreate(group_id, "peak_no", H5T_NATIVE_INT, dataspace_id, 
			  H5P_DEFAULT);
   status = H5Dwrite(dataset_id, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT,
		     &piv_data->peak_no[0][0]);
   status = H5Dclose(dataset_id);
   status = H5Sclose(dataspace_id);


   status = H5Gclose (group_id);
   status = H5Fclose(file_id); 
   return err_msg;
}


static gchar *
fread_hdf5_sc_position (const gchar *fname, 
                        GpivScalarData *scdata
                        )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *    Reads position data from hdf5 data file into GpivScalarData struct
 *
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL; 
    int i, j, rank_x, rank_y, rank_vx, rank_vy;
    float *point_x_hdf = NULL, *point_y_hdf = NULL;
/*
 * HDF declarations
 */
    hid_t       file_id, group_id, attribute_id, dataset_id, dataspace_id; 
    hsize_t     dims[2];
    herr_t      status;


    g_return_val_if_fail (scdata->point_x != NULL, "scdata->point_x != NULL");
    g_return_val_if_fail (scdata->point_y != NULL, "scdata->point_y != NULL");


    if ((i = H5Fis_hdf5(fname)) == 0) {
        err_msg = "LIBGPIV: fread_hdf5_sc_position: not an hdf5 file";
        gpiv_warning("%s", err_msg);
        return err_msg;
    }
    file_id = H5Fopen(fname, H5F_ACC_RDONLY, H5P_DEFAULT);
    group_id = H5Gopen (file_id, "POSITIONS");

/*
 * point_x
 */
    dataset_id = H5Dopen(group_id, "point_x");
    dataspace_id = H5Dget_space(dataset_id);
    rank_x = H5Sget_simple_extent_ndims (dataspace_id);
    if (rank_x != 1) {
        err_msg = "LIBGPIV: fread_hdf5_sc_position: rank_x != 1";
        gpiv_warning("%s", err_msg);
        return err_msg;
    }
    H5Sget_simple_extent_dims(dataspace_id, dims, NULL);
    scdata->nx = dims[0];
    point_x_hdf = gpiv_vector(scdata->nx) ;
    status = H5Dread(dataset_id, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, 
                     H5P_DEFAULT, point_x_hdf);
    status = H5Dclose(dataset_id);
    status = H5Sclose(dataspace_id);
	

/*
 * point_y
 */
    dataset_id = H5Dopen(group_id, "point_y");
    dataspace_id = H5Dget_space(dataset_id);
    rank_y = H5Sget_simple_extent_ndims (dataspace_id);
    if (rank_y != 1) {
        err_msg = "LIBGPIV: fread_hdf5_sc_position: rank_y != 1";
        gpiv_warning("%s", err_msg);
        return err_msg;
    }
    H5Sget_simple_extent_dims(dataspace_id, dims, NULL);
    scdata->ny = dims[0];
    point_y_hdf = gpiv_vector(scdata->ny) ;
    status = H5Dread(dataset_id, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, 
                     H5P_DEFAULT, point_y_hdf);
    status = H5Dclose(dataset_id);
    status = H5Sclose(dataspace_id);

/*
 * filling GpivPivData structure with point_x an point_y
 */
    for (i = 0; i < scdata->ny; i++) {
            for (j = 0; j < scdata->nx; j++) {
                scdata->point_x[i][j] = point_x_hdf[j];
                scdata->point_y[i][j] = point_y_hdf[i];
            }
    }
    gpiv_free_vector(point_x_hdf);
    gpiv_free_vector(point_y_hdf);


    H5Gclose (group_id);
    status = H5Fclose(file_id);
    return err_msg;
}


static gchar *
fwrite_hdf5_sc_position (const gchar *fname, 
                         GpivScalarData *scalar_data
                         )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *    Writes position data to hdf5 data file
 *
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    int i, j, rank_x, rank_y, rank_vx, rank_vy;
    float *point_x_hdf = NULL, *point_y_hdf = NULL;
/*
 * HDF declarations
 */
    hid_t       file_id, group_id, attribute_id, 
        dataset_id, dataspace_id, str_MC; 
    hsize_t     dims[2], dims_attrib[1];
    herr_t      status;


    g_return_val_if_fail (scalar_data->point_x != NULL, "scalar_data->point_x != NULL");
    g_return_val_if_fail (scalar_data->point_y != NULL, "scalar_data->point_y != NULL");


    if ((i = H5Fis_hdf5(fname)) == 0) {
        err_msg = "LIBGPIV: fwrite_hdf5_sc_position: not an hdf5 file";
        gpiv_warning("%s", err_msg);
        return err_msg;
    }
    file_id = H5Fcreate(fname, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
    group_id = H5Gcreate (file_id, "POSITIONS", 0);
    H5Gset_comment (group_id, ".", scalar_data->comment);
    dims_attrib[0] = 1;
    dataspace_id = H5Screate_simple(1, dims_attrib, NULL);
    attribute_id = H5Acreate(group_id, "scale", H5T_NATIVE_INT, 
                             dataspace_id, H5P_DEFAULT);
    H5Awrite(attribute_id, H5T_NATIVE_INT, &scalar_data->scale); 
    H5Aclose(attribute_id); 


/*
 * data dimension
 */
    str_MC = H5Tcopy (H5T_C_S1);
    H5Tset_size (str_MC, GPIV_MAX_CHARS);
    dims_attrib[0] = 1;
    dataspace_id = H5Screate_simple(1, dims_attrib, NULL);
    attribute_id = H5Acreate(group_id, "dimension", str_MC, 
                             dataspace_id, H5P_DEFAULT);
    if (scalar_data->scale) {
        H5Awrite(attribute_id, str_MC, "Positions in m");
    } else {
        H5Awrite(attribute_id, str_MC, "Positions in pixels");
    }

    H5Aclose(attribute_id); 

/*
 * point_x
 */
    point_x_hdf = gpiv_vector(scalar_data->nx);
    for (i = 0; i < scalar_data->nx; i++) point_x_hdf[i] = scalar_data->point_x[0][i];
    dims[0] = scalar_data->nx; 
    dataspace_id = H5Screate_simple(1, dims, NULL);
    dataset_id = H5Dcreate(group_id, "point_x", H5T_NATIVE_FLOAT, dataspace_id, 
                           H5P_DEFAULT);
    status = H5Dwrite(dataset_id, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, 
                      H5P_DEFAULT, point_x_hdf);
    status = H5Dclose(dataset_id);
    status = H5Sclose(dataspace_id);
    gpiv_free_vector(point_x_hdf);

/*
 * point_y
 */
    point_y_hdf = gpiv_vector(scalar_data->ny);
    for (i = 0; i < scalar_data->ny; i++) point_y_hdf[i] = scalar_data->point_y[i][0];
    dims[0] = scalar_data->ny; 
    dataspace_id = H5Screate_simple(1, dims, NULL);
    dataset_id = H5Dcreate(group_id, "point_y", H5T_NATIVE_FLOAT, 
                           dataspace_id, H5P_DEFAULT);
    status = H5Dwrite(dataset_id, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, 
                      H5P_DEFAULT, point_y_hdf);
    status = H5Dclose(dataset_id);
    status = H5Sclose(dataspace_id);
    gpiv_free_vector(point_y_hdf);


    status = H5Gclose (group_id);
/*     group_id = H5Gcreate (file_id, "DATA", 0); */
/*     status = H5Gclose (group_id); */
/*     group_id = H5Gcreate (file_id, "PARAMETERS", 0); */
/*     status = H5Gclose (group_id); */
    status = H5Fclose(file_id); 
    return err_msg;
}



static gchar *
fread_hdf5_scdata (const gchar *fname, 
                   GpivScalarData *scalar_data,
                   const gchar *DATA_KEY
                   )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Reads SCALAR data from a hdf5 data file
 *
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    gchar *RCSID = NULL;
    int i, j, rank_x, rank_y, rank_vx, rank_vy;
    float *point_x_hdf = NULL, *point_y_hdf = NULL;
/*
 * HDF declarations
 */
    hid_t       file_id, group_data, group_id, attribute_id, dataset_id, 
        dataspace_id; 
    hsize_t     dims[2];
    herr_t      status;


    g_return_val_if_fail (scalar_data->point_x != NULL, "scalar_data->point_x != NULL");
    g_return_val_if_fail (scalar_data->point_y != NULL, "scalar_data->point_y != NULL");
    g_return_val_if_fail (scalar_data->scalar != NULL, "scalar_data->scalar != NULL");
    g_return_val_if_fail (scalar_data->flag != NULL, "scalar_data->flag != NULL");

    if ((i = H5Fis_hdf5(fname)) == 0) {
        err_msg = "LIBGPIV: fread_hdf5_scdata: not an hdf5 file";
        gpiv_warning("%s", err_msg);
        return err_msg;
    }
    file_id = H5Fopen(fname, H5F_ACC_RDONLY, H5P_DEFAULT);
    group_data = H5Gopen (file_id, "SCALARS");
    group_id = H5Gopen (group_data, DATA_KEY);
    H5Gget_comment (group_id, ".", GPIV_MAX_CHARS, RCSID);
    scalar_data->comment = g_strdup (RCSID);
/*
 * scalar
 */
    dataset_id = H5Dopen(group_id, "scalar");
    dataspace_id = H5Dget_space(dataset_id);
    status = H5Dread(dataset_id, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, 
                     H5P_DEFAULT, scalar_data->scalar[0]);
    status = H5Dclose(dataset_id);
    status = H5Sclose(dataspace_id);

/*
 * flag
 */
    dataset_id = H5Dopen(group_id, "flag");
    dataspace_id = H5Dget_space(dataset_id);
    status = H5Dread(dataset_id, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, 
                     H5P_DEFAULT, scalar_data->flag[0]);
    status = H5Dclose(dataset_id);
    status = H5Sclose(dataspace_id);


    H5Gclose (group_id);
    H5Gclose (group_data);
    status = H5Fclose(file_id);
    return err_msg;
}



static gchar *
fwrite_hdf5_scdata (const char *fname, 
                    GpivScalarData *scalar_data,
                    const gchar *DATA_KEY
                    )
/*-----------------------------------------------------------------------------
 * DESCRIPTION:
 *     Writes _ONLY_ SCALAR data (no positions) to file in hdf version 5 format
 *
 *---------------------------------------------------------------------------*/
{
    gchar *err_msg = NULL;
    int i, j, k, line_nr=0;
    float *point_x_hdf = NULL;
/*
 * HDF file, dataset,-space identifier
 */
    hid_t       file_id, group_data, group_id, dataset_id, dataspace_id; 
    hsize_t     dims[2];
    herr_t      status;
    
    g_return_val_if_fail (scalar_data->point_x != NULL, "scalar_data->point_x != NULL");
    g_return_val_if_fail (scalar_data->point_y != NULL, "scalar_data->point_y != NULL");
    g_return_val_if_fail (scalar_data->scalar != NULL, "scalar_data->scalar != NULL");
    g_return_val_if_fail (scalar_data->flag != NULL, "scalar_data->flag != NULL");
    
    
    if ((i = H5Fis_hdf5(fname)) == 0) {
        err_msg = "LIBGPIV: fwrite_hdf5_scdata: not an hdf5 file";
        gpiv_warning("%s", err_msg);
        return err_msg;
    }
    file_id = H5Fopen(fname, H5F_ACC_RDWR, H5P_DEFAULT);
    group_data = H5Gopen (file_id, "SCALARS");
    group_id = H5Gcreate (group_data, DATA_KEY, 0);
    H5Gset_comment (group_id, ".", scalar_data->comment);

/*
 * Create the data space.
 */
   dims[0] = scalar_data->ny; 
   dims[1] = scalar_data->nx;


/* 
 * scalar
 */

   dataspace_id = H5Screate_simple(2, dims, NULL);
   dataset_id = H5Dcreate(group_id, "scalar", H5T_NATIVE_FLOAT, 
			  dataspace_id, H5P_DEFAULT);
   status = H5Dwrite(dataset_id, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, 
		     H5P_DEFAULT, &scalar_data->scalar[0][0]);
   status = H5Dclose(dataset_id);
   status = H5Sclose(dataspace_id);

/*
 * flag
 */

   dataspace_id = H5Screate_simple(2, dims, NULL);
   dataset_id = H5Dcreate(group_id, "flag", H5T_NATIVE_INT, dataspace_id, 
			  H5P_DEFAULT);
   status = H5Dwrite(dataset_id, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT,
		     &scalar_data->flag[0][0]);
   status = H5Dclose(dataset_id);
   status = H5Sclose(dataspace_id);


   status = H5Gclose (group_id);
   status = H5Gclose (group_data);
   status = H5Fclose(file_id);
   return err_msg;
}



