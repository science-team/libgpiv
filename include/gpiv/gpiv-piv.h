/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*-----------------------------------------------------------------------------

   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 Gerber van der Graaf

   This file is part of libgpiv.

   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

-------------------------------------------------------------------------------
LIBRARY:                libgpiv

SOURCE:                 piv.c

EXTERNAL FUNCTIONS:
                        gpiv_piv_count_pivdata_fromimage
                        gpiv_piv_select_int_point
                        gpiv_piv_interrogate_img
                        gpiv_piv_interrogate_ia
                        gpiv_piv_isizadapt
                        gpiv_piv_write_deformed_image
                        gpiv_piv_weight_kernel_lin
                        gpiv_fread_fftw_wisdom
                        gpiv_fwrite_fftw_wisdom


------------------------------------------------------------------------------*/

/*!
\file                   gpiv-piv.h
\brief                  module for PIV image evaluation

SOURCES:                lib/piv.c

LAST MODIFICATION DATE: $Id: gpiv-piv.h,v 1.2 2008-04-09 06:10:28 gerber Exp $
 */



#ifndef __LIBGPIV_PIV_H__
#define __LIBGPIV_PIVL_H__


#include <gpiv/gpiv-piv_par.h>
#include <gpiv/gpiv-valid_par.h>



#define GPIV_ZEROPAD_FACT 2     /**< magnification factor of zero-padded int.\ area */
#define GPIV_DIFF_ISI  0        /**< difference between interrogation sizes 
                                   of first and second image if zero 
                                   offset has been used */
#define GPIV_CUM_RESIDU_MIN 0.25 /**< minimum cumulative residu for convergence */
#define GPIV_MAX_PIV_SWEEP 10   /**< maximum number of PIV evaluation sweeps,
                                   starting from zero */
#define GPIV_SNR_DISABLE 88.0    /**< snr value for manually disabled estimator */
#define GPIV_DEFORMED_IMG_NAME "gpiv_defimg" /**< Deformed image to be stored in TMP_DIR */ 
#define GPIV_LOGFILE "gpiv.log"  /**< Log file to be stored in TMP_DIR */


#define CMPR_FACT 2            /**< Image compression factor for speeding up the
                                  evaluation */
#define GPIV_SHIFT_FACTOR 3    /**< For initial grid, apply 
                                  int_shift = int_size_i / GPIV_SHIFT_FACTOR */



/**
 * Type of sub-pixel estimation
 */
enum GpivIFit {
    GPIV_NONE = 0,     /**< No fitting */
    GPIV_GAUSS = 1,    /**< Gauss fitting */
    GPIV_POWER = 2,    /**< Power fitting */
    GPIV_GRAVITY = 3   /**< Plain gravity fitting */
};



/**
 * Interrogation geometry
 */
enum GpivIntGeo {
    GPIV_AOI = 0,      /**< Area of interst */
    GPIV_LINE_C = 1,   /**< Along column line */
    GPIV_LINE_R = 2,   /**< Along row line */
    GPIV_POINT = 3     /**< At single point */
};



typedef struct __GpivCovariance GpivCov;
/*!
 * \brief Covariance data
 *
 * The structure holds the 2-dimensional covariance data, some variables and 
 * other related parameters in order to 'fold' the data in an array.
 */
struct __GpivCovariance {	/**< data structure of covariance data */
    float **z;			/**< 2-dim array containing covariance data */
    float min;			/**< minimum covariance value */
    float max;			/**< maximum covariance value */

    long top_x;                 /**< x-position of maximum at pixel level */
    long top_y;                 /**< y-position of maximum at pixel level */
    float subtop_x;             /**< x-position of maximum at sub-pixel level */
    float subtop_y;             /**< y-position of maximum at sub-pixel level */
    float snr;                  /**< Signal to Noice defined by quotient of first 
                                   and second highest peaks */

    int z_rnl;			/**< lowest negative row index */
    int z_rnh;			/**< highest negative row index */
    int z_rpl;			/**< lowest positive row index */
    int z_rph;			/**< highest positive row index */

    int z_cnl;			/**< lowest negative column index */
    int z_cnh;			/**< highest negative column index */
    int z_cpl;			/**< lowest positive column index */
    int z_cph;			/**< highest positive index */

    int z_rl;			/**< general lowest row index */
    int z_rh;			/**< general highest row index */
    int z_cl;			/**< general lowest column index */
    int z_ch;			/**< general highest column index */
};



/*-----------------------------------------------------------------------------
 * Function prototypes
 */



/**
 *     Calculates the number of interrogation areas from the image sizes, 
 *     pre-shift and area of interest.
 *
 *     @param[in] image_par    structure of image parameters
 *     @param[in] piv_par      structure of piv evaluation parameters
 *     @param[out] nx          number of columns (second array index)
 *     @param[out] ny          number of rows (first array index)
 *     @return                 NULL on success or error message on failure
 */
gchar *
gpiv_piv_count_pivdata_fromimage        (const GpivImagePar     *image_par,
                                         const GpivPivPar       *piv_par,
                                         guint                  *nx,
                                         guint                  *ny
                                         );


/**
 *     PIV interrogation of an image pair at an entire grid.
 *
 *     @param[in] image        image containing data and header info
 *     @param[in] piv_par      image interrogation parameters
 *     @param[in] valid_par    PIV data validation parameters
 *     @param[out] verbose     prints progress of interrogation to stdout
 *     @return                 GpivPivData containing PIV estimators on success
 *                             or NULL on failure
 */
GpivPivData *
gpiv_piv_interrogate_img     		(const GpivImage        *image,
                              		const GpivPivPar       	*piv_par,
                              		const GpivValidPar     	*valid_par,
                              		const gboolean         	verbose
                              		);
 

/**
 *      Interrogates a single Interrogation Area.
 *
 *      @param[in] index_y      y-index of interrogation area position  
 *      @param[in] index_x      x-index of interrogation area position
 *      @param[in] image        structure of image
 *      @param[in] piv_par      structure of piv evaluation parameters
 *      @param[in] sweep        sweep number of iterative process 
 *      @param[in] last_sweep   flag for last sweep
 *      @param[in] int_area_1   first interrogation area
 *      @param[in] int_area_2   second interrogation area
 *      @param[out] cov          structure containing covariance data
 *      @param[out] piv_data    modified piv data at [index_y][index_x] from 
 *                              interrogation
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_piv_interrogate_ia      		(const guint           index_y,
                             		const guint            index_x,
                          		const GpivImage        *image,
                              		const GpivPivPar       *piv_par,
                              		const guint            sweep,
                              		const guint            last_sweep,
                              		gfloat                 **int_area_1,
                              		gfloat                 **int_area_2,
                              		GpivCov                *cov,
                              		GpivPivData            *piv_data
                              		);



/**
 *     Adjusts interrogation area sizes. For each interrogation sweep,
 *     (dest) int_size_i is halved, until it reaches (src)
 *     int_size_f. Then, isiz_last is set TRUE, which will avoid
 *     changing the interrogation sizes in next calls.
 *
 *     @param[in] piv_par_src          original parameters
 *     @param[out] piv_par_dest        actual parameters, to be modified 
 *                                     during sweeps
 *     @param[out] isiz_last           flag for last interrogation sweep
 *     @return                         void
 */
void
gpiv_piv_isizadapt      		(const GpivPivPar       *piv_par_src,
                         		GpivPivPar             	*piv_par_dest,
                         		gboolean               	*isiz_last
                         		);



/**
 *     Stores deformed image to file system with pre defined name to TMPDIR
 *     and prints message to stdout.
 *
 *     @param[in] image        image containing header and image data frames
 *     @return                 NULL on success or error message on failure
 */
gchar *
gpiv_piv_write_deformed_image  		(GpivImage 		*image
                                	);




/**
 *     Reads fftw wisdoms from file and stores into a (public) string.
 *
 *     @param[in] dir          direction of fft; forward (+1) or inverse (-1)
 *     @return                 void
 */
void
gpiv_fread_fftw_wisdom 			(const gint   		dir
                        		);



/**
 *     Writes fftw wisdoms to a file.
 *
 *     @param[in] dir          direction of fft; forward (+1) or inverse (-1)
 *     @return                 void
 *
 */
void 
gpiv_fwrite_fftw_wisdom        		(const gint     	dir
                                	);


/**
 *      Calculates dx, dy of piv_data_dest from piv_data_src
 *      by bi-linear interpolation of inner points with shifted knots
 *      or extrapolation of outer lying points.
 *
 *      @param[in] piv_data_src         input piv data
 *      @param[out] piv_data_dest       output piv data
 *      @return NULL on success or *err_msg on failure
 */
gchar *
gpiv_piv_dxdy_at_new_grid       	(const GpivPivData      *piv_data_src,
                                 	GpivPivData            	*piv_data_dest
                                 	);



/**
 *     Shifts the knots of a 2-dimensional grid containing PIV data for improved 
 *     (bi-linear) interpolation.
 *
 *     See: T. Blu, P. Thevenaz, "Linear Interpolation Revitalized",
 *     IEEE Trans. in Image Processing, vol13, no 5, May 2004
 *
 *     @param[in] piv_data     piv data that will be shifted in-place
 *     @return                 NULL on success or error message on failure
 */
gchar *
gpiv_piv_shift_grid     		(GpivPivData    	*piv_data
                         		);



/**
 *      Generates grid by Calculating the positions of interrogation areas.
 *
 *      @param[in] nx           number of horizontal grid points
 *      @param[in] ny           number of vertical grid points
 *      @param[in] image_par    structure of image parameters
 *      @param[in] piv_par      structure of piv evaluation parameters
 *      @return                 piv_data or NULL on failure
 */
GpivPivData *
gpiv_piv_gridgen        		(const guint            nx,
                         		const guint            	ny,
                         		const GpivImagePar     	*image_par,
                         		const GpivPivPar       	*piv_par
                         		);



/**
 *     Adjust grid nodes if zero_off or adaptive interrogation 
 *     area has been used. This is performed by modifying int_shift equal 
 *     to int_shift / GPIV_SHIFT_FACTOR, until it reaches (src)
 *     int_shift. Then, grid_last is set TRUE, which will avoid
 *     changing the interrogation shift in next calls and signal the
 *     (while loop in) the calling function.
 *     
 *     @param[in] image_par               image parameters
 *     @param[in] piv_par_src        piv evaluation parameters
 *     @param[in] piv_data                input PIV data
 *     @param[in] sweep                   interrogation sweep step
 *     @param[out] piv_par_dest       modified piv evaluation parameters
 *     @param[out] grid_last               flag if final grid refinement has been 
 *     @return                         piv_data or NULL on failure
 */
GpivPivData *
gpiv_piv_gridadapt      		(const GpivImagePar     *image_par, 
                         		const GpivPivPar       	*piv_par_src,
                         		GpivPivPar             	*piv_par_dest,
                         		const GpivPivData      	*piv_data,
                         		const guint            	sweep, 
                         		gboolean               	*grid_last
                         		);


#endif /* __LIBGPIV_PIV_H__ */
