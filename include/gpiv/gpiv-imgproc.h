/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*-----------------------------------------------------------------------------

   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 Gerber van der Graaf

   This file is part of libgpiv.

   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

-------------------------------------------------------------------------------
FILENAME:               imgproc.h
LIBRARY:                libgpiv:
EXTERNAL FUNCTIONS:
SOURCE:                 imgproc_par.c
                        gpiv_imgproc_parameters_set
                        gpiv_imgproc_default_parameters
                        gpiv_imgproc_fscan_parameters
                        gpiv_imgproc_read_parameters 
                        gpiv_imgproc_check_parameters
                        gpiv_imgproc_test_parameters
                        gpiv_imgproc_print_parameters
                        gpiv_imgproc_fprint_parameters
                        gpiv_imgproc_cp_parameters

SOURCE:                 imgproc.c
                        gpiv_imgproc_mktestimg
                        gpiv_imgproc_subtractimg
                        gpiv_imgproc_smooth
                        gpiv_imgproc_highlow
                        gpiv_imgproc_clip
                        gpiv_imgproc_getbit
                        gpiv_imgproc_fft
                        gpiv_imgproc_lowpass
                        gpiv_imgproc_highpass
                        gpiv_imgproc_correlate
                        gpiv_imgproc_convolve

-----------------------------------------------------------------------------*/
/*!
\file                  gpiv-imgproc.h
\brief                 module for image processing

SOURCE:                lib/imgproc.c

LAST MODIFICATION DATE:  $Id: gpiv-imgproc.h,v 1.2 2008-04-09 06:10:27 gerber Exp $
 */


#ifndef __LIBGPIV_IMGPROC_H__
#define __LIBGPIV_IMGPROC_H__

#define GPIV_IMGPROCPAR_MAX__THRESHOLD 255
#define GPIV_IMGPROCPAR_MAX__WINDOW 50

#define GPIV_IMGPROCPAR_KEY "IMGPROC"       /**< Key of image processing parameters */

/**
 * Image filter type
 */
enum GpivImgFilter {
    GPIV_IMGFI_MKTESTIMG,       /**< Generate test image */
    GPIV_IMGFI_SUBACK,          /**< Subtract background image */
    GPIV_IMGFI_SMOOTH,          /**< Smooth filter */
    GPIV_IMGFI_HILO,            /**< High-low filter */
    GPIV_IMGFI_CLIP,            /**< Clipping */

    GPIV_IMGFI_FFT,             /**< FFT image */
    GPIV_IMGFI_INVFFT,          /**< Inverse FFT image */
    GPIV_IMGFI_CORR,            /**< Correlate */
    GPIV_IMGFI_CONV,            /**< Convolve */
    GPIV_IMGFI_LOWPASS,         /**< Low pass filter **/
    GPIV_IMGFI_HIGHPASS,        /**< High pass filter */

    GPIV_IMGFI_GETBIT,  /**< Pointer operation to get the N least significant bits */
};

/**
 * Operator type when smoothing is performed
 */
enum GpivImgSmoothOperator {
    GPIV_IMGOP_EQUAL,    /**< No action */
    GPIV_IMGOP_SUBTRACT, /**< Subtract avarage */
    GPIV_IMGOP_ADD,      /**< Add avarage */
    GPIV_IMGOP_MULTIPLY, /**< Multiply with avarage */
    GPIV_IMGOP_DIVIDE    /**< Divide by avarage */
};


typedef struct __GpivImageProcPar GpivImageProcPar;

/*!
 * \brief Image processing parameters
 *
 * Though there are many and excellent (free) image processing programs available, 
 * some image processes have been included in this library that are more 
 * specific for PIV purposes and cannot be found in general image processing
 * programs. 
 * The parameters might be loaded from the configuration resources, 
 * with gpiv_scan_resourcefiles() or with gpiv_scan_parameter().
 */
struct __GpivImageProcPar {
    guint bit;                  /**< bit number */
    gboolean bit__set;          /**< flag if has bit been defined */

    enum GpivImgFilter filter;  /**< Filter type */
    gboolean filter__set;       /**< flag if filter has been defined */

    enum GpivImgSmoothOperator smooth_operator; /**< Type of sooting operaor */
    gboolean smooth_operator__set;              /**< flag smooth_operator if has been defined */

    guint window;               /**< geographic window in pixels */
    gboolean window__set;       /**< flag if window has been defined */

    guint threshold;            /**< minimum or maximum (intensity) value */
    gboolean threshold__set;    /**< flag if threshold has been defined */
};




/**
 *      Sets flag for image_proc_par__set
 *
 *      @param[in] flag			flag to enforce parameters set to defaults
 *      @param[out] image_proc_par	image processing parameters
 *      @return				void
 */
void
gpiv_imgproc_parameters_set (GpivImageProcPar *image_proc_par,
                             const gboolean flag
                             );



/**
 *     Sets default parameter values
 *
 *     @param[in] force                 flag to enforce parameters set to defaults
 *     @param[out] imgproc_par_default  structure of image processing parameters
 *     @return                          void
 */
void
gpiv_imgproc_default_parameters		(GpivImageProcPar       *imgproc_par_default,
					const gboolean         force
					);



/**
 *     Reads image processing parameters
 *
 *     @param[in] fp_h                 file to be read
 *     @param[in] image_proc_par       pointer to structure of image processing parameters
 *     @param[in] print_par            prints parameters to stdout during reading
 *     @param[out] image_proc_par      pointer to structure of image processing parameters
 *     @return void
 */
void
gpiv_imgproc_read_parameters		(FILE                   *fp_h, 
					GpivImageProcPar	*image_proc_par, 
					const gboolean		print_par
					);



/**
 *      Checks if all necessary image processing parameters have been read.
 *      If a parameter has not been read, it will be set to image_proc_par_default or to 
 *	its hard-coded default value in case image_proc_par_default is NULL.
 *
 *     @param[in] image_proc_par_default      image processing parameters containing default values
 *     @param[out] image_proc_par             structure of image processing parameters
 *     @return                                NULL on success or error message on failure
 */
gchar *
gpiv_imgproc_check_parameters_read	(GpivImageProcPar	*image_proc_par,
					const GpivImageProcPar	*image_proc_par_default
					);



/**
 *     Tests image processing parameters on validity.
 *
 *     @param[in] image_proc_par       structure of image processing parameters
 *     @return                         NULL on success or error message on failure
 */
gchar *
gpiv_imgproc_test_parameters		(const GpivImageProcPar *image_proc_par
					);



/**
 *     Prints image processing parameters to file.
 *
 *     @param[in] fp                   file pointer to which paraeters will 
 *                                     be printed. If NULL, stdout will be used
 *     @param[in] image_proc_par       structure of image processing parameters
 *     @return                         void
 */
void
gpiv_imgproc_print_parameters		(FILE                   *fp, 
					const GpivImageProcPar	*image_proc_par
					);


/**
 *     Copies image processing parameters.
 *
 *     @param[in] imgproc_par  image processing parameters to be copied
 *     @return                 GpivImageProcPar or NULL on failure
 */
GpivImageProcPar *
gpiv_imgproc_cp_parameters		(const GpivImageProcPar *imgproc_par
					);


/**
 *     Genarates test image for image processing. This is not an image for 
 *     testing on PIV interrogation!
 *
 *     @param[in] image_par            structure of image parameters
 *     @param[in] image_proc_par       structure of image processing parameters
 *     @return                         GpivImage or NULL on failure
 */
GpivImage *
gpiv_imgproc_mktestimg		(const GpivImagePar		*image_par,
				const GpivImageProcPar		*image_proc_par
				);


/**
 *     Subtracts image intensities of img_in from img_out. To avoid swapping of 
 *     intensities, output values are set to zero if img_in is larger than img_out.
 *     Images will have to be of identic dimensions.
 *
 *     @param[in] image_in      image containing the intensities to be subtracted
 *     @param[out] image_out    modified image. will have to exist and allocated
 *     @return                  NULL on success or error message on failure
 */
gchar *
gpiv_imgproc_subtractimg        (const GpivImage		*image_in,
                                 GpivImage			*image_out
                                 );


/**
 *     Smoothing filter by taking mean value of surrounding 
 *     window x window pixels
 *
 *     @param[in] image                image to be smoothed in-place
 *     @param[in] image_proc_par       structure of image processing parameters
 *     @return                         NULL on success or error message on failure
 */
gchar *
gpiv_imgproc_smooth		(GpivImage			*image, 
				const GpivImageProcPar		*image_proc_par
				);


/**
 *     Highpass filter on an image
 *     passing data from M - window,..,M/2, N - window,..,N/2
 *
 *     @param[in] image                image to be be high-passed in-place
 *     @param[in] image_proc_par       structure of image processing parameters
 *     @return                         NULL on success or error message on failure
 */
gchar *
gpiv_imgproc_highpass		(GpivImage			*image, 
				const GpivImageProcPar		*image_proc_par
				);


/**
 *     High-low filter to maximize contrast by stretching pixel values
 *     to local max and min within window x window area
 *
 *     @param[in] image                image to be filtered in-place
 *     @param[in] image_proc_par       structure of image processing parameters
 *     @return                         NULL on success or error message on failure
 */
gchar *
gpiv_imgproc_highlow		(GpivImage			*image,
				const GpivImageProcPar		*image_proc_par
				);


/**
 *     Sets all pixel values lower than threshold to zero
 *
 *     @param[in] image                image to be clipped in-place.
 *     @param[in] image_proc_par       structure of image processing parameters
 *     @return                         NULL on success or error message on failure
 */
gchar *
gpiv_imgproc_clip		(GpivImage			*image,
				const GpivImageProcPar		*image_proc_par
				);


/**
 *     Pointer operation to get the N least significant bits and moves them to 
 *     most the significant bits
 *
 *     @param[in] image                image to be filtered in-place
 *     @param[in] image_proc_par       structure of image processing parameters
 *     @return                         NULL on success or error message on failure
 */
gchar *
gpiv_imgproc_getbit		(GpivImage			*image,
				const GpivImageProcPar		*image_proc_par
				);


/**
 *     Fast Fourier Transformation of image
 *
 *     @param[in] image                input image to be modified in-place. 
 *     @param[in] image_proc_par       structure of image processing parameters
 *     @return                         NULL on success or error message on failure
 */
gchar *
gpiv_imgproc_fft			(GpivImage              *image,
					const GpivImageProcPar	*image_proc_par
					);


#ifndef USE_FFTW3
/**
 *     Lowpass filter on an image
 *
 *     @param[in] image_par            structure of image parameters
 *     @param[in] image_proc_par       structure of image processing parameters
 *     @param[in] img                  input image to be filtered in-place.
 *                                     Needs to be allocated
 *     @return                         NULL on success or error message on failure
 */
gchar *
gpiv_imgproc_lowpass			(GpivImagePar		image_par, 
					GpivImageProcPar	image_proc_par,
					guint16			**img
					);



/**
 *     Correlates two images
 *
 *     @param[in] image_par            structure of image parameters
 *     @param[in] image_proc_par       structure of image processing parameters
 *     @param[in] img1_in              first input image. Needs to be allocated
 *     @param[in] img2_in              second input image. Needs to be allocated
 *     @param[out] img_out             pointer to output image. Not necessarily allocated
 *     @return                         NULL on success or error message on failure
 */
gchar *
gpiv_imgproc_correlate			(GpivImagePar           image_par, 
					GpivImageProcPar	image_proc_par,
					guint16			**img1_in, 
					guint16			**img2_in, 
					guint16*		**img_out
					);



/**
 *     Convolves two images
 *
 *     @param[in] image_par            structure of image parameters
 *     @param[in] image_proc_par       structure of image processing parameters
 *     @param[in] img1_in              first input image. Needs to be allocated
 *     @param[in] img2_in              second input image. Needs to be allocated
 *     @param[out] img_out             pointer to output image. Not necessarily allocated
 *     @return                         NULL on success or error message on failure
 *
 */
gchar *
gpiv_imgproc_convolve		(GpivImagePar			image_par, 
				GpivImageProcPar		image_proc_par,
				 guint16			**img1_in, 
				 guint16			**img2_in, 
				 guint16*			**img_out
				 );

#endif /* USE_FFTW3 */


/**
 *     Image shifting and deformation routine for a single exposed, double 
 *     frame PIV image pair with magnitude of PIV estimations at each pixel.
 *     Deforms first frame halfway forward and second frame halfway backward.
 *
 *     @param[in] piv_data     PIV data
 *     @param[in] image        input image to be deformed in-place. Needs to be allocated
 *     @return                 NULL on success or error message on failure
 */
gchar *
gpiv_imgproc_deform		(GpivImage			*image,
				const GpivPivData		*piv_data
				);


#endif /* __LIBGPIV_IMGPROC_H__ */

