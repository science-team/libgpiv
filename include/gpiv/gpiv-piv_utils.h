/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 Gerber van der Graaf

   This file is part of libgpiv.

   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  




-------------------------------------------------------------------------------
LIBRARY:                 libgpiv:
EXTERNAL FUNCTIONS:
			 gpiv_null_pivdata
                         gpiv_alloc_pivdata
                         gpiv_check_alloc_pivdata
			 gpiv_free_pivdata
                         gpiv_0_pivdata
                         gpiv_cp_pivdata
                         gpiv_add_dxdy_pivdata
                         gpiv_sum_dxdy_pivdata

                         gpiv_alloc_cov
			 gpiv_free_cov

                         gpiv_piv_mpi_bcast_pivdata

LAST MODIFICATION DATE:  $Id: gpiv-piv_utils.h,v 1.2 2008-09-25 13:19:53 gerber Exp $
 --------------------------------------------------------------------------- */
/**
\file                   gpiv-piv_utils.h
\brief                  utilities module for GpivPivData structure

SOURCES:                lib/piv_utils.c

LAST MODIFICATION DATE:  $Id: gpiv-piv_utils.h,v 1.2 2008-09-25 13:19:53 gerber Exp $
 */

#ifndef __LIBGPIV_PIV_UTILS_H__
#define __LIBGPIV_PIV_UTILS_H__

/**
 *      Sets all elements of piv_data structure to NULL.
 *
 *      @param[in] piv_data     Input PIV data structure
 *      @param[out] piv_data    Output PIV data structure
 *      @return                 void
 */
void
gpiv_null_pivdata			(GpivPivData		*piv_data
					);


/**
 *      Allocates memory for GpivPivData.
 *
 *      @param[in] nx           number of nx  (column-wise data dimension)
 *      @param[in] ny           number of ny  (row-wise data dimension)
 *      @return                 piv_data on success or NULL on failure
 */
GpivPivData *
gpiv_alloc_pivdata			(const guint		nx,
					const guint		ny
					);

/**
 *     Checks if piv_data have been allocated.
 *
 *     @param[in] piv_data     PIV data set to be checked if allocated
 *     @return                 NULL on success or error message on failure
 */
gchar *
gpiv_check_alloc_pivdata		(const GpivPivData      *piv_data
					);

/**
 *      Frees memory for GpivPivData.
 *
 *      @param[in] piv_data     PIV data structure
 *      @param[out] piv_data    NULL pointer to point_x, point_y, dx, dy, snr 
 *                              and peak_no
 *      @return                 void
 */
void 
gpiv_free_pivdata			(GpivPivData		*piv_data
					);



/**
 *      Sets estimators, snr and peak_nr of piv_data to 0 or 0.0. 
 *      The structure will have to be allocated before (with 
 *      gpiv_alloc_pivdata).
 *
 *      @param[in] piv_data     PIV data
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_0_pivdata				(GpivPivData		*piv_data
					);



/**
 *      Returns a copy of piv_data.  
 *      Piv_data_in will have to be allocated with gpiv_alloc_pivdata and the 
 *      returned datawill have to be freed with gpiv_free_pivdata.
 *
 *      @param[in] piv_data     Input PIV data
 *      @return                 GpivPivData on success or NULL on failure
 */
GpivPivData *
gpiv_cp_pivdata				(const GpivPivData      *piv_data
					);


/**
 *      Overwrites piv_data_out with piv_data_in.
 *      Both PIV data sets will have to be allocated with gpiv_alloc_pivdata
 *      and must be of equal sizes.
 *
 *      @param[in] piv_data_in  Input PIV data
 *      @param[in] piv_data_out Output PIV data
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_ovwrt_pivdata			(const GpivPivData      *piv_data_in,
					GpivPivData		*piv_data_out
					);



/**
 *      Adds displacements (dx, dy), snr and peak_nr from piv_data_in to 
 *      piv_data_out. Both structures will have to be allocated before 
 *      (with gpiv_alloc_pivdata).
 *
 *      @param[in] piv_data_in          Input PIV data structure
 *      @param[out] piv_data_out        Output PIV data structure
 *      @return                         NULL on success or error message on failure
 */
gchar *
gpiv_add_dxdy_pivdata			(const GpivPivData      *piv_data_in,
					GpivPivData		*piv_data_out
					);



/**
 *      Adds all displacements in order to calculate residuals
 *      The structure will have to be allocated before (with 
 *      gpiv_alloc_pivdata).
 *
 *      @param[in] piv_data     PIV data structure
 *      @param[out] sum         resulting sum
 *      @return                 NULL on success or error message on failure
 */
gchar * 
gpiv_sum_dxdy_pivdata			(const GpivPivData      *piv_data,
					gfloat			*sum
					);



/**
 *      Plots piv data as vectors on screen with gnuplot.
 *
 *      @param[in] title                        title of plot
 *      @param[in] gnuplot_scale                vector scale
 *      @param[in] GNUPLOT_DISPLAY_COLOR        display color of window containing graph
 *      @param[in] GNUPLOT_DISPLAY_SIZE         display size of window containing graph
 *      @param[in] image_par                    image parameters
 *      @param[in] piv_par                      piv evaluation parameters
 *      @param[in] piv_data                     piv data
 *      @return                                 NULL on success or error message on failure
 */
gchar *
gpiv_piv_gnuplot			(const gchar            *title, 
					const gfloat		gnuplot_scale,
					const gchar		*GNUPLOT_DISPLAY_COLOR, 
					const guint		GNUPLOT_DISPLAY_SIZE,
					const GpivImagePar	*image_par, 
					const GpivPivPar	*piv_par,
					const GpivPivData	*piv_data
					);



/**
 *      Allocates memory for GpivCov.
 *
 *      @param[in] int_size0     size of zere-padded interrogation area
 *      @param[in] x_corr        two frame image / cross correlation
 *      @return                  covariance on success or NULL on failure
 */
GpivCov *
gpiv_alloc_cov				(const guint            int_size0,
					const gboolean		x_corr
					);

/**
 *      Frees memory for GpivCov.
 *
 *      @param[in] cov          Covariance structure
 *      @return                 void
 */
void 
gpiv_free_cov				(GpivCov		*cov
					);


/*
 * Some MPI routines
 */
#ifdef ENABLE_MPI

/**
 *      Scatters PivData structure for parallel processing with MPI
 *      using MPI_Scatter().
 *      Except of pd->nx and pd->ny, which should broadcasted in forward
 *      in order to allocate memory.)
 *
 *      @param[in] pd PIV       data to be scattered
 *      @param[in] nprocs       number of processes
 *      @param[out] pd_scat     scattered piv data
 *      @return                 void
 */
void
gpiv_piv_mpi_scatter_pivdata 		(GpivPivData 		*pd, 
                              		GpivPivData 		*pd_scat, 
                              		guint 			nprocs
                              		);

/**
 *     Gathers PivData structure for parallel processing with MPI
 *     using MPI_Gather().
 *     Counterpart of gpiv_piv_mpi_scatter_pivdata
 *
 *      @param[in] pd_scat      scattered piv data
 *      @param[out] pd          PIV data to be gathered
 *      @return                 void
 */
void 
gpiv_piv_mpi_gather_pivdata 		(GpivPivData 		*pd_scat, 
                             		GpivPivData 		*pd, 
                             		guint 			nprocs
					);

/**
 *      Calculates number of data counts[] for scatterv rows of data
 *
 *      @param[in] nx           number of x-data (columns)
 *      @param[in] ny           number of y-data (rows)
 *      @return                 pointer to gint array representing counts[]
 */
gint * 
gpiv_piv_mpi_compute_counts		(const guint 		nx, 
                            		const guint 		ny
					);

/**
 *      Calculates displacements array displs[] for scatterv rows of data
 *
 *      @param[in] counts       array containing number of data to be scattered to each node
 *      @param[in] nx           number of x-data (columns)
 *      @param[in] ny           number of x-data (rows)
 *      @return                 pointer to gint array representing displs[]
 */
gint * 
gpiv_piv_mpi_compute_displs		(gint 			*counts, 
                            		const guint 		nx, 
                            		const guint 		ny
					);

/**
 *      Scatters PivData structure to variable arrays for parallel processing 
 *      with MPI using MPI_Scatterv().
 *      Except of pd->nx and pd->ny, which should broadcasted in forward
 *      in order to allocate memory.)
 *
 *      @param[in] pd           PIV data to be scattered
 *      @param[out] pd_scat     scattered piv data
 *      @param[in] counts       array containing number of data to be scattered 
 *                              to each node, obtained with gpiv_piv_mpi_compute_counts()
 *      @param[in] displs       array containing displacement to each node, , obtained 
 *                              with gpiv_piv_mpi_compute_displs()
 *      @return                 void
 */
void
gpiv_piv_mpi_scatterv_pivdata 		(GpivPivData 		*pd, 
                               		GpivPivData 		*pd_scat, 
                               		gint 			*counts,
                               		gint 			*displs
					);

/**
 *      Gathers PivData structure to variable arrays for parallel processing 
 *      with MPI using MPI_Gatherv().
 *      Except of pd->nx and pd->ny, which should broadcasted in forward
 *      in order to allocate memory.)
 *
 *      @param[in] pd_scat      scattered PIV data
 *      @param[out] pd          PIV data to be gathered
 *      @param[in] counts       array containing number of data to be scattered 
 *                              to each node, obtained with gpiv_piv_mpi_compute_counts()
 *      @param[in] displs       array containing displacement to each node, , obtained 
 *                              with gpiv_piv_mpi_compute_displs()
 *      @return                 void
 */
void 
gpiv_piv_mpi_gatherv_pivdata 		(GpivPivData 		*pd_scat, 
                              		GpivPivData 		*pd, 
                              		gint 			*counts,
                              		gint 			*displs
					);

/**
 *      Broadcasts PivData structure to variable arrays for parallel processing 
 *      with MPI using MPI_Bcast().
 *      Except of pd->nx and pd->ny, which should broadcasted in forward
 *      in order to allocate memory.)
 *
 *      @param[out] pd          PIV data to be broadcasted
 *      @return                 void
 */
void 
gpiv_piv_mpi_bcast_pivdata 		(GpivPivData 		*pd
					);

#endif /* ENABLE_MPI */
#endif /* __LIBGPIV_PIV_UTILS_H__ */
