/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 Gerber van der Graaf

   This file is part of libgpiv.

   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  




-------------------------------------------------------------------------------
FILENAME:                gpiv-img_utils.h
LIBRARY:                 libgpiv:
EXTERNAL FUNCTIONS:
                         gpiv_alloc_img
                         gpiv_check_alloc_img
                         gpiv_free_img
                         gpiv_cp_img
                         gpiv_cp_img_data

                         gpiv_mpi_bcast_image
 
LAST MODIFICATION DATE:  $Id: gpiv-img_utils.h,v 1.2 2008-09-25 13:19:53 gerber Exp $
 --------------------------------------------------------------------------- */
/**
\file                   gpiv-img_utils.h
\brief                  utilities module for GpivImage structure

SOURCES:                lib/img_utils.c

LAST MODIFICATION DATE:  $Id: gpiv-img_utils.h,v 1.2 2008-09-25 13:19:53 gerber Exp $
*/

#ifndef __LIBGPIV_IMG_UTILS_H__
#define __LIBGPIV_IMG_UTILS_H__


/**
 *     Allocates memory for GpivImage frames
 *
 *     @param[in] image_par    image parameters
 *     @return                 GpivImage
 */
GpivImage *
gpiv_alloc_img 				(const GpivImagePar 	*image_par
					);


/**
 *     Checks if image frames have been allocated
 *     TODO BUGFIX: check on correct size
 *
 *     @param[in] image        image to be checked if allocated
 *     @return                 NULL on success or error message on failure
 */
gchar *     
gpiv_check_alloc_img 			(const GpivImage 	*image
                      			);



/**
 *     Frees memory of gpiv_image
 *
 *     @param[in] gpiv_image   image structure containging data frames and parameters
 *     @return                 void
 */
void 
gpiv_free_img 				(GpivImage 		*gpiv_image
               				);


/**
 *      Duplicates image. The returned image will have to be freed with 
 *      gpiv_free_img when no longer needed.
 *
 *     @param[in] image        source image
 *     @return                 GpivImage containing the copied header and 
 *                             image data from the source image 
 *                             or NULL on failure.
 */
GpivImage *
gpiv_cp_img 				(const GpivImage 	*image
             				);


/**
 *      Copies contents of img_src to img_dest. Image_src and image_dest
 *      will have to be allocated with gpiv_alloc_img before and will have to
 *      be freed with gpiv_free_img when no longer needed.
 *
 *     @param[in] image_src     source image to be copied
 *     @param[out] image_dest   destination image, copy of source
 *     @return                  NULL on success or error message on failure
 */
gchar *
gpiv_cp_img_data 			(const GpivImage 	*image_src, 
                  			GpivImage 		*image_dest
                  			);
#ifdef ENABLE_MPI

/**
 *     Broadcasts image for MPI
 *
 *     @param[in] image         image to be broadcasted
 *     @param[in] alloc_frame   boolean if frame buffer(s) will have to be allocated
 *				for non-root (rank != 0) nodes
 *     @return                  void
 */
void
gpiv_img_mpi_bcast_image		(GpivImage		*image,
					 const gboolean		alloc_frame
					);

/**
 *     Broadcasts image frame data fro MPI
 *
 *     @param[in] image         image to be broadcasted
 *     @return                  void
 */
void
gpiv_img_mpi_bcast_imgframe		(GpivImage		*image
					);

#endif /* ENABLE_MPI */
#endif /* __LIBGPIV_IMG_UTILS_H__ */
