/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*-----------------------------------------------------------------------------
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 Gerber van der Graaf
   <gerber_graaf@users.sourceforge.net>

   This file is part of libgpiv.

   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  




-------------------------------------------------------------------------------
LIBRARY:                libgpiv
EXTERNAL FUNCTIONS:
                        gpiv_trig_openrtfs
                        gpiv_trig_default_parameters
                        gpiv_trig_read_parameters
                        gpiv_trig_check_parameters_read
                        gpiv_trig_parameters_set
                        gpiv_trig_test_parameter
                        gpiv_trig_fprint_parameters

LAST MODIFICATION DATE:  $Id: gpiv-trig.h,v 1.2 2008-04-09 06:10:28 gerber Exp $
---------------------------------------------------------------------------- */

/*!
\file                   gpiv-trig.h
\brief                  module for triggering lightsource(s) and camera(s) using RTAI

SOURCES:                lib/trig.c, lib/trig_par.c

LAST MODIFICATION DATE:  $Id: gpiv-trig.h,v 1.2 2008-04-09 06:10:28 gerber Exp $
 */
#ifndef __LIBGPIV_TRIG_H__
#define __LIBGPIV_TRIG_H__



/* #include <rtai.h> */
/* #include <fcntl.h> */

/*
 * Default values and keys of GpivTrigPar
 */
#define GPIV_TRIGPAR_DEFAULT__CAP 250    /**< Default parameter for cam_acq_period of __GpivTrigTime */
#define GPIV_TRIGPAR_DEFAULT__LPW 0.02   /**< Default parameter for laser_trig_pw of __GpivTrigTime */
#define GPIV_TRIGPAR_DEFAULT__T2L 0.19   /**< Default parameter for time2laser of __GpivTrigTime */
#define GPIV_TRIGPAR_DEFAULT__DT 10      /**< Default parameter for dt of __GpivTrigTime */
#define GPIV_TRIGPAR_DEFAULT__MODE 2     /**< Default parameter for mode of __GpivTrigTime */
#define GPIV_TRIGPAR_DEFAULT__CYCLES 1   /**< Default parameter for cycles of __GpivTrigTime */
#define GPIV_TRIGPAR_DEFAULT__INCR_DT 1  /**< Default parameter for increment of __GpivTrigTime */

#define GPIV_TRIGPAR_KEY "TRIG"	/**< Key of trigger parameters */
#define GPIV_TRIGPAR_KEY__CAP "Ttime_cap"    /**< Key for cam_acq_period of __GpivTrigTime */
#define GPIV_TRIGPAR_KEY__LPW "Ttime_lpw"   /**< Key for laser_trig_pw of __GpivTrigTime */
#define GPIV_TRIGPAR_KEY__T2L "Ttime_t2l"  /**< Key for time2laser of __GpivTrigTime */
#define GPIV_TRIGPAR_KEY__DT "Ttime_dt"    /**< Key for dt of __GpivTrigTime */
#define GPIV_TRIGPAR_KEY__MODE "Ttime_mode"     /**< Key for mode of __GpivTrigTime */
#define GPIV_TRIGPAR_KEY__CYCLES "Ttime_cycles"   /**< Key for cycles of __GpivTrigTime */
#define GPIV_TRIGPAR_KEY__INCR_DT "Ttime_incr_dt"  /**< Key for increment of __GpivTrigTime */

#define GPIV_TRIGPAR_CAP_MIN 100         /**< Minimum value for cam_acq_period of __GpivTrigTime */
#define GPIV_TRIGPAR_LPW_MIN 0.02        /**< Minimum value for  laser_trig_pw of __GpivTrigTime */
#define GPIV_TRIGPAR_T2L_MIN 0.19        /**< Minimum value for  time2laser of __GpivTrigTime */
#define GPIV_TRIGPAR_DT_MIN 0.10         /**< Minimum value for  dt of __GpivTrigTime */
#define GPIV_TRIGPAR_MODE_MIN 1          /**< Minimum value for  mode of __GpivTrigTime */
#define GPIV_TRIGPAR_CYCLES_MIN 1        /**< Minimum value for  cycles of __GpivTrigTime */
#define GPIV_TRIGPAR_INCR_DT_MIN 0.01    /**< Minimum value for  increment of __GpivTrigTime */

#define GPIV_TRIGPAR_CAP_MAX 1000         /**< Maximum value for cam_acq_period of __GpivTrigTime */
#define GPIV_TRIGPAR_LPW_MAX 0.02         /**< Maximum value for  laser_trig_pw of __GpivTrigTime */
#define GPIV_TRIGPAR_T2L_MAX 0.19         /**< Maximum value for  time2laser of __GpivTrigTime */
#define GPIV_TRIGPAR_DT_MAX 100           /**< Maximum value for  dt of __GpivTrigTime */
#define GPIV_TRIGPAR_MODE_MAX 6           /**< Maximum value for  mode of __GpivTrigTime */
#define GPIV_TRIGPAR_CYCLES_MAX 1         /**< Maximum value for  cycles of __GpivTrigTime */
#define GPIV_TRIGPAR_INCR_DT_MAX 20       /**< Maximum value for  increment of __GpivTrigTime */

#define GPIV_NANO2MILI 0.001 * 0.001            /**< Constant to convert from nano ro milliseconds */
#define GPIV_MILI2NANO 1000 * 1000              /**< Constant to convert from milli to nanseconds */



/**
 * Fifo values
 */
enum GpivFifo {
    GPIV_FIFO_TIMING_PARAMETER = 1,
    GPIV_FIFO_START_COMMAND,
    GPIV_FIFO_STOP_COMMAND,
    GPIV_FIFO_ERROR,
    GPIV_FIFO_JITTER,
};

/**
 * Operating mode
 */
enum GpivTimingMode {
    GPIV_TIMER_MODE__PERIODIC = 1, /**< Indefinite until interrupted */
    GPIV_TIMER_MODE__DURATION,     /**< During specific amount of cycles */
    GPIV_TIMER_MODE__ONE_SHOT_IRQ, /**< Check. Single shot after irq signal */
    GPIV_TIMER_MODE__TRIGGER_IRQ,  /**< Check. Trigger on irq signal */
    GPIV_TIMER_MODE__INCREMENT,    /**< Incremented __GpivTrigTime::dt */
    GPIV_TIMER_MODE__DOUBLE,       /**< Check. Double exposure */
};
/*     GPIV_TIMER_MODE__SNAPSHOT, */
/*     GPIV_TIMER_MODE__EXIT, */



typedef struct __GpivTrigTime GpivTrigTime;

/*!
 * \brief Trigger parameters
 *
 * Used for triggering camera and light source (mostly a double-cavity Nd/YAGG laser)
 * and defining separation time between the frames of a PIV image pair etc. 
 * The parameters might be loaded from the configuration resources, 
 * with gpiv_scan_resourcefiles() or with gpiv_scan_parameter().
 */
struct __GpivTrigTime {
    RTIME cam_acq_period;       /**< camera acquisition period time in ns (period) */
    RTIME laser_trig_pw;        /**< laser trigger pulse width in ns (tp) */
    RTIME time2laser;           /**< time from laser trigger->Hi (tl) until laser pulse: 0.19 ms in ns */
    RTIME dt;                   /**< time between 2 laser exposures in ns */
    enum GpivTimingMode mode;   /**< operating mode (indefinite/interrupt/definite) */
    gint cycles;                /**< number of cycles, equal to the number of image pairs to be recorded */
    RTIME increment;            /**< increment in dt (ms) */
};
  

  
typedef struct __GpivTrigPar GpivTrigPar;

/*!
 * \brief Whether elements of __GpivTrigTime have been set
 *
 * These boolean parameters are set when parameters of  __GpivTrigTime have been
 * defined. Only for __GpivTrigTime, the __set parameters have been 
 * defined in a separate structure in this library.
 */
struct __GpivTrigPar {
    GpivTrigTime ttime; 

    gboolean ttime__cam_acq_period__set; /**< flag if cam_acq_period has been defined */
    gboolean ttime__laser_trig_pw__set;  /**< flag if laser_trig_pw has been defined */
    gboolean ttime__time2laser__set;     /**< flag if time2laser has been defined */
    gboolean ttime__dt__set;             /**< flag if dt has been defined */
    gboolean ttime__mode__set;           /**< flag if mode has been defined */
    gboolean ttime__cycles__set;         /**< flag if cycles has been defined */
    gboolean ttime__increment__set;      /**< flag if increment has been defined */
};



/**
 *      Opens communication channels to camlasco-rtl kernel module.
 *
 *      @param[in] init         initialization or uploading of trigger parameters
 *      @param[in] trig         trigger signel
 *      @param[in] stop         stop signal
 *      @param[in] error        error signal
 *      @return                 void
 */
gint 
gpiv_trig_openrtfs			(gint			*init, 
					gint			*trig, 
					gint			*stop, 
					gint			*error
					);



/**
 *      Sets default GpivTrigPar parameter values.
 *
 *      @param[in] force                flag to enforce parameters set to defaults
 *      @param[out] trig_par_default    structure of triggering parameters
 *      @return                         void
 */
void
gpiv_trig_default_parameters		(GpivTrigPar		*trig_par_default,
					const gboolean		force
					);



/**
 *      Read all GpivTrigPar parameters.
 *
 *      @param[in] fp_par       file pointer to parameter file
 *      @param[out] trig_par    triggering parameters
 *      @param[in] verbose      flag to print parametrs to stdout
 *      @return                 void
 */
void 
gpiv_trig_read_parameters		(FILE			*fp_par, 
					GpivTrigPar		*trig_par, 
					const gboolean		verbose
					);



/**
 *      Check out if all GpivTrigPar parameters have been read.
 *
 *      @param[in] trig_par_default     default trigger parameters. If NULL, library default
 *                                      values are used.
 *      @param[out] trig_par            trigger parameters
 *      @return                         NULL on success or *err_msg on failure
 */
gchar *
gpiv_trig_check_parameters_read		(GpivTrigPar		*trig_par,
					const GpivTrigPar	*trig_par_default
					);



/**
 *      Sets flags for __set variables of GpivTrigPar.
 *
 *      @param[in] flag         flag to be set 
 *      @param[out] trig_par    triggering parameters
 *      @return                 void
 */
void
gpiv_trig_parameters_set		(GpivTrigPar		*trig_par,
					const gboolean		flag
					);



/**
 *      Testing GpivTrigPar parameters on valid values.
 *
 *      @param[in]  trig_par    trigger parameters   
 *      @param[out] trig_par    trigger parameters
 *      @return                 NULL on success or *err_msg on failure
 */ 
gchar *
gpiv_trig_test_parameter		(const GpivTrigPar	*trig_par
					);



/**
 *      Prints GpivTrigPar parameters to fp_par_out.
 *
 *      @param[in] fp_par_out   output file
 *      @param[in] trig_par     triggering parameters
 *      @return                 void
 */
void 
gpiv_trig_print_parameters		(FILE			*fp_par_out, 
					const GpivTrigPar *trig_par
					);

#endif /*__LIBGPIV_TRIG_H__ */
