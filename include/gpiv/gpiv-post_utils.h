/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2007, 2008 Gerber van der Graaf

   This file is part of libgpiv.

   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  




-------------------------------------------------------------------------------
LIBRARY:                 libgpiv:
EXTERNAL FUNCTIONS:
			 gpiv_null_scdata
			 gpiv_alloc_scdata
			 gpiv_free_scdata

                         gpiv_null_bindata
			 gpiv_alloc_bindata
			 gpiv_free_bindata


LAST MODIFICATION DATE:  $Id: gpiv-post_utils.h,v 1.1 2007-11-23 16:12:18 gerber Exp $
 --------------------------------------------------------------------------- */
/**
\file                   gpiv-post_utils.h
\brief                  utilities for GpivScalarData and GpivBinData

SOURCES:                lib/post_utils.c

LAST MODIFICATION DATE:  $Id: gpiv-post_utils.h,v 1.1 2007-11-23 16:12:18 gerber Exp $
 */

#ifndef __LIBGPIV_POST_UTILS_H__
#define __LIBGPIV_POST_UTILS_H__


/**
 *      Sets all elements of sc_data structure to NULL.
 *
 *      @param[in] scal_data    Input Scalar data structure
 *      @param[out] scal_data   Output Scalar data structure
 *      @return                 void
 */
void
gpiv_null_scdata			(GpivScalarData		*scal_data
					);


/**
 *      Allocates memory for GpivScalarData.
 *
 *      @param[in] nx           columns array length of GpivScalarData
 *      @param[in] ny           row array length of GpivScalarData structure
 *      @return                 GpivScalarData or NULL on failure
 */
GpivScalarData *
gpiv_alloc_scdata			(const gint		nx,
					const gint		ny
					);


/**
 *      Frees memory for GpivScalarData.
 *
 *      @param[in] scal_data    scalar data structure
 *      @param[out] scal_data   NULL pointer to point_x, point_y, scalar, flag
 *      @return                 void
 */
void 
gpiv_free_scdata			(GpivScalarData		*scal_data
					);



/**
 *      Graphical output with gnuplot for scalar data.
 *
 *      @param[in] fname_out               file name containing plot
 *      @param[in] title                   title of plot
 *      @param[in] GNUPLOT_DISPLAY_COLOR   display color of window containing graph
 *      @param[in] GNUPLOT_DISPLAY_SIZE    display size of window containing graph
 *      @return                            NULL on success or error message on failure
 */
gchar *
gpiv_scalar_gnuplot			(const gchar		*fname_out, 
					const gchar		*title, 
					const gchar		*GNUPLOT_DISPLAY_COLOR, 
					const gint		GNUPLOT_DISPLAY_SIZE
					);



/**
 *      Sets all elements of bin_data structure to NULL.
 *
 *      @param[in] bin_data     Input Bin data structure
 *      @param[out] bin_data    Output Bin data structure
 *      @return                 void
 */
void
gpiv_null_bindata			(GpivBinData		*bin_data
					);


/**
 *      Allocates memory for GpivBinData.
 *
 *      @param[in] nbins        number of bins or klasses in GpivBinData
 *      @return                 GpivBinData on success or NULL on failure
 */
GpivBinData *
gpiv_alloc_bindata			(const guint		nbins
					);


/**
 *      Frees memory for GpivBinData.
 *
 *      @param[in] bin_data     data of bins
 *      @param[out] bin_data    NULL pointer to count, bound, centre
 *      @return                 void
 */
void
gpiv_free_bindata			(GpivBinData		*bin_data
					);


/**
 *      Calculates histogram from GpivPivData (NOT from GpivScalarData!!).
 *
 *      @param[in] data         Input data
 *      @param[out] klass       Output data
 *      @return                 void
 */
void 
gpiv_histo				(const GpivPivData      *data,
					GpivBinData		*klass 
					);



/**
 *      Calculates cumulative histogram from GpivPivData 
 *      (NOT from GpivScalarData!!).
 *
 *      @param[in] data         Input data
 *      @param[out] klass       Output data
 *      @return                 void
 */
void 
gpiv_cumhisto				(const GpivPivData      *data, 
					GpivBinData		*klass
					);



/**
 *      Plots histogram on screen with gnuplot.
 *
 *      @param[in] fname_out 	               output filename
 *      @param[in] title 	               plot title
 *      @param[in] GNUPLOT_DISPLAY_COLOR        display color of window containing graph
 *      @param[in] GNUPLOT_DISPLAY_SIZE         display size of window containing graph
 *      @return                                 NULL on success or error message on failure
 */
gchar *
gpiv_histo_gnuplot			(const gchar		*fname_out,
					const gchar		*title,
					const gchar		*GNUPLOT_DISPLAY_COLOR,
					const gint		GNUPLOT_DISPLAY_SIZE
					);


#endif /* __LIBGPIV_POST_UTILS_H__ */
