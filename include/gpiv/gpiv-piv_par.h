/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 Gerber van der Graaf

   This file is part of libgpiv.

   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  



------------------------------------------------------------------------------
FILENAME:                piv_par.c
LIBRARY:                 libgpiv
EXTERNAL FUNCTIONS:      
                         gpiv_piv_parameters_set
                         gpiv_piv_default_parameters
                         gpiv_piv_get_parameters_from_resources

                         gpiv_piv_read_parameters
                         gpiv_piv_check_parameters_read
		         gpiv_piv_test_parameters
                         gpiv_piv_print_parameters
                         gpiv_piv_cp_parameters
                         gpiv_piv_cp_undef_parameters
                         gpiv_piv_fread_hdf5_parameters
                         gpiv_piv_fwrite_hdf5_parameters

--------------------------------------------------------------------- */

/*!
\file                   gpiv-piv_par.h
\brief                  module for parameters for image evaluation

SOURCES:                lib/piv_par.c

LAST MODIFICATION DATE: $Id: gpiv-piv_par.h,v 1.3 2008-09-25 13:19:53 gerber Exp $
 */


#ifndef __LIBGPIV_PIVPAR_H__
#define __LIBGPIV_PIVPAR_H__

/*-----------------------------------------------------------------------------
 * Parameters for Piv evaluation
 */

#define GPIV_PIVPAR_KEY                      "PIV"           /**< Key of evaluation parameters */


/**
 * PIV interrogation scheme
 */
enum GpivIntScheme {
    GPIV_NO_CORR,
    GPIV_LK_WEIGHT,              /**< Linear kernel weighting if Interrogation Area's*/
    GPIV_ZERO_OFF_FORWARD,       /**< classical zero ofsetting */
    GPIV_ZERO_OFF_CENTRAL,       /**< zero ofsetting using central differential scheme */
    GPIV_IMG_DEFORM              /**< image deformation */
};


/**
 * Image evaluation or interrogation parameters for obtaining PIV estimators
 */
typedef struct __GpivPivPar GpivPivPar;

/*!
 * \brief Parameters for PIV evaluation (interrogation) of images
 *
 * These parameters are needed by the functions for interrogating a PIV image-pair,
 * like: interrogation schemes, sub-pixel interpolation methods, Area of Interest.
 * The parameters might be loaded from the configuration resources, 
 * with gpiv_scan_resourcefiles(), gpiv_scan_parameter() or with 
 * gpiv_piv_read_parameters().
 */
struct __GpivPivPar {
    guint col_start;                    /**< first column of image to use for interrogation */
    gboolean col_start__set;            /**< flag if col_start has been defined */

    guint col_end;                      /**< last column in image to use for interrogation */
    gboolean col_end__set;              /**< flag if col_end has been defined */

    guint ifit;                         /**< Type of sub-pixel estimation */
    gboolean ifit__set;                 /**< flag if ifit has been defined */

    guint int_geo;                      /**< Interrogation along a line or within an area of interest */
    gboolean int_geo__set;              /**< flag if int_geo has been defined */

    guint int_line_col;                 /**< interrogation along a column */
    gboolean int_line_col__set;         /**< flag if int_line_col has been defined */

    guint int_line_col_start;           /**< first column of image to use for interrogation along a line */
    gboolean int_line_col_start__set;   /**< flag if int_line_col_start has been defined */

    guint int_line_col_end;             /**< last column of image to use for interrogation along a line */
    gboolean int_line_col_end__set;     /**< flag if int_line_col_end has been defined */

    guint int_line_row;                 /**< interrogation along a row */
    gboolean int_line_row__set;         /**< flag if int_line_row has been defined */

    guint int_line_row_start;           /**< first row of image to use for interrogation along a column */
    gboolean int_line_row_start__set;   /**< flag if int_line_row_start has been defined */

    guint int_line_row_end;             /**< first row of image to use for interrogation along a column */
    gboolean int_line_row_end__set;     /**< flag if int_line_row_end has been defined */

    guint int_point_col;                /**< column of a interrogation area for single point */
    gboolean int_point_col__set;        /**< flag if int_point_col has been defined */

    guint int_point_row;                /**< row of a interrogation area for single point */
    gboolean int_point_row__set;        /**< flag if int_point_row has been defined */

    guint int_size_f;                   /**< final interrogation area size; to be ended with */
    gboolean int_size_f__set;           /**< flag if int_size_i has been defined */

    guint int_size_i;                   /**< initial interrogation area size; to start with */
    gboolean int_size_i__set;           /**< flag if int_size_f has been defined */

    gint int_shift;                     /**< interrogation area shift */
    gboolean int_shift__set;            /**< flag if int_shift has been defined */

    int int_deform;                     /**< deform image for improved accuracy */    
    gboolean int_deform__set;           /**< flag if int_deform has been defined */

    gint peak;                          /**< peak number of correlation function to be used as estimator */
    gboolean peak__set;                 /**< flag if peak has been defined */

    int pre_shift_col;                  /**< general pre-shift in horizontal direction (columns) */
    gboolean pre_shift_col__set;        /**< flag if pre_shift_col has been defined */

    int pre_shift_row;                  /**< general pre-shift in vertical direction (rows) */
    gboolean pre_shift_row__set;        /**< flag if pre_shift_row has been defined */

    int print_par;                      /**< print parameters */
    gboolean print_par__set;            /**< flag if print_par has been defined */

    int print_piv;                      /**< print piv data */
    gboolean print_piv__set;            /**< flag if print_piv has been defined */

    guint row_start;                    /**< last row in image to use for interrogation */
    gboolean row_start__set;            /**< flag if row_start has been defined */

    guint row_end;                      /**< first row of image to use for interrogation */
    gboolean row_end__set;              /**< flag if row_end has been defined */

    int spline_degree;                  /**< degree of b-splines to be used for image data interpolation */
    gboolean spline_degree__set;        /**< flag if spline_degree has been defined */

    enum GpivIntScheme int_scheme;      /**< Way to correct on biasing effects with value of GpivIntScheme */
    gboolean int_scheme__set;           /**< flag if int_scheme has been defined */

    gboolean gauss_weight_ia;           /**< Gaussian weighting to I.A. */
    gboolean gauss_weight_ia__set;      /**< flag if gauss_weight_ia has been defined */

    gboolean spof_filter;               /**< Symmetric Phase Only filtering of covariance function */
    gboolean spof_filter__set;          /**< flag if spof_filter has been defined */
};


/*
 * Function prototypes
 */

/**
 *      Sets default parameter values.
 *
 *      @param[in] force                flag to enforce parameters set to 
 *                                      defaults
 *      @param[out] piv_par_default     structure of piv evaluation 
 *                                      parameters
 *      @return                         void
 */
void
gpiv_piv_default_parameters		(GpivPivPar		*piv_par_default,
					const gboolean		force
					);



/**
 *      Reads piv parameters from system-wide gpiv.conf and $HOME/.gpivrc.
 *
 *      @param[in] localrc      resource filename containing parameter at 
                                current directory
 *      @param[in] verbose      prints parameter values when read
 *      @return                 GpivPivPar or NULL on failure
 */
GpivPivPar *
gpiv_piv_get_parameters_from_resources  (const gchar		*localrc,
                                         const gboolean		verbose
                                         );



/**
 *      Sets flags for all piv_par__set.
 *
 *      @param[in] flag         true (1) or false (0)
 *      @param[out] piv_par     PIV evaluation parameters
 *      @return                 void
 */
void
gpiv_piv_parameters_set			(GpivPivPar		*piv_par,
					const gboolean		flag
					);



/**
 *      Reads all parameters for PIV evaluation.
 *
 *      @param[in] fp           file pointer to parameter file.
 *                              If NULL, stdin will be used.      
 *      @param[in] verbose      flag to print parameters to stdout
 *      @param[out] piv_par     PIV evaluation parameters
 *      @return                 NULL on success or error message on failure
 */
void 
gpiv_piv_read_parameters		(FILE			*fp, 
					GpivPivPar		*piv_par,
					const gboolean		verbose
					);



/**
 *      Checks if all PIV parameters have been read. 
 *      If a parameter has not been read, it will be set to piv_par_default or to 
 *	its hard-coded default value in case piv_par_default is NULL.
 *
 *      @param[in] piv_par_default     default parameters. If NULL, library default 
 *                                     values are used.
 *      @param[out] piv_par            PIV evaluation parameters
 *      @return                        NULL on success or error message on failure
 */
gchar *
gpiv_piv_check_parameters_read		(GpivPivPar             *piv_par,
					const GpivPivPar	*piv_par_default
					);



/**
 *      Tests all piv parameters if have been read and have been defined to 
 *      valid values.
 *
 *      @param[in] image_par    image parameters
 *      @param[in] piv_par      PIV evaluation parameters
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_piv_testonly_parameters		(const GpivImagePar     *image_par, 
					const GpivPivPar	*piv_par
					);



/**
 *      Tests if all piv parameters have been read and have been defined 
 *      to valid values. Aplies missing parameters to defaults, as hard-coded
 *      in the library and adjusts parameters if necessary.
 *
 *      @param[in] image_par    image parameters
 *      @param[in] piv_par      PIV parameters
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_piv_testadjust_parameters		(const GpivImagePar     *image_par, 
					GpivPivPar		*piv_par
					);



/**
 *      Prints parameters to fp.
 *
 *      @param[in] fp           file pointer for output.
 *                              If NULL, stdout will be used.      
 *      @param[in] piv_par      PIV evaluation parameters
 *      @return                 void
 */
void 
gpiv_piv_print_parameters		(FILE                   *fp,
					const GpivPivPar	*piv_par
					);

/**
 *      Copies piv parameters.
 *
 *      @param[in] piv_par       piv parameters to be copied
 *      @return                  GpivPivPar or NULL on failure
 */
GpivPivPar *
gpiv_piv_cp_parameters			(const GpivPivPar       *piv_par
					);


/**
 *      Copies piv parameters from piv_par_src to piv_par_dest if 
 *      piv_par_dest have not been set.
 *
 *      @param[in] piv_par_src         piv parameters to be copied
 *      @param[out] piv_par_dest       the copied piv parameters
 *      @return                        void
 */
void
gpiv_piv_cp_undef_parameters		(const GpivPivPar       *piv_par_src, 
					GpivPivPar		*piv_par_dest
					);


/**
 *      Reads piv parameters from hdf5 data file.
 *
 *      @param[in] fname        input filename
 *      @return                 GpivPivPar of NULL on failure
 */
GpivPivPar *
gpiv_piv_fread_hdf5_parameters		(const gchar		*fname
					);


/**
 *      Writes piv parameters from hdf5 data file
 *
 *      @param[in] fname        output filename      
 *      @param[out] piv_par     PIV evaluation parameters  
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_piv_fwrite_hdf5_parameters         (const gchar            *fname,
                                         const GpivPivPar       *piv_par 
                                         );


#ifdef ENABLE_MPI
/**
 *      Broadcasts GpivPivPar for MPI
 *
 *	@param[in] piv_par	Parameter struct to be broadcasted
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_piv_mpi_bcast_pivpar		(GpivPivPar		*piv_par);
#endif /* ENABLE_MPI */
#endif /* __LIBGPIV_PIVPAR_H__ */
