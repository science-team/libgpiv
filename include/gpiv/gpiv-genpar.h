/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*-----------------------------------------------------------------------------

   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2008 Gerber van der Graaf

   This file is part of libgpiv.

   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

-------------------------------------------------------------------------------
FILENAME:               gpiv-genpar.h
LIBRARY:                libgpiv:
EXTERNAL FUNCTIONS:
SOURCE:                 genpar.c
                        gpiv_genpar_parameters_set
                        gpiv_genpar_default_parameters
                        gpiv_genpar_fscan_parameters
                        gpiv_genpar_read_parameters 
                        gpiv_genpar_check_parameters
                        gpiv_genpar_test_parameters
                        gpiv_genpar_print_parameters
                        gpiv_genpar_fprint_parameters
                        gpiv_genpar_cp_parameters


-----------------------------------------------------------------------------*/
/*!
\file                  gpiv-genpar.h
\brief                 General parameters

SOURCE:                lib/genpar.c

LAST MODIFICATION DATE:  $Id: gpiv-genpar.h,v 1.1 2008-04-22 12:58:02 gerber Exp $
 */


#ifndef __LIBGPIV_GENPAR_H__
#define __LIBGPIV_GENPAR_H__

#define GPIV_GENPAR_KEY "GENPAR"       /**< Key of general parameters */


typedef struct __GpivGenPar GpivGenPar;

/*!
 * \brief Parameters for general purposes
 *
 * These parameters are not bound by any of the image processing, interrogation or
 * data processing functions. They may be used for statistical processes, bulk 
 * processing etc.
 * The parameters might be loaded from the configuration resources, 
 * with gpiv_scan_resourcefiles() or with gpiv_scan_parameter().
 */
struct __GpivGenPar {
    guint first_dir;            /**< First numbered directory to be scanned */
    gboolean first_dir__set;    /**< flag if first_dir has been defined */

    guint last_dir;             /**< Last numbered directory to be scanned */
    gboolean last_dir__set;     /**< flag if last_dir has been defined */

    gboolean  dir_prefix;       /**< Use a prefix before the directory basename */
    gboolean dir_prefix__set;   /**< flag if dir_prefix has been defined */
 
    guint first_file;           /**< First numbered file to be scanned */
    gboolean first_file__set;   /**< flag if first_file has been defined */

    guint last_file;            /**< Last numbered file to be scanned */
    gboolean last_file__set;    /**< flag if last_file has been defined */

    gboolean file_prefix;       /**< Use a prefix before the file basename */
    gboolean file_prefix__set;  /**< flag if file_prefix has been defined */
};


/**
 *      Sets flag for genpar__set
 *
 *      @param[in] flag			flag to enforce parameters set to defaults
 *      @param[out] gen_par	        image processing parameters
 *      @return				void
 */
void
gpiv_genpar_parameters_set (GpivGenPar                          *gen_par,
			    const gboolean                      flag
			    );



/**
 *     Sets default parameter values
 *
 *     @param[in] force                 flag to enforce parameters set to defaults
 *     @param[out] gen_par_par_default  structure of general parameters
 *     @return                          void
 */
void
gpiv_genpar_default_parameters		(GpivGenPar             *gen_par_default,
					const gboolean          force
					);



/**
 *     Reads general parameters
 *
 *     @param[in] fp_h                 file to be read
 *     @param[in] gen_par              pointer to structure of general parameters
 *     @param[in] print_par            prints parameters to stdout during reading
 *     @param[out] general_par         pointer to structure of general parameters
 *     @return void
 */
void
gpiv_genpar_read_parameters		(FILE                   *fp_h, 
					GpivGenPar		*gen_par, 
					const gboolean		print_par
					);



/**
 *      Checks if all necessary general parameters have been read.
 *      If a parameter has not been read, it will be set to gen_par_default or to 
 *	its hard-coded default value in case gen_par_default is NULL.
 *
 *     @param[in] gen_par_default		general parameters containing default values
 *     @param[out] gen_par			structure of general parameters
 *     @return					NULL on success or error message on failure
 */
gchar *
gpiv_genpar_check_parameters_read	(GpivGenPar		*gen_par,
					const GpivGenPar	*gen_par_default
					);



/**
 *     Tests general parameters on validity.
 *
 *     @param[in] gen_par	       structure of general parameters
 *     @return                         NULL on success or error message on failure
 */
gchar *
gpiv_genpar_test_parameters		(const GpivGenPar       *gen_par
					);



/**
 *     Prints general parameters to file.
 *
 *     @param[in] fp                   file pointer to which paraeters will 
 *                                     be printed. If NULL, stdout will be used
 *     @param[in] gen_par	       structure of general parameters
 *     @return                         void
 */
void
gpiv_genpar_print_parameters		(FILE                   *fp, 
					const GpivGenPar	*gen_par
					);


/**
 *     Copies general parameters.
 *
 *     @param[in] genpar_par  general parameters to be copied
 *     @return                 GpivGenPar or NULL on failure
 */
GpivGenPar *
gpiv_genpar_cp_parameters		(const GpivGenPar       *gen_par
					);




#endif /* __LIBGPIV_GENPAR_H__ */
