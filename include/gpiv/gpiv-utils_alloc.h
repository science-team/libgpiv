/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2007, 2008 Gerber van der Graaf

   This file is part of libgpiv.

   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  




-------------------------------------------------------------------------------
LIBRARY:                 libgpiv:
EXTERNAL FUNCTIONS:
                         check_img_exist

                         gpiv_scan_iph,
                         gpiv_scan_iph_nl,
                         gpiv_fscan_iph, 
                         gpiv_scan_cph, 
                         gpiv_fscan_cph, 
                         gpiv_scan_fph, 
                         gpiv_fscan_fph, 
                         gpiv_scan_sph, 
                         gpiv_fscan_sph, 
                         gpiv_sort_3,


                         gpiv_matrix, 
                         gpiv_free_matrix
                         gpiv_matrix_index 
                         gpiv_free_matrix_index
			 gpiv_ucmatrix, 
                         gpiv_free_ucmatrix
                         gpiv_matrix_guint16
                         gpiv_free_matrix_guint16
			 gpiv_imatrix, 
                         gpiv_free_imatrix
                         gpiv_double_matrix
                         gpiv_free_double_matrix
                         gpiv_fftw_real_matrix
                         gpiv_free_fftw_real_matrix

                         gpiv_vector,
                         gpiv_free_vector, 
                         gpiv_vector_index,
                         gpiv_free_vector_index, 

                         gpiv_dvector,
                         gpiv_free_dvector, 
                         gpiv_dvector_index,
                         gpiv_free_dvector_index, 

                         gpiv_nulvector,
                         gpiv_free_nulvector,
                         gpiv_nulvector_index,
                         gpiv_free_nulvector_index, 

                         gpiv_ulvector,
                         gpiv_free_ulvector, 
                         gpiv_ulvector_index,
                         gpiv_free_ulvector_index, 

                         gpiv_ivector,
                         gpiv_free_ivector, 
                         gpiv_ivector_index,
                         gpiv_free_ivector_index
                         gpiv_gbolvector
                         gpiv_free_gbolvector


LAST MODIFICATION DATE:  $Id: gpiv-utils_alloc.h,v 1.1 2007-11-23 16:12:18 gerber Exp $
 --------------------------------------------------------------------------- */
/**
\file                   gpiv-utils_alloc.h
\brief                  utilities for memory allocation

SOURCES:                lib/utils_alloc.c

LAST MODIFICATION DATE:  $Id: gpiv-utils_alloc.h,v 1.1 2007-11-23 16:12:18 gerber Exp $
 */

#ifndef __L_UTILS_H__
#define __L_UTILS_H__

/**
 *     Allocates memory for 2-dimensional matrix of gfloat data
 *
 *     @param[in] nr 	       number of rows
 *     @param[in] nc 	       number of columns
 *     @return                 matrix
 */
gfloat **
gpiv_matrix				(long			nr,
					long			nc
					);



/**
 *     Frees memory for 2-dimensional array of gfloat data
 *
 *     @param[in] m 	       matrix 
 *     @return void
 */
void 
gpiv_free_matrix			(gfloat			**m
					);



/**
 *      Allocates memory for 2-dimensional matrix of gfloat data 
 *      with subscript range v[nrl..nrh][ncl..nch]
 *
 *      @param[in] nrl 	lowest row index number
 *      @param[in] nrh 		highest row index number
 *      @param[in] ncl 		lowest column index number
 *      @param[in] nch	        highest column index number
 *      @return 2-dimensional array
 */
gfloat **
gpiv_matrix_index			(const long		nrl,
					 const long		nrh,
					 const long		ncl,
					 const long 		nch
					 );



/**
 *      Frees memory for 2-dimensional array of gfloat data
 *      with subscript range v[nrl..nrh][ncl..nch]
 *
 *      @param[in] m 	        matrix
 *      @param[in] nrl 	        lowest row index number
 *      @param[in] nrh           highest row index number
 *      @param[in] ncl 	        lowest column index number
 *      @param[in] nch 	        highest column index number
 *      @return void
 */
void 
gpiv_free_matrix_index			(gfloat			**m,
					const long		nrl,
					const long		nrh,
					const long		ncl,
					const long		nch
					);



/**
 *      Allocates memory for 2-dimensional matrix of guchar data
 *
 *      @param[in] nr 	row index
 *      @param[in] nc 	column index
 *      @return matrix
 */
guchar **
gpiv_ucmatrix				(const long		nr,
					const long		nc
					);



/**
 *     Frees memory for 2-dimensional array of guchar data
 *
 *      @param[in] m               matrix
 *      @return void
 */
void 
gpiv_free_ucmatrix			(guchar			**m
					);



/**
 *      Allocates a guint8 matrix with subscript range m[0..nr][0..nc]
 *
 *      @param[in] nr              number of rows
 *      @param[in] nc              number of columns
 *      @return matrix
 */
guint8 **
gpiv_matrix_guint8			(const long		nr, 
					const long		nc
					);



/**
 *      Frees a guint8 matrix allocated by gpiv_matrix_guint8
 *
 *      @param[in] m 	        matrix 
 *      @return void
 */
void 
gpiv_free_matrix_guint8			(guint8			**m
					);


/**
 *      Allocates a guint16 matrix with subscript range m[0..nr][0..nc]
 *
 *      @param[in] nr              number of rows
 *      @param[in] nc              number of columns
 *      @return matrix
 */
guint16 **
gpiv_matrix_guint16			(const long		nr, 
					const long		nc
					);



/**
 *      Frees a guint16 matrix allocated by gpiv_matrix_guint16
 *
 *      @param[in] m 	        matrix 
 *      @return void
 */
void 
gpiv_free_matrix_guint16		(guint16		**m
					);



/**
 *      Allocates memory for 2-dimensional matrix of integer data
 *      with subscript range v[nrl..nrh][ncl..nch]
 *
 *	@param[in] nrl 	        lowest row index number
 *	@param[in] nrh 	        highest row index number
 *	@param[in] ncl 	        lowest column index number
 *	@param[in] nch 	        highest column index number
 *      @return matrix
 */
gint **
gpiv_imatrix_index			(const long		nrl,
					const long		nrh,
					const long		ncl,
					const long		nch
					);



/**
 *      Frees memory for 2-dimensional array of integer data
 *      with subscript range v[nrl..nrh][ncl..nch]
 *
 *      @param[in] m 	        matrix 
 *	@param[in] nrl 	        lowest row index number
 *	@param[in] nrh 	        highest row index number
 *	@param[in] ncl 	        lowest column index number
 *	@param[in] nch 	        highest column index number
 *      @return void
 */
void 
gpiv_free_imatrix_index			(gint			**m, 
					const long		nrl, 
					const long		nrh, 
					const long		ncl, 
                                        const long		nch
                                        );



/**
 *      Allocates memory for 2-dimensional matrix of integer data
 *      depreciated, use gpiv_imatrix_index instead
 *
 *	@param[in] nr 	        number of rows
 *	@param[in] nc 	        number of columns
 *      @return matrix
 */
gint **
gpiv_imatrix				(const long		nr,
					const long		nc
					);



/**
 *      Frees memory for 2-dimensional array of integer data
 *      unappreciated, use gpiv_free_imatrix_index instead
 *
 *      @param[in] m 	        matrix 
 *      @return void
 */
void 
gpiv_free_imatrix			(gint			**m
					);



double **
/**
 *      Allocates a contiguous 2-dimensional double matrix 
 *      of nr x nc
 *
 *      @param[in] nr              number of rows
 *      @param[in] nc              number of columns
 *      @return matrix
 */
gpiv_double_matrix			(const glong		nr, 
					const glong		nc 
					);



/**
 *      Frees a double matrix allocated by gpiv_double_matrix()
 *
 *      @param[in] m 	        matrix 
 *      @return void
 */
void 
gpiv_free_double_matrix			(double			**m
					);


#ifndef USE_FFTW3
/**
 *      Allocates a contiguous 2-dimensional fftw_real_matrix matrix 
 *      of nr x nc
 *
 *      @param[in] nr              number of rows
 *      @param[in] nc              number of columns
 *      @return matrix
 */
fftw_real **
gpiv_fftw_real_matrix			(glong			nr, 
					glong			nc 
					);



/**
 *      Frees a gdouble matrix allocated by gpiv_fftw_real_matrix_matrix()
 *
 *      @param[in] m 	        matrix 
 *      @return void
 */
void 
gpiv_free_fftw_real_matrix		(fftw_real		**m
					);

#endif /* USE_FFTW3 */


/**
 *      Allocates a contiguous 2-dimensional fftw_complex matrix 
 *      of nr x nc
 *
 *      @param[in] nr              number of rows
 *      @param[in] nc              number of columns
 *      @return matrix
 */
fftw_complex **
gpiv_fftw_complex_matrix		(const long		nr, 
					const long		nc 
					);



/**
 *      Frees a fftw_real matrix allocated by gpiv_fftw_complex_matrix()
 *
 *      @param[in] m 	        matrix 
 *      @return void
 */
void 
gpiv_free_fftw_complex_matrix		(fftw_complex		**m
					);


/**
 *      Allocates memory for a 1-dimensional vector of gfloat data
 *
 *      @param[in] nl 		vector length
 *      @return vector
 */
gfloat *
gpiv_vector				(const long		nl
					);



/**
 *      Frees memory for a 1-dimensional vector of gfloat data
 *
 *      @param[in] vector              vector of 1-dimensional gfloat data
 *      @return void
 */
void 
gpiv_free_vector			(gfloat			*vector
					);



/**
 *      Allocates memory for a 1-dimensional vector of gfloat data
 *      with subscript range v[nl..nh]
 *
 *      @param[in] nl 	       lowest index number
 *      @param[in] nh 	       highest index number
 *      @return vector
 */
gfloat *
gpiv_vector_index			(const long		nl,
					const long		nh
					);



/**
 *      Frees memory for a 1-dimensional vector of gfloat data
 *      with subscript range v[nrl..nrh][ncl..nch]
 *
 *      @param[in] vector         vector of 1-dimensional gfloat data
 *      @param[in] nl 	       lowest index number
 *      @param[in] nh 	       highest index number
 *      @return void
 */
void 
gpiv_free_vector_index			(gfloat			*vector,
					const long		nl,
					const long		nh
					);



/**
 *      Allocates memory for a 1-dimensional vector of gdouble data
 *
 *      @param[in] nl 		vector length
 *      @return vector
 */
gdouble *
gpiv_dvector				(const glong		nl
					);



/**
 *      Frees memory for a 1-dimensional vector of double data
 *
 *      @param[in] vector              vector of 1-dimensional gfloat data
 *      @return void
 */
void
gpiv_free_dvector			(gdouble		*vector
					);



/**
 *      Allocates memory for a 1-dimensional vector of double data
 *      with subscript range v[nl..nh]
 *
 *      @param[in] nl 	       lowest index number
 *      @param[in] nh 	       highest index number
 *      @return vector
 */
gdouble *
gpiv_dvector_index			(const long		nl, 
					const long		nh
					);



/**
 *      Frees memory for a 1-dimensional vector of double data
 *      with subscript range v[nrl..nrh][ncl..nch]
 *
 *      @param[in] vector         vector of 1-dimensional gfloat data
 *      @param[in] nl 	       lowest index number
 *      @param[in] nh 	       highest index number
 *      @return void
 */
void
gpiv_free_dvector_index			(gdouble		*vector, 
					const long		nl, 
					const long		nh
					);



/**
 *      Allocates memory for a 1-dimensional vector of long data
 *
 *      @param[in] nl 		vector length
 *      @return vector
 */
long *
gpiv_nulvector				(long			nl
					);



/**
 *      Frees memory for a 1-dimensional vector of long data
 *
 *      @param[in] vector              vector of 1-dimensional gfloat data
 *      @return void
 */
void 
gpiv_free_nulvector			(long			*vector
					);



/**
 *      Allocates memory for a 1-dimensional vector of long data
 *      with subscript range v[nl..nh]
 *
 *      @param[in] nl 	       lowest index number
 *      @param[in] nh 	       highest index number
 *      @return vector
 */
long *
gpiv_nulvector_index			(const long		nl,
					const long		nh
					);



/**
 *      Frees memory for a 1-dimensional vector of long data
 *      with subscript range v[nrl..nrh][ncl..nch]
 *
 *      @param[in] vector         vector of 1-dimensional gfloat data
 *      @param[in] nl 	       lowest index number
 *      @param[in] nh 	       highest index number
 *      @return void
 */
void 
gpiv_free_nulvector_index		(long			*vector,
					const long		nl,
					const long		nh
					);



/**
 *      Allocates memory for a 1-dimensional vector of unsigned long data
 *
 *      @param[in] nl 		vector length
 *      @return vector
 */
unsigned long *
gpiv_ulvector				(const long		nl
					);



/**
 *      Frees memory for a 1-dimensional vector of unsigned long data
 *
 *      @param[in] vector              vector of 1-dimensional gfloat data
 *      @return void
 */
void 
gpiv_free_ulvector			(unsigned long		*vector
					);



/**
 *      Allocates memory for a 1-dimensional vector of unsigned long data
 *      with subscript range v[nl..nh]
 *
 *      @param[in] nl 	       lowest index number
 *      @param[in] nh 	       highest index number
 *      @return vector
 */
unsigned long *
gpiv_ulvector_index			(const long		nl,
					const long		nh
					 );



/**
 *      Frees memory for a 1-dimensional vector of unsigned long data
 *      with subscript range v[nrl..nrh][ncl..nch]
 *
 *      @param[in] vector         vector of 1-dimensional gfloat data
 *      @param[in] nl 	       lowest index number
 *      @param[in] nh 	       highest index number
 *      @return void
 */
void 
gpiv_free_ulvector_index		(unsigned long		*vector,
					const long		nl,
					const long		nh
					);



/**
 *      Allocates memory for a 1-dimensional vector of integer data
 *
 *      @param[in] nl 		vector length
 *      @return vector
 */
gint *
gpiv_ivector				(const long		nl
					);



/**
 *      Frees memory for a 1-dimensional vector of integer data
 *
 *      @param[in] vector              vector of 1-dimensional gfloat data
 *      @return void
 */
void 
gpiv_free_ivector			(gint			*vector
					);



/**
 *      Allocates memory for a 1-dimensional vector of integer data
 *      with subscript range v[nl..nh]
 *
 *      @param[in] nl 	       lowest index number
 *      @param[in] nh 	       highest index number
 *      @return vector
 */
gint *
gpiv_ivector_index			(const long		nl,
					const long		nh
					);



/**
 *      Frees memory for a 1-dimensional vector of integer data
 *      with subscript range v[nrl..nrh][ncl..nch]
 *
 *      @param[in] vector         vector of 1-dimensional gfloat data
 *      @param[in] nl 	       lowest index number
 *      @param[in] nh 	       highest index number
 *      @return void
 */
void 
gpiv_free_ivector_index			(gint			*vector,
					const long		nl,
					const long		nh
					);



/**
 *     Allocates memory for a 1-dimensional vector of gboolean data
 *
 *      @param[in] nl 		vector length
 *      @return vector
 */
gboolean *
gpiv_gbolvector				(const glong		nl
					);



/**
 *     Frees memory for a 1-dimensional vector of gboolean data
 *
 *      @param[in] vector       vector of 1-dimensional boolean data
 *      @return void
 */
void 
gpiv_free_gbolvector			(gboolean		*vector
					);



#endif /* __L_UTILS_H__ */
