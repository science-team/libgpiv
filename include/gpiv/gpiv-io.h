/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 Gerber van der Graaf
   This file is part of libgpiv.

   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  




---------------------------------------------------------------
FILENAME:                io.c
LIBRARY:                 libgpiv:
EXTERNAL FUNCTIONS:      gpiv_io_make_fname
                         gpiv_read_header

			 gpiv_fread_image
			 gpiv_read_image
                         gpiv_write_image

                         gpiv_find_pivdata_origin
                         gpiv_read_pivdata
                         gpiv_read_pivdata_fastx
                         gpiv_write_pivdata
			 gpiv_fread_scdata
                         gpiv_write_scdata

			 gpiv_fwrite_griddata
			 gpiv_print_histo
                         gpiv_print_cumhisto_eqdatbin
                         gpiv_fprint_cumhisto_eqdatbin

                         gpiv_fread_raw_image
                         gpiv_read_raw_image
                         gpiv_fwrite_raw_image
                         gpiv_write_raw_image

                         gpiv_read_png_image
                         gpiv_write_png_image

                         gpiv_fcreate_hdf5
                         gpiv_fread_hdf5_image
                         gpiv_fwrite_hdf5_image
                         gpiv_fread_hdf5_parameters
                         gpiv_fwrite_hdf5_parameters
                         gpiv_fread_hdf5_position
                         gpiv_fwrite_hdf5_position
                         gpiv_fwrite_hdf5_piv_position
			 gpiv_fread_hdf5_pivdata
			 gpiv_fwrite_hdf5_pivdata
                         gpiv_fread_hdf5_scdata
			 gpiv_fwrite_hdf5_scdata
                         gpiv_fread_hdf5_histo
			 gpiv_fwrite_hdf5_histo

                         gpiv_fread_davis_image
                         gpiv_read_davis_image

LAST MODIFICATION DATE:  $Id: gpiv-io.h,v 1.3 2008-05-07 08:24:44 gerber Exp $
 --------------------------------------------------------------- */

/*!
\file                   gpiv-io.h
\brief                  module for input/output.

SOURCES:                lib/io.c, lib/io_hdf.c  lib/io_image.c  lib/io_png.c  
                        lib/io_raw.c

LAST MODIFICATION DATE:  $Id: gpiv-io.h,v 1.3 2008-05-07 08:24:44 gerber Exp $

 */
/*---------------------------------------------------------------------------*/

#ifndef __LIBGPIV_IO_H__
#define __LIBGPIV_IO_H__



#define GPIV_PIV_FMT "%4.0f %4.0f %12.3f %12.3f %12.3f %2d\n"	/**< Piv data Format */
#define GPIV_PIV_S_FMT "%-12.5f %-12.5f %-12.5f  %-12.5f  %-6.2f     %-2d\n"	/**< Piv scaled data Format */
#define GPIV_SCALAR_FMT "%4.0f %4.0f %12.3f     %2d\n"	        /**< Scalar data Format with PIV data positions */
#define GPIV_SCALAR_S_FMT "%-12.3f %-12.3f %-12.3f     %-2d\n"	/**< Scalar data Format with PIV scaled data positions */

#define GPIV_EXT_HEADER        ".h"            /**< Extension of image header file name */
#define GPIV_EXT_RAW_IMAGE     ".r"	       /**< Extension of raw image file name */
#define GPIV_EXT_PNG_IMAGE     ".png"          /**< Extension of png (Portable Network Graphics) formatted image file name*/
#define GPIV_EXT_PNG_IMAGE_UPCASE ".PNG"       /**< Uppercase extension of png */
#define GPIV_EXT_PNG_IMAGE_PROC "_proc.png"    /**< Extension of processed png image file name */
#define GPIV_EXT_PGM_IMAGE     ".pgm"          /**< Extension of raw portable graymap (pgm) image file name */
#define GPIV_EXT_PGM_IMAGE_UPCASE ".PGM"       /**< Uppercase extension of pgm */
#define GPIV_EXT_RAW_IMAGE_PROC "_proc.r"      /**< Extension of processed raw binary image file name */
#define GPIV_EXT_PAR           ".par"	       /**< Extension of file with used parameters */

#define GPIV_EXT_TA            ".ta"           /**< Extension of time averaged file name */
#define GPIV_EXT_SUBSTR        ".substr"       /**< Extension of file name with substracted data */

#define GPIV_EXT_GPIV          ".h5"	       /**< Extension of gpiv file name (HDF 5 format) */
#define GPIV_EXT_GPIV_UPCASE   ".H5"           /**< Uppercase extension of gpiv file name (HDF 5 format) */
#define GPIV_EXT_PIV           ".piv"	       /**< Extension of piv file name (ASCII format) */
#define GPIV_EXT_DAVIS         ".img"          /**< DaVis image format */
#define GPIV_EXT_DAVIS_UPCASE  ".IMG"          /**< Uppercase DaVis image format */
#define GPIV_EXT_COV           ".cov"	       /**< Extension of covariance data  */
#define GPIV_EXT_INT           ".int"	       /**< Extension of interrogation area */
#define GPIV_EXT_OLD           ".old.piv"      /**< Extension of old displacements */

#define GPIV_EXT_ERR_PIV       ".err.piv"      /**< Extension of erroneous-corrected piv file name */
#define GPIV_EXT_ERR_STAT      ".stat"         /**< Extension of residu statistics */
#define GPIV_EXT_PLK           ".plk"	       /**< Extension of peaklocking output */
#define GPIV_EXT_UVHISTO       ".pdf"          /**< Extension of displacement histogram */

#define GPIV_EXT_VOR           ".vor"	       /**< Extension of vorticity strain file name (ASCII format) */
#define GPIV_EXT_NSTR          ".nstr"	       /**< Extension of normal strain file name (ASCII format) */
#define GPIV_EXT_SSTR          ".sstr"	       /**< Extension of shear strain file name (ASCII format) */
#define GPIV_EXT_MANI          ".ma.piv"	       /**< Extension of manipulated file name (ASCII format) */
#define GPIV_EXT_SA            ".sa.piv"       /**< Extension of spatial averaged file name (ASCII format)*/
#define GPIV_EXT_SC            ".sc.piv"       /**< Extension of scaled data file name (ASCII format)*/


/* #define HD5_IMAGE_INT */                          /* Use integer for image data in HDF5 format */
#define GPIV_DAVIS_IMG_DEPTH   12              /**< Depth of mages generated by LaVision's Davis (general: PCO cameras) */
#define GPIV_IMG_PARAM_RESOURCES /**<  As x_corr and depth are required 
to read properly the gpiv_image but may not
 * necesarraly be present in the header for some image formats under specific
 * situations, it is obtained from the parameter resources
 */



enum GpivDataFormat { 
    GPIV_RR = 1, 
    GPIV_DAV = 2 
};



/**
 *      Constructs (output) filename from base name and extension. 
 *
 *      @param[in] fname_base   file base name
 *      @param[in] EXT          file extension name
 *      @param[out] fname_out   completed filename
 *      @return                 void
 */
void 
gpiv_io_make_fname			(const gchar		*fname_base,
					const gchar		*EXT,
					gchar			*fname_out
					);


/**
 *      Reads image from fp. Image will have to be png formatted.
 *
 *      @param[in] fp           file pointer. If NULL, stdin will be used.
 *      @return                 pointer to GpivImage
 */
GpivImage *
gpiv_read_image				(FILE			*fp
					);



/**
 *      Reads image from file fname.
 *
 *      @param[in] fname        complete filename. This is used to
 *                              pick the image format loader. Eventually 
 *                              checking the format signature is done in the
 *                              loader itself
 *      @return                 GpivImage or NULL on failure
 */
GpivImage *
gpiv_fread_image			(const gchar		*fname
					);



/**
 *      Determines the name of the program that generated the data.
 *
 *      @param[in] line         character line containing program that generated data
 *      @return pdf             enumerator of data origin
 */
enum GpivDataFormat
gpiv_find_pivdata_origin		(const gchar		line[GPIV_MAX_CHARS]
					);	



/**
 *      Reads PIV data from file fname.
 *
 *      @param[in] fname        complete input filename.
 *      @return                 GpivPivData or NULL on failure
 */
GpivPivData *
gpiv_fread_pivdata			(const gchar		*fname
					);



/**
 *      Writes PIV data to file fname in ASCII format.
 *
 *      @param[in] piv_data     PIV data
 *      @param[in] fname        complete output filename.
 *      @param[in] free         flag if allocated memory if piv_data will be freed
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_fwrite_pivdata			(const gchar		*fname,
					GpivPivData		*piv_data,
					const gboolean		free
					);



/**
 *      Reads PIV data from fp.
 *
 *      @param[in] fp           input file. If NULL, stdin is used.
 *      @return                 GpivPivData
 */
GpivPivData *
gpiv_read_pivdata			(FILE			*fp
					);



/**
 *      Reads PIV data from fp with fast running x-position variables.
 *      (1st column in data stream)
 *
 *      @param[in] fp           input file. If NULL, stdin is used.
 *      @return                 GpivPivData
 */
GpivPivData *
gpiv_read_pivdata_fastx			(FILE			*fp
					);



/**
 *      Writes PIV data to fp in ASCII format.
 *
 *      @param[in] piv_data     PIV data
 *      @param[in] fp           output file. If NULL, stout is used.
 *      @param[in] free         flag if allocated memory if piv_data will be freed
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_write_pivdata			(FILE			*fp,
					GpivPivData		*piv_data,
					const gboolean		free
					);



/**
 *      Reads scalar data from file fname.
 *
 *      @param[in] fname        complete input filename.
 *      @return                 GpivScalarData or NULL on failure
 */
GpivScalarData *
gpiv_fread_scdata			(const gchar		*fname
					);



/**
 *      Writes scalar data to file fname in ASCII format.
 *
 *      @param[in] scalar_data  scalar data
 *      @param[in] fname        complete output filename.
 *      @param[in] free         flag if allocated memory if piv_data will be freed
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_fwrite_scdata			(const gchar		*fname,
					GpivScalarData		*scalar_data,
					const gboolean		free
					);



/**
 *      Reads scalar data form ascii file.
 *
 *      @param[in] fp           input file. If NULL, stin is used.
 *      @return                 GpivScalarData or NULL on failure
 */
GpivScalarData *
gpiv_read_scdata			(FILE			*fp
					);



/**
 *      Writes scalar data to file.
 *
 *      @param[in] fp           output file. If NULL, stout is used.
 *      @param[in] scalar_data  scalar data containing all variables in a datafile
 *      @param[in] free         flag if allocated memory if scalar_data will be freed
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_write_scdata			(FILE			*fp,
					GpivScalarData		*scalar_data,
					const gboolean		free
					);



/**
 *      Writes scalar data to file in grid format for gnuplot.
 *
 *      @param[in] fp           output file. If NULL, stdout is used.
 *      @param[in] scalar_data  scalar data containing all variables in a datafile
 *      @param[in] free         flag if allocated memory if scalar_data will be freed
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_write_sc_griddata			(FILE			*fp,
					GpivScalarData		*scalar_data,
					const gboolean		free
					);



/**
 *      Writes scalar data to file in grid format for plotmtv.
 *
 *      @param[in] fp           output file. If NULL, stdout is used.
 *      @param[in] scalar_data  scalar data containing all variables in a datafile
 *      @param[in] free         flag if allocated memory if scalar_data will be freed
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_write_sc_mtvgriddata		(FILE			*fp, 
					GpivScalarData		*scalar_data, 
					const gboolean		free
					);


/**
 *      Writes bins data to file in histogram format.
 *
 *      @param[in] fp           output file. If NULL, stdout is used.
 *      @param[in] bin_data     data containing the values for each bin
 *      @param[in] free         flag if allocated memory if scalar_data will be freed
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_print_histo			(FILE			*fp, 
					GpivBinData		*bin_data, 
					const gboolean		free
					);


/**
 *      Writing cumulative histogram data with an equal number of date per bin 
 *      or klass to fp. Special output for validation; work around to print
 *      float data as y-values.
 *
 *      @param[in] fp           output file
 *      @param[in] klass        struct GpivBinData containing the values for each bin
 *      @param[in] free         flag if allocated memory if scalar_data will be freed
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_print_cumhisto_eqdatbin		(FILE 			*fp,
					GpivBinData 		*klass,
					const gboolean		free
					);


/**
 *      Reads an image from raw binary file.
 *      Expects the arrray (s) have not been allocated yet.
 *      Eventually, required header info will be read from (system) resources
 *      gpiv.conf or $HOME/.gpivrc.
 *
 *      @param[in] fp           input file. If NULL, stdin will be used
 *      @return                 GpivImage on success or NULL on failure
 */
GpivImage *
gpiv_read_raw_image			(FILE			*fp
					);


/**
 *      Reads an image from raw binary file.
 *      Expects the arrray(s) have not been allocated yet.
 *      Reads image data from file.r, header from file.h
 *      Eventually, required header info will be read from (system) resources
 *      gpiv.conf or $HOME/.gpivrc.
 *
 *      @param[in] fname        complete image filename (ending on .r) 
 *      @return                 GpivImage on success or NULL on failure
 */
GpivImage *
gpiv_fread_raw_image			(const gchar		*fname
					);


/**
 *      Writes raw binary image frames to fp.
 *
 *      @param[in] fp           output file. If NULL, stdout will be used
 *      @param[in] gpiv_image   image frames and header
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_write_raw_image			(FILE			*fp,
					GpivImage		*gpiv_image
					);


/**
 *      Writes raw binary image data to file.r and header to file.h.
 *
 *      @param[in] fname        complete image filename (ending on .r)
 *      @param[in] gpiv_image   image frames and header info
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_fwrite_raw_image			(const gchar		*fname,
					GpivImage		*gpiv_image
					);


/**
 *      Reads png formatted image.
 *
 *      @param[in] fp           file pointer
 *      @return                 pointer to GpivImage
 */
GpivImage *
gpiv_read_png_image			(FILE			*fp
					);


/**
 *      Writes image data and header to png formatted image. 
 *      Optionally frees allocated memory.
 *
 *      @param[in] fp           output file.
 *      @param[in] gpiv_image   image header and data
 *      @param[in] free         boolean whether to free memory of image arrays
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_write_png_image			(FILE			*fp,
					GpivImage		*gpiv_image,
					const gboolean		free
					);


/**
 *      Creates an hdf5 data file with POSITION, DATA and PARAMETERS groups.
 *
 *      @param[in] fname        pointer to complete filename
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_fcreate_hdf5			(const gchar		*fname
					);


/**
 *      Reads parameters from hdf5 data file.
 *
 *      @param[in] fname        pointer to complete filename
 *      @param[in] par_key      key for apropiate parameter
 *      @param[out] pstruct     parameter structure
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_fread_hdf5_parameters		(const gchar		*fname,
					const gchar		*par_key,
					void			*pstruct
					);


/**
 *      Reads image data to a hdf5 data file.
 *
 *      @param[in] fname        pointer to complete filename
 *      @return                 GpivImage on success or NULL on failure
 */
GpivImage *
gpiv_fread_hdf5_image			(const gchar		*fname
					);


/**
 *      Writes image data to file in hdf version 5 format.
 *
 *      @param[in] fname        pointer to complete filename
 *      @param[in] image        image frkames and header
 *      @param[in] free         boolean whether to free memory of image arrays
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_fwrite_hdf5_image			(const gchar		*fname, 
					GpivImage		*image,
					const gboolean		free
					);


/**
 *      Writes parameters to hdf5 data file.
 *
 *      @param[in] fname        pointer to complete filename
 *      @param[in] par_key      key for apropiate parameter
 *      @param[in] pstruct      parameter structure
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_fwrite_hdf5_parameters		(const gchar		*fname,
					const gchar		*par_key,
					void			*pstruct
					);


/**
 *      Reads piv data from hdf5 data file.
 *
 *      @param[in] fname        pointer to complete filename
 *      @param[in] DATA_KEY     location of the data in the DATA group in the hdf file
 *      @return                 GpivPivData or NULL on failure
 */
GpivPivData *
gpiv_fread_hdf5_pivdata			(const gchar		*fname,
					const gchar		*DATA_KEY
					);


/**
 *      Writes PIV data to file in hdf version 5 format.
 *
 *      @param[in] fname        pointer to complete filename
 *      @param[in] piv_data     piv data containing all variables in a datafile
 *      @param[in] DATA_KEY     location of the data in the DATA group in the hdf file
 *      @param[in] free         boolean whether to free memory of PIV data
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_fwrite_hdf5_pivdata		(const gchar		*fname, 
					GpivPivData		*piv_data, 
					const gchar		*DATA_KEY,
					const gboolean		free
					);


/**
 *      Reads SCALAR data from a hdf5 data file.
 *
 *      @param[in] fname         pointer to complete filename
 *      @param[in] DATA_KEY      key to specify data type/origin (NORMAL_STRAIN, ...)
 *      @return                  GpivScalarData or NULL on failure
 */
GpivScalarData *
gpiv_fread_hdf5_scdata			(const gchar		*fname,
					const gchar		*DATA_KEY
					);


/**
 *      Writes SCALAR data to a file in hdf version 5 format.
 *
 *      @param[in] fname        pointer to complete filename
 *      @param[in] scalar_data  struct GpivScalarData containing all variables in a datafile
 *      @param[in] DATA_KEY     identifier of data type
 *      @param[in] free         boolean whether to free memory of SCALAR data
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_fwrite_hdf5_scdata			(const gchar		*fname, 
					GpivScalarData		*scalar_data, 
					const gchar		*DATA_KEY,
					const gboolean		free
					);


/**
 *      Reads histogram data to ouput file in hdf version 5 format.
 *
 *      @param[in] fname        pointer to complete filename
 *      @param[in] klass        struct bin containing the values for each bin
 *      @param[in] DATA_KEY     identifier of data type (PEAK_LOCK, RESIDUS, ...)
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_fread_hdf5_histo			(const gchar		*fname, 
					GpivBinData		*klass, 
					const gchar		*DATA_KEY
                         );


/**
 *      Writes histogram data to ouput file in hdf version 5 format.
 *
 *      @param[in] fname        pointer to complete filename
 *      @param[in] klass        struct bin containing the values for each bin
 *      @param[in] DATA_KEY     identifier of data type (PEAK_LOCK, RESIDUS, ...)
 *      @return                 NULL on success or error message on failure
 */
gchar *
gpiv_fwrite_hdf5_histo			(const gchar            *fname, 
					const GpivBinData      *klass, 
					const gchar            *DATA_KEY
					);


/**
 *      Reads Davis formatted image, with ext .IMG from file.
 *
 *      @param[in] fp           pointer to input file
 *      @return                 GpivImage on success or NULL on failure
 */
GpivImage *
gpiv_read_davis_image			(FILE			*fp
					);


#endif /* __LIBGPIV_IO_H__ */
