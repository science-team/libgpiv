/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*-----------------------------------------------------------------------------
   libgpiv - library for Particle Image Velocimetry

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008 Gerber van der Graaf
   <gerber_graaf@users.sourceforge.net>

   This file is part of libgpiv.

   Libgpiv is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  




-------------------------------------------------------------------------------
LIBRARY:                libgpiv
FILENAME:               cam.h
SOURCE:                 cam.c
EXTERNAL FUNCTIONS:
                        gpiv_cam_get_camvar
                        gpiv_cam_free_camvar
                        gpiv_cam_default_parameters
                        gpiv_cam_read_parameters
                        gpiv_cam_check_parameters_read
                        gpiv_cam_parameters_set
                        gpiv_cam_test_parameter
                        gpiv_cam_fprint_parameters

---------------------------------------------------------------------------- */

/*!
\file                   gpiv-cam.h
\brief                  module for IEEE1394 camera control

SOURCES:                lib/cam.c, cam_par.c

LAST MODIFICATION DATE:  $Id: gpiv-cam.h,v 1.2 2008-04-09 06:10:27 gerber Exp $
 */
#ifndef __LIBGPIV_CAM_H__
#define __LIBGPIV_CAM_H__



/*
 * Default values of CamPar
 */
#define GPIV_CAMPAR_DEFAULT__MODE 1   		/**< Default camera mode */
#define GPIV_CAMPAR_DEFAULT__CYCLES 1 		/**< Default number of cycles */
#define GPIV_CAMPAR_DEFAULT__FNAME "gpiv_img" 	/**< Default output image name */

#define GPIV_CAMPAR_CYCLES_MIN 1 /**<  */
#define GPIV_CAMPAR_CYCLES_MAX 1 /**<  */

/*
 * Keys of CamPar to read values from configuration files
 */
#define GPIV_CAMPAR_KEY "CAM"	         /**< Key of camera parameters */
#define GPIV_CAMPAR_KEY__MODE "Mode"     /**<  Parameter key for camera mode */
#define GPIV_CAMPAR_KEY__CYCLES "Cycles" /**<  Parameter key for number of cycles */
#define GPIV_CAMPAR_KEY__FNAME "Fname"   /**<  Parameter key for  output image name */

#define GPIV_CAMPAR_CYCLES_MIN 1
#define GPIV_CAMPAR_CYCLES_MAX 1

/**
 * Camera modes
 */
enum GpivCamMode {
    GPIV_CAM_MODE__PERIODIC = 1,/**< periodic */
    GPIV_CAM_MODE__DURATION,    /**< continuoulsy */
    GPIV_CAM_MODE__ONE_SHOT_IRQ,/**< single image pair */
    GPIV_CAM_MODE__TRIGGER_IRQ, /**< on wxternal trigger signals */
    GPIV_CAM_MODE__INCREMENT,   /**< ? */
    GPIV_CAM_MODE__DOUBLE,      /**< ? */
};

typedef struct __GpivCamPar GpivCamPar;
/*!
 * \brief Parameters for camera settings
 *
 * The parameters might be loaded from the configuration resources, 
 * with gpiv_scan_resourcefiles() or with gpiv_scan_parameter().
*/
struct __GpivCamPar {
    enum GpivCamMode mode;      /**< operating mode (indefinite/interrupt/definite) */
    guint cycles;               /**< If GpivTrigPar not used. Number of cycles (equal to number of images to be recorded?) */
    gchar fname[GPIV_MAX_CHARS];/**< image file name */
    gboolean mode__set;        	/**< flag if mode has been defined */
    gboolean cycles__set;      	/**< flag if cycles has been defined */
    gboolean fname__set;       	/**< flag if fname has been defined */
};



typedef struct __GpivCamVar GpivCamVar;
/*!
 * \brief Camera variables
 *
 *  These variables are determined by the camera when connecting to the appliacation. 
 */
struct __GpivCamVar {
    guint numNodes;             /**< number of nodes available */
    guint numCameras;           /**< number of cameras available */
    guint maxspeed;             /**< maximum frame rate speed */
    guint port;                 /**< port number */
    raw1394handle_t handle;    	/**< handle */
    nodeid_t *camera_nodes;    	/**< camera node */

    dc1394_cameracapture *capture;
    dc1394_camerainfo *camera;
    dc1394_feature_info *feature_info;
    dc1394_feature_set *feature_set;
    dc1394_miscinfo *misc_info;
};




/**
 *     Get varaiables of connected cameras using firewire
 *
 *     @param[in] verbose      prints camera info to stdout
 *     @return                 GpivCamVar or NULL on failure
 */
GpivCamVar *
gpiv_cam_get_camvar 			(const gboolean 	verbose
                     			);



/**
 *     Free memory variables of connected cameras using firewire
 *
 *     @param[out] cam_var     structure of camera variables
 *     @return                 NULL on success or error message on failure
 */
gchar *
gpiv_cam_free_camvar			(GpivCamVar 		*cam_var
                      			);



/**
 *     Sets default GpivCamPar parameter values
 *
 *     @param[out] cam_par_default     structure of camera parameters
 *     @param[out] force       forces to set to default, even if already defined
 *     @return                 NULL on success or error message on failure
 */
void
gpiv_cam_default_parameters 		(GpivCamPar 		*cam_par_default,
                            	 	const gboolean 		force
                             		);



/**
 *     Read all GpivCamPar parameters
 *
 *     @param[in] fp_par       file pointer to parameter file
 *     @param[in] verbose      flag to print parametrs to stdout
 *     @param[out] cam_par     parameters of camera parameters
 *     @return                 void
 */
void 
gpiv_cam_read_parameters 		(FILE	 		*fp_par, 
                          		GpivCamPar 		*cam_par, 
                          		const gboolean 		verbose
                          		);



/**
 *      Checks if all GpivCamPar parameters have been read.
 *      If a parameter has not been read, it will be set to *cam_par_default or to 
 *	its hard-coded default value in case *cam_par_default is NULL.
 *
 *     @param[in] cam_par_default      default parameters. If NULL, library 
 *                                     default values are used
 *     @param[out] cam_par     parameters of camera parameters
 *     @return                 NULL on success or error message on failure
 */
gchar *
gpiv_cam_check_parameters_read 		(GpivCamPar 		*cam_par,
                                 	const GpivCamPar 	*cam_par_default
                                	);



/**
 *     Sets flags for __set variables of GpivCamPar 
 *
 *     @param[in] flag         flag representing __set value
 *     @param[out] cam_par     parameters for recording images
 *     @return                 void
 */
void
gpiv_cam_parameters__set 		(GpivCamPar 		*cam_par,
                         	 	const gboolean 		flag
                          		);



/**
 *     Testing GpivCamPar parameters on valid values and initializing derived 
 *     variables 
 *
 *     @param[in] cam_par      parameters for camera
 *     @return                 NULL on success or error message on failure
 */ 
gchar *
gpiv_cam_test_parameter	 		(const GpivCamPar 	*cam_par
                         		);



/**
 *     Prints GpivCamPar parameters to fp_par_out
 *
 *     @param[in] fp_par_out   output file
 *     @param[in] cam_par      parameters for camera
 *     @return                 void
 */
void 
gpiv_cam_print_parameters 		(FILE 			*fp_par_out, 
                           		const GpivCamPar 	*cam_par
                           		);



#endif /*__LIBGPIV_CAM_H__ */
