1. INTRODUCTION
===============

1.1 GENERAL DESCRIPTION
=======================

LIBGPIV is a software library for the so-called Digital Particle Image
Velocimetry (DPIV). It consists the core functions for image
recording, image processing, image interrogation (resulting into
estimators of particle image displacements, i.e. PIV data), validation
of PIV data (on peak-locking effects, erroneous vectors (outliers),
checking on velocity gradient), post-processing (functions for
calculating (scalar) derivatives of the PIV data, like vorticity and
strain), input/output functions, memory allocation etc.

The library has been implemented in several command-line driven
programs that can be found in the GPIVTOOLS distribution. A Graphic
User Interface (GPIV) is available that controls all processes
and parameters and displays its results graphically in a more
advanced way. GPIV has been implemented with the GTK/GNOME libraries
and resides in GPIV distribution. PyGpiv is a Python module wrapped 
from this library for writing your own scripts. 

The library has been written in ANSI-C and have been developed on a
GNU/LINUX box and should work on any UNIX-like system.
The main web page can be found at:  http://gpiv.sourceforge.net/


2 LICENSE
===========

LIBGPIV is Free Software licensed under the GNU
Public license terms. See the COPYING file for the license.


3 COMPILATION AND INSTALLATION 
================================ 

See INSTALL for a more extended description.

The following libraries are needed to build this library:
- fftw and (rfftw) library for Fast Fourier Transformation 
  (http://www.fftw.org)
- Perl, version 5.005_03 or newer
- hdf5 library
- gslcblas library
- gsl library
- Gnuplot

Unpacking and compilation:
gzip -d gpiv-0.2.tar.gz
tar -xvf gpiv-0.2.tar
./configure
make: to build the library and tools
make install
make uninstall: removes installed files and directries that have been
created.

There are also pre-compiled (.deb) packages available that eases the
installation.

Libgpiv uses g_get_tmp_dir () from the Glib library to obtain the tmp
directory. Therefore, set TMPDIR, TMP or TEMP to your local temporary
directory. For bash shell: export TMPDIR="/home/my_user_derectory/tmp".
Else, the system-wide /tmp directory will be used.

The configuration for the parameters is found in /etc/gpiv.conf. If
absent, hard-coded default values will be used.


4. FINAL REMARKS
=================

I hope that the programs will work fine (cross your fingers) and that
you'll enjoy it. Remarks, suggestions **and patches** for 
improvements/additions are welcome.

Gerber van der Graaf


